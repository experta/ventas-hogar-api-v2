-- Create table
create table VENTAS_EMISIONES
(
  id                 NUMBER(19) GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  motivo_rechazo     VARCHAR2(1000 CHAR),
  reintentos_emision NUMBER(10),
  timestamp_emision  TIMESTAMP(6)
)
tablespace TS_APIHOGAR_DATA;

-- Create/Recreate primary, unique and foreign key constraints
alter table VENTAS_EMISIONES
  add primary key (ID)
  using index
  tablespace TS_APIHOGAR_DATA;

CREATE OR REPLACE PUBLIC SYNONYM VENTAS_EMISIONES FOR API_HOGAR.VENTAS_EMISIONES;
GRANT DELETE, INSERT, SELECT, UPDATE ON API_HOGAR.VENTAS_EMISIONES TO API_HOGAR_USER;
GRANT SELECT ON API_HOGAR.VENTAS_EMISIONES TO C##MRAMOS;

-------------------------------------------------------------------------------

-- Create table
create table VENTAS_PAGOS
(
  id                       NUMBER(19) GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  tarjeta_vencimiento_anio NUMBER(10),
  banco                    VARCHAR2(100 CHAR),
  cbu                      VARCHAR2(50 CHAR),
  tarjeta_empresa          VARCHAR2(100 CHAR),
  medio_pago               VARCHAR2(50 CHAR),
  tarjeta_vencimiento_mes  NUMBER(10),
  tarjeta_numero           VARCHAR2(20 CHAR)
)
tablespace TS_APIHOGAR_DATA;

-- Create/Recreate primary, unique and foreign key constraints
alter table VENTAS_PAGOS
  add primary key (ID)
  using index
  tablespace TS_APIHOGAR_DATA;

CREATE OR REPLACE PUBLIC SYNONYM VENTAS_PAGOS FOR API_HOGAR.VENTAS_PAGOS;
GRANT DELETE, INSERT, SELECT, UPDATE ON API_HOGAR.VENTAS_PAGOS TO API_HOGAR_USER;
GRANT SELECT ON API_HOGAR.VENTAS_PAGOS TO C##MRAMOS;

-------------------------------------------------------------------------------

-- Create table
create table VENTAS_VIVIENDAS
(
  id                        NUMBER(19) GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  direccion_calle           VARCHAR2(100 CHAR),
  direccion_departamento    VARCHAR2(100 CHAR),
  direccion_localizacion_id VARCHAR2(100 CHAR),
  direccion_numero          VARCHAR2(100 CHAR),
  direccion_piso            VARCHAR2(100 CHAR),
  direccion_tomador         NUMBER(1),
  metros_cuadrados          NUMBER(10),
  tiene_rejas               NUMBER(1),
  tipo_vivienda             VARCHAR2(100 CHAR)
)
tablespace TS_APIHOGAR_DATA;

-- Create/Recreate primary, unique and foreign key constraints
alter table VENTAS_VIVIENDAS
  add primary key (ID)
  using index
  tablespace TS_APIHOGAR_DATA;

CREATE OR REPLACE PUBLIC SYNONYM VENTAS_VIVIENDAS FOR API_HOGAR.VENTAS_VIVIENDAS;
GRANT DELETE, INSERT, SELECT, UPDATE ON API_HOGAR.VENTAS_VIVIENDAS TO API_HOGAR_USER;
GRANT SELECT ON API_HOGAR.VENTAS_VIVIENDAS TO C##MRAMOS;

-------------------------------------------------------------------------------

-- Create table
create table VENTAS_CLIENTES
(
  id                        NUMBER(19) GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  actividad                 VARCHAR2(500 CHAR),
  apellido                  VARCHAR2(100 CHAR),
  condicion_iva             VARCHAR2(50 CHAR),
  direccion_calle           VARCHAR2(100 CHAR),
  direccion_departamento    VARCHAR2(20 CHAR),
  direccion_localizacion_id VARCHAR2(20 CHAR),
  direccion_numero          VARCHAR2(20 CHAR),
  direccion_piso            VARCHAR2(20 CHAR),
  documento_numero          VARCHAR2(20 CHAR),
  documento_tipo            VARCHAR2(15 CHAR),
  email                     VARCHAR2(100 CHAR),
  fecha_nacimiento          DATE,
  nacionalidad              VARCHAR2(50 CHAR),
  nombre                    VARCHAR2(100 CHAR),
  pep                       NUMBER(1),
  razon_social              VARCHAR2(200 CHAR),
  sexo                      VARCHAR2(15 CHAR),
  telefono_numero           VARCHAR2(11 CHAR),
  telefono_prefijo          VARCHAR2(11 CHAR),
  tipo_persona              VARCHAR2(50 CHAR)
)
tablespace TS_APIHOGAR_DATA;

-- Create/Recreate primary, unique and foreign key constraints
alter table VENTAS_CLIENTES
  add primary key (ID)
  using index
  tablespace TS_APIHOGAR_DATA;

CREATE OR REPLACE PUBLIC SYNONYM VENTAS_CLIENTES FOR API_HOGAR.VENTAS_CLIENTES;
GRANT DELETE, INSERT, SELECT, UPDATE ON API_HOGAR.VENTAS_CLIENTES TO API_HOGAR_USER;
GRANT SELECT ON API_HOGAR.VENTAS_CLIENTES TO C##MRAMOS;

-------------------------------------------------------------------------------

create table VENTAS
(
  id                        NUMBER(19) GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  fecha_creacion            DATE,
  fecha_inicio_cobertura    DATE,
  fecha_ultima_modificacion DATE,
  nro_poliza                VARCHAR2(255 CHAR),
  vendedor_id               VARCHAR2(255 CHAR),
  tipo_plan                 VARCHAR2(50 CHAR),
  id_cliente                NUMBER(19),
  id_emision                NUMBER(19),
  id_pago                   NUMBER(19),
  id_vivienda               NUMBER(19),
  beneficio                 VARCHAR2(30 CHAR)
)
tablespace TS_APIHOGAR_DATA;

-- Create/Recreate primary, unique and foreign key constraints
alter table VENTAS
  add primary key (ID)
  using index
  tablespace TS_APIHOGAR_DATA;

alter table VENTAS
  add constraint V_ID_EMISION_FK foreign key (ID_EMISION)
  references VENTAS_EMISIONES (ID);

alter table VENTAS
  add constraint V_ID_PAGO_FK foreign key (ID_PAGO)
  references VENTAS_PAGOS (ID);

alter table VENTAS
  add constraint V_ID_VIVIENDA_FK foreign key (ID_VIVIENDA)
  references VENTAS_VIVIENDAS (ID);

alter table VENTAS
  add constraint VID_CLIENTE_FK foreign key (ID_CLIENTE)
  references VENTAS_CLIENTES (ID);

CREATE OR REPLACE PUBLIC SYNONYM VENTAS FOR API_HOGAR.VENTAS;
GRANT DELETE, INSERT, SELECT, UPDATE ON API_HOGAR.VENTAS TO API_HOGAR_USER;
GRANT SELECT ON API_HOGAR.VENTAS TO C##MRAMOS;

-------------------------------------------------------------------------------

-- Create table
create table VENTAS_COTIZACIONES
(
  id                NUMBER(19) GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  estado            VARCHAR2(15 CHAR),
  fecha_a_emitir    DATE,
  fecha_creacion    DATE,
  fecha_vencimiento DATE,
  plan_id           VARCHAR2(100 CHAR) not null,
  impuestos         NUMBER(19,2),
  premio            NUMBER(19,2),
  prima             NUMBER(19,2),
  descuento         NUMBER(3),
  id_venta          NUMBER(19)
)
tablespace TS_APIHOGAR_DATA;
-- Create/Recreate primary, unique and foreign key constraints
alter table VENTAS_COTIZACIONES
  add primary key (ID)
  using index
  tablespace TS_APIHOGAR_DATA;

alter table VENTAS_COTIZACIONES
  add constraint VC_ID_VENTA_FK foreign key (ID_VENTA)
  references VENTAS (ID);

CREATE OR REPLACE PUBLIC SYNONYM VENTAS_COTIZACIONES FOR API_HOGAR.VENTAS_COTIZACIONES;
GRANT DELETE, INSERT, SELECT, UPDATE ON API_HOGAR.VENTAS_COTIZACIONES TO API_HOGAR_USER;
GRANT SELECT ON API_HOGAR.VENTAS_COTIZACIONES TO C##MRAMOS;

-------------------------------------------------------------------------------

-- Create table
create table VENTAS_COTIZACIONES_COBERTURAS
(
  id              NUMBER(19) GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  monto_asegurado NUMBER(19,2),
  opcion          VARCHAR2(50 CHAR),
  tipo_cobertura  VARCHAR2(50 CHAR),
  id_cotizacion   NUMBER(19)
)
tablespace TS_APIHOGAR_DATA;
-- Create/Recreate primary, unique and foreign key constraints
alter table VENTAS_COTIZACIONES_COBERTURAS
  add primary key (ID)
  using index
  tablespace TS_APIHOGAR_DATA;
alter table VENTAS_COTIZACIONES_COBERTURAS
  add constraint VCC_ID_COTIZACION_FK foreign key (ID_COTIZACION)
  references VENTAS_COTIZACIONES (ID);

CREATE OR REPLACE PUBLIC SYNONYM VENTAS_COTIZACIONES_COBERTURAS FOR API_HOGAR.VENTAS_COTIZACIONES_COBERTURAS;
GRANT DELETE, INSERT, SELECT, UPDATE ON API_HOGAR.VENTAS_COTIZACIONES_COBERTURAS TO API_HOGAR_USER;
GRANT SELECT ON API_HOGAR.VENTAS_COTIZACIONES_COBERTURAS TO C##MRAMOS;

-------------------------------------------------------------------------------

-- Create table
create table VENTAS_ESPECIFICOS
(
  id                   NUMBER(19) GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  apellido             VARCHAR2(100 CHAR),
  dni                  VARCHAR2(10 CHAR),
  fecha_nacimiento     DATE,
  modelo               VARCHAR2(100 CHAR),
  monto_asegurado      NUMBER(19,2),
  nombre               VARCHAR2(100 CHAR),
  numero_serie         VARCHAR2(100 CHAR),
  tipo_bien_especifico VARCHAR2(50 CHAR),
  tipo_cobertura       VARCHAR2(30 CHAR),
  tipo_especifico      VARCHAR2(30 CHAR),
  id_venta             NUMBER(19)
)
tablespace TS_APIHOGAR_DATA;

-- Create/Recreate primary, unique and foreign key constraints
alter table VENTAS_ESPECIFICOS
  add primary key (ID)
  using index
  tablespace TS_APIHOGAR_DATA;

alter table VENTAS_ESPECIFICOS
  add constraint VE_ID_VENTA_FK foreign key (ID_VENTA)
  references VENTAS (ID);

CREATE OR REPLACE PUBLIC SYNONYM VENTAS_ESPECIFICOS FOR API_HOGAR.VENTAS_ESPECIFICOS;
GRANT DELETE, INSERT, SELECT, UPDATE ON API_HOGAR.VENTAS_ESPECIFICOS TO API_HOGAR_USER;
GRANT SELECT ON API_HOGAR.VENTAS_ESPECIFICOS TO C##MRAMOS;

-------------------------------------------------------------------------------

-- Create table
create table VENTAS_PLANES
(
  id       NUMBER(19) GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  id_plan  VARCHAR2(50 CHAR) not null,
  id_venta NUMBER(19)
)
tablespace TS_APIHOGAR_DATA;

-- Create/Recreate primary, unique and foreign key constraints
alter table VENTAS_PLANES
  add primary key (ID)
  using index
  tablespace TS_APIHOGAR_DATA;

alter table VENTAS_PLANES
  add constraint VP_ID_VENTA_FK foreign key (ID_VENTA)
  references VENTAS (ID);

CREATE OR REPLACE PUBLIC SYNONYM VENTAS_PLANES FOR API_HOGAR.VENTAS_PLANES;
GRANT DELETE, INSERT, SELECT, UPDATE ON API_HOGAR.VENTAS_PLANES TO API_HOGAR_USER;
GRANT SELECT ON API_HOGAR.VENTAS_PLANES TO C##MRAMOS;

-------------------------------------------------------------------------------

ALTER TABLE ventas
ADD tipo_plan VARCHAR2(50 CHAR);

UPDATE ventas v SET v.tipo_plan = 'ENLATADO' WHERE v.id in (SELECT id_venta FROM ventas_planes where id_plan != 'PAS_CONFIGURABLE' and id_plan not like 'ALRIO%');
UPDATE ventas v SET v.tipo_plan = 'CONFIGURABLE' WHERE v.id in (SELECT id_venta FROM ventas_planes where id_plan = 'PAS_CONFIGURABLE');
UPDATE ventas v SET v.tipo_plan = 'ENLATADO_ALRIO' WHERE v.id in (SELECT id_venta FROM ventas_planes where id_plan like 'ALRIO%');

ALTER TABLE ventas
ADD canal_venta VARCHAR2(50 CHAR);

UPDATE ventas v SET v.canal_venta = 'WEB_EXPERTA_PAS';

alter table ventas add cant_cuotas varchar2(255 char);
UPDATE ventas v SET v.cant_cuotas = '12';

alter table ventas add tipo_medio_pago varchar2(255 char);
UPDATE ventas v SET v.tipo_medio_pago = 'AUTOMATICO';
UPDATE ventas v SET v.tipo_medio_pago = 'MANUAL' Where v.id_pago IN (SELECT p.id FROM ventas_pagos p where p.medio_pago = 'CUPONERA');

alter table ventas_cotizaciones add cuotas NUMBER(2);
UPDATE ventas_cotizaciones v SET v.cuotas = 12;

alter table ventas add emitiendo number(1);

alter table VENTAS_CLIENTES add SUJETO_OBLIGADO number(1);

alter table API_HOGAR.ventas add modalidad_comision varchar2(255 char);

alter table API_HOGAR.ventas add clausula_estabilizacion number(1,0);

alter table API_HOGAR.ventas_clientes add telefono_texto varchar2(100 char);