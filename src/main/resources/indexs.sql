CREATE INDEX API_HOGAR.idx_ventas_especificos ON API_HOGAR.ventas_especificos (id_venta) tablespace TS_APIHOGAR_IDX;
CREATE INDEX API_HOGAR.idx_ventas_cotizaciones_coberturas ON API_HOGAR.ventas_cotizaciones_coberturas(id_cotizacion) tablespace TS_APIHOGAR_IDX;
CREATE INDEX API_HOGAR.idx_ventas_vendedor ON API_HOGAR.ventas(vendedor_id) tablespace TS_APIHOGAR_IDX;
CREATE INDEX API_HOGAR.idx_ventas_vendedor_fecha ON API_HOGAR.ventas(vendedor_id, fecha_ultima_modificacion) tablespace TS_APIHOGAR_IDX;
CREATE INDEX API_HOGAR.IDX$$_00010001 ON API_HOGAR.VENTAS_COTIZACIONES (ID_VENTA)TABLESPACE TS_APIHOGAR_IDX;
CREATE INDEX API_HOGAR.IDX$$_00010002 ON API_HOGAR.VENTAS_PLANES(ID_VENTA)TABLESPACE TS_APIHOGAR_IDX;
