package ar.com.experta.ventashogarapi.core.model;

import java.util.Objects;

public class Direccion {

    private String calle;
    private String numero;
    private String piso;
    private String departamento;
    private Localizacion localizacion;

    public Direccion() {
    }

    public Direccion(Localizacion localizacion) {
        this.localizacion = localizacion;
    }

    public Direccion(String calle, String numero, String piso, String departamento, Localizacion localizacion) {
        this.calle = calle;
        this.numero = numero;
        this.piso = piso;
        this.departamento = departamento;
        this.localizacion = localizacion;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public Localizacion getLocalizacion() {
        return localizacion;
    }

    public void setLocalizacion(Localizacion localizacion) {
        this.localizacion = localizacion;
    }

    public String toString (){

        return  (calle != null ? calle : "") +
                (calle != null && numero != null ? " " + numero : "") +
                (piso != null  && !piso.isEmpty() ? " Piso: " + piso : "" ) +
                (departamento!=null && !departamento.equals("") ? " Departamento: " + departamento : "" ) +
                (localizacion!=null && localizacion.getCodigoPostal()!=null && !localizacion.getCodigoPostal().isEmpty() ? " (" + localizacion.getCodigoPostal() + ")" : "" ) +
                (localizacion!=null && localizacion.getLocalidad()!=null && !localizacion.getLocalidad().isEmpty() ? ", " + localizacion.getLocalidad() : "" ) +
                (localizacion!=null && localizacion.getProvincia()!=null && !localizacion.getProvincia().isEmpty() &&
                (localizacion.getLocalidad() == null || !localizacion.getProvincia().equals(localizacion.getLocalidad())) ? ", " + localizacion.getProvincia() : "" )+
                (localizacion!=null ? ", Argentina" : "");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Direccion)) return false;
        Direccion direccion = (Direccion) o;
        return Objects.equals(getCalle(), direccion.getCalle()) &&
                Objects.equals(getNumero(), direccion.getNumero()) &&
                Objects.equals(getPiso(), direccion.getPiso()) &&
                Objects.equals(getDepartamento(), direccion.getDepartamento()) &&
                Objects.equals(getLocalizacion(), direccion.getLocalizacion());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCalle(), getNumero(), getPiso(), getDepartamento(), getLocalizacion());
    }
}
