package ar.com.experta.ventashogarapi.core.model.enums;

public enum Sexo {
    MASCULINO ("Masculino", "Sexo Masculino según figura en el DNI"),
    FEMENINO ("Femenino", "Sexo Femenino según figura en el DNI");

    private String label;
    private String description;

    Sexo(String label, String description) {
        this.label = label;
        this.description = description;
    }

    public String label() {
        return label;
    }

    public String description() {
        return description;
    }
}
