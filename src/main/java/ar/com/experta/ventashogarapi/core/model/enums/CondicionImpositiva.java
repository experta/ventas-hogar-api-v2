package ar.com.experta.ventashogarapi.core.model.enums;

public enum CondicionImpositiva {
    CONSUMIDOR_FINAL ("Consumidor Final", "Personas fisicas que destine el servicio para uso o consumo privado."),
    EXENTO ("Exento","Personas fisicas o juridicas que desarrollan actividades exentas de pago de IVA."),
    RESPONSABLE_INSCRIPTO ("Responsable inscripto","Persona fisica o juridica que forma parte del Régimen General impositivo."),
    MONOTRIBUTISTA ("Monotributista","Persona fisica inscripta en el régimen simplificado de pago de impuestos para pequeños contribuyentes.");

    private String label;
    private String description;

    CondicionImpositiva(String label, String description) {
        this.label = label;
        this.description = description;
    }

    public String label() {
        return label;
    }

    public String description() {
        return description;
    }
}
