package ar.com.experta.ventashogarapi.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Arrays;
import java.util.Objects;

public class Localizacion {
    private String id;
    private String codigoPostal;
    private String localidad;
    private String provincia;
    private String zonaHogar;

    final static private String ZONAS_RIESGOS_ROBO[] = {"1","2","10"};
    final static private String CP_RIESGOS_INCENDIO[] = {"9408","9407","9405","9400","9301","9410","9420"};


    public Localizacion() {
    }

    public Localizacion(String id) {
        this.setId(id);
    }

    public Localizacion(String id, String zonaHogar) {
        this.setId(id);
        this.zonaHogar = zonaHogar;
    }

    public Localizacion(String id, String codigoPostal, String localidad, String provincia, String zonaHogar) {
        this.id = id;
        this.codigoPostal = codigoPostal;
        this.localidad = localidad;
        this.provincia = provincia;
        this.zonaHogar = zonaHogar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
        if (id != null && id.length() > 4)
            this.codigoPostal = id.substring(0,4);
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getZonaHogar() {
        return zonaHogar;
    }

    public void setZonaHogar(String zonaHogar) {
        this.zonaHogar = zonaHogar;
    }

    public String getLabel() {
        return this.localidad != null && this.provincia != null
                ? this.localidad + (this.localidad.equals(this.provincia) ? "" : ", " + this.provincia)
                : null;
    }

    public Boolean esZonaRiesgoRobo() {
        return this.zonaHogar != null ? Boolean.valueOf(Arrays.stream(ZONAS_RIESGOS_ROBO).anyMatch(this.zonaHogar::equals)) : null;
    }

    public Boolean esZonaRiesgoIncendio() {
        return this.codigoPostal != null ? Boolean.valueOf(Arrays.stream(CP_RIESGOS_INCENDIO).anyMatch(this.codigoPostal::equals)) : null;
    }


    @JsonIgnore
    public String getSubcp() {
        return id.substring(4);
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Localizacion that = (Localizacion) o;
        return id.equals(that.id);
    }

    public int hashCode() {
        return Objects.hash(id);
    }
}

