package ar.com.experta.ventashogarapi.core.model;

import java.util.List;

public class NegocioOptionClausulas extends NegocioOption{
    private List<NegocioOptionClausulas> subclausulas;

    public NegocioOptionClausulas(String value, String label) {
        super(value, label, null);
    }

    public NegocioOptionClausulas(String value, String label, List<NegocioOptionClausulas> subclausulas) {
        super(value, label, null);
        this.subclausulas = subclausulas;
    }

    public NegocioOptionClausulas(String value, String label, String description, List<NegocioOptionClausulas> subclausulas) {
        super(value, label, description);
        this.subclausulas = subclausulas;
    }

    public List<NegocioOptionClausulas> getSubclausulas() {
        return subclausulas;
    }

    public void setSubclausulas(List<NegocioOptionClausulas> subclausulas) {
        this.subclausulas = subclausulas;
    }
}
