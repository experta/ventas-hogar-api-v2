package ar.com.experta.ventashogarapi.core.model.enums;

public enum TipoDocumento {
    DNI,
    CUIT;
}
