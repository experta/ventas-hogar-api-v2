package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.MedioPago;

public class PagoCuponera extends Pago{

    public PagoCuponera() {
        super(MedioPago.CUPONERA);
    }

}
