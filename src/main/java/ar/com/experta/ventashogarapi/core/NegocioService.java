package ar.com.experta.ventashogarapi.core;

import ar.com.experta.ventashogarapi.commons.exceptions.NotImplementedException;
import ar.com.experta.ventashogarapi.core.model.*;
import ar.com.experta.ventashogarapi.core.model.enums.*;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.GlmHogarClient;
import ar.com.experta.ventashogarapi.infraestructure.negocioclient.ConsultasNegocio;
import ar.com.experta.ventashogarapi.infraestructure.negocioclient.NegocioClient;
import ar.com.experta.ventashogarapi.infraestructure.vendedoresclient.VendedoresClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class NegocioService {

    @Autowired
    private GlmHogarClient glmHogarClient;
    @Autowired
    private NegocioClient negocioClient;
    @Autowired
    private PlanService planService;
    @Autowired
    private BeneficiosService beneficiosService;
    @Autowired
    private VendedoresClient vendedoresClient;

    public List<NegocioOptionTipoPlan> getTiposPlanes(Optional<String> idVendedor, CanalVenta canalVenta) {
        List<Plan> planes;

        if (idVendedor.isPresent()){
            planes = planService.getPlanes(idVendedor.get());
        }else{
            planes = planService.getEncabezadosPlanes();
        }

        planes = planes
                .stream()
                .filter(plan -> plan
                        .getTiposProductos()
                        .stream()
                        .filter(tp -> tp.canalesVenta().contains(canalVenta))
                        .findAny()
                        .isPresent() )
                .collect(Collectors.toList());

        TipoPlan tipoPlanDefault = planes.stream().min(Comparator.comparingInt(Plan::getPrioridadTipoPlan)).get().getTipoPlan();

        List<NegocioOptionTipoPlan> options = planes.stream()
                .map(plan -> plan.getTipoPlan())
                .distinct()
                .collect(Collectors.toList())
                .stream()
                .sorted(Comparator.comparingInt(TipoPlan::prioridad))
                .map(p -> new NegocioOptionTipoPlan(p, new HashSet<>(), p.equals(tipoPlanDefault)))
                .collect(Collectors.toList());


        List<Plan> finalPlanes = planes;
        options.stream()
                .forEach(negocioOptionTipoPlan -> finalPlanes.stream()
                                                    .forEach(plan -> {
                                                        if (plan.getTipoPlan().toString().equals(negocioOptionTipoPlan.getValue()) &&
                                                                plan.getBeneficios() != null)
                                                            negocioOptionTipoPlan.getBeneficios().addAll(plan.getBeneficios());
                                                        }));

        return options;
    }

    public List<NegocioOption> getTiposVivienda(TipoPlan tipoPlan) {
        if (tipoPlan != null) {
            if (tipoPlan.viviendaPermanente())
                return Arrays.asList(new NegocioOption(TipoVivienda.VIVIENDA_PERMANENTE.toString(), TipoVivienda.VIVIENDA_PERMANENTE.label(), TipoVivienda.VIVIENDA_PERMANENTE.description()));
            List<TipoVivienda> tiposVivienda = Arrays.asList(TipoVivienda.EDIFICIO_PB, TipoVivienda.EDIFICIO_PA, TipoVivienda.CASA, TipoVivienda.COUNTRY);
            return tiposVivienda.stream().map(p -> new NegocioOption(p.toString(), p.label(), p.description())).collect(Collectors.toList());
        }
        List<TipoVivienda> tiposVivienda = Arrays.asList(TipoVivienda.values());
        return tiposVivienda.stream().map(p -> new NegocioOption(p.toString(), p.label(), p.description())).collect(Collectors.toList());
    }

    public List<NegocioOption> getTiposCoberturas() {
        List<TipoCobertura> tiposCobertura = Arrays.asList(TipoCobertura.values());
        return tiposCobertura.stream().map(p -> new NegocioOption(p.toString(), p.label(), p.description())).collect(Collectors.toList());
    }

    public List<NegocioOption> getCondicionesImpositivas(TipoPersona tipoPersona) {
        if (tipoPersona != null)
            return negocioClient.getOptions(ConsultasNegocio.GET_CONDICIONES_IMPOSITIVAS_OPTIONS_BY_TIPO_PERSONA,tipoPersona.toString());
        return negocioClient.getOptions(ConsultasNegocio.GET_CONDICIONES_IMPOSITIVAS_OPTIONS);
    }

    public List<NegocioOption> getTiposDocumento(TipoPersona tipoPersona, CondicionImpositiva condicionImpositiva) {
        if (tipoPersona != null && condicionImpositiva != null)
            return negocioClient.getOptions(ConsultasNegocio.GET_TIPOS_DOCUMENTO_OPTIONS_BY_CONDICION_AND_PERSONA,tipoPersona.toString(), condicionImpositiva.toString());
        if (tipoPersona != null)
            return negocioClient.getOptions(ConsultasNegocio.GET_TIPOS_DOCUMENTO_OPTIONS_BY_PERSONA,tipoPersona.toString());
        if (condicionImpositiva != null)
            return negocioClient.getOptions(ConsultasNegocio.GET_TIPOS_DOCUMENTO_OPTIONS_BY_CONDICION,condicionImpositiva.toString());
        return negocioClient.getOptions(ConsultasNegocio.GET_TIPOS_DOCUMENTO_OPTIONS);
    }

    public List <NegocioOption> getLocalidades(String codigoPostal) {
        return negocioClient.getOptions(ConsultasNegocio.GET_LOCALIZACIONES_OPTIONS_BY_CP, codigoPostal);
    }

    public Localizacion getLocalizacion(String id) {
        return this.negocioClient.getLocalizacion(id);
    }

    public List<NegocioOption> getCanalesVenta() {
        return Stream.of(CanalVenta.values())
                .filter(t -> t.label()!=null)
                .map(t -> new NegocioOption(t.toString(), t.label(), null))
                .collect(Collectors.toList());
    }

    public List<NegocioOption> getTiposBienesEspecificos(CategoriaBienEspecifico categoriaBienEspecifico) {
        return Stream.of(TipoBienEspecifico.values())
                .filter(t -> t.categoriaBienEspecifico().equals(categoriaBienEspecifico))
                .map(t -> new NegocioOption(t.toString(), t.label(), t.description()))
                .collect(Collectors.toList());
    }

    public List<NegocioOption> getMediosPago(IdPlan idPlan, String idVendedor, TipoMedioPago tipoMedioPago) {
        Plan plan = planService.getPlan(idPlan);
        Vendedor vendedor = vendedoresClient.getVendedor(idVendedor);

        return Arrays.asList(MedioPago.values()).stream()
                .filter(t -> ((plan.getAceptaCuponera() && vendedor.getCuponeraHabilitada() ) || !t.equals(MedioPago.CUPONERA))
                        && (tipoMedioPago == null || t.tipoMedioPago().equals(tipoMedioPago)))
                .map(t -> new NegocioOption(t.toString(), t.label(), null))
                .collect(Collectors.toList());
    }

    public List<NegocioOption> getTiposMedioPago(TipoPlan tipoPlan, String idVendedor) {
        Vendedor vendedor = vendedoresClient.getVendedor(idVendedor);

        return Arrays.asList(MedioPago.values()).stream()
                .filter(t -> (vendedor.getCuponeraHabilitada() || !t.equals(MedioPago.CUPONERA)) && tipoPlan.tiposMedioPago().contains(t.tipoMedioPago()))
                .map(t -> new NegocioOption(t.tipoMedioPago().toString(), t.tipoMedioPago().label(), null))
                .distinct()
                .collect(Collectors.toList());
    }

    public List<NegocioOption> getCantidadCuotas(TipoPlan tipoPlan) {
        return tipoPlan.tipoCantidadCuotas().stream()
                .filter(t -> t.activo())
                .map(t -> new NegocioOption(t.value(), t.label(), null))
                .collect(Collectors.toList());
    }

    public List<NegocioOption> getBancos() {
        return negocioClient.getOptions(ConsultasNegocio.GET_BANCOS_OPTIONS);
    }

    public String getBanco(String cbu) {
        return negocioClient.getOptions(ConsultasNegocio.GET_BANCOS_BY_CBU, cbu).get(0).getValue();
    }

    public List<NegocioOption> getMarcasTarjetasCredito() {
        return negocioClient.getOptions(ConsultasNegocio.GET_MARCAS_TARJETAS_CREDITO_OPTIONS);
    }

    public List<NegocioOption> getNacionalidades() {
        return negocioClient.getOptions(ConsultasNegocio.GET_NACIONALIDADES_OPTIONS);
    }

    public List<NegocioOption> getActividadesTomador() {
        return negocioClient.getOptions(ConsultasNegocio.GET_ACTIVIDADES_TOMADOR_OPTIONS);
    }

    public List<NegocioOption> getEstadosVenta() {
        return Stream.of(EstadoVenta.values())
                .filter(t -> t.label()!=null)
                .map(t -> new NegocioOption(t.toString(), t.label(), null))
                .collect(Collectors.toList());
    }

    public List<NegocioOption> getBeneficios(Optional<String> idVendedor, Optional<TipoPlan> tipoPlan) {
        Vendedor vendedor = null;

        if (idVendedor.isPresent())
            vendedor = vendedoresClient.getVendedor(idVendedor.get());

        Vendedor finalVendedor = vendedor;
        return Stream.of(Beneficio.values())
                .filter(b -> !idVendedor.isPresent() || b.tiposProductos().stream().filter(finalVendedor.getTipoProductos()::contains).findAny().isPresent() )
                .filter(b -> !tipoPlan.isPresent() || b.tiposPlanes().contains(tipoPlan.get()) )
                .map(b -> new NegocioOption(b.toString(), b.label(), b.descripcion()))
                .collect(Collectors.toList());
    }

    public Validacion validarBeneficios(Beneficio beneficio, String documentoCliente) {
        if (beneficio.equals(Beneficio.AUTOS_HOGAR)){
            TipoDocumento tipoDocumento = documentoCliente.length()==11 ? TipoDocumento.CUIT : TipoDocumento.DNI;
            Documento documento = new Documento(tipoDocumento, documentoCliente);

            if (beneficiosService.esClienteAutos(documento))
                return new Validacion(Boolean.TRUE, beneficio.validacionMessage());
            return new Validacion(Boolean.FALSE, "El cliente no tiene poliza de autos vigente al día de la fecha");
        }
        if (beneficio.equals(Beneficio.EMPLEADOS_DIRECTV)) {
            if (beneficiosService.esNominaDirectv(documentoCliente))
                return new Validacion(Boolean.TRUE, beneficio.validacionMessage());
            return new Validacion(Boolean.FALSE, "El cliente no es un empelado en nomina de DirectTV habilitado para emitir este producto");
        }
        if (beneficio.equals(Beneficio.EMPLEADOS_GW)) {
            if (beneficiosService.esNominaGrupoW(documentoCliente))
                return new Validacion(Boolean.TRUE, beneficio.validacionMessage());
            return new Validacion(Boolean.FALSE, "El cliente no es un empelado en nomina del Grupo Werthein habilitado para emitir este producto");
        }
        return null;

    }

    public InputStream getManualStream(TipoPlan tipoPlan) throws IOException {
        if (tipoPlan.equals(TipoPlan.CONFIGURABLE)){
            return  ResourceUtils.getURL("classpath:manuales/Manual_de_producto-A_tu_medida-Enero_2025.pdf").openStream();
        }else if (tipoPlan.equals(TipoPlan.ENLATADO)){
           return   ResourceUtils.getURL("classpath:manuales/Manual_de_producto-Paquetes-Enero_2025.pdf").openStream();
        }else if (tipoPlan.equals(TipoPlan.ENLATADO_HOGAR_DGO)){
            return   ResourceUtils.getURL("classpath:manuales/Manual_de_producto-Hogar+DGO-Febrero_2025.pdf").openStream();
        }else{
            throw new NotImplementedException();
        }
    }

    public String getManualNombre(TipoPlan tipoPlan) throws IOException {
        if (tipoPlan.equals(TipoPlan.CONFIGURABLE)){
            return  "Manual de producto - A tu medida - Enero 2025.pdf";
        }else if (tipoPlan.equals(TipoPlan.ENLATADO)){
            return "Manual de producto - Paquetes - Enero 2025.pdf";
        }else if (tipoPlan.equals(TipoPlan.ENLATADO_HOGAR_DGO)){
            return "Manual de producto - Hogar+DGO - Febrero 2025.pdf";
        }else{
            throw new NotImplementedException();
        }
    }

    public List<NegocioOptionClausulas> getMedidasSeguridad(String localizacionId, TipoVivienda tipoVivienda, TipoPlan tipoPlan) {
        Vivienda vivienda = new Vivienda(
                tipoVivienda,
                new Direccion(
                        new Localizacion(
                                localizacionId,
                                localizacionId != null ? negocioClient.getZonaRiesgo(localizacionId) : null)),
                tipoPlan);
        return vivienda.getMedidasSeguridad();
    }
}
