package ar.com.experta.ventashogarapi.core.model.enums;

import java.util.Arrays;
import java.util.List;

public enum TipoPlan {

    CONFIGURABLE (
            100,
            Boolean.FALSE,
            "A tu medida",
            "A tu medida PAS",
            "Plan personalizable con coberturas y montos variables",
            Arrays.asList(OpcionPlan.METROS_CUADRADOS),
            Arrays.asList(TipoMedioPago.AUTOMATICO, TipoMedioPago.MANUAL),
            Arrays.asList(CantidadCuotas.CUOTAS3, CantidadCuotas.CUOTAS2, CantidadCuotas.CUOTAS1),
            Boolean.FALSE,
            Boolean.FALSE,
            Boolean.FALSE
    ),
    ENLATADO (
            200,
            Boolean.TRUE,
            "Paquetes",
            "Paquetes PAS",
            "Planes cerrados con coberturas y montos fijos.",
            null,
            Arrays.asList(TipoMedioPago.AUTOMATICO, TipoMedioPago.MANUAL),
            Arrays.asList(CantidadCuotas.CUOTAS3, CantidadCuotas.CUOTAS2, CantidadCuotas.CUOTAS1),
            Boolean.FALSE,
            Boolean.FALSE,
            Boolean.FALSE
    ),
    ENLATADO_ECOMMERCE (
            300,
            Boolean.TRUE,
            "Paquetes E-commerce",
            "Paquetes E-commerce",
            "Planes cerrados con coberturas y montos fijos.",
            null,
            Arrays.asList(TipoMedioPago.AUTOMATICO),
//            Arrays.asList(CantidadCuotas.CUOTAS12)
            Arrays.asList(CantidadCuotas.CUOTAS3),
            Boolean.FALSE,
            Boolean.FALSE,
            Boolean.FALSE
    ),
    ENLATADO_ELEBAR (
            301,
            Boolean.TRUE,
            "Paquetes",
            "Paquetes Elebar",
            "Planes cerrados con coberturas y montos fijos.",
            null,
            Arrays.asList(TipoMedioPago.AUTOMATICO),
//            Arrays.asList(CantidadCuotas.CUOTAS12)
            Arrays.asList(CantidadCuotas.CUOTAS3),
            Boolean.FALSE,
            Boolean.FALSE,
            Boolean.FALSE
    ),
    ENLATADO_REBA (
            302,
            Boolean.TRUE,
            "Paquetes",
            "Paquetes Reba",
            "Planes cerrados con coberturas y montos fijos.",
            null,
            Arrays.asList(TipoMedioPago.AUTOMATICO),
//            Arrays.asList(CantidadCuotas.CUOTAS12)
            Arrays.asList(CantidadCuotas.CUOTAS3),
            Boolean.FALSE,
            Boolean.FALSE,
            Boolean.FALSE
    ),
    ENLATADO_ALRIO (
            303,
            Boolean.TRUE,
            "Paquetes Complejo al Rio",
            "Paquetes Al Rio",
            "Planes cerrados con coberturas y montos fijos, especialmente configurados para Complejo al Rio",
            null,
            Arrays.asList(TipoMedioPago.AUTOMATICO),
//            Arrays.asList(CantidadCuotas.CUOTAS10)
            Arrays.asList(CantidadCuotas.CUOTAS3),
            Boolean.FALSE,
            Boolean.FALSE,
            Boolean.FALSE
    ),
    ENLATADO_ALIANZA_ASEGURALO (
            304,
            Boolean.TRUE,
            "Paquetes - Alianza Aseguralo",
            "Paquetes Aseguralo",
            "Planes cerrados con coberturas y montos fijos especiales para alianza de Aseguralo.",
            null,
            Arrays.asList(TipoMedioPago.AUTOMATICO),
            Arrays.asList(CantidadCuotas.CUOTAS3),
            Boolean.FALSE,
            Boolean.FALSE,
            Boolean.TRUE
    ),
    ENLATADO_ALIANZA_COMPARAENCASA (
            305,
            Boolean.TRUE,
            "Paquetes - Alianza Compara en Casa",
            "Paquetes Compara en Casa",
            "Planes cerrados con coberturas y montos fijos especiales para alianza de Compara en Casa.",
            null,
            Arrays.asList(TipoMedioPago.AUTOMATICO),
            Arrays.asList(CantidadCuotas.CUOTAS3),
            Boolean.FALSE,
            Boolean.TRUE,
            Boolean.TRUE
    ),
    ENLATADO_ALIANZA_BTF (
            306,
            Boolean.TRUE,
            "Paquetes - Alianza Banco de Tierra del Fuego",
            "Paquetes Banco de Tierra del Fuego",
            "Planes cerrados con coberturas y montos fijos especiales para alianza de Banco de Tierra del Fuego.",
            null,
            Arrays.asList(TipoMedioPago.AUTOMATICO),
            Arrays.asList(CantidadCuotas.CUOTAS3),
            Boolean.FALSE,
            Boolean.TRUE,
            Boolean.FALSE
    ),
    ENLATADO_HOGAR_DGO (
            400,
            Boolean.TRUE,
            "Paquetes - Hogar + DirecTV GO",
            "Paquetes Hogar + DirecTV GO",
            "Planes cerrados con coberturas y montos fijos para Hogar con DGO.",
            null,
            Arrays.asList(TipoMedioPago.AUTOMATICO),
            Arrays.asList(CantidadCuotas.CUOTAS3),
            Boolean.FALSE,
            Boolean.FALSE,
            Boolean.FALSE
    ),
    CONFIGURABLE_CDTV (
            401,
            Boolean.FALSE,
            "A tu medida para Clientes DirecTV",
            "A tu medida Clientes DirecTV",
            "Plan personalizable con coberturas y montos variables para clientes de DirecTV.",
            Arrays.asList(OpcionPlan.METROS_CUADRADOS),
            Arrays.asList(TipoMedioPago.AUTOMATICO),
//            Arrays.asList(CantidadCuotas.CUOTAS10)
            Arrays.asList(CantidadCuotas.CUOTAS3),
            Boolean.FALSE,
            Boolean.FALSE,
            Boolean.FALSE
    ),
    ENLATADO_CDTV (
            402,
            Boolean.TRUE,
            "Paquetes para Clientes DirecTV",
            "Paquetes Clientes DirecTV",
            "Planes cerrados con coberturas y montos fijos especiales para clientes de DirecTV.",
            null,
            Arrays.asList(TipoMedioPago.AUTOMATICO),
//            Arrays.asList(CantidadCuotas.CUOTAS10)
            Arrays.asList(CantidadCuotas.CUOTAS3),
            Boolean.FALSE,
            Boolean.FALSE,
            Boolean.FALSE
            ),
    CONFIGURABLE_AFINIDAD (
            403,
            Boolean.FALSE,
            "A tu medida para Grupo de Afinidad del Grupo Werthein",
            "A tu medida Afinidad Grupo Werthein",
            "Plan personalizable con coberturas y montos variables para Grupo de Afinidad del Grupo Werthein",
            Arrays.asList(OpcionPlan.METROS_CUADRADOS),
            Arrays.asList(TipoMedioPago.AUTOMATICO, TipoMedioPago.MANUAL),
            Arrays.asList(CantidadCuotas.CUOTAS3, CantidadCuotas.CUOTAS2, CantidadCuotas.CUOTAS1),
            Boolean.FALSE,
            Boolean.FALSE,
            Boolean.FALSE
    ),
    ENLATADO_AFINIDAD (
            404,
            Boolean.TRUE,
            "Paquetes para Grupo de Afinidad del Grupo Werthein",
            "Paquetes Afinidad Grupo Werthein",
            "Planes cerrados con coberturas y montos fijos especiales para Grupo de Afinidad del Grupo Werthein",
            null,
            Arrays.asList(TipoMedioPago.AUTOMATICO, TipoMedioPago.MANUAL),
            Arrays.asList(CantidadCuotas.CUOTAS3, CantidadCuotas.CUOTAS2, CantidadCuotas.CUOTAS1),
            Boolean.FALSE,
            Boolean.FALSE,
            Boolean.FALSE
    ),
    CONFIGURABLE_GH (
            405,
            Boolean.FALSE,
            "A tu medida - Promoción Gran Hermano 2024",
            "A tu medida PAS",
            "Plan personalizable con coberturas y montos variables",
            Arrays.asList(OpcionPlan.METROS_CUADRADOS),
            Arrays.asList(TipoMedioPago.AUTOMATICO, TipoMedioPago.MANUAL),
            Arrays.asList(CantidadCuotas.CUOTAS3, CantidadCuotas.CUOTAS2, CantidadCuotas.CUOTAS1),
            Boolean.FALSE,
            Boolean.FALSE,
            Boolean.FALSE
    ),
    ENLATADO_GH (
            406,
            Boolean.TRUE,
            "Paquetes - Promoción Gran Hermano 2024",
            "Paquetes PAS",
            "Planes cerrados con coberturas y montos fijos.",
            null,
            Arrays.asList(TipoMedioPago.AUTOMATICO, TipoMedioPago.MANUAL),
            Arrays.asList(CantidadCuotas.CUOTAS3, CantidadCuotas.CUOTAS2, CantidadCuotas.CUOTAS1),
            Boolean.FALSE,
            Boolean.FALSE,
            Boolean.FALSE
    ),
    ENLATADO_ALIANZA_DTV (
            407,
            Boolean.TRUE,
            "Paquetes - Alianza DirecTV",
            "Paquetes Clientes DirecTV",
            "Planes cerrados con coberturas y montos fijos especiales para alianza para clientes de DirecTV.",
            null,
            Arrays.asList(TipoMedioPago.OP_BANCARIA_DTV),
            Arrays.asList(CantidadCuotas.CUOTAS12),
            Boolean.TRUE,
            Boolean.FALSE, Boolean.TRUE
    ),


    ;

    private Integer prioridad;
    private Boolean enlatado;
    private String label;
    private String labelMetricas;
    private String description;
    private List<OpcionPlan> opciones;
    private List<TipoMedioPago> tiposMedioPago;
    private List<CantidadCuotas> cantidadCuotas;
    private Boolean vigenciaRetroactiva;
    private Boolean clausulaEstabilizacionFija;
    private Boolean viviendaPermanente;

    TipoPlan(Integer prioridad, Boolean enlatado, String label, String labelMetricas, String description, List<OpcionPlan> opciones, List<TipoMedioPago> tiposMedioPago, List<CantidadCuotas> cantidadCuotas, Boolean vigenciaRetroactiva, Boolean clausulaEstabilizacionFija, Boolean viviendaPermanente) {
        this.prioridad = prioridad;
        this.enlatado = enlatado;
        this.label = label;
        this.labelMetricas = labelMetricas;
        this.description = description;
        this.opciones = opciones;
        this.tiposMedioPago = tiposMedioPago;
        this.cantidadCuotas = cantidadCuotas;
        this.vigenciaRetroactiva = vigenciaRetroactiva;
        this.clausulaEstabilizacionFija = clausulaEstabilizacionFija;
        this.viviendaPermanente = viviendaPermanente;
    }

    public Integer prioridad() {
        return prioridad;
    }

    public String label() {
        return label;
    }

    public String labelMetricas() {
        return labelMetricas;
    }

    public String description() {
        return description;
    }

    public Boolean enlatado() {
        return enlatado;
    }

    public List<TipoMedioPago> tiposMedioPago() {
        return tiposMedioPago;
    }

    public List<CantidadCuotas> tipoCantidadCuotas() {
        return cantidadCuotas;
    }

    public List<OpcionPlan> opciones() {
        return opciones;
    }

    public Boolean vigenciaRetroactiva() {
        return vigenciaRetroactiva;
    }

    public Boolean getClausulaEstabilizacionFija() {
        return clausulaEstabilizacionFija;
    }

    public Boolean viviendaPermanente() {
        return viviendaPermanente;
    }
}
