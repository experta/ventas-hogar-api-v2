package ar.com.experta.ventashogarapi.core.model.enums;

public enum EstadoVenta {
    BORRADOR(null),
    COTIZADA("Cotizada"),
    VENCIDA("Vencidas"),
    INHABILITADA("Inhabilitada"),
    BORRADOR_A_EMITIR(null),
    A_EMITIR("Emision pendiente"),
    EMITIDA("Emitida"),
    EMITIENDO("En proceso de emision"),
    EMISION_FALLIDA("Emision fallida"),
    RECHAZADA("Emision rechazada");

    private String label;

    EstadoVenta(final String label) {
        this.label = label;
    }

    public String label () {
        return label;
    }
}
