package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Plan {

    private IdPlan idPlan;
    private String nombrePlan;
    private TipoPlan tipoPlan;
    private List<TipoProductos> tiposProductos;
    private List<PlanItem> items;
    private List<Beneficio> beneficios;

    public Plan() {
    }

    public Plan(IdPlan idPlan) {
        this.idPlan = idPlan;
    }

    public Plan(IdPlan idPlan, String nombrePlan, TipoPlan tipoPlan, List<TipoProductos> tiposProductos, List<PlanItem> items) {
        this.idPlan = idPlan;
        this.nombrePlan = nombrePlan;
        this.tipoPlan = tipoPlan;
        this.tiposProductos = tiposProductos;
        this.items = items;
    }

    public IdPlan getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(IdPlan idPlan) {
        this.idPlan = idPlan;
    }

    public String getNombrePlan() {
        return nombrePlan;
    }

    public void setNombrePlan(String nombrePlan) {
        this.nombrePlan = nombrePlan;
    }

    public List<PlanItem> getItems() {
        return items;
    }

    public void setItems(List<PlanItem> items) {
        this.items = items;
    }

    public List<TipoProductos> getTiposProductos() {
        return tiposProductos;
    }

    public void setTiposProductos(List<TipoProductos> tiposProductos) {
        this.tiposProductos = tiposProductos;
    }

    public TipoPlan getTipoPlan() {
        return tipoPlan;
    }

    public void setTipoPlan(TipoPlan tipoPlan) {
        this.tipoPlan = tipoPlan;
    }

    public List<Beneficio> getBeneficios() {
        return beneficios;
    }

    public void setBeneficios(List<Beneficio> beneficios) {
        this.beneficios = beneficios;
    }

    @JsonIgnore
    public Boolean getAceptaCuponera() {
        return tipoPlan.tiposMedioPago().contains(TipoMedioPago.MANUAL);
    }

    @JsonIgnore
    public Integer getPrioridadTipoPlan(){
        return tipoPlan.prioridad();
    }

    @JsonIgnore
    public List<NegocioOptionClausulas> getServiciosAsistencias(){
        return getServiciosAsistencias(null);
    }

    @JsonIgnore
    public List<NegocioOptionClausulas> getServiciosAsistencias(List<String> planesMantenimiento){

        List<NegocioOptionClausulas> clausulasServiciosEmergencia = new ArrayList<>();
        clausulasServiciosEmergencia.add(
                new NegocioOptionClausulas(
                        "0",
                        "Servicios En Instalaciones Eléctricas, Plomería, Gas, Cerrajería, Vidrieria."
                ));
        clausulasServiciosEmergencia.add(
                new NegocioOptionClausulas(
                        "0",
                        "Prestaciones Programadas – Servicio De Conexión: Ilimitado."
                ));
        clausulasServiciosEmergencia.add(
                new NegocioOptionClausulas("1",
                        "Reembolso De Gastos Por Alquiler De Grupo Electrogeno."
                )
        );
        List<NegocioOptionClausulas> clausulasAsistenciasSiniestros = new ArrayList<>();
        clausulasAsistenciasSiniestros.add(
                new NegocioOptionClausulas(
                        "0",
                        "Seguridad Y Vigilancia Para El Domicilio."
                ));
        clausulasAsistenciasSiniestros.add(
                new NegocioOptionClausulas(
                        "0",
                        "Traslado Y Guarda De Muebles."
                ));
        clausulasAsistenciasSiniestros.add(
                new NegocioOptionClausulas("1",
                        "Reembolso De Gastos De Hospedaje Y Alimentos."
                )
        );
        List<NegocioOptionClausulas> clausulasServiciosMantenimiento = new ArrayList<>();
        clausulasServiciosMantenimiento.add(
                new NegocioOptionClausulas(
                        "0",
                        "Mantenimiento De Aire Acondicionado Y Estufas."
                ));
        clausulasServiciosMantenimiento.add(
                new NegocioOptionClausulas(
                        "0",
                        "Servicio General De Plomería."
                ));
        clausulasServiciosMantenimiento.add(
                new NegocioOptionClausulas("1",
                        "Servicio General De Cerrajería."
                ));
        clausulasServiciosMantenimiento.add(
                new NegocioOptionClausulas("2",
                        "Servicios Al Hogar: Instalación De Luminarias, Destape De Cañerías, Instalación De Grifos, Reparación De Filtraciones De Humedad, Trabajos Generales De Albañilería, Instalación De Cortinas."
                )
        );

        List<NegocioOptionClausulas> serviciosAsistencias = new ArrayList<>();
        serviciosAsistencias.add(
                new NegocioOptionClausulas(
                        "0",
                        "SERVICIOS DE EMERGENCIA",
                        clausulasServiciosEmergencia
                ));
        serviciosAsistencias.add(
                new NegocioOptionClausulas("1",
                        "ASISTENCIAS DERIVADAS DE SINIESTROS",
                        clausulasAsistenciasSiniestros
                ));

        String aclaracionPlanesMantenimiento = "";
        if (planesMantenimiento != null && !planesMantenimiento.isEmpty()){
            if (planesMantenimiento.size() > 1){
                aclaracionPlanesMantenimiento = " (Exclusivo para planes ";
                for (int n=1; n<=planesMantenimiento.size(); n++){
                    if (n==1)
                        aclaracionPlanesMantenimiento = aclaracionPlanesMantenimiento + planesMantenimiento.get(n-1);
                    else if (n==planesMantenimiento.size())
                        aclaracionPlanesMantenimiento = aclaracionPlanesMantenimiento + " y " + planesMantenimiento.get(n-1);
                    else
                        aclaracionPlanesMantenimiento = aclaracionPlanesMantenimiento + ", " + planesMantenimiento.get(n-1);
                }
                aclaracionPlanesMantenimiento = aclaracionPlanesMantenimiento + ")";
            } else {
                aclaracionPlanesMantenimiento = " (Exclusivo para plan " + planesMantenimiento.get(0) + ")";
            }
        }

        if (this.idPlan.servicioMantenimiento() || (planesMantenimiento != null && !planesMantenimiento.isEmpty()))
            serviciosAsistencias.add(
                    new NegocioOptionClausulas(
                            "2",
                            "SERVICIOS DE MANTENIMIENTO" + aclaracionPlanesMantenimiento,
                            clausulasServiciosMantenimiento)
            );
        return serviciosAsistencias;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Plan)) return false;
        Plan plan = (Plan) o;
        return Objects.equals(idPlan, plan.idPlan);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPlan);
    }
}
