package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.MedioPago;

import java.util.Objects;

public class PagoDebito extends Pago{

    private String banco;
    private String cbu;

    public PagoDebito() {
        super(MedioPago.DEBITO);
    }

    public PagoDebito(String banco, String cbu) {
        super(MedioPago.DEBITO);
        this.banco = banco;
        this.cbu = cbu;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getCbu() {
        return cbu;
    }

    public void setCbu(String cbu) {
        this.cbu = cbu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PagoDebito)) return false;
        if (!super.equals(o)) return false;
        PagoDebito that = (PagoDebito) o;
        return getBanco() == that.getBanco() &&
                Objects.equals(getCbu(), that.getCbu());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getBanco(), getCbu());
    }
}
