package ar.com.experta.ventashogarapi.core.model.enums;

public enum TipoPersona {
    FISICA ("Persona fisica", "Ser humano capaz de adquirir derechos y obligaciones."),
    JURIDICA ("Persona juridica","Entidad de derecho como por ejemplo empresas, corporaciones, asociaciones, fundaciones, etc.");

    private String label;
    private String description;

    TipoPersona(String label, String description) {
        this.label = label;
        this.description = description;
    }

    public String label() {
        return label;
    }

    public String description() {
        return description;
    }
}
