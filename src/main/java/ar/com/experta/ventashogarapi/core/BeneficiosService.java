package ar.com.experta.ventashogarapi.core;

import ar.com.experta.ventashogarapi.core.model.Documento;
import ar.com.experta.ventashogarapi.core.model.Plan;
import ar.com.experta.ventashogarapi.core.model.Vendedor;
import ar.com.experta.ventashogarapi.core.model.enums.Beneficio;
import ar.com.experta.ventashogarapi.infraestructure.autosclient.AutosClient;
import ar.com.experta.ventashogarapi.infraestructure.autosclient.model.PolizaAutos;
import ar.com.experta.ventashogarapi.infraestructure.personasclient.PersonasClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

@Component
public class BeneficiosService {

    @Autowired
    private AutosClient autosClient;

    @Autowired
    private PersonasClient personasClient;

    public Plan configurarBeneficios(Plan plan) {
        Stream.of(Beneficio.values())
                .forEach(beneficio -> {
                    if (!Collections.disjoint(plan.getTiposProductos(), beneficio.tiposProductos())
                            && beneficio.tiposPlanes().contains(plan.getTipoPlan())) {
                        if (plan.getBeneficios() == null)
                            plan.setBeneficios(new ArrayList<>());
                        plan.getBeneficios().add(beneficio);
                    }
                });

        return plan;
    }

    public Plan configurarBeneficios(Plan plan, Vendedor vendedor) {
        Stream.of(Beneficio.values())
                .forEach(beneficio -> {
                    if (!Collections.disjoint(plan.getTiposProductos(), beneficio.tiposProductos())
                            && !Collections.disjoint(vendedor.getTipoProductos(), beneficio.tiposProductos())
                            && beneficio.tiposPlanes().contains(plan.getTipoPlan())) {
                        if (plan.getBeneficios() == null)
                            plan.setBeneficios(new ArrayList<>());
                        plan.getBeneficios().add(beneficio);
                    }
                });

        return plan;
    }

    public boolean esClienteAutos(Documento documento) {
        List<PolizaAutos> polizasAutos = autosClient.getGetPolizasAutos(documento);

        return polizasAutos.stream()
                    .filter(polizaAutos -> !polizaAutos.getFechaInicioVigencia().isAfter(LocalDate.now()) &&
                                           polizaAutos.getFechaFinVigencia().isAfter(LocalDate.now()))
                    .findAny()
                    .isPresent();
    }

    public Boolean esNominaDirectv(String documentoCliente) {
        if (personasClient.existeNominaDirectv(documentoCliente))
            return Boolean.TRUE;
        return Boolean.FALSE;
    }

    public Boolean esNominaGrupoW(String documentoCliente) {
        if (personasClient.existeNominaGrupoW(documentoCliente))
            return Boolean.TRUE;
        return Boolean.FALSE;
    }
}
