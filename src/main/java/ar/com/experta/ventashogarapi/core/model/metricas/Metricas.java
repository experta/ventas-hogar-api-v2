package ar.com.experta.ventashogarapi.core.model.metricas;

import java.util.List;

public class Metricas {

    private String name;
    private List<MetricasDataset> series;

    public Metricas() {
    }

    public Metricas(String name, List<MetricasDataset> series) {
        this.name = name;
        this.series = series;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MetricasDataset> getSeries() {
        return series;
    }

    public void setSeries(List<MetricasDataset> series) {
        this.series = series;
    }
}
