package ar.com.experta.ventashogarapi.core.model.enums;

public enum IdPlan {
    PAS_CONFIGURABLE (Boolean.FALSE, Boolean.TRUE),
    PAS_CONFIGURABLE_2023(Boolean.FALSE, Boolean.TRUE),
    PAS_CONFIGURABLE_CDTV(Boolean.FALSE, Boolean.TRUE),
    PAS_CONFIGURABLE_2024(Boolean.TRUE, Boolean.TRUE),
    PAS_CONFIGURABLE_CDTV_2024(Boolean.TRUE, Boolean.TRUE),
    PAS_CONFIGURABLE_AFINIDAD(Boolean.TRUE, Boolean.TRUE),
    PAS_CONFIGURABLE_GH(Boolean.TRUE, Boolean.TRUE),
    CERCANIA(Boolean.FALSE, Boolean.FALSE),
    PROTECCION(Boolean.FALSE, Boolean.FALSE),
    SEGURIDAD_0(Boolean.FALSE, Boolean.TRUE),
    SEGURIDAD_1(Boolean.FALSE, Boolean.FALSE),
    SEGURIDAD_2(Boolean.FALSE, Boolean.FALSE),
    SEGURIDAD_3(Boolean.FALSE, Boolean.TRUE),
    CERCANIA_2(Boolean.TRUE, Boolean.FALSE),
    PROTECCION_2(Boolean.TRUE, Boolean.FALSE),
    SEGURIDAD_0_2(Boolean.TRUE, Boolean.TRUE),
    SEGURIDAD_1_2(Boolean.TRUE, Boolean.FALSE),
    SEGURIDAD_2_2(Boolean.TRUE, Boolean.FALSE),
    SEGURIDAD_3_2(Boolean.TRUE, Boolean.TRUE),
    CERCANIA_CDTV(Boolean.TRUE, Boolean.FALSE),
    PROTECCION_CDTV(Boolean.TRUE, Boolean.FALSE),
    SEGURIDAD_0_CDTV(Boolean.TRUE, Boolean.TRUE),
    SEGURIDAD_1_CDTV(Boolean.TRUE, Boolean.FALSE),
    SEGURIDAD_2_CDTV(Boolean.TRUE, Boolean.FALSE),
    SEGURIDAD_3_CDTV(Boolean.TRUE, Boolean.TRUE),
    CERCANIA_AFINIDAD(Boolean.TRUE, Boolean.FALSE),
    PROTECCION_AFINIDAD(Boolean.TRUE, Boolean.FALSE),
    SEGURIDAD_0_AFINIDAD(Boolean.TRUE, Boolean.TRUE),
    SEGURIDAD_1_AFINIDAD(Boolean.TRUE, Boolean.FALSE),
    SEGURIDAD_2_AFINIDAD(Boolean.TRUE, Boolean.FALSE),
    SEGURIDAD_3_AFINIDAD(Boolean.TRUE, Boolean.TRUE),
    CERCANIA_GH(Boolean.TRUE, Boolean.FALSE),
    PROTECCION_GH(Boolean.TRUE, Boolean.FALSE),
    SEGURIDAD_0_GH(Boolean.TRUE, Boolean.TRUE),
    SEGURIDAD_1_GH(Boolean.TRUE, Boolean.FALSE),
    SEGURIDAD_2_GH(Boolean.TRUE, Boolean.FALSE),
    SEGURIDAD_3_GH(Boolean.TRUE, Boolean.TRUE),
    ELEBAR_1_1(Boolean.FALSE, Boolean.FALSE),
    ELEBAR_1_2(Boolean.FALSE, Boolean.FALSE),
    ELEBAR_1_3(Boolean.FALSE, Boolean.TRUE),
    ELEBAR_2_1(Boolean.FALSE, Boolean.FALSE),
    ELEBAR_2_2(Boolean.FALSE, Boolean.FALSE),
    ELEBAR_2_3(Boolean.FALSE, Boolean.TRUE),
    ELEBAR_3_1(Boolean.TRUE, Boolean.FALSE),
    ELEBAR_3_2(Boolean.TRUE, Boolean.FALSE),
    ELEBAR_3_3(Boolean.TRUE, Boolean.FALSE),
    ELEBAR_3_4(Boolean.TRUE, Boolean.FALSE),
    REBANKING_AHORRO(Boolean.TRUE, Boolean.FALSE),
    REBANKING_BALANCE(Boolean.TRUE, Boolean.FALSE),
    REBANKING_SEGURIDAD(Boolean.TRUE, Boolean.TRUE),
    ALRIO1(Boolean.TRUE, Boolean.FALSE),
    ALRIO2(Boolean.TRUE, Boolean.FALSE),
    ALRIO3(Boolean.TRUE, Boolean.FALSE),
    ALRIO4(Boolean.TRUE, Boolean.FALSE),
    ALRIO5(Boolean.TRUE, Boolean.FALSE),
    ALRIO6(Boolean.TRUE, Boolean.FALSE),
    ALRIO7(Boolean.TRUE, Boolean.FALSE),
    ALRIO8 (Boolean.TRUE, Boolean.FALSE),
    DTV0 (Boolean.TRUE, Boolean.FALSE),
    ASEGURALO_1 (Boolean.TRUE, Boolean.TRUE),
    ASEGURALO_2 (Boolean.TRUE, Boolean.TRUE),
    ASEGURALO_3 (Boolean.TRUE, Boolean.TRUE),
    ASEGURALO_4 (Boolean.TRUE, Boolean.TRUE),
    COMPARAENCASA_1 (Boolean.TRUE, Boolean.TRUE),
    COMPARAENCASA_2 (Boolean.TRUE, Boolean.TRUE),
    COMPARAENCASA_3 (Boolean.TRUE, Boolean.TRUE),
    BTF_1 (Boolean.TRUE, Boolean.TRUE),
    BTF_2 (Boolean.TRUE, Boolean.TRUE),
    BTF_3 (Boolean.TRUE, Boolean.TRUE),
    BTF_4 (Boolean.TRUE, Boolean.TRUE),
    DGO_LITE (Boolean.TRUE, Boolean.FALSE),
    DGO_FULL(Boolean.TRUE, Boolean.FALSE)
    ;

    private Boolean activo;
    private Boolean servicioMantenimiento;

    IdPlan(final Boolean activo, final Boolean servicioMantenimiento) {
        this.activo = activo;
        this.servicioMantenimiento = servicioMantenimiento;
    }

    public Boolean activo () {
        return activo;
    }

    public Boolean servicioMantenimiento() {
        return servicioMantenimiento;
    }
}
