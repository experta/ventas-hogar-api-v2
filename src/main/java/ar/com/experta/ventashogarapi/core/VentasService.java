package ar.com.experta.ventashogarapi.core;

import ar.com.experta.ventashogarapi.commons.exceptions.badgateway.*;
import ar.com.experta.ventashogarapi.commons.exceptions.badrequest.*;
import ar.com.experta.ventashogarapi.commons.exceptions.notfound.CotizacionNotFoundException;
import ar.com.experta.ventashogarapi.commons.exceptions.notfound.VentaNotFoundException;
import ar.com.experta.ventashogarapi.commons.exceptions.unprocessable.ClienteFraudeException;
import ar.com.experta.ventashogarapi.commons.exceptions.unprocessable.CodigoDGONotFoudException;
import ar.com.experta.ventashogarapi.core.model.*;
import ar.com.experta.ventashogarapi.core.model.enums.*;
import ar.com.experta.ventashogarapi.infraestructure.db.VentasDAO;
import ar.com.experta.ventashogarapi.infraestructure.dtvhubclient.DtvHubClient;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.GlmHogarClient;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.ClienteRechazadoDuplicadoException;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.ClienteRechazadoFraudeException;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.CoreBusinessException;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.CoreFailException;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.CotizacionGLM;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.EmisionGlm;
import ar.com.experta.ventashogarapi.infraestructure.impresoracontratosclient.ImpresoraContratosClient;
import ar.com.experta.ventashogarapi.infraestructure.impresoracotizacionclient.ImpresoraCotizacionClient;
import ar.com.experta.ventashogarapi.infraestructure.valoresviviendaclient.ValoresViviendaClient;
import ar.com.experta.ventashogarapi.infraestructure.valoresviviendaclient.model.ValoresVivienda;
import ar.com.experta.ventashogarapi.infraestructure.vendedoresclient.VendedoresClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

@Component
public class VentasService {

    private static final long DIAS_DURACION_COTIZACION = 30;
    private MathContext DEFAULT_MATH_CONTEXT = new MathContext(2, RoundingMode.HALF_UP);
    private BigDecimal REDONDEO_MILLONES = new BigDecimal(1000,DEFAULT_MATH_CONTEXT);

    private static final Logger logger = LoggerFactory.getLogger(VentasService.class);

    @Autowired
    private VentasDAO ventasDAO;
    @Autowired
    private GlmHogarClient coreClient;
    @Autowired
    private VendedoresClient vendedoresClient;
    @Autowired
    private NegocioService negocioService;
    @Autowired
    private ImpresoraContratosClient impresoraContratosClient;
    @Autowired
    private ImpresoraCotizacionClient impresoraCotizacionClient;
    @Autowired
    private ValoresViviendaClient valoresViviendaClient;
    @Autowired
    private PlanService planService;
    @Autowired
    private BeneficiosService beneficiosService;
    @Autowired
    private DtvHubClient dtvHubClient;
    @Value("${swagger.environment}")
    private String environment;

    private TipoVivienda tipoViviendaAlRio = TipoVivienda.EDIFICIO_PA;
    private Localizacion localizacionAlRio = new Localizacion("1638000",
            "1638",
            "VICENTE LOPEZ",
            "BUENOS AIRES",
            "1");

    /**
     * Crea una cotizacion con los datos ingresados y los obtenidos del core de seguros
     */
    private Cotizacion crearCotizacion(String idCotizacion, String idVendedor, IdPlan idPlan, List<Cobertura> coberturas, Vivienda vivienda, Cliente cliente, LocalDate fechaInicioCobertura,TipoMedioPago tipoMedioPago, CantidadCuotas cantidadCuotas, ModalidadComision modalidadComision, Boolean clausulaEstabilizacion, Beneficio beneficio){
        try {
            CotizacionGLM cotizacionGlm = coreClient.cotizar(idVendedor,
                                                                planService.getPlan(idPlan),
                                                                coberturas,
                                                                vivienda,
                                                                null,
                                                                cliente,
                                                                null,
                                                                fechaInicioCobertura,
                                                                tipoMedioPago,
                                                                cantidadCuotas,
                                                                modalidadComision,
                                                                clausulaEstabilizacion
            );
            return new Cotizacion(
                    idCotizacion,
                    idPlan,
                    coberturas,
                    EstadoCotizacion.COTIZADA,
                    LocalDate.now(),
                    LocalDate.now().plusDays(DIAS_DURACION_COTIZACION),
                    cotizacionGlm.getPrima(),
                    cotizacionGlm.getPremio(),
                    cotizacionGlm.getImpuestos(),
                    cotizacionGlm.getCuotas(),
                    beneficio != null ? beneficio.descuento() : null);
        } catch (CoreBusinessException e) {
            throw new BadRequestException(e.getMessages());
        } catch (CoreFailException e){
            logger.error(e.getMessage());
            throw new GLMBadGatewayException(e);
        }
    }

    private Venta recotizarVenta(Venta venta){
        venta.getVivienda().getDireccion().setLocalizacion(negocioService.getLocalizacion(venta.getVivienda().getDireccion().getLocalizacion().getId()));
        List<Cotizacion> cotizaciones =  venta.getCotizaciones()
                .stream()
                .map(c -> this.crearCotizacion(c.getId(),venta.getVendedor().getId(), c.getIdPlan(), c.getCoberturas(), venta.getVivienda(), venta.getCliente(),venta.getFechaInicioCobertura(), venta.getTipoMedioPago(), venta.getCantidadCuotas(), venta.getModalidadComision(), venta.getClausulaEstabilizacion(), venta.getBeneficio() ))
                .collect(Collectors.toList());
        venta.setCotizaciones(cotizaciones);
        return venta;
    }

    private Venta composeVenta(Venta venta) {
        venta = this.composeVendedor(venta);
        venta = this.composeLocalizaciones(venta);
        venta = this.composeMedidasSeguridad(venta);
        venta = this.composePlanes(venta);
        venta = this.composeMontosRecomendados(venta);
        venta = this.composeBeneficio(venta);
        venta = this.composeCotizaciones(venta);
        if (venta.getClausulaEstabilizacion() != null && venta.getClausulaEstabilizacion())
            venta = this.composePorcentajeEstabilizacion(venta);

        return venta;
    }

    public Venta composePorcentajeEstabilizacion(Venta venta) {
        Plan plan = planService.getPlan( venta.getCotizaciones().get(0).getIdPlan());
        try {
            venta.setPorcentajeEstabilizacion(coreClient.getPorcentajesEstabilizacion(plan, venta.getModalidadComision(), venta.getBeneficio()).get(0).getCodigo());
        } catch (Exception e){
            logger.error("No se pudo cargar el porcentaje de la clausula de estabilizacion", e);
        }
        return venta;
    }

    private Venta composeBeneficio(Venta venta){
        if (!venta.getEstadoVenta().equals(EstadoVenta.EMITIDA) &&
                !venta.getEstadoVenta().equals(EstadoVenta.EMITIENDO) &&
                !venta.getEstadoVenta().equals(EstadoVenta.RECHAZADA)){
            if ( venta.getBeneficio() != null &&
                    venta.getCliente() != null &&
                    venta.getCliente().getDocumento() != null &&
                    venta.getCliente().getDocumento().getNumero() != null){
                venta.setValidacionBeneficio(negocioService.validarBeneficios(venta.getBeneficio(), venta.getCliente().getDocumento().getNumero()));
            }
        }

        return venta;
    }

    private Venta composeCotizaciones(Venta venta){
        for (Cotizacion cotizacion : venta.getCotizaciones()) {
            cotizacion.setNombrePlan(this.planService.getPlan(cotizacion.getIdPlan()).getNombrePlan());
        }
        return venta;
    }

    private Venta composeMontosRecomendados(Venta venta) {
        for (Cotizacion cotizacion : venta.getCotizaciones()){
            Plan plan = venta.getPlanes().stream().filter(p -> p.getIdPlan().equals(cotizacion.getIdPlan())).findAny().get();
            if (plan.getTipoPlan().equals(TipoPlan.CONFIGURABLE)){
                try{
                    PlanItem planItemIncendioEdificio = plan.getItems()
                            .stream()
                            .filter(planItem -> planItem.getTipoCobertura().equals(TipoCobertura.INCENDIO_EDIFICIO) ||
                                    planItem.getTipoCobertura().equals(TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO) ||
                                    planItem.getTipoCobertura().equals(TipoCobertura.INCENDIO_EDIFICIO_PRORRATA))
                            .findFirst()
                            .orElseThrow(() -> new PlanConfigurationException(("El plan configurable no tiene cobertura de incendio")));

                    Cobertura coberturaIncendioEdificio = cotizacion.getCoberturas()
                            .stream()
                            .filter(cobertura -> cobertura.getTipoCobertura().equals(TipoCobertura.INCENDIO_EDIFICIO) ||
                                    cobertura.getTipoCobertura().equals(TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO) ||
                                    cobertura.getTipoCobertura().equals(TipoCobertura.INCENDIO_EDIFICIO_PRORRATA))
                            .findFirst()
                            .orElseThrow(() -> new PlanConfigurationException(("El plan configurable no tiene cobertura de incendio")));

                    this.actualizarMontosRecomendadosCoberturaIncendio(coberturaIncendioEdificio, planItemIncendioEdificio, venta.getVivienda().getDireccion().getLocalizacion().getId(), venta.getVivienda().getTipoVivienda(), venta.getVivienda().getMetrosCuadrados());

                }catch (Exception ex){
                    logger.error(ex.getMessage());
                }
            }
        }
        return venta;
    }

    private Venta composePlanes(Venta venta) {
        if (venta != null && venta.getPlanes()!=null)
            venta.setPlanes(venta.getPlanes()
                    .stream()
                    .map(plan -> planService.getPlan(plan.getIdPlan()))
                    .collect(Collectors.toList()));
        return venta;
    }

    private Venta composeVendedor(Venta venta) {
        //Obtengo el vendedor
        if (venta != null && venta.getVendedor() != null && venta.getVendedor().getId() != null)
            venta.setVendedor(vendedoresClient.getVendedor(venta.getVendedor().getId()));

        return venta;
    }

    private Venta composeLocalizaciones(Venta venta){
        //Obtengo la localizacion de la vivienda
        if (venta != null
                && venta.getVivienda() != null
                && venta.getVivienda().getDireccion() != null
                && venta.getVivienda().getDireccion().getLocalizacion() != null
                && venta.getVivienda().getDireccion().getLocalizacion().getId() != null)
            venta.getVivienda().getDireccion().setLocalizacion(negocioService.getLocalizacion(venta.getVivienda().getDireccion().getLocalizacion().getId()));

        //Obtengo la localizacion del cliente
        if (venta != null
                && venta.getCliente() != null
                && venta.getCliente().getDireccion() != null
                && venta.getCliente().getDireccion().getLocalizacion() != null
                && venta.getCliente().getDireccion().getLocalizacion().getId() != null){
            if (venta != null
                    && venta.getVivienda() != null
                    && venta.getVivienda().getDireccion() != null
                    && venta.getVivienda().getDireccion().getLocalizacion() != null
                    && venta.getVivienda().getDireccion().getLocalizacion().getId() != null
                    && venta.getVivienda().getDireccion().getLocalizacion().getId().equals(venta.getCliente().getDireccion().getLocalizacion().getId())) {
                venta.getCliente().getDireccion().setLocalizacion(venta.getVivienda().getDireccion().getLocalizacion());
            }else{
                venta.getCliente().getDireccion().setLocalizacion(negocioService.getLocalizacion(venta.getCliente().getDireccion().getLocalizacion().getId()));
            }
        }

        return venta;
    }

    private Venta composeMedidasSeguridad(Venta venta){
        //Seteo el plan para las medidas de seguridad
        if (venta != null
                && venta.getVivienda() != null)
            venta.getVivienda().setTipoPlan(venta.getTipoPlan());
        return venta;
    }

    private void inicializarConfigurable(Plan plan, List<Cobertura> coberturas, String idLocalizacion, TipoVivienda tipoVivienda, Integer metrosCuadrados) {
        try {
            PlanItem planItemIncendioEdificio = plan.getItems()
                    .stream()
                    .filter(planItem -> planItem.getTipoCobertura().equals(TipoCobertura.INCENDIO_EDIFICIO) ||
                            planItem.getTipoCobertura().equals(TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO) ||
                            planItem.getTipoCobertura().equals(TipoCobertura.INCENDIO_EDIFICIO_PRORRATA))
                    .findFirst()
                    .orElseThrow(() -> new PlanConfigurationException(("El plan configurable no tiene cobertura de incendio")));

            Cobertura coberturaPrincipal = coberturas
                    .stream()
                    .filter(cobertura -> cobertura.getTipoCobertura().equals(TipoCobertura.INCENDIO_EDIFICIO) ||
                            cobertura.getTipoCobertura().equals(TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO) ||
                            cobertura.getTipoCobertura().equals(TipoCobertura.INCENDIO_EDIFICIO_PRORRATA))
                    .findFirst()
                    .orElseThrow(() -> new PlanConfigurationException(("El plan configurable no tiene cobertura de incendio")));

            this.actualizarMontosRecomendadosCoberturaIncendio(coberturaPrincipal, planItemIncendioEdificio, idLocalizacion, tipoVivienda, metrosCuadrados);
            coberturaPrincipal.setMontoAsegurado(coberturaPrincipal.getMontoMinimoRecomendado());
            plan.getItems()
                    .stream()
                    .filter(planItem -> !planItem.getTipoCobertura().esAdicional() && planItemIncendioEdificio.getTipoCobertura().equals(planItem.getTipoCoberturaRelacionada()))
                    .forEach(planItem -> this.actualizarCoberturasRelacionadas(plan, planItem, coberturas, coberturaPrincipal.getMontoAsegurado()));

        }catch (Exception ex){
            logger.error(ex.getMessage());
        }
    }

    private void actualizarMontosRecomendadosCoberturaIncendio(Cobertura cobertura, PlanItem planItem, String idLocalizacion, TipoVivienda tipoVivienda, Integer metrosCuadrados){
        if (idLocalizacion == null || tipoVivienda == null || metrosCuadrados == null){
            cobertura.setMontoMinimoRecomendado(cobertura.getMontoAsegurado().multiply(new BigDecimal(1.1)).divide(new BigDecimal(1), 0, RoundingMode.HALF_UP));
            cobertura.setMontoMaximoRecomendado(cobertura.getMontoAsegurado().multiply(new BigDecimal(0.9)).divide(new BigDecimal(1), 0, RoundingMode.HALF_UP));
        }else {
            ValoresVivienda valoresVivienda = valoresViviendaClient.getValores(idLocalizacion, tipoVivienda, metrosCuadrados);

            if (valoresVivienda.getValorDesde().compareTo(planItem.getMontoMinimoFijo()) > 0) {
                if (valoresVivienda.getValorDesde().compareTo(planItem.getMontoMaximoFijo()) < 0) {
                    cobertura.setMontoMinimoRecomendado(valoresVivienda.getValorDesde());
                } else {
                    cobertura.setMontoMinimoRecomendado(planItem.getMontoMaximoFijo());
                }
            } else {
                cobertura.setMontoMinimoRecomendado(planItem.getMontoMinimoFijo());
            }
            if (valoresVivienda.getValorHasta().compareTo(planItem.getMontoMaximoFijo()) < 0) {
                if (valoresVivienda.getValorHasta().compareTo(planItem.getMontoMinimoFijo()) > 0) {
                    cobertura.setMontoMaximoRecomendado(valoresVivienda.getValorHasta());
                } else {
                    cobertura.setMontoMaximoRecomendado(planItem.getMontoMinimoFijo());
                }
            } else {
                cobertura.setMontoMaximoRecomendado(planItem.getMontoMaximoFijo());
            }
        }
    }

    private void actualizarCoberturasRelacionadas(Plan plan, PlanItem planItemActualizar, List<Cobertura> coberturas, BigDecimal nuevoMontoCoberturaRelacionada){

        Optional<Cobertura> optionalCoberturaActualizar = coberturas
                .stream()
                .filter(cobertura -> cobertura.getTipoCobertura().equals(planItemActualizar.getTipoCobertura()))
                .findFirst();

        if (optionalCoberturaActualizar.isPresent()) {
            BigDecimal minimoPorRelacion = planItemActualizar.getPorcentajeMinimoSobreRelacion().multiply(nuevoMontoCoberturaRelacionada).divide(new BigDecimal(100), 0, RoundingMode.HALF_UP);
            BigDecimal maximoPorRelacion = planItemActualizar.getPorcentajeMaximoSobreRelacion().multiply(nuevoMontoCoberturaRelacionada).divide(new BigDecimal(100), 0, RoundingMode.HALF_UP);
            Cobertura cobertura = optionalCoberturaActualizar.get();
            Boolean actualizarRelaciados = FALSE;
            BigDecimal minimo = minimoPorRelacion.compareTo(planItemActualizar.getMontoMinimoFijo()) < 0 ?
                    planItemActualizar.getMontoMinimoFijo() :
                    minimoPorRelacion.compareTo(planItemActualizar.getMontoMaximoFijo()) > 0 ?
                            planItemActualizar.getMontoMaximoFijo() :
                            minimoPorRelacion;
            BigDecimal maximno = maximoPorRelacion.compareTo(planItemActualizar.getMontoMaximoFijo()) > 0 ?
                    planItemActualizar.getMontoMaximoFijo() :
                    maximoPorRelacion.compareTo(planItemActualizar.getMontoMinimoFijo()) < 0 ?
                            planItemActualizar.getMontoMinimoFijo() :
                            maximoPorRelacion;

            if (planItemActualizar.getMontoDefault().compareTo(minimo) < 0) {
                cobertura.setMontoAsegurado(minimo);
                actualizarRelaciados = TRUE;
            } else if (planItemActualizar.getMontoDefault().compareTo(maximno) > 0) {
                cobertura.setMontoAsegurado(maximno);
                actualizarRelaciados = TRUE;
            }
            if (actualizarRelaciados)
                plan.getItems()
                        .stream()
                        .filter(planItem -> !planItem.getTipoCobertura().esAdicional() && planItemActualizar.getTipoCobertura().equals(planItem.getTipoCoberturaRelacionada()))
                        .forEach(planItem -> this.actualizarCoberturasRelacionadas(plan, planItem, coberturas, cobertura.getMontoAsegurado()));
        }
    }

    private Venta sortCotizaciones (Venta venta){
        for (Cotizacion cotizacion : venta.getCotizaciones()){
            List<PlanItem> planItems = this.planService.getPlan(cotizacion.getIdPlan()).getItems();
            List<TipoCobertura> orderList = planItems.stream().sorted(Comparator.comparing(PlanItem::getOrden)).map(PlanItem::getTipoCobertura).collect(Collectors.toList());
            Collections.sort(cotizacion.getCoberturas(), Comparator.comparing(cobertura -> orderList.indexOf(cobertura.getTipoCobertura())));
        }
        return venta;
    }

    public Venta crearVenta(CanalVenta canalVenta, String idVendedor, TipoPlan tipoPlan, Cliente cliente, Vivienda vivienda, LocalDate fechaInicioCobertura, TipoMedioPago tipoMedioPago, CantidadCuotas cantidadCuotas, ModalidadComision modalidadComision, Boolean clausulaEstabilizacion, Beneficio beneficio) {
//        if ( environment.equals("Produccion") && !tipoPlan.enlatado())
//            throw new CotizadorDeshabilatodException();

        //Obtengo el vendedor
        Vendedor vendedor = vendedoresClient.getVendedor(idVendedor);

        //Obtengo la localizacion
        if (tipoPlan.equals(TipoPlan.ENLATADO_ALRIO)) {
            //Los Enlatados AlRio tienen la localizacion y tipo de vivienda fijos
            vivienda.getDireccion().setLocalizacion(localizacionAlRio);
            vivienda.setTipoVivienda(tipoViviendaAlRio);
        }else
            vivienda.getDireccion().setLocalizacion(negocioService.getLocalizacion(vivienda.getDireccion().getLocalizacion().getId()));

        //La venta se crea con los planes que tiene permisos el vendedor
        List<Plan> planes = planService.getPlanes(vendedor.getTipoProductos(), tipoPlan);

        //Si el vendedor no tiene planes configurados para ese tipo de Plan devuelve error
        if (planes.isEmpty())
            throw new VendedorNoPlanesException(tipoPlan);

        //Verifico si el vendedor tiene habilitado el beneficio
        if (beneficio != null &&
            !Stream.of(Beneficio.values())
                    .filter(b -> b.tiposProductos().stream().filter(vendedor.getTipoProductos()::contains).findAny().isPresent() )
                    .findAny().isPresent() )
            throw new VendedorBeneficioInvalidoException(vendedor.getNombre(), beneficio);

        //Verifico si el beneficio corresponde al tipo de plan
        if (beneficio != null && !beneficio.tiposPlanes().contains(tipoPlan))
            throw new BeneficioInvalidoPlanException(beneficio, tipoPlan);

        //Si el tipo de pago es null por default se usa el automatico
        tipoMedioPago = tipoMedioPago != null ? tipoMedioPago : tipoPlan.tiposMedioPago().get(0);

        //Si la cantidad de cuotas es null el default
        cantidadCuotas = cantidadCuotas != null ? cantidadCuotas : tipoPlan.tipoCantidadCuotas().get(0);

        modalidadComision = modalidadComision != null ? modalidadComision : ModalidadComision.getDefault();

        clausulaEstabilizacion = tipoPlan.getClausulaEstabilizacionFija() ? tipoPlan.getClausulaEstabilizacionFija() : clausulaEstabilizacion;

        List<Cotizacion> cotizaciones = new ArrayList<>();

        for (Plan plan : planes) {
            List<Cobertura> coberturasDefault = plan.getItems()
                    .stream()
                    .filter(item -> item.getObligatorio())
                    .map(item -> new Cobertura(item.getTipoCobertura(),
                                                        item.getMontoDefault(),
                                                        item.getOpciones() != null && !item.getOpciones().isEmpty()?
                                                                TipoCobertura.valueOf(item.getOpciones().get(0).getValue())
                                                                : null))
                    .collect(Collectors.toList());

            if (plan.getTipoPlan().equals(TipoPlan.CONFIGURABLE))
                this.inicializarConfigurable(plan, coberturasDefault, vivienda.getDireccion().getLocalizacion().getId(), vivienda.getTipoVivienda(), vivienda.getMetrosCuadrados());
            try {
                cotizaciones.add(this.crearCotizacion(null, idVendedor, plan.getIdPlan(), coberturasDefault, vivienda, cliente, fechaInicioCobertura, tipoMedioPago, cantidadCuotas, modalidadComision, clausulaEstabilizacion, beneficio));
            }catch ( BadRequestException e){
                if (e.getMessages() != null)
                    throw new PlanConfigurationException(e.getMessages().stream().collect(Collectors    .joining(",")));
                else
                    throw new PlanConfigurationException(e.getMessage());            }
        }

        Venta venta = new Venta(canalVenta, LocalDateTime.now(),LocalDateTime.now(),vendedor,cliente,tipoPlan,planes,vivienda, cotizaciones, fechaInicioCobertura, tipoMedioPago, cantidadCuotas, modalidadComision, clausulaEstabilizacion, beneficio);
        venta = this.ventasDAO.save(venta);
        logger.info("Venta creada. ID: " + venta.getId());
        return venta;
    }

    public Venta getVenta(String idVenta) {
        Venta venta = ventasDAO.get(idVenta);
        if (venta == null)
            throw new VentaNotFoundException(idVenta);

        return this.composeVenta(venta);
    }

    public Cotizacion getCotizacion(String idVenta, String idCotizacion) {
        Cotizacion cotizacion = ventasDAO.get(idVenta)
                .getCotizaciones()
                .stream()
                .filter(c -> c.getId().equals(idCotizacion))
                .findAny()
                .orElseThrow(CotizacionNotFoundException::new);
        cotizacion.setNombrePlan(this.planService.getPlan(cotizacion.getIdPlan()).getNombrePlan());
        return cotizacion;
    }

    public List<Venta> searchVentas(String idVendedor, LocalDate fechaDesde, Optional<LocalDate> fechaHasta, Optional<String> numero, Optional<String> nombre, Optional<String> email, Optional<EstadoVenta> estadoVenta) {
        List<Venta> lista = ventasDAO.search(idVendedor,
                                fechaDesde,
                                fechaHasta.isPresent() ? fechaHasta.get() : LocalDate.now(),
                                numero,
                                nombre,
                                email,
                                estadoVenta);
        Collections.sort(lista, Collections.reverseOrder());
        for (Venta venta : lista)
            this.composeCotizaciones(venta);
        return lista;
    }

    public void putCotizacion(String idVenta, String idCotizacion, List<Cobertura> items) {
        // Busco la venta
        Venta venta = this.getVenta(idVenta);

        //Busco la cotizacion actual
        Cotizacion cotizacionActual = venta.getCotizaciones()
                .stream()
                .filter(c -> c.getId().equals(idCotizacion))
                .findAny()
                .orElseThrow(CotizacionNotFoundException::new);

        if (venta.getTipoPlan().enlatado())
            throw new RecotizandoEnlatadoException();

//        this.validarCoberturasPlanConfigurable(cotizacionActual.getIdPlan(), items);

        //Recotizo con los nuevos items
        Cotizacion cotizacionNueva = this.crearCotizacion(idCotizacion, venta.getVendedor().getId(), cotizacionActual.getIdPlan(),items, venta.getVivienda(), venta.getCliente(), venta.getFechaInicioCobertura(), venta.getTipoMedioPago(), venta.getCantidadCuotas(), venta.getModalidadComision(), venta.getClausulaEstabilizacion(), venta.getBeneficio());

        //Reemplazo la cotizacion
        Collections.replaceAll(venta.getCotizaciones(), cotizacionActual,cotizacionNueva);

        //Guardo la venta
        ventasDAO.save(venta);
    }

    public void patchEstadoCotizacion(String idVenta, String idCotizacion, EstadoCotizacion estadoCotizacion) {
        // No se puede modificar el estado de cotizacion a VENCIDA
        if (estadoCotizacion.equals(EstadoCotizacion.VENCIDA))
            throw new ModificacionVentaRechazadaException(ModificacionVentaRechazadaException.MotivoRechazo.COTIZACION_VENCIDA_NO_VALIDA);

        // Busco la venta
        Venta venta = ventasDAO.get(idVenta);

        // Si quiero marcar una cotizacion a emitir tengo que desmarcar el resto
        if (estadoCotizacion.equals(EstadoCotizacion.A_EMITIR))
            venta.getCotizaciones()
                    .stream()
                    .filter(cotizacion -> cotizacion.getEstado().equals(EstadoCotizacion.A_EMITIR))
                    .forEach(cotizacion -> cotizacion.setEstado(EstadoCotizacion.COTIZADA));

        // Marco la cotizacion deseada con el estado ingresado
        venta.getCotizaciones()
                .stream()
                .filter(cotizacion -> cotizacion.getId().equals(idCotizacion))
                .findAny()
                .orElseThrow(CotizacionNotFoundException::new)
                .setEstado(estadoCotizacion);

        ventasDAO.save(venta);

    }

    public void putFechaInicioCobertura(String idVenta, LocalDate fechaInicioCobertura) {
        // Busco la venta
        Venta venta = ventasDAO.get(idVenta);

        // No se puede modificar la venta si esta emitida
        if (venta.getEstadoVenta().equals(EstadoVenta.EMITIDA))
            throw new ModificacionVentaRechazadaException(ModificacionVentaRechazadaException.MotivoRechazo.VENTA_EMITIDA);

        if (!venta.getFechaInicioCobertura().equals(fechaInicioCobertura)) {
            venta.setFechaInicioCobertura(fechaInicioCobertura);
            ventasDAO.save(venta);
        }

    }

    public void putTipoMedioPago(String idVenta, TipoMedioPago tipoMedioPago) {
        Venta venta = ventasDAO.get(idVenta);

        if (tipoMedioPago != null && (venta.getTipoMedioPago() == null || !venta.getTipoMedioPago().equals(tipoMedioPago)) ){
            if (venta.getEstadoVenta().equals(EstadoVenta.EMITIDA))
                throw new ModificacionVentaRechazadaException(ModificacionVentaRechazadaException.MotivoRechazo.VENTA_EMITIDA);
            venta.setTipoMedioPago(tipoMedioPago);
            this.recotizarVenta(venta);
            this.ventasDAO.save(venta);
        }
    }

    public void putCantidadCuotas(String idVenta, CantidadCuotas cantidadCuotas) {
        Venta venta = ventasDAO.get(idVenta);

        if (cantidadCuotas != null && (venta.getCantidadCuotas() == null || !venta.getCantidadCuotas().equals(cantidadCuotas)) ){
            if (venta.getEstadoVenta().equals(EstadoVenta.EMITIDA))
                throw new ModificacionVentaRechazadaException(ModificacionVentaRechazadaException.MotivoRechazo.VENTA_EMITIDA);
            venta.setCantidadCuotas(cantidadCuotas);
            this.recotizarVenta(venta);
            this.ventasDAO.save(venta);
        }
    }

    public void putBeneficio(String idVenta, Beneficio beneficio) {
        // Busco la venta
        Venta venta = ventasDAO.get(idVenta);

        // No se puede modificar la venta si esta emitida
        if (venta.getEstadoVenta().equals(EstadoVenta.EMITIDA))
            throw new ModificacionVentaRechazadaException(ModificacionVentaRechazadaException.MotivoRechazo.VENTA_EMITIDA);

        if (beneficio != null && (venta.getBeneficio() == null || !venta.getBeneficio().equals(beneficio))) {
            //Obtengo el vendedor
            Vendedor vendedor = vendedoresClient.getVendedor(venta.getVendedor().getId());

            //Verifico si el vendedor tiene habilitado el beneficio
            if (beneficio != null &&
                    !Stream.of(Beneficio.values())
                            .filter(b -> b.tiposProductos().stream().filter(vendedor.getTipoProductos()::contains).findAny().isPresent())
                            .findAny().isPresent())
                throw new VendedorBeneficioInvalidoException(vendedor.getNombre(), beneficio);

            //Verifico si el beneficio corresponde al tipo de plan
            if (beneficio != null && !beneficio.tiposPlanes().contains(venta.getTipoPlan()))
                throw new BeneficioInvalidoPlanException(beneficio, venta.getTipoPlan());
        }

        if (venta.getBeneficio() == null || !venta.getBeneficio().equals(beneficio)) {
            venta.setBeneficio(beneficio);
            venta.getCotizaciones().stream().forEach(cotizacion -> cotizacion.setDescuento(beneficio.descuento()));
            ventasDAO.save(venta);
        }
    }

    public void deleteBeneficio(String idVenta) {
        // Busco la venta
        Venta venta = ventasDAO.get(idVenta);

        // No se puede modificar la venta si esta emitida
        if (venta.getEstadoVenta().equals(EstadoVenta.EMITIDA))
            throw new ModificacionVentaRechazadaException(ModificacionVentaRechazadaException.MotivoRechazo.VENTA_EMITIDA);

        if (venta.getBeneficio() != null) {
            venta.setBeneficio(null);
            venta.getCotizaciones().stream().forEach(cotizacion -> cotizacion.setDescuento(null));
            ventasDAO.save(venta);
        }
    }

    public void putModalidadComision(String idVenta, ModalidadComision modalidadComision) {
        Venta venta = ventasDAO.get(idVenta);

        if (modalidadComision != null && (venta.getModalidadComision() == null || !venta.getModalidadComision().equals(modalidadComision))){
            if (venta.getEstadoVenta().equals(EstadoVenta.EMITIDA))
                throw new ModificacionVentaRechazadaException(ModificacionVentaRechazadaException.MotivoRechazo.VENTA_EMITIDA);
            venta.setModalidadComision(modalidadComision);
            this.recotizarVenta(venta);
            ventasDAO.save(venta);
        }

    }

    public void putClausulaEstabilizacion(String idVenta, Boolean clausulaEstabilizacion) {
        Venta venta = ventasDAO.get(idVenta);

        if (clausulaEstabilizacion != null && (venta.getModalidadComision() == null || !venta.getModalidadComision().equals(clausulaEstabilizacion))){
            if (venta.getEstadoVenta().equals(EstadoVenta.EMITIDA))
                throw new ModificacionVentaRechazadaException(ModificacionVentaRechazadaException.MotivoRechazo.VENTA_EMITIDA);
            venta.setClausulaEstabilizacion(clausulaEstabilizacion);
            this.recotizarVenta(venta);
            ventasDAO.save(venta);
        }
    }

    public String getPorcentajeEstabilizacion(String idVenta) {
        Venta venta = ventasDAO.get(idVenta);
        Plan plan = planService.getPlan( venta.getCotizaciones().get(0).getIdPlan());
        try {
            return coreClient.getPorcentajesEstabilizacion(plan, venta.getModalidadComision(), venta.getBeneficio()).get(0).getCodigo();
        } catch (Exception e){
            logger.error("No se pudo cargar el porcentaje de la clausula de estabilizacion", e);
        }
        return "-";
    }

    public void putVendedor(String idVenta, Vendedor vendedor) {
        Venta venta = ventasDAO.get(idVenta);
        if (vendedor != null && !venta.getVendedor().getId().equals(vendedor.getId()))
            venta.setVendedor(vendedor);

        ventasDAO.save(venta);
    }


    public void putCliente(String idVenta, Cliente cliente) {
        Venta venta = ventasDAO.get(idVenta);

        if (cliente != null && (venta.getCliente() == null || !venta.getCliente().equals(cliente)) ) {
            this.putCliente(venta, cliente);
            ventasDAO.save(venta);
        }
    }

    public void putCliente(Venta venta, Cliente cliente) {
        // No se puede modificar la venta si esta emitida
        if (venta.getEstadoVenta().equals(EstadoVenta.EMITIDA))
            throw new ModificacionVentaRechazadaException(ModificacionVentaRechazadaException.MotivoRechazo.VENTA_EMITIDA);

        venta.setCliente(cliente);
    }

    public void putVivienda(String idVenta, Vivienda vivienda) {
        Venta venta = ventasDAO.get(idVenta);

        if (vivienda != null && (venta.getVivienda() == null || !venta.getVivienda().equals(vivienda)) ){
            this.putVivienda(venta, vivienda);
            ventasDAO.save(venta);
        }

    }

    public void putVivienda(Venta venta, Vivienda vivienda) {
        // No se puede modificar la venta si esta emitida
        if (venta.getEstadoVenta().equals(EstadoVenta.EMITIDA))
            throw new ModificacionVentaRechazadaException(ModificacionVentaRechazadaException.MotivoRechazo.VENTA_EMITIDA);

        // Si esta en cotizada y se modifica el tipo de vivienda o la ubicacion o los metros cuadrados se modifican los precios
        if (!venta.getVivienda().getTipoVivienda().equals(vivienda.getTipoVivienda())
                || !venta.getVivienda().getDireccion().getLocalizacion().getId().equals(vivienda.getDireccion().getLocalizacion().getId())
                || (venta.getVivienda().getMetrosCuadrados() != null &&
                !venta.getVivienda().getMetrosCuadrados().equals(vivienda.getMetrosCuadrados()))) {
            if (venta.getEstadoVenta().equals(EstadoVenta.COTIZADA) || venta.getEstadoVenta().equals(EstadoVenta.BORRADOR) || venta.getEstadoVenta().equals(EstadoVenta.VENCIDA)) {
                venta.setVivienda(vivienda);
                venta = this.recotizarVenta(venta);
            } else if (!venta.getVivienda().getTipoVivienda().equals(vivienda.getTipoVivienda())
                    || !venta.getVivienda().getDireccion().getLocalizacion().getId().equals(vivienda.getDireccion().getLocalizacion().getId())) {
                throw new ModificacionVentaRechazadaException(ModificacionVentaRechazadaException.MotivoRechazo.VENTA_A_EMITIR_PRECIO);
            }
        }
        venta.setVivienda(vivienda);
    }

    public void putPago(String idVenta, Pago pago) {
        Venta venta = ventasDAO.get(idVenta);

        if (pago != null && (venta.getPago() == null || !venta.getPago().equals(pago)) ){
            this.putPago(venta, pago);
            this.ventasDAO.save(venta);
        }
    }

    public void putPago(Venta venta, Pago pago) {
        // No se puede modificar la venta si esta emitida
        if (venta.getEstadoVenta().equals(EstadoVenta.EMITIDA))
            throw new ModificacionVentaRechazadaException(ModificacionVentaRechazadaException.MotivoRechazo.VENTA_EMITIDA);

        if (venta.getTipoMedioPago() != null && !pago.getMedioPago().tipoMedioPago().equals(venta.getTipoMedioPago()))
            throw new MedioPagoInvalidoException(pago.getMedioPago(), venta.getTipoMedioPago());

        if (pago.getMedioPago().equals(MedioPago.CREDITO)) {
            PagoCredito pagoCredito = (PagoCredito) pago;
            LocalDate fechaVencimiento = LocalDate.of(pagoCredito.getAnioVencimiento(), pagoCredito.getMesVencimiento(), 1);
            fechaVencimiento = fechaVencimiento.plusMonths(1);
            fechaVencimiento = fechaVencimiento.minusDays(1);
            if (fechaVencimiento.isBefore(LocalDate.now()))
                throw new FechaVencimientoVencidaException();
        }else if (pago.getMedioPago().equals(MedioPago.DEBITO)){
            try{
                PagoDebito pagoDebito = (PagoDebito) pago;
                pagoDebito.setBanco(negocioService.getBanco(pagoDebito.getCbu()));
            }catch (Exception e){
                throw new CBUBancoInvalidoException();
            }
        }

        venta.setPago(pago);
    }



    public void putBienesEspecificos(String idVenta, List<Especifico> especificos) {
        Venta venta = ventasDAO.get(idVenta);

        if (especificos != null && ( venta.getEspecificos() == null || !venta.getEspecificos().equals(especificos)) ){
            this.putBienesEspecificos(venta, especificos);
            ventasDAO.save(venta);
        }
    }

    public void putBienesEspecificos(Venta venta, List<Especifico> especificos) {
        venta.setEspecificos(especificos);
    }

    public Emision postEmision(String idVenta) {
        Venta venta = this.getVenta(idVenta);
        if(venta.getNroPoliza() != null)
            throw new VentaEmitidaException();
        if(venta.getEmitiendo() != null && venta.getEmitiendo())
            throw new VentaEmitiendoException();

        return this.postEmision(venta);
    }

    public Emision postEmision(String idVenta, Cliente cliente, Vivienda vivienda, Pago pago, List<Especifico> especificos, String numeroReferencia) {
        Venta venta = this.getVenta(idVenta);
        if(venta.getNroPoliza() != null)
            throw new VentaEmitidaException();
        if(venta.getEmitiendo() != null && venta.getEmitiendo())
            throw new VentaEmitiendoException();

        if (cliente != null && (venta.getCliente() == null || !venta.getCliente().equals(cliente)) ) {
            this.putCliente(venta, cliente);
        }
        if (vivienda != null && (venta.getVivienda() == null || !venta.getVivienda().equals(vivienda)) ){
            this.putVivienda(venta, vivienda);
        }
        if (pago != null && (venta.getPago() == null || !venta.getPago().equals(pago)) ){
            this.putPago(venta, pago);
        }
        if (especificos != null && ( venta.getEspecificos() == null || !venta.getEspecificos().equals(especificos)) ){
            this.putBienesEspecificos(venta, especificos);
        }
        if (numeroReferencia != null && (venta.getNumeroReferencia() == null || !venta.getNumeroReferencia().equals(numeroReferencia)) ){
            venta.setNumeroReferencia(numeroReferencia);
        }

        return this.postEmision(venta);
    }

    private Emision postEmision(Venta venta) {
        venta = this.composeLocalizaciones(venta);
        Cotizacion cotizacionAEmitir = venta.getCotizaciones().size() > 1 ?
                venta.getCotizaciones()
                        .stream()
                        .filter(cotizacion -> cotizacion.getEstado().equals(EstadoCotizacion.A_EMITIR))
                        .findAny()
                        .orElseThrow(() -> new CotizacionAEmitirNoIndicadaException()) :
                venta.getCotizaciones().get(0);

        Plan plan = planService.getPlan( cotizacionAEmitir.getIdPlan());

        venta = this.ventaValidaParaEmitir(venta, plan);

        if(this.ventasDAO.getEmitiendo(venta.getId()))
            throw new VentaEmitidaException();
        this.ventasDAO.saveEmitiendo(venta.getId());

        // Emito en GLM
        try {
            EmisionGlm emisionGlm = this.coreClient.emitir(venta.getVendedor().getId(),
                    plan,
                    cotizacionAEmitir.getCoberturas(),
                    venta.getVivienda(),
                    venta.getEspecificos(),
                    venta.getCliente(),
                    venta.getPago(),
                    venta.getFechaInicioCobertura(),
                    venta.getTipoMedioPago(),
                    venta.getCantidadCuotas(),
                    venta.getModalidadComision(),
                    venta.getClausulaEstabilizacion(),
                    venta.getBeneficio(),
                    venta.getNumeroReferencia()
            );

            if(venta.getTipoPlan().equals(TipoPlan.ENLATADO_HOGAR_DGO)){
                try{
                    dtvHubClient.asignarPolizaCodigoDTVGO(venta.getNumeroReferencia(), emisionGlm.getNumeroPoliza());
                }catch (Exception exception){
                    logger.error("No se pudo asignar la poliza al codigo de DGO: ");
                }
            }

            this.ventasDAO.saveVentaEmitida(venta.getId(), emisionGlm.getNumeroPoliza());
            this.ventasDAO.saveCotizacionEmitida(cotizacionAEmitir.getId(), EstadoCotizacion.EMITIDA);


        }catch (ClienteRechazadoFraudeException e){
            venta.getEmision().setMotivoError(ClienteFraudeException.getMensaje(venta.getCanalVenta()));
            venta.getEmision().setRechazado(TRUE);
            venta.setEmitiendo(FALSE);
            this.ventasDAO.save(venta);
            throw new ClienteFraudeException(venta.getCanalVenta());
        }catch (ClienteRechazadoDuplicadoException e){
            venta.getEmision().setMotivoError(ClienteDuplicadoException.getMensaje(venta.getCanalVenta()));
            venta.getEmision().setRechazado(TRUE);
            venta.setEmitiendo(FALSE);
            this.ventasDAO.save(venta);
            throw new ClienteDuplicadoException(venta.getCanalVenta());
        }catch (CoreBusinessException e){
            venta.getEmision().setMotivoError(String.join(".", e.getMessages()));
            venta.setEmitiendo(FALSE);
            this.ventasDAO.save(venta);
            throw new BadRequestException(e.getMessages());
        }catch (CoreFailException e) {
            venta.getEmision().setMotivoError("Hubo un error en el sistema. Reintente mas tarde");
            venta.setEmitiendo(FALSE);
            this.ventasDAO.save(venta);
            logger.error(e.getMessage());
            throw new GLMBadGatewayException(e);
        }catch (Exception e) {
            venta.getEmision().setMotivoError("Hubo un error en el sistema. Reintente mas tarde");
            venta.setEmitiendo(FALSE);
            this.ventasDAO.save(venta);
            logger.error(e.getMessage());
            throw new GLMBadGatewayException(e);
        }

        return venta.getEmision();
    }


    public Map<String, Object> getImpresionCotizacion(String idVenta, List<String> ids) {
        Venta venta = this.getVenta(idVenta);

        venta = this.sortCotizaciones(venta);
        byte[] bytes =  impresoraCotizacionClient.getImpresion(venta, ids);

        Map <String,Object> map = new HashMap<>();
        map.put("filename","cotizacion-" + venta.getId() + ".pdf");
        map.put("bytes", bytes);
        return map;
    }

    private Venta ventaValidaParaEmitir(Venta venta, Plan plan) {
        if (venta.getEmision() == null) {
            venta.setEmision(new Emision(null,
                    0,
                    LocalDateTime.now(),
                    FALSE));
        }else{
            venta.getEmision().setReintentos(venta.getEmision().getReintentos().intValue()+1);
            venta.getEmision().setTimestamp(LocalDateTime.now());
        }

        if (venta.getVivienda().getDireccionTomador() && !venta.getVivienda().getDireccion().getLocalizacion().equals(venta.getCliente().getDireccion().getLocalizacion())){
            venta.getEmision().setMotivoError(String.join(".", ClienteAutosInvalidoException.MENSAJE));
            ventasDAO.save(venta);
            throw new DireccionTomadorNoCotizadaException();
        }

        if (!venta.getVivienda().getDireccionTomador() && ( venta.getVivienda().getDireccion().getCalle() == null ||
                                                            venta.getVivienda().getDireccion().getCalle().isEmpty() ||
                                                            venta.getVivienda().getDireccion().getNumero() == null ||
                                                            venta.getVivienda().getDireccion().getNumero().isEmpty() )){
            venta.getEmision().setMotivoError(String.join(".", ClienteAutosInvalidoException.MENSAJE));
            ventasDAO.save(venta);
            throw new FaltaDireccionViviendaException();
        }

        if (venta.getBeneficio() != null && venta.getBeneficio().equals(Beneficio.AUTOS_HOGAR)){
            boolean esClienteAutos;
            try{
                esClienteAutos = beneficiosService.esClienteAutos(venta.getCliente().getDocumento());
            }catch (Exception exception){
                venta.getEmision().setMotivoError(String.join(".", ClienteAutosBadGatewayException.MENSAJE));
                ventasDAO.save(venta);
                throw new ClienteAutosBadGatewayException(exception);
            }
            if(!esClienteAutos){
                venta.getEmision().setMotivoError(String.join(".", ClienteAutosInvalidoException.MENSAJE));
                ventasDAO.save(venta);
                throw new ClienteAutosInvalidoException();
            }
        }

        if (venta.getBeneficio() != null && venta.getBeneficio().equals(Beneficio.EMPLEADOS_DIRECTV)){
            boolean esNominaDTV;
            try{
                esNominaDTV = beneficiosService.esNominaDirectv(venta.getCliente().getDocumento().getNumero());
            }catch (Exception exception){
                venta.getEmision().setMotivoError(String.join(".", NominaDirectvBadGatewayException.MENSAJE));
                ventasDAO.save(venta);
                throw new NominaDirectvBadGatewayException(exception);
            }
            if(!esNominaDTV){
                venta.getEmision().setMotivoError(String.join(".", NominaDirectvInvalidoException.MENSAJE));
                ventasDAO.save(venta);
                throw new NominaDirectvInvalidoException();
            }
        }

        if (venta.getBeneficio() != null && venta.getBeneficio().equals(Beneficio.EMPLEADOS_GW)){
            boolean esNominaGW;
            try{
                esNominaGW = beneficiosService.esNominaGrupoW(venta.getCliente().getDocumento().getNumero());
            }catch (Exception exception){
                venta.getEmision().setMotivoError(String.join(".", NominaGrupoWBadGatewayException.MENSAJE));
                ventasDAO.save(venta);
                throw new NominaGrupoWBadGatewayException(exception);
            }
            if(!esNominaGW){
                venta.getEmision().setMotivoError(String.join(".", NominaGrupoWInvalidoException.MENSAJE));
                ventasDAO.save(venta);
                throw new NominaGrupoWInvalidoException();
            }
        }

        if(venta.getTipoPlan().equals(TipoPlan.ENLATADO_HOGAR_DGO)){
            try{
                String codigoDGO = dtvHubClient.getCodigoDTVGO(plan.getIdPlan());
                venta.setNumeroReferencia(codigoDGO);
            }catch (Exception exception){
                venta.getEmision().setMotivoError(String.join(".", CodigoDGONotFoudException.MENSAJE));
                ventasDAO.save(venta);
                throw new CodigoDGONotFoudException();
            }
        }

        ventasDAO.save(venta);
        return venta;
    }


    public Map<String,Object> getImpresionPoliza(String idVenta) {
        Venta venta = ventasDAO.get(idVenta);

        if ( !venta.getEstadoVenta().equals(EstadoVenta.EMITIDA) || venta.getNroPoliza() == null)
            throw new VentaNoEmitidaException();

        byte[] bytes = impresoraContratosClient.getImpresion(venta.getNroPoliza());

        Map <String,Object> map = new HashMap<>();
        map.put("filename","poliza-" + venta.getNroPoliza() + ".pdf");
        map.put("bytes", bytes);

        return map;
    }

    /**
     * Valida que el listado de coberturas se corresponda con el Plan, que los montos esten correctos
     * y completa las coberturas fijas con los montos correspondientes
     */
    private void validarCoberturasPlanConfigurable(IdPlan idPlan, List<Cobertura> coberturas) {
        Plan plan = this.planService.getPlan(idPlan);


        for (Cobertura cobertura : coberturas) {
            //Valido que no haya coberturas repetidas
            if (coberturas.stream().filter(c -> c.getTipoCobertura().equals(cobertura.getTipoCobertura())).collect(Collectors.toList()).size() > 1)
                throw new CoberturaRepetidaException(cobertura.getTipoCobertura().label());
            //Valido que todas las coberturas esten en el plan
            if (!plan.getItems().stream().filter(planItem -> cobertura.getTipoCobertura().equals(planItem.getTipoCobertura())).findAny().isPresent())
                throw new CoberturaFueraPlanException(cobertura.getTipoCobertura().label());
        }

        //Valido que esten todas las coberturas obligatorias
        for (PlanItem planItem : plan.getItems().stream().filter(i -> i.getObligatorio()).collect(Collectors.toList())){
            List<Cobertura> faltantes = coberturas.stream().filter(cobertura -> cobertura.getTipoCobertura().equals(planItem.getTipoCobertura())).collect(Collectors.toList());
            if (faltantes != null && !faltantes.isEmpty())
                throw new FaltanCoberturasObligatoriasException(faltantes.stream().map(cobertura -> cobertura.getTipoCobertura().label()).reduce(", ", String::concat));
        }

        //Valido que esten todas las coberturas obligatorias por estar relacionadas obligatoriamente a una cobertura optativa
        for (PlanItem planItem : plan.getItems().stream().filter(i -> i.getObligatorioSiRelacionada()).collect(Collectors.toList())){
            Cobertura coberturaRelacionada = coberturas.stream().filter(cobertura -> cobertura.getTipoCobertura().equals(planItem.getTipoCoberturaRelacionada())).findAny().orElse(null);
            if (coberturaRelacionada != null){
                Cobertura coberturaObligotoriaSiRelacionada = coberturas.stream().filter(cobertura -> cobertura.getTipoCobertura().equals(planItem.getTipoCobertura())).findAny().orElse(null);
                if (coberturaObligotoriaSiRelacionada == null)
                    throw new FaltanCoberturasObligatoriasPorRelacionException(planItem.getTipoCobertura().label(), planItem.getTipoCoberturaRelacionada().label());
            }
        }

        //Valido que las coberturas con monto fijo esten con ese monto
        for (PlanItem planItem : plan.getItems().stream().filter(i -> i.getMontoFijo() && i.getTipoCoberturaRelacionada() == null).collect(Collectors.toList())){
            Cobertura coberturaMontoFijo = coberturas.stream().filter(cobertura -> cobertura.getTipoCobertura().equals(planItem.getTipoCobertura())).findAny().orElse(null);
            if (coberturaMontoFijo != null && !coberturaMontoFijo.getMontoAsegurado().equals(planItem.getMontoDefault()))
                throw new CoberturaMontoInvalidoException(planItem.getTipoCobertura().label(), planItem.getMontoDefault());
        }

        //Valido que todas las coberturas no esten fuera del rango minimo y maximo
        for (Cobertura cobertura : coberturas){
            PlanItem planItem = plan.getItems().stream().filter(p -> cobertura.getTipoCobertura().equals(p.getTipoCobertura())).findAny().get();
            if (cobertura.getMontoAsegurado().compareTo(planItem.getMontoMinimoFijo()) > 0 || cobertura.getMontoAsegurado().compareTo(planItem.getMontoMaximoFijo()) < 0)
                throw new CoberturaFueraRangoException(planItem.getTipoCobertura().label(), planItem.getMontoMinimoFijo(), planItem.getMontoMaximoFijo());
        }

        //Valido que las coberturas con monto fijo relacionado esten con ese monto
        for (PlanItem planItem : plan.getItems().stream().filter(i -> i.getMontoFijo() && i.getTipoCoberturaRelacionada() != null).collect(Collectors.toList())){
            Cobertura coberturaMontoFijoSiRelacionado = coberturas.stream().filter(cobertura -> cobertura.getTipoCobertura().equals(planItem.getTipoCobertura())).findAny().orElse(null);
            if (coberturaMontoFijoSiRelacionado != null){
                Cobertura coberturaRelacionada = coberturas.stream().filter(cobertura -> cobertura.getTipoCobertura().equals(planItem.getTipoCoberturaRelacionada())).findAny().orElse(null);
                BigDecimal montoRelacionado = coberturaRelacionada.getMontoAsegurado().multiply(planItem.getPorcentajeMinimoSobreRelacion()).setScale(2, BigDecimal.ROUND_HALF_EVEN);
                if (!coberturaMontoFijoSiRelacionado.getMontoAsegurado().setScale(2, BigDecimal.ROUND_HALF_EVEN).equals(montoRelacionado))
                    throw new CoberturaMontoInvalidoRelacionadoException(planItem.getTipoCobertura().label(),planItem.getTipoCoberturaRelacionada().label(),planItem.getPorcentajeMinimoSobreRelacion(),montoRelacionado);
            }
        }

        //Valido que las coberturas con opcion con máximo respeten el maximo
        for (Cobertura cobertura : coberturas){
            PlanItem planItem = plan.getItems().stream().filter(p -> cobertura.getTipoCobertura().equals(p.getTipoCobertura())).findAny().get();
            if (planItem.getOpciones() != null && cobertura.getOpcion() == null)
                throw new OpcionCoberturaObligatoriaException(planItem.getTipoCobertura().label());

            if (planItem.getOpciones() != null && cobertura.getOpcion() != null
                    && !planItem.getOpciones().stream().filter( o -> o.getValue().equals(cobertura.getOpcion().toString())).findAny().isPresent())
                throw new OpcionCoberturaInvalidaException(planItem.getTipoCobertura().label());

            NegocioOptionOpcionCobertura negocioOptionOpcionCobertura = (NegocioOptionOpcionCobertura) planItem.getOpciones().stream().filter( o -> o.getValue().equals(cobertura.getOpcion().toString())).findAny().orElse(null);
            if (cobertura.getMontoAsegurado().compareTo(negocioOptionOpcionCobertura.getMontoMaximoFijo()) < 0)
                throw new CoberturaFueraRangoException(planItem.getTipoCobertura().label(), planItem.getMontoMinimoFijo(), negocioOptionOpcionCobertura.getMontoMaximoFijo());
        }

        //Valido que todas las coberturas que estan relacionadas cumplan con el rango minimo y maximo relacionado
        for (PlanItem planItem : plan.getItems().stream().filter(i -> i.getTipoCoberturaRelacionada() != null).collect(Collectors.toList())){
            Cobertura cobertura = coberturas.stream().filter(c -> c.getTipoCobertura().equals(planItem.getTipoCobertura())).findAny().orElse(null);
            if (cobertura != null){
                Cobertura coberturaRelacionada = coberturas.stream().filter(c -> c.getTipoCobertura().equals(planItem.getTipoCoberturaRelacionada())).findAny().orElse(null);
                if (coberturaRelacionada != null) {
                    BigDecimal minimoRelacionado = coberturaRelacionada.getMontoAsegurado().multiply(planItem.getPorcentajeMinimoSobreRelacion()).setScale(2, BigDecimal.ROUND_HALF_EVEN);
                    BigDecimal maximoRelacionado = coberturaRelacionada.getMontoAsegurado().multiply(planItem.getPorcentajeMaximoSobreRelacion()).setScale(2, BigDecimal.ROUND_HALF_EVEN);
                    if (cobertura.getMontoAsegurado().compareTo(minimoRelacionado) > 0 || cobertura.getMontoAsegurado().compareTo(maximoRelacionado) < 0)
                        throw new CoberturaFueraRangoRelacionadoException(
                                planItem.getTipoCobertura().label(),
                                planItem.getTipoCoberturaRelacionada().label(),
                                planItem.getPorcentajeMinimoSobreRelacion(),
                                planItem.getPorcentajeMaximoSobreRelacion(),
                                minimoRelacionado,
                                maximoRelacionado);
                }
            }
        }

    }
}
