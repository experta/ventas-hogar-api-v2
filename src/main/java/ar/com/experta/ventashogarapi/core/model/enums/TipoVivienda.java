package ar.com.experta.ventashogarapi.core.model.enums;

public enum TipoVivienda {
    CASA ("Casa", "Vivienda construida de manera aislada a cualquier otra edificación, pudiendo o no compartir medianera con otra propiedad"),
    COUNTRY ("Country", "Vivienda ubicada en zonas residenciales, urbanizaciones, barrios privados o countries, cuyo perímetro está cercado y controlado por servicios privados de vigilancia."),
    EDIFICIO_PB ("Departamento (PB o 1er piso)", "Vivienda ubicada en edificios o propiedades horizontales, cuya ubicación no supere el primer piso"),
    EDIFICIO_PA ("Departamento (Desde 2do piso)", "Vivienda ubicada en edificios, propiedades horizontales, cuya ubicación supere el primer piso"),
    VIVIENDA_PERMANENTE ("Vivienda permanente", "Cualquier tipo de vivienda donde recida el cliente"),
    ;

    private String label;
    private String description;

    TipoVivienda(String label, String description) {
        this.label = label;
        this.description = description;
    }

    public String label() {
        return label;
    }

    public String description() {
        return description;
    }
}
