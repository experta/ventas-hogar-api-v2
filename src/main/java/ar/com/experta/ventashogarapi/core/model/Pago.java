package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.MedioPago;

import java.util.Objects;

public abstract class Pago {

    private MedioPago medioPago;

    public Pago(MedioPago tipo) {
        this.medioPago = tipo;
    }

    public MedioPago getMedioPago() {
        return medioPago;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pago)) return false;
        Pago pago = (Pago) o;
        return getMedioPago() == pago.getMedioPago();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMedioPago());
    }
}
