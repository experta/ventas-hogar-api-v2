package ar.com.experta.ventashogarapi.core.model.enums;

public enum TipoMedioPago {

    AUTOMATICO ("Tarjeta de Crédito / Débito Automático"),
    MANUAL ("Cuponera"),
    OP_BANCARIA_DTV("Operatoria Bancaria")
    ;

    private String label;

    TipoMedioPago(String label) {
        this.label = label;
    }

    public String label() {
        return label;
    }

}
