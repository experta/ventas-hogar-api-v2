package ar.com.experta.ventashogarapi.core.model.enums;

import java.util.Arrays;
import java.util.List;

public enum TipoProductos {

    HOGAR_EXPERTA_PRODUCTORES (Arrays.asList(CanalVenta.WEB_PAS, CanalVenta.WEB_SERVICE)),
    HOGAR_EXPERTA_ECOMMERCE (Arrays.asList(CanalVenta.WEB_PAS, CanalVenta.WEB_ECOMMERCE_WL)),
    HOGAR_ELEBAR_ALIANZA (Arrays.asList(CanalVenta.WEB_PAS, CanalVenta.WEB_ECOMMERCE_WL)),
    HOGAR_REBANKING_ALIANZA (Arrays.asList(CanalVenta.WEB_PAS, CanalVenta.WEB_ECOMMERCE_WL)),
    HOGAR_ALRIO_ALIANZA (Arrays.asList(CanalVenta.WEB_PAS)),
    HOGAR_CALLCENTER_ALIANZA (Arrays.asList(CanalVenta.WEB_PAS)),
    HOGAR_CLIENTES_DTV_VAAS_ALIANZA(Arrays.asList(CanalVenta.WEB_PAS, CanalVenta.WEB_ECOMMERCE_WL)),
    HOGAR_BENEFICIO_WERTHEIN (Arrays.asList(CanalVenta.WEB_PAS)),
    HOGAR_BENEFICIO_DIRECTV (Arrays.asList(CanalVenta.WEB_PAS)),
    HOGAR_BENEFICIO_AFINIDAD_GW (Arrays.asList(CanalVenta.WEB_PAS)),
    HOGAR_PROMOCION_GH (Arrays.asList(CanalVenta.WEB_PAS)),
    HOGAR_CLIENTES_DTV_ALIANZA(Arrays.asList(CanalVenta.HUB_DIRECTV,CanalVenta.WEB_PAS)),
    HOGAR_ASEGURALO_ALIANZA(Arrays.asList(CanalVenta.WEB_SERVICE,CanalVenta.WEB_PAS)),
    HOGAR_COMPARAENCASA_ALIANZA(Arrays.asList(CanalVenta.WEB_SERVICE,CanalVenta.WEB_PAS)),
    HOGAR_BTF_ALIANZA(Arrays.asList(CanalVenta.WEB_PAS)),
    HOGAR_DGO_ALIANZA(Arrays.asList(CanalVenta.WEB_PAS))
    ;


    private List<CanalVenta> canalesVenta;

    TipoProductos(List<CanalVenta> canalesVenta) {
        this.canalesVenta = canalesVenta;
    }

    public List<CanalVenta> canalesVenta() {
        return canalesVenta;
    }
}
