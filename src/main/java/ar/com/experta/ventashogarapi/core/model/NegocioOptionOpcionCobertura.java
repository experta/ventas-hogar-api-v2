package ar.com.experta.ventashogarapi.core.model;

import java.math.BigDecimal;

public class NegocioOptionOpcionCobertura extends NegocioOption{
    private BigDecimal montoMaximoFijo;

    public NegocioOptionOpcionCobertura() {
    }

    public NegocioOptionOpcionCobertura(String value, String label, String description, BigDecimal montoMaximoFijo) {
        super(value, label, description);
        this.montoMaximoFijo = montoMaximoFijo;
    }
    public NegocioOptionOpcionCobertura(String value, String label, String description, BigDecimal montoMaximoFijo, Boolean defecto) {
        super(value, label, description, defecto);
        this.montoMaximoFijo = montoMaximoFijo;
    }

    public BigDecimal getMontoMaximoFijo() {
        return montoMaximoFijo;
    }

    public void setMontoMaximoFijo(BigDecimal montoMaximoFijo) {
        this.montoMaximoFijo = montoMaximoFijo;
    }

}
