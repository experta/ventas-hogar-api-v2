package ar.com.experta.ventashogarapi.core.model;

import java.util.Objects;

public class Telefono {

    private String prefijo;
    private String numero;

    public Telefono() {
    }

    public Telefono(String prefijo, String numero) {
        this.prefijo = prefijo;
        this.numero = numero;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Telefono)) return false;
        Telefono telefono = (Telefono) o;
        return Objects.equals(getPrefijo(), telefono.getPrefijo()) &&
                Objects.equals(getNumero(), telefono.getNumero());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPrefijo(), getNumero());
    }

    public String getNumeroCompleto() {
        return (prefijo != null ? prefijo : "") + " " + (numero != null ? numero : "");
    }
}
