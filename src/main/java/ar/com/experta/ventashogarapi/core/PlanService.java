package ar.com.experta.ventashogarapi.core;

import ar.com.experta.ventashogarapi.core.model.Plan;
import ar.com.experta.ventashogarapi.core.model.Vendedor;
import ar.com.experta.ventashogarapi.core.model.enums.IdPlan;
import ar.com.experta.ventashogarapi.core.model.enums.TipoPlan;
import ar.com.experta.ventashogarapi.core.model.enums.TipoProductos;
import ar.com.experta.ventashogarapi.infraestructure.db.PlanDAO;
import ar.com.experta.ventashogarapi.infraestructure.vendedoresclient.VendedoresClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PlanService {

    @Autowired
    private PlanDAO planDao;
    @Autowired
    private VendedoresClient vendedoresClient;
    @Autowired
    private BeneficiosService beneficiosService;

    public List<Plan> getEncabezadosPlanes() {
        List<Plan> lista = planDao.getPlanes();
        lista.forEach(l -> l.setItems(null));
        lista.stream().forEach(plan -> beneficiosService.configurarBeneficios(plan));
        return lista;
    }

    public List<Plan> getPlanes(String idVendedor) {
        return this.getPlanes(idVendedor, null);
    }

    public List<Plan> getPlanes(String idVendedor, TipoPlan tipoPlan) {
        Vendedor vendedor = idVendedor != null ? vendedoresClient.getVendedor(idVendedor) : null;
        List<Plan> planes = planDao.getPlanes(vendedor != null ? vendedor.getTipoProductos() : null, tipoPlan);
        if (vendedor != null)
            planes.stream().forEach(plan -> beneficiosService.configurarBeneficios(plan, vendedor));
        return planes;
    }

    public List<Plan> getPlanes(List<TipoProductos> roles, TipoPlan tipoPlan) {
        List<Plan> planes = planDao.getPlanes(roles, tipoPlan);
        planes.stream().forEach(plan -> beneficiosService.configurarBeneficios(plan));
        return planes;
    }

    public Plan getPlan(IdPlan idPlan) {
        Plan plan = planDao.getPlan(idPlan);
        beneficiosService.configurarBeneficios(plan);
        return plan;
    }

    public void savePlan(Plan plan) {
        this.planDao.save(plan);
    }
}