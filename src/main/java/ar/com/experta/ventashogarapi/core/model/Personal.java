package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.TipoCobertura;
import ar.com.experta.ventashogarapi.core.model.enums.TipoEspecifico;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Personal extends Especifico {
    private String dni;
    private String nombre;
    private String apellido;
    private LocalDate fechaNacimiento;

    public Personal() {
        super(TipoEspecifico.PERSONAL);
    }

    public Personal(TipoCobertura tipoCobertura, BigDecimal montoAsegurado, String dni, String nombre, String apellido, LocalDate fechaNacimiento) {
        super(TipoEspecifico.PERSONAL, tipoCobertura, montoAsegurado);
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @JsonIgnore
    public String getNombreCompleto(){
        return (this.nombre != null ? this.nombre : "") +
                (this.apellido != null ? (this.nombre != null ? " " : "") + this.apellido : "");
    }
}
