package ar.com.experta.ventashogarapi.core.model.enums;

import com.fasterxml.jackson.annotation.JsonIgnore;

public enum TipoCobertura {

    INCENDIO_EDIFICIO(
            "Incendio de Edificio",
            "Indemnización al asegurado por las pérdidas ocurridas por incendio, caída de un rayo o explosión que afecte al edificio. La protección comprende también los incendios que puedan haberse generado en las inmediaciones de la vivienda asegurada.",
            null,
            Boolean.FALSE),
    INCENDIO_EDIFICIO_PRORRATA( 
            "Incendio de Edificio (A Prorrata)",
            "Indemnización al asegurado por las pérdidas ocurridas por incendio, caída de un rayo o explosión que afecte al edificio. La protección comprende también los incendios que puedan haberse generado en las inmediaciones de la vivienda asegurada. La cobertura es a prorrata",
            null,
            Boolean.FALSE),
    INCENDIO_EDIFICIO_ABSOLUTO( 
            "Incendio de Edificio (1er Riesgo Absoluto)",
            "Indemnización al asegurado por las pérdidas ocurridas por incendio, caída de un rayo o explosión que afecte al edificio. La protección comprende también los incendios que puedan haberse generado en las inmediaciones de la vivienda asegurada. La cobertura es a primer riesgo absoluto",
            null,
            Boolean.FALSE),
    INCENDIO_CONTENIDO( 
            "Incendio de Contenido General",
            "Cobertura de los bienes muebles propiedad del Asegurado, sus familiares, personal de servicio y huéspedes. La Cobertura es a Primer Riesgo Absoluto y Sin Franquicia.",
            null,
            Boolean.FALSE),
    RIESGOS_NATURALES( 
            "Riesgos de la Naturaleza",
            "Contando con la cobertura de Incendio, la póliza también brinda protección al Asegurado por los daños y las pérdidas como consecuencia de Huracán, Vendaval, Ciclón o Tornado que afecte a la vivienda asegurada. La cobertura es a Prorrata y Sin Franquicia.",
            null,
            Boolean.FALSE),
    GRANIZO_EDIFICIO( 
            "Granizo",
            "Contando con la cobertura de Incendio, la póliza también brinda protección al Asegurado por los daños y las pérdidas como consecuencia de Granizo que afecte a la vivienda asegurada y su contenido.",
            null,
            Boolean.FALSE),
    AD_HVCT_IE( 
            "Adicional de Riesgos de la Naturaleza (Edificio)",
            "Contando con la cobertura de Incendio, la póliza también brinda protección al Asegurado por los daños y las pérdidas como consecuencia de Huracán, Vendaval, Ciclón o Tornado que afecte a la vivienda asegurada. La cobertura es sin Franquicia.",
            null,
            Boolean.TRUE),
    AD_HVCT_IE_PRORRATA(
            "Adicional de Riesgos de la Naturaleza (Edificio)",
            "Contando con la cobertura de Incendio, la póliza también brinda protección al Asegurado por los daños y las pérdidas como consecuencia de Huracán, Vendaval, Ciclón o Tornado que afecte a la vivienda asegurada. La cobertura es a Primer Riesgo Absoluto y Sin Franquicia.",
            null,
            Boolean.TRUE),
    AD_HVCT_IE_ABSOLUTO(
            "Adicional de Riesgos de la Naturaleza (Edificio)",
            "Contando con la cobertura de Incendio, la póliza también brinda protección al Asegurado por los daños y las pérdidas como consecuencia de Huracán, Vendaval, Ciclón o Tornado que afecte a la vivienda asegurada. La cobertura es a Prorrata y Sin Franquicia.",
            null,
            Boolean.TRUE),
    AD_HVCT_IC(
            "Adicional de Riesgos de la Naturaleza (Contenido)",
            "Contando con la cobertura de Incendio, la póliza también brinda protección al Asegurado por los daños y las pérdidas como consecuencia de Huracán, Vendaval, Ciclón o Tornado que afecte al contenido de la vivienda asegurada. La cobertura es sin Franquicia.",
            null,
            Boolean.TRUE),
    AD_HVCT_IC_PRORRATA(
            "Adicional de Riesgos de la Naturaleza (Contenido)",
            "Contando con la cobertura de Incendio, la póliza también brinda protección al Asegurado por los daños y las pérdidas como consecuencia de Huracán, Vendaval, Ciclón o Tornado que afecte a la vivienda asegurada. La cobertura es a Prorrata y Sin Franquicia.",
            null,
            Boolean.TRUE),
    AD_HVCT_IC_ABSOLUTO(
            "Adicional de Riesgos de la Naturaleza (Contenido)",
            "Contando con la cobertura de Incendio, la póliza también brinda protección al Asegurado por los daños y las pérdidas como consecuencia de Huracán, Vendaval, Ciclón o Tornado que afecte a la vivienda asegurada. La cobertura es a Primer Riesgo Absoluto y Sin Franquicia.",
            null,
            Boolean.TRUE),
    AD_GRANIZO_EDIFICIO( 
            "Adicional de Granizo (Edificio) - (Sublimitada al 5%)",
            "Contando con la cobertura de Incendio de Edificio, la póliza también brinda protección al Asegurado por los daños y las pérdidas como consecuencia de Granizo que afecte a la vivienda asegurada. Sublimitada al 5% de la suma asegurada de Incendio de Edificio.",
            null,
            Boolean.TRUE),
    AD_GRANIZO_CONTENIDO(
            "Adicional de Granizo (Contenido) (Sublimitada al 5%)",
            "Contando con la cobertura de Incendio de Contendio, la póliza también brinda protección al Asegurado por los daños y las pérdidas como consecuencia de Granizo que afecte al contenido de la vivienda asegurada. Sublimitada al 5% de la suma asegurada de Incendio de Contenido.",
            null,
            Boolean.TRUE),
    RESPONSABILIDAD_CIVIL_LINDEROS(
            "Responsabilidad Civil Linderos",
            "El Asegurador se obliga a mantener indemne al Asegurado por cuanto deba a un tercero, en razón de la responsabilidad civil en que incurra, exclusivamente como consecuencia de la acción directa o indirecta de un incendio o una explosión que acontezca en la vivienda asegurada. El Asegurador solo responde por los daños materiales a bienes de propiedad de terceros con expresa exclusión de lesiones o muertes producidas a terceros. La cobertura es a Primer Riesgo Absoluto. El Asegurado participará en cada reclamo con el 5 % del siniestro con un mínimo del 1% y un máximo del 3% de la Suma Asegurada.",
            null,
            Boolean.FALSE),
    ALIMENTOS_FREEZER( 
            "Alimentos en Freezer",
            "Con esta cobertura el Asegurado obtiene una indemnización por los alimentos guardados en freezers y heladeras que hayan perdido frío como consecuencia de una falta en el suministro de energía eléctrica por más de DOCE (12) horas consecutivas imputable a la empresa de distribución de electricidad. La cobertura otorgada es a Primer Riesgo Absoluto y Sin Franquicia.",
            null,
            Boolean.FALSE),
    RETIRO_ESCOMBROS( 
            "Retiro de Escombros (Edificio)",
            "Gastos de retiro de escombros de los bienes afectados por incendio. La cobertura es a Primer Riesgo Absoluto y Sin Franquicia.",
            null,
            Boolean.FALSE),
    RETIRO_ESCOMBROS_CONTENIDO(
            "Retiro de Escombros (Contenido)",
            "Gastos de retiro de escombros de los bienes afectados por incendio. La cobertura es a Primer Riesgo Absoluto y Sin Franquicia.",
            null,
            Boolean.FALSE),
    GASTOS_ALOJAMIENTO(
            "Gastos de Alojamiento",
            "Gastos que debe asumir el Asegurado para proporcionarse hospedaje en un hotel o alquiler temporario de una vivienda alternativa por la inhabitabilidad de la propia por un incendio. La cobertura es a Primer Riesgo Absoluto y Sin Franquicia.",
            null,
            Boolean.FALSE),
    DANIOS_ESTETICOS( 
            "Daños Estéticos por Incendio",
            "Un arreglo parcial como consecuencia de un siniestro de incendio, puede provocar un menoscabo en la armonía inicial del ambiente afectado, por ejemplo diferente tonalidad de pintura o revestimientos. Hasta el importe asignado a esta cobertura adicional la compañía indemnizará los gastos de armonizar el ambiente. Esta cobertura es a Primer Riesgo Absoluto.",
            null,
            Boolean.FALSE),
    ROBO( 
            "Robo y/o Hurto del Contenido General",
            "Indemnización al Asegurado por el robo o hurto de los bienes que sean de su propiedad, de sus familiares, de sus huéspedes y de su personal doméstico (cuando hayan sido sustraídos del interior de la vivienda asegurada). Si se producen daños en la vivienda, el 15% de la suma asegurada disponible en la cobertura de Robo o Hurto podrá ser destinada a la reparación de los daños. La cobertura es a Primer Riesgo Absoluto.",
            "Franquicia: 10% de la perdida o daño, mínimo $10.000",
            Boolean.FALSE),
    ROBO_ELEBAR(
            "Robo y/o Hurto del Contenido General",
            "Indemnización al Asegurado por el robo o hurto de los bienes que sean de su propiedad, de sus familiares, de sus huéspedes y de su personal doméstico (cuando hayan sido sustraídos del interior de la vivienda asegurada). Si se producen daños en la vivienda, el 15% de la suma asegurada disponible en la cobertura de Robo o Hurto podrá ser destinada a la reparación de los daños. La cobertura es a Primer Riesgo Absoluto.",
            "Franquicia: 10% de la perdida o daño",
            Boolean.FALSE),
    VIVIENDA_VERANO(
            "Vivienda de Verano",
            "Con este adicional de cobertura, el Asegurador extiende su responsabilidad por los riesgos de Incendio y Robo y/o Hurto a los bienes de su propiedad que, estando comprendidos en el Contenido General de la vivienda permanente asegurada, hayan sido trasladados e instalado en la vivienda que ocupe en calidad de inquilino o locatario temporal con motivo de sus vacaciones. El plazo máximo de cobertura es de 45 días corridos. La suma asegurada total se limita al 50% de la prevista para la cobertura de Robo o Hurto del Contenido General de la vivienda permanente asegurada. La vivienda vacacional puede estar en Argentina o en los países limítrofes. La cobertura es a Primer Riesgo Absoluto y Sin Franquicia.",
            null,
            Boolean.FALSE),
    RIESGO_ELECTRO(
            "Todo Riesgo Electro",
            "Se cubren los bienes mientras se encuentren dentro de la vivienda asegurada y por cualquier pérdida o daño que no se exceptúe expresamente en la póliza. Dentro de una única suma asegurada y sin necesidad de dar el detalle de los bienes asegurados ni su valor unitario, la cobertura ampara los siguientes aparatos: heladeras y freezers, lavarropas y secarropas, lavavajillas, cocinas eléctricas, hornos eléctricos, hornos de microondas, aparatos de aire acondicionado, televisores, equipos de audio, reproductores y/o grabadores de video y/o audio en cualquier formato y proyectores de imágenes, pantallas y/o televisores de LCD, LED o similares, PC de escritorio, impresoras, scanners, monitores, consolas de video juegos y cavas de vinos. Se excluyen equipos electrónicos portátiles. La cobertura otorgada es a Primer Riesgo Absoluto y con cláusula de reposición a nuevo para bienes de hasta tres años de antigüedad.  El ámbito de la cobertura queda circunscripto a la vivienda asegurada ubicada en el domicilio de riesgo indicado en las Condiciones Particulares.",
            "Franquicia: El Asegurado participará en cada siniestro con el 10 % de la pérdida y con un mínimo de $ 10.000.-",
            Boolean.FALSE),
    RIESGO_ELECTRO_ELEBAR( 
            "Todo Riesgo Electro",
            "Comprende bienes tales como televisores, video reproductores, video grabadores, reproductores de DVD, fax, audio, microondas, computadoras de escritorio, notebooks, proyectores, cámaras fotográficas, filmadoras, grabadores, y aparatos electrónicos domésticos en general; los que están cubiertos dentro de la vivienda asegurada. Se cubre, a Primer Riesgo Absoluto (2), el daño o la pérdida producidos por cualquier causa que no se exceptúe expresamente. Entre las exclusiones se destaca que no se encuentran cubiertas las pérdidas o daños como consecuencia de un acto intencional o negligencia inexcusable del Asegurado, pérdidas y/o daños que sean consecuencia directa del funcionamiento continuo, fallas en el funcionamiento, desgaste normal, los debidos a defectos de fabricación daños por los cuales sea responsable el fabricante y otras. Esta cobertura se otorga sin necesidad de brindar el detalle ni el valor unitario de los bienes asegurados. La cobertura se otorga a Primer Riesgo Absoluto y sin Franquicia.",
            null,
            Boolean.FALSE),
    RIESGO_ELECTRO_ELEBAR_2(
            "Todo Riesgo Electro",
            "Se cubren los bienes mientras se encuentren dentro de la vivienda asegurada y por cualquier pérdida o daño que no se exceptúe expresamente en la póliza. Dentro de una única suma asegurada y sin necesidad de dar el detalle de los bienes asegurados ni su valor unitario, la cobertura ampara los siguientes aparatos: heladeras y freezers, lavarropas y secarropas, lavavajillas, cocinas eléctricas, hornos eléctricos, hornos de microondas, aparatos de aire acondicionado, televisores, equipos de audio, reproductores y/o grabadores de video y/o audio en cualquier formato y proyectores de imágenes, pantallas y/o televisores de LCD, LED o similares, PC de escritorio, impresoras, scanners, monitores, consolas de video juegos y cavas de vinos. Se excluyen equipos electrónicos portátiles. La cobertura otorgada es a Primer Riesgo Absoluto. Aparatos Electrodomésticos- Reposición a nuevo para bienes de hasta un año de antigüedad: En caso de pérdida total se indemnizará el bien afectado por su valor a nuevo, entendiéndose como tal lo que valdría al momento del siniestro otro bien nuevo de la misma o análoga clase y capacidad, sin depreciación por uso y antigüedad. Para aplicar esta garantía, la Suma Asegurada declarada en la póliza para Aparatos Electrodomésticos asegurados bajo la Cobertura de Bienes Especificados deberá ser el Valor a Nuevo de los aparatos asegurados, sin depreciación por uso y antigüedad. La indemnización no podrá sobrepasar el valor de la suma asegurada que figura en las Condiciones Particulares de la póliza. El ámbito de la cobertura queda circunscripto a la vivienda asegurada ubicada en el domicilio de riesgo indicado en las Condiciones Particulares.",
            "Franquicia: 10% del siniestro",
            Boolean.FALSE),
    BIENES_ESPECIFICOS(
            "Bienes Especificados (Incen, Robo y Hurto)",
            "Se cubren los bienes mientras se encuentren dentro de la vivienda asegurada y por el daño o la pérdida producidos por Robo, Hurto o su tentativa, y por la acción directa o indirecta de Incendio, Rayo o Explosión. La cobertura otorgada es a Prorrata. Se admitirán única y exclusivamente Instrumentos musicales, bicicletas y artículos deportivos. El Asegurado participará en cada siniestro con el 10 % de la pérdida y con un mínimo de $ 10.000.",
            null,
            Boolean.FALSE),
    TR_ELECTRO_NOTEBOOK( 
            "Notebook en Mundo Entero",
            "Se cubre en cualquier parte del Mundo y por cualquier pérdida o daño que no se exceptúe expresamente en la póliza. La cobertura otorgada es a Prorrata.",
            "Franquicia: 10% del siniestro, mínimo $15.000",
            Boolean.FALSE),
    TR_ELECTRO_NOTEBOOK_ELEBAR(
            "Notebook en Mundo Entero",
            "Se cubre en cualquier parte del Mundo y por cualquier pérdida o daño que no se exceptúe expresamente en la póliza. La cobertura otorgada es a Prorrata.",
            "Franquicia: 15% del siniestro",
            Boolean.FALSE),
    ELECTRO_ESPECIFICOS(
            "Todo Riesgo Electrodomésticos Seleccionados",
            "Dentro de una única suma asegurada y sin necesidad de dar el detalle de los bienes asegurados ni su valor unitario, nuestro plan contempla cobertura para heladeras, freezers, lavarropas, secarropas, hornos de microondas, televisores y equipos de audio y video, que se encuentren dentro de la vivienda asegurada y por cualquier pérdida o daños que no se exceptúen expresamente en la póliza. La cobertura es a Primer Riesgo Absoluto. El Asegurado participará en cada siniestro con el 10 % de la pérdida y con un mínimo de $ 10.000. Reposición a nuevo para bienes de hasta 3 años de antiguedad: En caso de pérdida total se indemnizará el bien afectado por su valor a nuevo, entendiéndose como tal lo que valdría al momento del siniestro otro bien nuevo de la misma o análoga clase y capacidad, sin depreciación por uso y antigüedad. Para aplicar esta garantía, la Suma Asegurada declarada en la póliza para Aparatos Electrodomésticos asegurados bajo la Cobertura de Bienes Especificados deberá ser el Valor a Nuevo de los aparatos asegurados, sin depreciación por uso y antigüedad. La indemnización no podrá sobrepasar el valor de la suma asegurada que figura en las Condiciones Particulares de la póliza. El ámbito de la cobertura queda circunscripto a la vivienda asegurada ubicada en el domicilio de riesgo indicado en las Condiciones Particulares.",
            null,
            Boolean.FALSE),
    TR_ELECTRO_DETALLE( 
            "Todo Riesgo Electro con detalle",
            "Se cubren los bienes mientras se encuentren dentro de la vivienda asegurada y por cualquier pérdida o daño que no se exceptúe expresamente en la póliza. La cobertura otorgada es a Prorrata y sin Franquicia. Se admitirán única y exclusivamente Televisores y pantallas, dispositivos de audio, cavas de vino eléctricas y PC de escritorio (no se incluyen accesorios).",
            null,
            Boolean.FALSE),
    CRISTALES(
            "Cristales",
            "Bajo esta cobertura, el Asegurador indemnizará al Asegurado los daños sufridos por rotura o rajadura de los cristales, vidrios, espejos y demás piezas vítreas o similares instaladas en posición vertical en la vivienda asegurada. La cobertura otorgada es a Primer Riesgo Absoluto y Sin Franquicia.",
            null,
            Boolean.FALSE),
    CRISTALES_ELEBAR(
            "Cristales",
            "Bajo esta cobertura, el Asegurador indemnizará al Asegurado los daños sufridos por rotura o rajadura de los cristales, vidrios, espejos y demás piezas vítreas o similares instaladas en posición vertical en la vivienda asegurada. La cobertura otorgada es a Primer Riesgo Absoluto.",
            null,
            Boolean.FALSE),
    DANIOS_AGUA(
            "Daños por Agua Corriente",
            "Esta cobertura tiene por objeto indemnizar al Asegurado por la pérdida o daños al mobiliario por la acción directa del agua proveniente de una filtración, derrame, desborde o escape como consecuencia de la rotura, obstrucción o falla de la instalación destinada a contener el agua y distribuirla por la vivienda. La cobertura otorgada es a Primer Riesgo Absoluto y Sin Franquicia.",
            null,
            Boolean.FALSE),
    RESPONSABILIDAD_CIVIL_PRIVADOS( 
            "Responsabilidad Civil por Hechos Privados",
            "Con esta cobertura el Asegurador se obliga a mantener indemne al Asegurado por cuanto deba a un tercero, en razón de la responsabilidad civil en que incurra, exclusivamente como consecuencia de hechos imputables al Asegurado y/o su cónyuge siempre que conviva con él y/o cualquier otra persona por quien el Asegurado sea legalmente responsable, acaecidos no solo en la vivienda asegurada sino en cualquier lugar de la República Argentina. Los hechos no deben guardar relación alguna con actividades profesionales comerciales o industriales desarrolladas por el Asegurado o las personas por las que él responde. La cobertura otorgada es a Primer Riesgo Absoluto. El Asegurado participará en cada reclamo con el 10 % del siniestro con un mínimo del 1% y un máximo del 5% de la Suma Asegurada.",
            null,
            Boolean.FALSE),
    RESPONSABILIDAD_CIVIL_ANIMALES( 
            "-",
            "-",
            null,
            Boolean.FALSE),
    AD_RESPONSABILIDAD_CIVIL_ANIMALES( 
            "Adicional de Resp. Civil por Animales Domésticos",
            "Contando con la cobertura de Responsabilidad Civil Hechos Privados, el Asegurador se obliga a mantener indemne al Asegurado por cuanto deba a un tercero, en razón de la responsabilidad civil en que incurra, como consecuencia de los hechos generados por sus animales domésticos.",
            null,
            Boolean.TRUE),
    PALOS_GOLF( 
            "Jugadores de Golf",
            "Daños o Pérdida de los Palos: Mientras se hallen en el domicilio del Asegurado, depositados o en uso por el Asegurado en un club o cancha de golf y sus dependencias, reconocida por la Asociación Argentina de Golf y/o mientras el Asegurado los trasladare desde su domicilio a un club o cancha de golf y sus dependencias y/o estén en poder de un caddie, dentro de los límites de la República Argentina. Incendio, Robo y/o Hurto de Efectos Personales (excluyendo relojes, joyas, alhajas, dijes, adornos, medallas, trofeos, monedas, dinero, valores, títulos y/o estampillas): Mientras se hallen depositados o en poder del Asegurado en un club o cancha de golf y sus dependencias, reconocida por la Asociación Argentina de Golf, dentro de los límites de la  República Argentina y hasta la suma asegurada para este ítem, indicada en las Condiciones Particulares. Hoyo en Uno: Se cubren los gastos en que incurra el Asegurado en el caso de realizar el hoyo al primer golpe (Hoyo en uno) en tanto sea bajo los reglamentos de la Asociación Argentina de Golf. Responsabilidad Civil: El Asegurador se obliga a mantener indemne al Asegurado por cuanto deba a un tercero, en razón de la responsabilidad civil en que incurra exclusivamente como consecuencia de accidentes causados por él mientras estuviese jugando al golf, en cualquier cancha de golf reconocida por la Asociación Argentina de Golf dentro de los límites de la República Argentina.",
            null,
            Boolean.FALSE),
    ACCIDENTES_PERSONALES( 
            "Accidentes Personales",
            "El Asegurador se compromete al pago de las prestaciones estipuladas para el caso de fallecimiento o de invalidez permanente del Asegurado, como consecuencia de un accidente que ocurra dentro de la vivienda asegurada, con motivo o en ocasión de su trabajo y durante la vigencia del seguro. Entre otras exclusiones, esta cobertura no responde por enfermedades de ningún tipo aunque sean originadas por la picadura de insectos, ni por lesiones o afecciones devenidas por la exposición a radiaciones (incluyendo las solares) ni por exposición a las condiciones atmosféricas o ambientales ni por lesiones imputables a esfuerzo ni o por estado de ebriedad o por estar el Personal de Servicio Doméstico Asegurado bajo la influencia de estupefacientes o alcaloides. Tampoco responde por los accidentes causados por vértigos, vahídos, lipotimias, convulsiones o parálisis salvo que estos trastornos sean consecuencia de un accidente cubierto. Se deben detallar las personas cubiertas con Nombre Apellido, tipo y número de documento.",
            null,
            Boolean.FALSE),
    ACCIDENTES_PERSONALES_PCP("Accidentes Personales (PCP)",
            "El Asegurador se compromete al pago de las prestaciones estipuladas para el caso de fallecimiento o de invalidez permanente del Personal de Servicio Doméstico Asegurado, como consecuencia de un accidente que ocurra dentro de la vivienda asegurada, con motivo o en ocasión de su trabajo y durante la vigencia del seguro. Entre otras exclusiones, esta cobertura no responde por enfermedades de ningún tipo aunque sean originadas por la picadura de insectos, ni por lesiones o afecciones devenidas por la exposición a radiaciones (incluyendo las solares) ni por exposición a las condiciones atmosféricas o ambientales ni por lesiones imputables a esfuerzo ni o por estado de ebriedad o por estar el Asegurado bajo la influencia de estupefacientes o alcaloides. Tampoco responde por los accidentes causados por vértigos, vahídos, lipotimias, convulsiones o parálisis salvo que estos trastornos sean consecuencia de un accidente cubierto. Cobertura Adicional de Reintegro de Gastos por Asistencia Médica y Farnacéutica. El Asegurador se compromete al pago de los gastos de asistencia médico-farmacéutica, vinculados con el accidente cubierto por sobre los gastos cubiertos por la Obra Social o Medicina Prepaga a la cual se encuentre adherido el Personal de Servicio Doméstico Asegurado, incluyendo el importe no cubierto de medicamentos recetados. Hasta el 2.5% de la suma asegurada de muerte de Accidentes Personales (PCP)",
            null,
            Boolean.FALSE),
    GASTOS_MEDICOS(
            "Reintegro de Gastos por Asistencia Médica y Farnacéutica por Accidente (PCP)",
            "El Asegurador se compromete al pago de los gastos de asistencia médico-farmacéutica, vinculados con el accidente cubierto por sobre los gastos cubiertos por la Obra Social o Medicina Prepaga a la cual se encuentre adherido el Personal de Servicio Doméstico Asegurado, incluyendo el importe no cubierto de medicamentos recetados.",
            null,
            Boolean.FALSE),
    DANIOS_AGUA_EDIFICIO( 
            "Daños por Agua al Contenido y Edificio",
            "Esta cobertura tiene por objeto indemnizar al Asegurado por la pérdida o daños al mobiliario y/o al Edificio por la acción directa del agua proveniente de una filtración, derrame, desborde o escape como consecuencia de la rotura, obstrucción o falla de la instalación destinada a contener el agua y distribuirla por la vivienda. La cobertura otorgada es a Primer Riesgo Absoluto y Sin Franquicia.",
            null,
            Boolean.FALSE),
    ROBO_PORTATILES( 
            "Robo de Equipos Electrónicos y Deportivos en Argentina",
            "Se cubre Los Equipos Electrónicos y Artículos Deportivos que con su específico detalle hayan sido incorporadas en la póliza. La cobertura comprende los riesgos de Incendio, Robo y/o Hurto mientras se encuentren dentro de la vivienda asegurada. La cobertura de los riesgos de Incendio y Robo se amplían a todo el territorio de la República Argentina, en tanto dichos bienes se encuentren en todo momento bajo la custodia personal directa del Asegurado salvo cuando se encuentren en el baúl u otro compartimiento similar debidamente cerrado con llave, de un vehículo automotor de transporte público o privado en el que se traslade el Asegurado, y no pudieran ser vistos desde el exterior. Equipos Electrónicos Portátiles Admitidos: Se admitirán única y exclusivamente Cámara fotográficas y/o filmadoras, con excepción de las denominadas Cámaras Deportivas (Sport Cameras), Notebooks, Ultrabook, Laptops, Ipads y Tablets. Artículos Deportivos Admitidos: Tenis, Pádel, Raquetball, Squash, Ping Pong, Pelota Paleta, Badminton: Solo las raquetas y/o Paletas. Polo, Golf, Cricket, Beisbol, Softbol, Hockey sobre Césped: Solo los Palos, con excepción de Beisbol y Softbol en los que se admitirán los guantes. Piragüismo, Canotaje, Remo: Solo el bote. Esquí Acuático, Wakeboard, Surf: Solo las Tablas. Windsurf: Tabla y Vela Completa. Paddle Surf: Tabla y Remo. Tiro con Arco: Sólo el Arco. Atletismo: Solo las garrochas y jabalinas. Patinaje: Solo los Patines. Hockey sobre Hielo: Solo los Patines y Palos. Esquí Alpino o de Fondo: Solo las tablas, los bastones y las botas.",
            "Franquicia: 10% del siniestro, mínimo $10.000",
            Boolean.FALSE),
    ROBO_BICICLETA( 
            "Robo de Bicicletas en Argentina",
            "Se cubre la o las bicicletas que con su específico detalle hayan sido incorporadas en la póliza. La cobertura comprende los riesgos de Incendio, Robo y/o Hurto mientras se encuentren dentro de la vivienda asegurada. La cobertura de los riesgos de Incendio y Robo se amplían a todo el territorio de la República Argentina, en tanto la bicicleta se encuentren en todo momento bajo la custodia personal directa del Asegurado salvo cuando ésta se encuentre adherida a un punto fijo y firme al suelo, mediante cadena o cable de acero con candado o traba con cerrojo.",
            "Franquicia: 10% del siniestro, mínimo $10.000",
            Boolean.FALSE),
    HONORARIOS_PROFESIONALES ( 
            "Honorarios Profesionales",
            "Se cubren los Honorarios Profesionales necesariamente incurridos en la reconstrucción de la propiedad afectada por un incendio. La cobertura otorgada es a Primer Riesgo Absoluto.",
            null,
            Boolean.FALSE),
    DOCUMENTOS_PERSONALES("Documentos Personales",
            "Con este adicional de cobertura, se cubren los gastos de reposición de Documentos Personales como Consecuencia de Robo o Hurto que se haya producido en la vivienda del Asegurado. La cobertura otorgada es a Primer Riesgo Absoluto y Sin Franquicia.",
            null,
            Boolean.FALSE),
    BIENES_BAULERAS ("Bienes ubicados en Bauleras",
            "El Asegurador extiende su responsabilidad por los riesgos de Incendio y Robo a los bienes de su propiedad que se encuentren en su baulera, en el mismo edificio donde se ubica la vivienda asegurada. La cobertura otorgada es a Primer Riesgo Absoluto y Sin Franquicia. No se indemnizarán las pérdidas por Hurto o las pérdidas de bienes que no sean de propiedad del Asegurado.",
            null,
            Boolean.FALSE)
    ;

    private String label;
    private String description;
    private String descriptionFranquicia;
    private Boolean esAdicional;
    
    TipoCobertura(String label, String description, String descriptionFranquicia, Boolean esAdicional) {
        this.label = label;
        this.description = description;
        this.descriptionFranquicia = descriptionFranquicia;
        this.esAdicional = esAdicional;
    }

    public String label() {
        return label;
    }

    public String description() {
        return description;
    }

    public String descriptionFranquicia() {
        return descriptionFranquicia;
    }

    public Boolean esAdicional() {
        return esAdicional;
    }
    @JsonIgnore
    public Boolean show() { return !label.equals("-");}
}
