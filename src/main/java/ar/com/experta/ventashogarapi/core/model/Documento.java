package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.TipoDocumento;

import java.util.Objects;

public class Documento {

    public static final String PREFIJO_CUIT_FEMENINO = "27";
    public static final String PREFIJO_CUIT_MASCULINO = "20";

    private TipoDocumento tipo;
    private String numero;

    public Documento() {
    }

    public Documento(TipoDocumento tipo, String numero) {
        this.tipo = tipo;
        this.numero = numero;
    }

    public TipoDocumento getTipo() {
        return tipo;
    }

    public void setTipo(TipoDocumento tipo) {
        this.tipo = tipo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Documento)) return false;
        Documento documento = (Documento) o;
        return getTipo() == documento.getTipo() &&
                Objects.equals(getNumero(), documento.getNumero());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTipo(), getNumero());
    }
}
