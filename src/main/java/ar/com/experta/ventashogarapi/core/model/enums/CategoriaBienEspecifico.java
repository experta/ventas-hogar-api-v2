package ar.com.experta.ventashogarapi.core.model.enums;

public enum CategoriaBienEspecifico {
    BIENES_GENERALES,
    ELECTRODOMESTICOS,
    NOTEBOOK,
    PORTATILES,
    BICICLETAS;
}
