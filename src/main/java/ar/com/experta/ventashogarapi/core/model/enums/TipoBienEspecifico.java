package ar.com.experta.ventashogarapi.core.model.enums;

public enum TipoBienEspecifico {

    NOTEBOOK ( CategoriaBienEspecifico.NOTEBOOK, "Notebook", "Computadoras personales de todo tipo: Laptops, Notepads, Ultrabooks, etc. (Hasta 4 unidades)", Integer.valueOf(4)),
    BICICLETA_BG ( CategoriaBienEspecifico.BIENES_GENERALES, "Bicicleta", "Bicicleta movil de todo tipo. (Hasta 3 unidades)", Integer.valueOf(3)),
    ARTIC_DEPORTIVOS_BG ( CategoriaBienEspecifico.BIENES_GENERALES, "Articulos deportivos", "Pelotas, Raquetas, Paracaidas, Velas, Canoas, etc. (Hasta 3 unidades)", Integer.valueOf(3)),
    INSTR_MUSICALES_BG ( CategoriaBienEspecifico.BIENES_GENERALES, "Instrumentos musicales", "Todo tipo de instrumentos de cuerda, viento y percusión. (Hasta 3 unidades)", Integer.valueOf(3)),
    NOTEBOOK_BG ( CategoriaBienEspecifico.BIENES_GENERALES, "Notebook", "Computadoras personales de todo tipo: Laptops, Notepads, Ultrabooks, etc. (Hasta 4 unidades)", Integer.valueOf(4)),
    PC_ELEC ( CategoriaBienEspecifico.ELECTRODOMESTICOS, "PC de escritorio", "Computadora de escritorío de todo tipo y monitor. Se excluyen periféricos (mouse, teclado y parlantes) y otros accesorios. (Hasta 3 unidades)", Integer.valueOf(3)),
    AUDIO_ELEC ( CategoriaBienEspecifico.ELECTRODOMESTICOS, "Dispositivo de Audio", "Todo tipo de equipos de audio, parlantes, etc. (Hasta 3 unidades)", Integer.valueOf(3)),
    CAVA_ELEC ( CategoriaBienEspecifico.ELECTRODOMESTICOS, "Cava de vinos", "Todo tipo de cava de vinos. (Hasta 3 unidades)", Integer.valueOf(3)),
    TV_ELEC ( CategoriaBienEspecifico.ELECTRODOMESTICOS, "Television o Pantallas", "Todo tipo de instrumentos de televisores, pantalla o monitores. (Hasta 5 unidades)", Integer.valueOf(5)),
    NOTEBOOK_POR ( CategoriaBienEspecifico.PORTATILES, "Notebook", "Computadoras personales de todo tipo: Laptops, Ultrabooks, etc. (Hasta 4 unidades)", Integer.valueOf(4)),
    FILMADORAS_POR ( CategoriaBienEspecifico.PORTATILES, "Filmadoras", "Todo tipo de cámaras filmadoras portátiles. Excluyendo Gopros o similares. (Hasta 3 unidades)", Integer.valueOf(3)),
    ARTIC_DEPORTIVOS_POR ( CategoriaBienEspecifico.PORTATILES, "Articulos deportivos", "Pelotas, Raquetas, Paracaidas, Velas, Canoas, etc. (Hasta 3 unidades)", Integer.valueOf(3)),
    TABLETS_POR ( CategoriaBienEspecifico.PORTATILES, "Tablets", "Tablets y pantallas interactivas. (Hasta 4 unidades)", Integer.valueOf(4)),
    IPADS_POR ( CategoriaBienEspecifico.PORTATILES, "Ipads", "Todo tipo de IPAD. (Hasta 4 unidades)", Integer.valueOf(4)),
    CAMARAS_POR ( CategoriaBienEspecifico.PORTATILES, "Camara fotográfica", "Todo tipo de camara fotógrafica. Excluyendo Gopros o similares. (Hasta 3 unidades)", Integer.valueOf(3)),
    BICICLETA ( CategoriaBienEspecifico.BICICLETAS, "Bicicleta", "Bicicleta movil de todo tipo. (Hasta 3 unidades)", Integer.valueOf(3));

    private CategoriaBienEspecifico categoriaBienEspecifico;
    private String label;
    private String description;
    private Integer unidadesPermitidas;

    TipoBienEspecifico(CategoriaBienEspecifico categoriaBienEspecifico, String label, String description, Integer unidadesPermitidas) {
        this.categoriaBienEspecifico = categoriaBienEspecifico;
        this.label = label;
        this.description = description;
        this.unidadesPermitidas = unidadesPermitidas;
    }

    public String label() {
        return label;
    }

    public String description() {
        return description;
    }

    public CategoriaBienEspecifico categoriaBienEspecifico() { return categoriaBienEspecifico;}

    public Integer unidadesPermitidas(){return unidadesPermitidas;}


}
