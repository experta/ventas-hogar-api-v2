package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.TipoProductos;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;
import java.util.Objects;

public class Vendedor {

    private String id;
    private String nombre;
    private List<TipoProductos> tipoProductos;
    private Boolean cuponeraHabilitada;

    public Vendedor() {
    }

    public Vendedor(String id) {
        this.id = id;
    }

    public Vendedor(String id, String nombre, List<TipoProductos> tipoProductos, Boolean cuponeraHabilitada) {
        this.id = id;
        this.nombre = nombre;
        this.tipoProductos = tipoProductos;
        this.cuponeraHabilitada = cuponeraHabilitada;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @JsonIgnore
    public List<TipoProductos> getTipoProductos() {
        return tipoProductos;
    }

    public void setTipoProductos(List<TipoProductos> tipoProductos) {
        this.tipoProductos = tipoProductos;
    }

    @JsonIgnore
    public Boolean getCuponeraHabilitada() {
        return cuponeraHabilitada;
    }

    public void setCuponeraHabilitada(Boolean cuponeraHabilitada) {
        this.cuponeraHabilitada = cuponeraHabilitada;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vendedor)) return false;
        Vendedor vendedor = (Vendedor) o;
        return Objects.equals(id, vendedor.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
