package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.TipoPlan;
import ar.com.experta.ventashogarapi.core.model.enums.TipoVivienda;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Vivienda {

    private TipoVivienda tipoVivienda;
    private Boolean direccionTomador;
    private Direccion direccion;
    private Boolean tieneRejas;
    private Integer metrosCuadrados;
    @JsonIgnore
    private TipoPlan tipoPlan;

    public Vivienda() {
    }

    public Vivienda(TipoVivienda tipoVivienda, Direccion direccion, TipoPlan tipoPlan) {
        this.tipoVivienda = tipoVivienda;
        this.direccion = direccion;
        this.tipoPlan = tipoPlan;
    }

    public Vivienda(TipoVivienda tipoVivienda, Boolean direccionTomador, Direccion direccion, Boolean tieneRejas, Integer metrosCuadrados) {
        this.tipoVivienda = tipoVivienda;
        this.direccionTomador = direccionTomador != null ? direccionTomador : Boolean.FALSE;
        this.direccion = direccion;
        this.tieneRejas = tieneRejas;
        this.metrosCuadrados = metrosCuadrados;
    }

    public Vivienda(TipoVivienda tipoVivienda, Direccion direccion, Integer metrosCuadrados) {
        this.tipoVivienda = tipoVivienda;
        this.direccion = direccion;
        this.metrosCuadrados = metrosCuadrados;
        this.direccionTomador = Boolean.FALSE;
    }

    public Integer getMetrosCuadrados() {
        return metrosCuadrados;
    }

    public void setMetrosCuadrados(Integer metrosCuadrados) {
        this.metrosCuadrados = metrosCuadrados;
    }

    public Boolean getDireccionTomador() {
        return direccionTomador;
    }

    public void setDireccionTomador(Boolean direccionTomador) {
        this.direccionTomador = direccionTomador;
    }

    public TipoVivienda getTipoVivienda() {
        return tipoVivienda;
    }

    public void setTipoVivienda(TipoVivienda tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public Boolean getTieneRejas() {
        return tieneRejas;
    }

    public void setTieneRejas(Boolean tieneRejas) {
        this.tieneRejas = tieneRejas;
    }

    public TipoPlan getTipoPlan() {
        return tipoPlan;
    }

    public void setTipoPlan(TipoPlan tipoPlan) {
        this.tipoPlan = tipoPlan;
    }

    public Boolean getRequiereRejas() {
        if (tipoPlan != null && tipoPlan.equals(TipoPlan.ENLATADO_ALIANZA_ASEGURALO))
            return Boolean.FALSE;
        return this.tipoVivienda != null && this.direccion != null && this.direccion.getLocalizacion() != null && this.direccion.getLocalizacion().esZonaRiesgoRobo() != null ?
                !this.tipoVivienda.equals(TipoVivienda.EDIFICIO_PA) && !this.tipoVivienda.equals(TipoVivienda.COUNTRY) && this.direccion.getLocalizacion().esZonaRiesgoRobo() :
                null;
    }

    public List<NegocioOptionClausulas> getMedidasSeguridad() {
        List<NegocioOptionClausulas> medidasSeguridadMinimas = new ArrayList<>();

        if (tipoPlan != null && tipoPlan.equals(TipoPlan.ENLATADO_ALIANZA_ASEGURALO)) {
            medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                    "Que todas las puertas de acceso al departamento o las del edificio que den a la calle, o patios o jardines accesibles desde aquella, cuenten con cerraduras doble paleta."));
        }else {
            medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                    "Que todas las puertas de acceso al departamento o las del edificio que den a la calle, o patios o jardines accesibles desde aquella, cuenten con cerraduras."));
        }
        if (tipoPlan == null || !tipoPlan.equals(TipoPlan.ENLATADO_ALIANZA_ASEGURALO)) {
            if (this.getRequiereRejas() != null) {
                if (this.getRequiereRejas())
                    medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                            "Cuente con rejas de protección de hierro en todas las ventanas y puertas con paneles de vidrio, ubicadas en la planta baja y 1er. piso que den a la calle, o patios o jardines que por su parte sean accesibles desde la calle, es decir que no lo impidan muros, cercos o rejas de 1,80 m. como mínimo de altura."));
            } else {
                medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                        "Según la localidad en la que esté ubicada la vivienda, que cuente con rejas de protección de hierro en todas las ventanas y puertas con paneles de vidrio, ubicadas en la planta baja y 1er. piso que den a la calle, o patios o jardines que por su parte sean accesibles desde la calle, es decir que no lo impidan muros, cercos o rejas de 1,80 m. como mínimo de altura. No aplica para casa en Country/Barrio Cerrado con seguridad privada."));
            }
        }

        if (tipoPlan != null && tipoPlan.equals(TipoPlan.ENLATADO_HOGAR_DGO)) {
            medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                    "Deberán ser viviendas construidas totalmente de material, con techos sólidos e incombustibles."));
        } else if (tipoPlan != null && tipoPlan.equals(TipoPlan.ENLATADO_ALIANZA_ASEGURALO)) {
                medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                        "Deberán ser viviendas construidas totalmente de material, con techos sólidos e incombustibles,  se aceptan construcciones hasta el 50% de madera."));

        }else if (this.direccion.getLocalizacion().esZonaRiesgoIncendio() != null) {
            if (this.direccion.getLocalizacion().esZonaRiesgoIncendio())
                medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                        "Deberán ser viviendas construidas totalmente de material, material ignífugo, mampostería o construcción en seco, con techos sólidos e incombustibles."));
            else {
                medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                        "Esté edificada de medianera a medianera y no linde con un terreno baldío, obra en construcción o edificio abandonado, salvo que cuente con muros, cercos o rejas de una altura mínima de 1,80 m que impidan todo acceso que no sea por la puerta de calle del edificio."));
                medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                        "Deberán ser viviendas construidas totalmente de material, con techos sólidos e incombustibles."));
            }
        }else {
            medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                    "Según la localidad en la que esté ubicada la vivienda, Que esté edificada de medianera a medianera y no linde con un terreno baldío, obra en construcción o edificio abandonado, salvo que cuente con muros, cercos o rejas de una altura mínima de 1,80 m que impidan todo acceso que no sea por la puerta de calle del edificio."));
            medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                    "Deberán ser viviendas construidas totalmente de material, con techos sólidos e incombustibles."));
            medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                    "Según la localidad en la que esté ubicada la vivienda, deberá ser viviendas construidas totalmente de material ignífugo, mampostería o construcción en seco."));
        }

        if (tipoPlan != null && tipoPlan.equals(TipoPlan.ENLATADO_ALIANZA_ASEGURALO)) {
            medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                    "Deberá estar habitada permanentemente."));
        }else{
            if (this.tipoVivienda != null) {
                if (tipoVivienda.equals(TipoVivienda.COUNTRY)) {
                    medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                            "Deberá estar habitada permanentemente o con porteros, serenos o caseros, o ser viviendas ubicadas dentro de Countries ó Barrios Cerrados con vigilancia privada permanente."));
                } else {
                    medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                            "Deberá estar habitada permanentemente o con porteros, serenos o caseros."));
                }
            } else {
                medidasSeguridadMinimas.add(new NegocioOptionClausulas(String.valueOf(medidasSeguridadMinimas.size()),
                        "Deberán ser viviendas habitadas permanentemente o con porteros, serenos o caseros, o de ser viviendas ubicadas dentro de Countries ó Barrios Cerrados con vigilancia privada permanente."));
            }
        }

        List<NegocioOptionClausulas> medidasSeguridad = new ArrayList<>();
        medidasSeguridad.add(
                new NegocioOptionClausulas(
                        "0",
                        "El Asegurado se obliga y garantiza que, en todo momento, el riesgo asegurado contará con todas las medidas de seguridad que se detallan a continuación:",
                        medidasSeguridadMinimas
                )
        );

        if (tipoPlan != null && tipoPlan.equals(TipoPlan.ENLATADO_ALIANZA_ASEGURALO)) {
            List<NegocioOptionClausulas> medidasSeguridadGranizo = new ArrayList<>();
            medidasSeguridadGranizo.add(new NegocioOptionClausulas("0", "Techo construido total o parcialmente de fibrocemento, cartón, plástico, vidrio o materiales similares, salvo sus tragaluces."));
            medidasSeguridad.add(
                    new NegocioOptionClausulas(
                            "1",
                            "A los efectos de la Cobertura de Granizo, y/o huracán, vendaval, ciclón y tornado, se excluye:",
                            medidasSeguridadGranizo
                    )
            );

        }else {
            List<NegocioOptionClausulas> medidasSeguridadGranizo = new ArrayList<>();
            medidasSeguridadGranizo.add(new NegocioOptionClausulas("0", "Paredes exteriores construidas de mampostería, ladrillo y/u hormigón."));
            medidasSeguridadGranizo.add(new NegocioOptionClausulas("1", "No tener techo construido total o parcialmente de fibrocemento, cartón, plástico, vidrio o materiales similares, salvo sus tragaluces."));
            medidasSeguridad.add(
                    new NegocioOptionClausulas(
                            "1",
                            "A los efectos de la Cobertura de Granizo, la vivienda deberá contar con:",
                            medidasSeguridadGranizo
                    )
            );
        }
        medidasSeguridad.add(
                new NegocioOptionClausulas(
                        "2",
                        "El incumplimiento de cualquiera de las cargas antes mencionadas y/o la falta de alguna de las medidas de seguridad referidas precedentemente, le significará al asegurado la caducidad de sus derechos de acuerdo a las previsiones del Artículo 36 de la Ley de Seguros (Nº 17.418), quedando en consecuencia el asegurador liberado de toda responsabilidad emergente de este contrato de seguro."
                )
        );
        return medidasSeguridad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vivienda)) return false;
        Vivienda vivienda = (Vivienda) o;
        return tipoVivienda == vivienda.tipoVivienda && Objects.equals(direccionTomador, vivienda.direccionTomador) && Objects.equals(direccion, vivienda.direccion) && Objects.equals(tieneRejas, vivienda.tieneRejas) && Objects.equals(metrosCuadrados, vivienda.metrosCuadrados);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tipoVivienda, direccionTomador, direccion, tieneRejas, metrosCuadrados);
    }
}
