package ar.com.experta.ventashogarapi.core;

import ar.com.experta.ventashogarapi.core.model.enums.Beneficio;
import ar.com.experta.ventashogarapi.core.model.enums.CanalVenta;
import ar.com.experta.ventashogarapi.core.model.enums.TipoPlan;
import ar.com.experta.ventashogarapi.core.model.metricas.Metricas;
import ar.com.experta.ventashogarapi.core.model.metricas.MetricasDataset;
import ar.com.experta.ventashogarapi.infraestructure.db.MetricasDAO;
import ar.com.experta.ventashogarapi.infraestructure.db.model.VentaDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class MetricasService {

    @Autowired
    private MetricasDAO metricasDAO;

    public Integer getCotizacionesTotales(LocalDate fechaDesde, LocalDate fechaHasta, TipoPlan tipoPlan, CanalVenta canalVenta, Beneficio beneficio) {
        return metricasDAO.getCotizacionesTotales(fechaDesde, fechaHasta != null ? fechaHasta : LocalDate.now(), tipoPlan, canalVenta, beneficio);
    }

    public Integer getEmisionesTotales(LocalDate fechaDesde, LocalDate fechaHasta, TipoPlan tipoPlan, CanalVenta canalVenta, Beneficio beneficio) {
        return metricasDAO.getEmisionesTotales(fechaDesde, fechaHasta != null ? fechaHasta : LocalDate.now(), tipoPlan, canalVenta, beneficio);
    }

    public List<Metricas> getVentasPorDia(LocalDate fechaDesde, LocalDate fechaHasta, TipoPlan tipoPlan, CanalVenta canalVenta, Beneficio beneficio) {
        fechaHasta = fechaHasta != null ? fechaHasta : LocalDate.now();
        Map<String, Integer> cotizaciones = metricasDAO.getCotizacionesPorDia(fechaDesde, fechaHasta, tipoPlan, canalVenta, beneficio);
        Map<String, Integer> emisiones = metricasDAO.getEmisionesPorDia(fechaDesde, fechaHasta, tipoPlan, canalVenta, beneficio);

        List<MetricasDataset> dataCotizaciones = new ArrayList<>();
        List<MetricasDataset> dataEmisiones = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM");
        for (LocalDate fecha = fechaDesde; !fecha.isEqual(fechaHasta); fecha = fecha.plusDays(1)) {
            dataCotizaciones.add(new MetricasDataset(
                    fecha.format(formatter),
                    cotizaciones.get(fecha.format(formatter)) != null ? cotizaciones.get(fecha.format(formatter)) : new Integer(0)
            ));
            dataEmisiones.add(new MetricasDataset(
                    fecha.format(formatter),
                    emisiones.get(fecha.format(formatter)) != null ? emisiones.get(fecha.format(formatter)) : new Integer(0)
            ));
        }
        List<Metricas> metricas = new ArrayList<>();
        metricas.add(new Metricas("Cotizaciones",dataCotizaciones));
        metricas.add(new Metricas("Emisiones",dataEmisiones));
        return metricas;
    }

    public List<Metricas> getVentasPorMes(LocalDate fechaDesde, LocalDate fechaHasta, TipoPlan tipoPlan, CanalVenta canalVenta, Beneficio beneficio) {
        fechaDesde = fechaDesde.withDayOfMonth(1);
        fechaHasta = fechaHasta != null ? fechaHasta.withDayOfMonth(fechaHasta.getMonth().length(fechaHasta.isLeapYear())) : LocalDate.now();

        Map<String, Integer> cotizaciones = metricasDAO.getCotizacionesPorMes(fechaDesde, fechaHasta, tipoPlan, canalVenta, beneficio);
        Map<String, Integer> emisiones = metricasDAO.getEmisionesPorMes(fechaDesde, fechaHasta, tipoPlan, canalVenta, beneficio);

        List<MetricasDataset> dataCotizaciones = new ArrayList<>();
        List<MetricasDataset> dataEmisiones = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/yyyy");
        for (LocalDate fecha = fechaDesde; fecha.isBefore(fechaHasta); fecha = fecha.plusMonths(1)) {
            dataCotizaciones.add(new MetricasDataset(
                    fecha.format(formatter),
                    cotizaciones.get(fecha.format(formatter)) != null ? cotizaciones.get(fecha.format(formatter)) : new Integer(0)
            ));
            dataEmisiones.add(new MetricasDataset(
                    fecha.format(formatter),
                    emisiones.get(fecha.format(formatter)) != null ? emisiones.get(fecha.format(formatter)) : new Integer(0)
            ));
        }
        List<Metricas> metricas = new ArrayList<>();
        metricas.add(new Metricas("Cotizaciones",dataCotizaciones));
        metricas.add(new Metricas("Emisiones",dataEmisiones));
        return metricas;
    }

    public List<VentaDb> getReporteCotizaciones(LocalDate fechaDesde, LocalDate fechaHasta,TipoPlan tipoPlan,CanalVenta canalVenta, Beneficio beneficio) {
        fechaHasta= fechaHasta != null ? fechaHasta :LocalDate.now();
        List<VentaDb> ventaDbList=metricasDAO.getVentasReporteCotizaciones(fechaDesde,fechaHasta,tipoPlan,canalVenta, beneficio);
        return ventaDbList;
    }

    public List<MetricasDataset> getTotales(LocalDate fechaDesde, LocalDate fechaHasta) {
        List<MetricasDataset> dataTotales = new ArrayList<>();
        dataTotales.add(new MetricasDataset("Cotizaciones",this.getCotizacionesTotales(fechaDesde, fechaHasta, null, null, null)));
        dataTotales.add(new MetricasDataset("Emisiones",this.getEmisionesTotales(fechaDesde, fechaHasta, null, null, null)));
        return dataTotales;
    }

    public List<MetricasDataset> getTotalesCotizacionesTipoPlan(LocalDate fechaDesde, LocalDate fechaHasta) {
        List<MetricasDataset> dataTotales = new ArrayList<>();
        for (TipoPlan tipoPlan : TipoPlan.values())
            dataTotales.add(new MetricasDataset(tipoPlan.labelMetricas(),this.getCotizacionesTotales(fechaDesde, fechaHasta, tipoPlan, null, null)));
        return dataTotales;
    }

    public List<MetricasDataset> getTotalesEmisionesTipoPlan(LocalDate fechaDesde, LocalDate fechaHasta) {
        List<MetricasDataset> dataTotales = new ArrayList<>();
        for (TipoPlan tipoPlan : TipoPlan.values())
            dataTotales.add(new MetricasDataset(tipoPlan.labelMetricas(),this.getEmisionesTotales(fechaDesde, fechaHasta, tipoPlan, null, null)));
        return dataTotales;
    }

    public List<MetricasDataset> getTotalesCotizacionesCanalesVenta(LocalDate fechaDesde, LocalDate fechaHasta) {
        List<MetricasDataset> dataTotales = new ArrayList<>();
        for (CanalVenta canalVenta : CanalVenta.values())
            dataTotales.add(new MetricasDataset(canalVenta.label(),this.getCotizacionesTotales(fechaDesde, fechaHasta, null, canalVenta, null)));
        return dataTotales;
    }

    public List<MetricasDataset> getTotalesEmisionesCanalesVenta(LocalDate fechaDesde, LocalDate fechaHasta) {
        List<MetricasDataset> dataTotales = new ArrayList<>();
        for (CanalVenta canalVenta : CanalVenta.values())
            dataTotales.add(new MetricasDataset(canalVenta.label(),this.getEmisionesTotales(fechaDesde, fechaHasta, null, canalVenta, null)));
        return dataTotales;
    }

    public List<MetricasDataset> getTotalesCotizacionesBeneficios(LocalDate fechaDesde, LocalDate fechaHasta) {
        List<MetricasDataset> dataTotales = new ArrayList<>();
        for (Beneficio beneficio : Beneficio.values())
            dataTotales.add(new MetricasDataset(beneficio.label(),this.getCotizacionesTotales(fechaDesde, fechaHasta, null, null, beneficio)));
        return dataTotales;
    }

    public List<MetricasDataset> getTotalesEmisionesBeneficios(LocalDate fechaDesde, LocalDate fechaHasta) {
        List<MetricasDataset> dataTotales = new ArrayList<>();
        for (Beneficio beneficio : Beneficio.values())
            dataTotales.add(new MetricasDataset(beneficio.label(),this.getEmisionesTotales(fechaDesde, fechaHasta, null, null, beneficio)));
        return dataTotales;
    }
}
