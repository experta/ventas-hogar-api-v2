package ar.com.experta.ventashogarapi.core.model;

import java.util.List;

public class NegocioOptionItemPlan extends NegocioOption{
    private List<String> disclaimers;

    public NegocioOptionItemPlan(String value, String label) {
        super(value, label, null);
    }

    public NegocioOptionItemPlan(String value, String label, String description, List<String> disclaimers) {
        super(value, label, description);
        this.disclaimers = disclaimers;
    }

    public List<String> getDisclaimers() {
        return disclaimers;
    }

    public void setDisclaimers(List<String> disclaimers) {
        this.disclaimers = disclaimers;
    }
}
