package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.TipoCobertura;
import ar.com.experta.ventashogarapi.core.model.enums.TipoEspecifico;

import java.math.BigDecimal;
import java.util.Objects;

public abstract class Especifico {

    private TipoCobertura tipoCobertura;
    private TipoEspecifico tipoEspecifico;
    private BigDecimal montoAsegurado;

    public Especifico(TipoEspecifico tipoEspecifico) {
        this.tipoEspecifico = tipoEspecifico;
    }

    public Especifico(TipoEspecifico tipoEspecifico, TipoCobertura tipoCobertura, BigDecimal montoAsegurado) {
        this.tipoCobertura = tipoCobertura;
        this.tipoEspecifico = tipoEspecifico;
        this.montoAsegurado = montoAsegurado;
    }

    public TipoCobertura getTipoCobertura() {
        return tipoCobertura;
    }

    public void setTipoCobertura(TipoCobertura tipoCobertura) {
        this.tipoCobertura = tipoCobertura;
    }

    public TipoEspecifico getTipoEspecifico() {
        return tipoEspecifico;
    }

    public void setTipoEspecifico(TipoEspecifico tipoEspecifico) {
        this.tipoEspecifico = tipoEspecifico;
    }

    public BigDecimal getMontoAsegurado() {
        return montoAsegurado;
    }

    public void setMontoAsegurado(BigDecimal montoAsegurado) {
        this.montoAsegurado = montoAsegurado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Especifico)) return false;
        Especifico that = (Especifico) o;
        return getTipoCobertura() == that.getTipoCobertura() &&
                getTipoEspecifico() == that.getTipoEspecifico() &&
                Objects.equals(getMontoAsegurado(), that.getMontoAsegurado());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTipoCobertura(), getTipoEspecifico(), getMontoAsegurado());
    }
}
