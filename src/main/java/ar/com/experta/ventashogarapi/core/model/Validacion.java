package ar.com.experta.ventashogarapi.core.model;

public class Validacion {
    private Boolean valido;
    private String error;

    public Validacion(Boolean valido, String error) {
        this.valido = valido;
        this.error = error;
    }

    public Boolean getValido() {
        return valido;
    }

    public void setValido(Boolean valido) {
        this.valido = valido;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
