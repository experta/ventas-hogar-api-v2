package ar.com.experta.ventashogarapi.core.model.enums;

public enum CantidadCuotas {

    CUOTAS1("1", "1 Cuota", Boolean.TRUE),
    CUOTAS2("2", "2 Cuotas", Boolean.TRUE),
    CUOTAS3("3", "3 Cuotas", Boolean.TRUE),
    CUOTAS4("4", "4 Cuotas", Boolean.FALSE),
    CUOTAS6("6", "6 Cuotas", Boolean.FALSE),
    CUOTAS8("8", "8 Cuotas", Boolean.FALSE),
    CUOTAS10("10", "10 Cuotas", Boolean.FALSE),
    CUOTAS12("12", "12 Cuotas", Boolean.FALSE);

    private String value;
    private String label;
    private Boolean activo;

    CantidadCuotas(String value, String label, Boolean activo) {
        this.value = value;
        this.label = label;
        this.activo = activo;
    }

    public static CantidadCuotas fromValue(String value) {
        switch (Integer.valueOf(value)) {
            case 1:
                return CUOTAS1;
            case 2:
                return CUOTAS2;
            case 3:
                return CUOTAS3;
            case 4:
                return CUOTAS4;
            case 6:
                return CUOTAS6;
            case 8:
                return CUOTAS8;
            case 10:
                return CUOTAS10;
            case 12:
                return CUOTAS12;
        }
        return null;
    }

    public String value() {
        return value;
    }

    public String label() {
        return label;
    }

    public Boolean activo() {
        return activo;
    }

}
