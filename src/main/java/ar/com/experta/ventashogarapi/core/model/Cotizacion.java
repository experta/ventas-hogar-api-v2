package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.EstadoCotizacion;
import ar.com.experta.ventashogarapi.core.model.enums.IdPlan;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class Cotizacion {

    private String id;
    private IdPlan idPlan;
    private String nombrePlan;
    private List<Cobertura> coberturas;
    private EstadoCotizacion estado;
    private LocalDate fechaCreacion;
    private LocalDate fechaVencimiento;
    private LocalDate fechaAEmitir;
    private BigDecimal prima;
    private BigDecimal premio;
    private BigDecimal impuestos;
    private Integer cuotas;
    private Integer descuento;

    public Cotizacion() {
    }

    public Cotizacion(String id, IdPlan idPlan, List<Cobertura> coberturas, EstadoCotizacion estado, LocalDate fechaCreacion, LocalDate fechaVencimiento, BigDecimal prima, BigDecimal premio, BigDecimal impuestos, Integer cuotas, Integer descuento) {
        this.id = id;
        this.idPlan = idPlan;
        this.coberturas = coberturas;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;
        this.fechaVencimiento = fechaVencimiento;
        this.prima = prima;
        this.premio = premio;
        this.impuestos = impuestos;
        this.cuotas = cuotas;
        this.descuento = descuento;
    }

    public Cotizacion(String id, IdPlan idPlan, List<Cobertura> coberturas, EstadoCotizacion estado, LocalDate fechaCreacion, LocalDate fechaVencimiento, LocalDate fechaAEmitir, BigDecimal prima, BigDecimal premio, BigDecimal impuestos, Integer cuotas, Integer descuento) {
        this.id = id;
        this.idPlan = idPlan;
        this.coberturas = coberturas;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;
        this.fechaVencimiento = fechaVencimiento;
        this.fechaAEmitir = fechaAEmitir;
        this.prima = prima;
        this.premio = premio;
        this.impuestos = impuestos;
        this.cuotas = cuotas;
        this.descuento = descuento;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public IdPlan getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(IdPlan idPlan) {
        this.idPlan = idPlan;
    }

    public List<Cobertura> getCoberturas() {
        return coberturas;
    }

    public void setCoberturas(List<Cobertura> coberturas) {
        this.coberturas = coberturas;
    }

    public EstadoCotizacion getEstado() {
        return estado;
    }

    public void setEstado(EstadoCotizacion estado) {
        this.estado = estado;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public LocalDate getFechaAEmitir() {
        return fechaAEmitir;
    }

    public void setFechaAEmitir(LocalDate fechaAEmitir) {
        this.fechaAEmitir = fechaAEmitir;
    }

    public BigDecimal getPrima() {
        return prima;
    }

    public void setPrima(BigDecimal prima) {
        this.prima = prima;
    }

    public BigDecimal getPremio() {
        return premio;
    }

    public void setPremio(BigDecimal premio) {
        this.premio = premio;
    }

    public BigDecimal getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(BigDecimal impuestos) {
        this.impuestos = impuestos;
    }

    public Integer getCuotas() {
        return cuotas;
    }

    public void setCuotas(Integer cuotas) {
        this.cuotas = cuotas;
    }

    public Integer getDescuento() {
        return descuento;
    }

    public void setDescuento(Integer descuento) {
        this.descuento = descuento;
    }

    public BigDecimal getPremioBonificado(){
        if (descuento != null)
            return premio.multiply(BigDecimal.valueOf(100-descuento.intValue())).divide(BigDecimal.valueOf(100), 0, RoundingMode.HALF_UP);
        return null;
    }

    public BigDecimal getPrimaBonificada() {
        if (descuento != null)
            return prima.multiply(BigDecimal.valueOf(100-descuento.intValue())).divide(BigDecimal.valueOf(100), 0, RoundingMode.HALF_UP);
        return null;
    }

    public BigDecimal getAhorro(){
        if (descuento != null)
            return premio.multiply(BigDecimal.valueOf(descuento.intValue())).divide(BigDecimal.valueOf(100), 0, RoundingMode.HALF_UP);
        return null;
    }

    public String getNombrePlan() {
        return nombrePlan;
    }

    public void setNombrePlan(String nombrePlan) {
        this.nombrePlan = nombrePlan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cotizacion)) return false;
        Cotizacion that = (Cotizacion) o;
        return Objects.equals(id, that.id) && Objects.equals(idPlan, that.idPlan) && Objects.equals(coberturas, that.coberturas) && estado == that.estado && Objects.equals(fechaCreacion, that.fechaCreacion) && Objects.equals(fechaVencimiento, that.fechaVencimiento) && Objects.equals(fechaAEmitir, that.fechaAEmitir) && Objects.equals(prima, that.prima) && Objects.equals(premio, that.premio) && Objects.equals(impuestos, that.impuestos) && Objects.equals(cuotas, that.cuotas) && Objects.equals(descuento, that.descuento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idPlan, coberturas, estado, fechaCreacion, fechaVencimiento, fechaAEmitir, prima, premio, impuestos, cuotas, descuento);
    }
}
