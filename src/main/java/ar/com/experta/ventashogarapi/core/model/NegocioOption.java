package ar.com.experta.ventashogarapi.core.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class NegocioOption {
    private String value;
    private String label;
    private String description;
    @JsonProperty("default")
    private Boolean defecto;
    public NegocioOption() {
    }

    public NegocioOption(String value, String label, String description) {
        this.value = value;
        this.label = label;
        this.description = description;
    }

    public NegocioOption(String value, String label, String description, Boolean defecto) {
        this.value = value;
        this.label = label;
        this.description = description;
        this.defecto = defecto;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getDefecto() {
        return defecto;
    }

    public void setDefecto(Boolean defecto) {
        this.defecto = defecto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NegocioOption)) return false;
        NegocioOption that = (NegocioOption) o;
        return Objects.equals(getValue(), that.getValue()) && Objects.equals(getLabel(), that.getLabel()) && Objects.equals(getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue(), getLabel(), getDescription());
    }
}
