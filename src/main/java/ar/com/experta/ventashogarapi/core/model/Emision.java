package ar.com.experta.ventashogarapi.core.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

public class  Emision {

    private String motivoError;
    private Integer reintentos;
    private LocalDateTime timestamp;
    private Boolean rechazado;

    public Emision() {
    }

    public Emision(String motivoError, Integer reintentos, LocalDateTime timestamp, Boolean rechazado) {
        this.motivoError = motivoError;
        this.reintentos = reintentos;
        this.timestamp = timestamp;
        this.rechazado = rechazado;
    }

    public String getMotivoError() {
        return motivoError;
    }

    public void setMotivoError(String motivoError) {
        this.motivoError = motivoError;
    }

    public LocalDate getFechaEmision() {
        return timestamp.toLocalDate();
    }

    public Integer getReintentos() {
        return reintentos;
    }

    public void setReintentos(Integer reintentos) {
        this.reintentos = reintentos;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Boolean getRechazado() {
        return rechazado;
    }

    public void setRechazado(Boolean rechazado) {
        this.rechazado = rechazado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Emision)) return false;
        Emision emision = (Emision) o;
        return Objects.equals(getMotivoError(), emision.getMotivoError()) && Objects.equals(getReintentos(), emision.getReintentos()) && Objects.equals(getTimestamp(), emision.getTimestamp()) && Objects.equals(getRechazado(), emision.getRechazado());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMotivoError(), getReintentos(), getTimestamp(), getRechazado());
    }
}
