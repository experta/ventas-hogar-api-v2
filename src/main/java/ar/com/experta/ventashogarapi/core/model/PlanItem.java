package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.CategoriaBienEspecifico;
import ar.com.experta.ventashogarapi.core.model.enums.TipoCobertura;
import ar.com.experta.ventashogarapi.core.model.enums.TipoEspecifico;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class PlanItem {

    private Integer orden;
    private TipoCobertura tipoCobertura;
    private List<NegocioOption> opciones;
    private Boolean obligatorio;
    private Boolean obligatorioSiRelacionada;
    private Boolean montoFijo;
    private BigDecimal montoDefault;
    private BigDecimal montoMinimoFijo;
    private BigDecimal montoMaximoFijo;
    private TipoCobertura tipoCoberturaRelacionada;
    private BigDecimal porcentajeMinimoSobreRelacion;
    private BigDecimal porcentajeMaximoSobreRelacion;
    private TipoEspecifico tipoEspecificoCobertura;
    private CategoriaBienEspecifico categoriaBienEspecifico;

    public PlanItem() {
    }

    public PlanItem(Integer orden, TipoCobertura tipoCobertura, BigDecimal montoDefault) {
        this.orden = orden;
        this.tipoCobertura = tipoCobertura;
        this.obligatorio = Boolean.TRUE;
        this.obligatorioSiRelacionada = Boolean.FALSE;
        this.montoFijo = Boolean.TRUE;
        this.montoDefault = montoDefault;
    }


    public PlanItem(Integer orden, TipoCobertura tipoCobertura, Boolean obligatorio, Boolean montoFijo, BigDecimal montoDefault, BigDecimal montoMinimoFijo, BigDecimal montoMaximoFijo, TipoCobertura tipoCoberturaRelacionada, BigDecimal porcentajeMinimoSobreRelacion, BigDecimal porcentajeMaximoSobreRelacion, TipoEspecifico tipoEspecificoCobertura, CategoriaBienEspecifico categoriaBienEspecifico) {
        this.orden = orden;
        this.tipoCobertura = tipoCobertura;
        this.obligatorio = obligatorio;
        this.obligatorioSiRelacionada = Boolean.FALSE;
        this.montoFijo = montoFijo;
        this.montoDefault = montoDefault;
        this.montoMinimoFijo = montoMinimoFijo;
        this.montoMaximoFijo = montoMaximoFijo;
        this.tipoCoberturaRelacionada = tipoCoberturaRelacionada;
        this.porcentajeMinimoSobreRelacion = porcentajeMinimoSobreRelacion;
        this.porcentajeMaximoSobreRelacion = porcentajeMaximoSobreRelacion;
        this.tipoEspecificoCobertura = tipoEspecificoCobertura;
        this.categoriaBienEspecifico = categoriaBienEspecifico;
    }

    public PlanItem(Integer orden, TipoCobertura tipoCobertura, List<NegocioOption> opcionesIncendioEdificio, Boolean obligatorio, Boolean montoFijo, BigDecimal montoDefault, BigDecimal montoMinimoFijo, BigDecimal montoMaximoFijo, TipoCobertura tipoCoberturaRelacionada, BigDecimal porcentajeMinimoSobreRelacion, BigDecimal porcentajeMaximoSobreRelacion, TipoEspecifico tipoEspecificoCobertura, CategoriaBienEspecifico categoriaBienEspecifico) {
        this.orden = orden;
        this.tipoCobertura = tipoCobertura;
        this.opciones = opcionesIncendioEdificio;
        this.obligatorio = obligatorio;
        this.obligatorioSiRelacionada = Boolean.FALSE;
        this.montoFijo = montoFijo;
        this.montoDefault = montoDefault;
        this.montoMinimoFijo = montoMinimoFijo;
        this.montoMaximoFijo = montoMaximoFijo;
        this.tipoCoberturaRelacionada = tipoCoberturaRelacionada;
        this.porcentajeMinimoSobreRelacion = porcentajeMinimoSobreRelacion;
        this.porcentajeMaximoSobreRelacion = porcentajeMaximoSobreRelacion;
        this.tipoEspecificoCobertura = tipoEspecificoCobertura;
        this.categoriaBienEspecifico = categoriaBienEspecifico;
    }

    public PlanItem(Integer orden, TipoCobertura tipoCobertura, Boolean obligatorio, Boolean obligatorioSiRelacionada, Boolean montoFijo, BigDecimal montoDefault, BigDecimal montoMinimoFijo, BigDecimal montoMaximoFijo, TipoCobertura tipoCoberturaRelacionada, BigDecimal porcentajeMinimoSobreRelacion, BigDecimal porcentajeMaximoSobreRelacion, TipoEspecifico tipoEspecificoCobertura, CategoriaBienEspecifico categoriaBienEspecifico) {
        this.orden = orden;
        this.tipoCobertura = tipoCobertura;
        this.obligatorio = obligatorio;
        this.obligatorioSiRelacionada = obligatorioSiRelacionada;
        this.montoFijo = montoFijo;
        this.montoDefault = montoDefault;
        this.montoMinimoFijo = montoMinimoFijo;
        this.montoMaximoFijo = montoMaximoFijo;
        this.tipoCoberturaRelacionada = tipoCoberturaRelacionada;
        this.porcentajeMinimoSobreRelacion = porcentajeMinimoSobreRelacion;
        this.porcentajeMaximoSobreRelacion = porcentajeMaximoSobreRelacion;
        this.tipoEspecificoCobertura = tipoEspecificoCobertura;
        this.categoriaBienEspecifico = categoriaBienEspecifico;
    }


    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public TipoCobertura getTipoCobertura() {
        return tipoCobertura;
    }

    public void setTipoCobertura(TipoCobertura tipoCobertura) {
        this.tipoCobertura = tipoCobertura;
    }

    public List<NegocioOption> getOpciones() {
        return opciones;
    }

    public void setOpciones(List<NegocioOption> opciones) {
        this.opciones = opciones;
    }

    public Boolean getObligatorio() {
        return obligatorio;
    }

    public void setObligatorio(Boolean obligatorio) {
        this.obligatorio = obligatorio;
    }

    public Boolean getMontoFijo() {
        return montoFijo;
    }

    public void setMontoFijo(Boolean montoFijo) {
        this.montoFijo = montoFijo;
    }

    public BigDecimal getMontoDefault() {
        return montoDefault;
    }

    public void setMontoDefault(BigDecimal montoDefault) {
        this.montoDefault = montoDefault;
    }

    public BigDecimal getMontoMinimoFijo() {
        return montoMinimoFijo;
    }

    public void setMontoMinimoFijo(BigDecimal montoMinimoFijo) {
        this.montoMinimoFijo = montoMinimoFijo;
    }

    public BigDecimal getMontoMaximoFijo() {
        return montoMaximoFijo;
    }

    public void setMontoMaximoFijo(BigDecimal montoMaximoFijo) {
        this.montoMaximoFijo = montoMaximoFijo;
    }

    public TipoCobertura getTipoCoberturaRelacionada() {
        return tipoCoberturaRelacionada;
    }

    public void setTipoCoberturaRelacionada(TipoCobertura tipoCoberturaRelacionada) {
        this.tipoCoberturaRelacionada = tipoCoberturaRelacionada;
    }

    public BigDecimal getPorcentajeMinimoSobreRelacion() {
        return porcentajeMinimoSobreRelacion;
    }

    public void setPorcentajeMinimoSobreRelacion(BigDecimal porcentajeMinimoSobreRelacion) {
        this.porcentajeMinimoSobreRelacion = porcentajeMinimoSobreRelacion;
    }

    public BigDecimal getPorcentajeMaximoSobreRelacion() {
        return porcentajeMaximoSobreRelacion;
    }

    public void setPorcentajeMaximoSobreRelacion(BigDecimal porcentajeMaximoSobreRelacion) {
        this.porcentajeMaximoSobreRelacion = porcentajeMaximoSobreRelacion;
    }

    public TipoEspecifico getTipoEspecificoCobertura() {
        return tipoEspecificoCobertura;
    }

    public void setTipoEspecificoCobertura(TipoEspecifico tipoEspecificoCobertura) {
        this.tipoEspecificoCobertura = tipoEspecificoCobertura;
    }

    public CategoriaBienEspecifico getCategoriaBienEspecifico() {
        return categoriaBienEspecifico;
    }

    public void setCategoriaBienEspecifico(CategoriaBienEspecifico categoriaBienEspecifico) {
        this.categoriaBienEspecifico = categoriaBienEspecifico;
    }

    public NegocioOptionItemPlan getDetalleTipoCobertura(){
        List<String> clausulas = new ArrayList<>();
        Locale locale = new Locale("es","ARG");
        NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
        numberFormat.setMinimumFractionDigits(2);
        if (this.tipoCoberturaRelacionada != null) {
            if (this.tipoCobertura != null && this.tipoCobertura.esAdicional() &&
                    ((this.obligatorioSiRelacionada != null && this.obligatorioSiRelacionada) ||
                            (this.obligatorio != null && this.obligatorio))) {
                clausulas.add("Adicional incluido con la cobertura de " +  this.tipoCoberturaRelacionada.label());
            }else if (this.tipoCobertura != null && this.tipoCobertura.esAdicional()) {
                clausulas.add("Adicional optativo de la cobertura de " + this.tipoCoberturaRelacionada.label());
            }else if (this.obligatorioSiRelacionada != null && this.obligatorioSiRelacionada) {
                clausulas.add("Cobertura relacionada obligatoriamente con " + this.tipoCoberturaRelacionada.label());
            }

            if (this.montoFijo != null && this.montoFijo) {
                if (this.montoMinimoFijo != null && this.montoMinimoFijo.intValue() == 0 && this.porcentajeMinimoSobreRelacion != null)
                    clausulas.add("Monto: "
                            + numberFormat.getNumberInstance().format(this.porcentajeMinimoSobreRelacion)
                            + "% de " + this.tipoCoberturaRelacionada.label() + " hasta $"
                            + numberFormat.getNumberInstance().format(this.montoMaximoFijo));
                else if (this.porcentajeMinimoSobreRelacion != null && this.montoMinimoFijo != null && this.montoMaximoFijo != null)
                    clausulas.add("Monto: "
                            + numberFormat.getNumberInstance().format(this.porcentajeMinimoSobreRelacion)
                            + "% de " + this.tipoCoberturaRelacionada.label() + " desde $"
                            + numberFormat.getNumberInstance().format(this.montoMinimoFijo)
                            + " hasta $"
                            + numberFormat.getNumberInstance().format(this.montoMaximoFijo));
            } else {
                if ((this.porcentajeMinimoSobreRelacion != null && this.porcentajeMinimoSobreRelacion.intValue() != 0)
                        || (this.montoMinimoFijo != null && this.montoMinimoFijo.intValue() != 0))
                    if (this.montoMinimoFijo != null && this.montoMinimoFijo.intValue() != 0 && (this.porcentajeMinimoSobreRelacion == null || this.porcentajeMinimoSobreRelacion.intValue() == 0))
                        clausulas.add("Monto mínimo: $"
                                + numberFormat.getNumberInstance().format(this.montoMinimoFijo));
                    else if ((this.montoMinimoFijo == null || this.montoMinimoFijo.intValue() == 0) && this.porcentajeMinimoSobreRelacion != null && this.porcentajeMinimoSobreRelacion.intValue() != 0)
                        clausulas.add("Monto mínimo: "
                                + numberFormat.getNumberInstance().format(this.porcentajeMinimoSobreRelacion)
                                + " % de " + this.tipoCoberturaRelacionada.label());
                    else if (this.montoMinimoFijo != null && this.porcentajeMinimoSobreRelacion != null)
                        clausulas.add("Monto mínimo: $"
                                + numberFormat.getNumberInstance().format(this.montoMinimoFijo)
                                + " o "
                                + numberFormat.getNumberInstance().format(this.porcentajeMinimoSobreRelacion)
                                + " % de " + this.tipoCoberturaRelacionada.label());
                if ((this.porcentajeMaximoSobreRelacion != null && this.porcentajeMaximoSobreRelacion.intValue() != 0)
                        || (this.montoMaximoFijo != null && this.montoMaximoFijo.intValue() != 0))
                    if (this.montoMaximoFijo != null && this.montoMaximoFijo.intValue() != 0 && (this.porcentajeMaximoSobreRelacion == null || this.porcentajeMaximoSobreRelacion.intValue() == 0))
                        clausulas.add("Monto máximo: $"
                                + numberFormat.getNumberInstance().format(this.montoMaximoFijo));
                    else if ((this.montoMaximoFijo == null || this.montoMaximoFijo.intValue() == 0) && this.porcentajeMaximoSobreRelacion != null && this.porcentajeMaximoSobreRelacion.intValue() != 0)
                        clausulas.add("Monto máximo: "
                                + numberFormat.getNumberInstance().format(this.porcentajeMaximoSobreRelacion)
                                + " % de " + this.tipoCoberturaRelacionada.label());
                    else if (this.montoMaximoFijo != null && this.porcentajeMaximoSobreRelacion != null)
                        clausulas.add("Monto máximo: $"
                                + numberFormat.getNumberInstance().format(this.montoMaximoFijo)
                                + " o "
                                + numberFormat.getNumberInstance().format(this.porcentajeMaximoSobreRelacion)
                                + " % de " + this.tipoCoberturaRelacionada.label());
            }
        }
        return new NegocioOptionItemPlan(
                this.tipoCobertura.toString(),
                this.tipoCobertura.label(),
                this.tipoCobertura.description() + (this.tipoCobertura.descriptionFranquicia() != null ? (" " + this.tipoCobertura.descriptionFranquicia()) : ""),
                clausulas.size() != 0 ? clausulas : null);
    }

    public Boolean getObligatorioSiRelacionada() {
        return obligatorioSiRelacionada;
    }

    public void setObligatorioSiRelacionada(Boolean obligatorioSiRelacionada) {
        this.obligatorioSiRelacionada = obligatorioSiRelacionada;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlanItem)) return false;
        PlanItem planItem = (PlanItem) o;
        return Objects.equals(orden, planItem.orden) && tipoCobertura == planItem.tipoCobertura && Objects.equals(opciones, planItem.opciones) && Objects.equals(obligatorio, planItem.obligatorio) && Objects.equals(obligatorioSiRelacionada, planItem.obligatorioSiRelacionada) && Objects.equals(montoFijo, planItem.montoFijo) && Objects.equals(montoDefault, planItem.montoDefault) && Objects.equals(montoMinimoFijo, planItem.montoMinimoFijo) && Objects.equals(montoMaximoFijo, planItem.montoMaximoFijo) && tipoCoberturaRelacionada == planItem.tipoCoberturaRelacionada && Objects.equals(porcentajeMinimoSobreRelacion, planItem.porcentajeMinimoSobreRelacion) && Objects.equals(porcentajeMaximoSobreRelacion, planItem.porcentajeMaximoSobreRelacion) && tipoEspecificoCobertura == planItem.tipoEspecificoCobertura && categoriaBienEspecifico == planItem.categoriaBienEspecifico;
    }

    @Override
    public int hashCode() {
        return Objects.hash(orden, tipoCobertura, opciones, obligatorio, obligatorioSiRelacionada, montoFijo, montoDefault, montoMinimoFijo, montoMaximoFijo, tipoCoberturaRelacionada, porcentajeMinimoSobreRelacion, porcentajeMaximoSobreRelacion, tipoEspecificoCobertura, categoriaBienEspecifico);
    }
}
