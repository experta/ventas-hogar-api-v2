package ar.com.experta.ventashogarapi.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

@Component
public class AdministracionService {

    @Autowired
    private CacheManager cacheManager;

    public void flushCacheEnlatados() {
        cacheManager.getCache("cacheEnlatados").clear();
    }

    public void flushCachePorcentajes() {
        cacheManager.getCache("cachePorcentajesEstabilizacion").clear();
    }

}
