package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.TipoCobertura;

import java.math.BigDecimal;
import java.util.Objects;

public class Cobertura {

    private TipoCobertura tipoCobertura;
    private BigDecimal montoAsegurado;
    private TipoCobertura opcion;
    private BigDecimal montoMinimoRecomendado;
    private BigDecimal montoMaximoRecomendado;

    public Cobertura() {
    }

    public Cobertura(TipoCobertura tipoCobertura, BigDecimal montoAsegurado, TipoCobertura opcion) {
        this.tipoCobertura = tipoCobertura;
        this.montoAsegurado = montoAsegurado;
        this.opcion = opcion;
    }

    public Cobertura(TipoCobertura tipoCobertura, BigDecimal montoAsegurado, TipoCobertura opcion, BigDecimal montoMinimoRecomendado, BigDecimal montoMaximoRecomendado) {
        this.tipoCobertura = tipoCobertura;
        this.montoAsegurado = montoAsegurado;
        this.opcion = opcion;
        this.montoMinimoRecomendado = montoMinimoRecomendado;
        this.montoMaximoRecomendado = montoMaximoRecomendado;
    }

    public TipoCobertura getTipoCobertura() {
        return tipoCobertura;
    }

    public void setTipoCobertura(TipoCobertura tipoCobertura) {
        this.tipoCobertura = tipoCobertura;
    }

    public BigDecimal getMontoAsegurado() {
        return montoAsegurado;
    }

    public void setMontoAsegurado(BigDecimal montoAsegurado) {
        this.montoAsegurado = montoAsegurado;
    }

    public TipoCobertura getOpcion() {
        return opcion;
    }

    public void setOpcion(TipoCobertura opcion) {
        this.opcion = opcion;
    }

    public BigDecimal getMontoMinimoRecomendado() {
        return montoMinimoRecomendado;
    }

    public void setMontoMinimoRecomendado(BigDecimal montoMinimoRecomendado) {
        this.montoMinimoRecomendado = montoMinimoRecomendado;
    }

    public BigDecimal getMontoMaximoRecomendado() {
        return montoMaximoRecomendado;
    }

    public void setMontoMaximoRecomendado(BigDecimal montoMaximoRecomendado) {
        this.montoMaximoRecomendado = montoMaximoRecomendado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cobertura)) return false;
        Cobertura cobertura = (Cobertura) o;
        return getTipoCobertura() == cobertura.getTipoCobertura() && Objects.equals(getMontoAsegurado(), cobertura.getMontoAsegurado()) && getOpcion() == cobertura.getOpcion() && Objects.equals(getMontoMinimoRecomendado(), cobertura.getMontoMinimoRecomendado()) && Objects.equals(getMontoMaximoRecomendado(), cobertura.getMontoMaximoRecomendado());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTipoCobertura(), getMontoAsegurado(), getOpcion(), getMontoMinimoRecomendado(), getMontoMaximoRecomendado());
    }
}
