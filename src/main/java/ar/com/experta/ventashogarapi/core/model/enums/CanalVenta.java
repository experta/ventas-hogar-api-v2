package ar.com.experta.ventashogarapi.core.model.enums;

public enum CanalVenta {

    WEB_PAS("Web PAS") ,
    WEB_ECOMMERCE_WL("Ecommerce") ,
    WEB_SERVICE ("WebService"),
    HUB_DIRECTV("Hub DirecTV");

    private String label;

    CanalVenta(final String label) {
        this.label = label;
    }

    public String label () {
        return label;
    }
}
