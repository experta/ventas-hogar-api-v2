package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.MedioPago;

public class PagoOpBancariaDTV extends Pago{

    public PagoOpBancariaDTV() {
        super(MedioPago.OP_BANCARIA_DTV);
    }

}
