package ar.com.experta.ventashogarapi.core;

import ar.com.experta.ventashogarapi.commons.exceptions.UnauthorizedException;
import ar.com.experta.ventashogarapi.commons.exceptions.badgateway.BadGatewayException;
import ar.com.experta.ventashogarapi.commons.exceptions.badrequest.RefreshJWTIncorrectoException;
import ar.com.experta.ventashogarapi.commons.exceptions.badrequest.UsuarioPasswordIncorrectosException;
import ar.com.experta.ventashogarapi.core.model.Login;
import ar.com.experta.ventashogarapi.core.model.enums.CanalVenta;
import ar.com.experta.ventashogarapi.infraestructure.keycloakclient.KeycloakClient;
import ar.com.experta.ventashogarapi.infraestructure.keycloakclient.model.UsuarioKeycloak;
import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Date;
import java.util.List;

@Service
public class KeycloakService {

    @Autowired
    private KeycloakClient keycloakClient;

    @Value("${verificarRoles}")
    private boolean VERIFICAR_ROLES;
    @Value("${listOfAuthorizedApiKeys}")
    private List<String> apiKeysAuthorized;
    @Value("${authorization.gateway-ecommerce-apikey}")
    private String GATEWAY_ECOMMERCE_APIKEY;
    @Value("${authorization.hub-directv-apikey}")
    private String HUB_DIRECTV_APIKEY;
    public static final String ROL_CANAL_WEB_SERVICE = "ROL_BROKER";
    public static final String ROL_CANAL_WEBPAS = "ROL_SEGUROS";

    public Login login(String username, String password) {
        try {
            UsuarioKeycloak usuario = keycloakClient.login(username, password);
            return new Login(usuario.getAccess_token(),usuario.getRefresh_token());
        } catch (HttpClientErrorException error) {
            if (error.getStatusCode().equals(HttpStatus.BAD_REQUEST)){
                throw new UsuarioPasswordIncorrectosException();
            }
            throw new BadGatewayException("Hubo un error al iniciar sesión", error.getMessage());
        }
    }

    public Login refreshToken(String requestRefreshTokenKeycloak) {
        try {
            UsuarioKeycloak usuario = keycloakClient.refreshToken(requestRefreshTokenKeycloak);
            return new Login(usuario.getAccess_token(),usuario.getRefresh_token());
        } catch (HttpClientErrorException error) {
            if (error.getStatusCode().equals(HttpStatus.BAD_REQUEST)){
                throw new RefreshJWTIncorrectoException();
            }
            throw new BadGatewayException("Hubo un error al refrescar el JWT", error.getMessage());
        }
    }

    public void validateJwt(String jwtBearer){
        if (VERIFICAR_ROLES) {
            try {
                jwtBearer = jwtBearer.replace("Bearer", "").trim()
                        .replace("bearer", "");
                if (apiKeysAuthorized.contains(jwtBearer))
                    return;
                DecodedJWT jwt = JWT.decode(jwtBearer);
                if (jwt.getExpiresAt().before(new Date()))
                    throw new UnauthorizedException("Sesion vencida");
                List<String> roles = ((List) jwt.getClaim("realm_access").asMap().get("roles"));
                if (!roles.contains("ROL_BROKER") && !roles.contains("ROL_SEGUROS"))
                    throw new UnauthorizedException("El usuario no tiene permisos para consumir el recurso");
            } catch (Exception e) {
                throw new UnauthorizedException(e.getClass().equals(UnauthorizedException.class) ? e.getMessage() : "Token inválido");
            }
        }
    }

    public CanalVenta getCanalVenta(String jwtBearer) {
        try {
            jwtBearer = jwtBearer.replace("Bearer", "").trim()
                    .replace("bearer", "");
            if (jwtBearer.equals(GATEWAY_ECOMMERCE_APIKEY))
                return CanalVenta.WEB_ECOMMERCE_WL;
            if (jwtBearer.equals(HUB_DIRECTV_APIKEY))
                return CanalVenta.HUB_DIRECTV;

            DecodedJWT jwt = JWT.decode(jwtBearer);
            List<String> roles = ((List) jwt.getClaim("realm_access").asMap().get("roles"));
            if (roles.contains(ROL_CANAL_WEB_SERVICE))
                return CanalVenta.WEB_SERVICE;
            if (roles.contains(ROL_CANAL_WEBPAS))
                return CanalVenta.WEB_PAS;
        } catch (Exception e) {
            if (!VERIFICAR_ROLES)
                return CanalVenta.WEB_PAS;
            throw new UnauthorizedException(e.getClass().equals(UnauthorizedException.class) ? e.getMessage() : "Token inválido");
        }
        throw new UnauthorizedException("Token inválido para canal de venta");
    }
}
