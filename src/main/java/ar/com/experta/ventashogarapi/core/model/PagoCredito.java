package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.MedioPago;

import java.util.Objects;

public class PagoCredito extends Pago{

    private String empresaTarjeta;
    private String numeroTarjeta;
    private Integer anioVencimiento;
    private Integer mesVencimiento;

    public PagoCredito() {
        super(MedioPago.CREDITO);
    }

    public PagoCredito(String empresaTarjeta, String numeroTarjeta, Integer anioVencimiento, Integer mesVencimiento) {
        super(MedioPago.CREDITO);
        this.empresaTarjeta = empresaTarjeta;
        this.numeroTarjeta = numeroTarjeta;
        this.anioVencimiento = anioVencimiento;
        this.mesVencimiento = mesVencimiento;
    }

    public String getEmpresaTarjeta() {
        return empresaTarjeta;
    }

    public void setEmpresaTarjeta(String empresaTarjeta) {
        this.empresaTarjeta = empresaTarjeta;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public Integer getAnioVencimiento() {
        return anioVencimiento;
    }

    public void setAnioVencimiento(Integer anioVencimiento) {
        this.anioVencimiento = anioVencimiento;
    }

    public Integer getMesVencimiento() {
        return mesVencimiento;
    }

    public void setMesVencimiento(Integer mesVencimiento) {
        this.mesVencimiento = mesVencimiento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PagoCredito)) return false;
        if (!super.equals(o)) return false;
        PagoCredito that = (PagoCredito) o;
        return getEmpresaTarjeta() == that.getEmpresaTarjeta() &&
                Objects.equals(getNumeroTarjeta(), that.getNumeroTarjeta()) &&
                Objects.equals(getAnioVencimiento(), that.getAnioVencimiento()) &&
                Objects.equals(getMesVencimiento(), that.getMesVencimiento());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getEmpresaTarjeta(), getNumeroTarjeta(), getAnioVencimiento(), getMesVencimiento());
    }
}
