package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.CondicionImpositiva;
import ar.com.experta.ventashogarapi.core.model.enums.Sexo;
import ar.com.experta.ventashogarapi.core.model.enums.TipoDocumento;
import ar.com.experta.ventashogarapi.core.model.enums.TipoPersona;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;
import java.util.Objects;

public class Cliente {

    private Documento documento;
    private String nombre;
    private String apellido;
    private String razonSocial;
    private String email;
    private String nacionalidad;
    private LocalDate fechaNacimiento;
    private Sexo sexo;
    private String actividad;
    private Boolean pep;
    private String motivoPep;
    private Boolean sujetoObligado;
    private Telefono telefono;
    private String telefonoTexto;
    private Direccion direccion;
    private TipoPersona tipoPersona;
    private CondicionImpositiva condicionImpositiva;

    public Cliente() {
    }

    //Datos necesarios para cotizar
    public Cliente(Documento documento, String nombre, String apellido, String razonSocial, String email, Telefono telefono, String telefonoTexto, TipoPersona tipoPersona, CondicionImpositiva condicionImpositiva) {        this.documento = documento;
        this.nombre = nombre;
        this.apellido = apellido;
        this.razonSocial = razonSocial;
        this.email = email;
        this.telefono = telefono;
        this.telefonoTexto = telefonoTexto;
        this.tipoPersona = tipoPersona;
        this.condicionImpositiva = condicionImpositiva;
    }

    //Datos necesarios para emitir
    public Cliente(Documento documento, String nombre, String apellido, String razonSocial, String email, String nacionalidad, LocalDate fechaNacimiento,  Sexo sexo, String actividad, Boolean pep, String motivoPep, Boolean sujetoObligado, Telefono telefono, String telefonoTexto, Direccion direccion, TipoPersona tipoPersona, CondicionImpositiva condicionImpositiva) {
        this.documento = documento;
        this.nombre = nombre;
        this.apellido = apellido;
        this.razonSocial = razonSocial;
        this.email = email;
        this.nacionalidad = nacionalidad;
        this.sexo = sexo;
        this.fechaNacimiento = fechaNacimiento;
        this.telefono = telefono;
        this.telefonoTexto = telefonoTexto;
        this.direccion = direccion;
        this.tipoPersona = tipoPersona;
        this.condicionImpositiva = condicionImpositiva;
        this.actividad = actividad;
        this.pep = pep;
        this.motivoPep = motivoPep;
        this.sujetoObligado = sujetoObligado;
    }

    public Sexo getSexo() {
        if ( this.sexo != null)
            return sexo;
        if (this.tipoPersona != null && this.tipoPersona.equals(TipoPersona.FISICA) &&
                this.documento != null && this.documento.getTipo() != null && this.documento.getTipo().equals(TipoDocumento.CUIT)){
            if (this.documento != null && this.documento.getNumero() != null && this.documento.getNumero().startsWith(Documento.PREFIJO_CUIT_FEMENINO))
                return Sexo.FEMENINO;
            if (this.documento != null && this.documento.getNumero() != null && this.documento.getNumero().startsWith(Documento.PREFIJO_CUIT_MASCULINO))
                return Sexo.MASCULINO;
        }
        return null;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public CondicionImpositiva getCondicionImpositiva() {
        return condicionImpositiva;
    }

    public void setCondicionImpositiva(CondicionImpositiva condicionImpositiva) {
        this.condicionImpositiva = condicionImpositiva;
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Telefono getTelefono() {
        return telefono;
    }

    public void setTelefono(Telefono telefono) {
        this.telefono = telefono;
    }

    public String getTelefonoTexto() {
        return telefonoTexto;
    }

    public void setTelefonoTexto(String telefonoTexto) {
        this.telefonoTexto = telefonoTexto;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public TipoPersona getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(TipoPersona tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public Boolean getPep() {
        return pep;
    }

    public void setPep(Boolean pep) {
        this.pep = pep;
    }

    public String getMotivoPep() {
        return motivoPep;
    }

    public void setMotivoPep(String motivoPep) {
        this.motivoPep = motivoPep;
    }

    public Boolean getSujetoObligado() {
        return sujetoObligado;
    }

    public void setSujetoObligado(Boolean sujetoObligado) {
        this.sujetoObligado = sujetoObligado;
    }

    @JsonIgnore
    public String getApellidoNombre() {
        return this.apellido != null && this.nombre != null ?
                this.apellido + ", " + this.nombre : null;
    }

    @JsonIgnore
    public String getNombreCompleto() {
        return this.apellido != null && this.nombre != null ?
                this.nombre + " " + this.apellido : null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cliente)) return false;
        Cliente cliente = (Cliente) o;
        return Objects.equals(documento, cliente.documento) && Objects.equals(nombre, cliente.nombre) && Objects.equals(apellido, cliente.apellido) && Objects.equals(razonSocial, cliente.razonSocial) && Objects.equals(email, cliente.email) && Objects.equals(nacionalidad, cliente.nacionalidad) && Objects.equals(fechaNacimiento, cliente.fechaNacimiento) && sexo == cliente.sexo && Objects.equals(actividad, cliente.actividad) && Objects.equals(pep, cliente.pep) && Objects.equals(motivoPep, cliente.motivoPep) && Objects.equals(sujetoObligado, cliente.sujetoObligado) && Objects.equals(telefono, cliente.telefono) && Objects.equals(direccion, cliente.direccion) && tipoPersona == cliente.tipoPersona && condicionImpositiva == cliente.condicionImpositiva;
    }

    @Override
    public int hashCode() {
        return Objects.hash(documento, nombre, apellido, razonSocial, email, nacionalidad, fechaNacimiento, sexo, actividad, pep, motivoPep, sujetoObligado, telefono, direccion, tipoPersona, condicionImpositiva);
    }
}
