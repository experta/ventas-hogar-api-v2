package ar.com.experta.ventashogarapi.core.model.enums;

public enum EstadoCotizacion {
    COTIZADA,
    VENCIDA,
    A_EMITIR,
    EMITIDA;
}
