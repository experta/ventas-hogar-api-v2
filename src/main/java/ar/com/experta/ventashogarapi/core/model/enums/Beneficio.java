package ar.com.experta.ventashogarapi.core.model.enums;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Arrays;
import java.util.List;

public enum Beneficio {
    AUTOS_HOGAR ("Autos+Hogar",
            "Descuento del 30% sobre la póliza de Hogar durante 1 año. Válido para clientes con póliza de Autos TC o TR vigentes al día de emisión",
            "El cliente tiene poliza activa de autos",
            30,
            Arrays.asList(TipoProductos.HOGAR_EXPERTA_PRODUCTORES),
            Arrays.asList(TipoPlan.CONFIGURABLE)),
    EMPLEADOS_GW ("Benef. Empleados Grupo Werthein",
            "Descuento del 40% sobre la póliza de Hogar; válido para empleados del Grupo Werthein",
            "El cliente es empleado del Grupo Werthein",
            40,
            Arrays.asList(TipoProductos.HOGAR_BENEFICIO_WERTHEIN),
            Arrays.asList(TipoPlan.CONFIGURABLE, TipoPlan.ENLATADO)),
    EMPLEADOS_DIRECTV ("Benef. Empleados Directv",
            "Descuento del 40% sobre la póliza de Hogar; válido para empleados de Directv",
            "El cliente es empleado de DirecTV",
            40,
            Arrays.asList(TipoProductos.HOGAR_BENEFICIO_DIRECTV),
            Arrays.asList(TipoPlan.CONFIGURABLE));

    private String label;
    private String descripcion;
    private String validacionMessage;
    private Integer descuento;
    private List<TipoProductos> tiposProductos;
    private List<TipoPlan> tiposPlanes;

    Beneficio(final String label, final String descripcion, String validacionMessage, final int descuento, List<TipoProductos> tiposProductos, List<TipoPlan> tiposPlanes) {
        this.descuento = Integer.valueOf(descuento);
        this.label = label;
        this.descripcion = descripcion;
        this.validacionMessage = validacionMessage;
        this.tiposProductos = tiposProductos;
        this.tiposPlanes = tiposPlanes;
    }

    public Integer descuento () {
        return descuento;
    }

    public String label () { return label;}

    public String descripcion (){ return descripcion;}

    @JsonIgnore
    public List<TipoProductos> tiposProductos(){return tiposProductos;}

    @JsonIgnore
    public List<TipoPlan> tiposPlanes(){return tiposPlanes;}

    public String validacionMessage() {
        return validacionMessage;
    }
}
