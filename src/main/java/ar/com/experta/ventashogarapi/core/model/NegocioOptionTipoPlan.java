package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.Beneficio;
import ar.com.experta.ventashogarapi.core.model.enums.OpcionPlan;
import ar.com.experta.ventashogarapi.core.model.enums.TipoPlan;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class NegocioOptionTipoPlan extends NegocioOption{

    private TipoPlan tipoPlan;
    private Set<Beneficio> beneficios;

    public NegocioOptionTipoPlan(TipoPlan tipoPlan, Set<Beneficio> beneficios, Boolean defecto) {
        super(tipoPlan.toString(), tipoPlan.label(), tipoPlan.description(), defecto);
        this.tipoPlan = tipoPlan;
        this.beneficios = beneficios;
    }

    public Set<Beneficio> getBeneficios() {
        return beneficios;
    }

    public Boolean getEnlatado() {
        return tipoPlan.enlatado();
    }

    public List<NegocioOption> getTiposMedioPago() {
        return this.tipoPlan.tiposMedioPago().stream().map(m -> new NegocioOption(m.toString(), m.label(), null)).collect(Collectors.toList());
    }

    public List<OpcionPlan> getOpciones() {
        return this.tipoPlan.opciones() != null ? this.tipoPlan.opciones() : Collections.emptyList();
    }

}
