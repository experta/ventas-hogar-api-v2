package ar.com.experta.ventashogarapi.core.model;

public enum ModalidadComision {

    FULL(Float.valueOf(20), "20%", Boolean.TRUE);

    private Float comision;
    private String label;
    private Boolean esDefault;

    ModalidadComision(Float comision, String label, Boolean esDefault) {
        this.comision = comision;
        this.label = label;
        this.esDefault = esDefault;
    }

    public String label() {
        return label;
    }

    public Float getComision() {
        return comision;
    }

    public static ModalidadComision getDefault(){
        return FULL;
    }
}
