package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.TipoBienEspecifico;
import ar.com.experta.ventashogarapi.core.model.enums.TipoCobertura;
import ar.com.experta.ventashogarapi.core.model.enums.TipoEspecifico;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;

public class BienEspecifico extends Especifico {

    private TipoBienEspecifico tipoBienEspecifico;
    private String modelo;
    private String numeroSerie;

    public BienEspecifico() {
        super(TipoEspecifico.BIEN_ESPECIFICO);
    }

    public BienEspecifico(TipoCobertura tipoCobertura, BigDecimal montoAsegurado, TipoBienEspecifico tipoBienEspecifico, String modelo, String numeroSerie) {
        super(TipoEspecifico.BIEN_ESPECIFICO, tipoCobertura, montoAsegurado);
        this.tipoBienEspecifico = tipoBienEspecifico;
        this.modelo = modelo;
        this.numeroSerie = numeroSerie;
    }

    public TipoBienEspecifico getTipoBienEspecifico() {
        return tipoBienEspecifico;
    }

    public void setTipoBienEspecifico(TipoBienEspecifico tipoBienEspecifico) {
        this.tipoBienEspecifico = tipoBienEspecifico;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    @JsonIgnore
    public String getDescripcionCompleta(){
        return  (this.modelo != null ? this.modelo : "") +
                (this.numeroSerie != null ? (this.modelo != null ? " - " : "") + this.numeroSerie : "");
    }


}
