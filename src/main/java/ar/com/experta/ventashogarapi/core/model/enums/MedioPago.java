package ar.com.experta.ventashogarapi.core.model.enums;

public enum MedioPago {
    DEBITO ("Débito Automático", TipoMedioPago.AUTOMATICO),
    CREDITO ("Tarjeta de Crédito", TipoMedioPago.AUTOMATICO),
    CUPONERA("Cuponera", TipoMedioPago.MANUAL),
    OP_BANCARIA_DTV ("Operatoria Bancaria", TipoMedioPago.OP_BANCARIA_DTV)
    ;

    private String label;
    private TipoMedioPago tipoMedioPago;

    MedioPago(String label, TipoMedioPago tipoMedioPago) {
        this.label = label;
        this.tipoMedioPago = tipoMedioPago;
    }

    public String label() {
        return label;
    }

    public TipoMedioPago tipoMedioPago() {
        return tipoMedioPago;
    }

}
