package ar.com.experta.ventashogarapi.core.model;

import ar.com.experta.ventashogarapi.core.model.enums.*;
import ar.com.experta.ventashogarapi.infraestructure.db.model.VentaDb;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class Venta implements Comparable<Venta>{

    private String id;
    private LocalDateTime fechaCreacion;
    private LocalDateTime fechaUltimaModificacion;
    private CanalVenta canalVenta;
    private Vendedor vendedor;
    private Cliente cliente;
    private TipoPlan tipoPlan;
    private List<Plan> planes;
    private Vivienda vivienda;
    private Pago pago;
    private LocalDate fechaInicioCobertura;
    private List<Cotizacion> cotizaciones;
    private List<Especifico> especificos;
    private TipoMedioPago tipoMedioPago;
    private CantidadCuotas cantidadCuotas;
    private ModalidadComision modalidadComision;
    private Boolean clausulaEstabilizacion;
    private String porcentajeEstabilizacion;
    private Beneficio beneficio;
    private String numeroReferencia;
    private Validacion validacionBeneficio;

    private String nroPoliza;
    private Boolean emitiendo;
    private Emision emision;

    private VentaDb ventaDb;

    public Venta() {
    }

    //Constructor de nueva venta
    public Venta(CanalVenta canalVenta, LocalDateTime fechaCreacion, LocalDateTime fechaUltimaModificacion, Vendedor vendedor, Cliente cliente, TipoPlan tipoPlan, List<Plan> planes, Vivienda vivienda, List<Cotizacion> cotizaciones, LocalDate fechaInicioCobertura, TipoMedioPago tipoMedioPago, CantidadCuotas cantidadCuotas, ModalidadComision modalidadComision, Boolean clausulaEstabilizacion, Beneficio beneficio) {
        this.canalVenta = canalVenta;
        this.fechaCreacion = fechaCreacion;
        this.fechaUltimaModificacion = fechaUltimaModificacion;
        this.vendedor = vendedor;
        this.cliente = cliente;
        this.tipoPlan = tipoPlan;
        this.planes = planes;
        this.vivienda = vivienda;
        this.cotizaciones = cotizaciones;
        this.fechaInicioCobertura = fechaInicioCobertura;
        this.beneficio = beneficio;
        this.tipoMedioPago = tipoMedioPago;
        this.cantidadCuotas = cantidadCuotas;
        this.modalidadComision = modalidadComision;
        this.clausulaEstabilizacion = clausulaEstabilizacion;
    }

    // Constructor para VentaDB
    public Venta(String id, CanalVenta canalVenta, LocalDateTime fechaCreacion, LocalDateTime fechaUltimaModificacion, Vendedor vendedor, Cliente cliente, TipoPlan tipoPlan, List<Plan> planes, Vivienda vivienda, Pago pago, LocalDate fechaInicioCobertura, List<Cotizacion> cotizaciones, Beneficio beneficio, String nroPoliza, Emision emision, List<Especifico> especificos, TipoMedioPago tipoMedioPago, CantidadCuotas cantidadCuotas, ModalidadComision modalidadComision, Boolean clausulaEstabilizacion, String numeroReferencia, Boolean emitiendo) {
        this.id = id;
        this.canalVenta = canalVenta;
        this.fechaCreacion = fechaCreacion;
        this.fechaUltimaModificacion = fechaUltimaModificacion;
        this.vendedor = vendedor;
        this.cliente = cliente;
        this.tipoPlan = tipoPlan;
        this.planes = planes;
        this.vivienda = vivienda;
        this.pago = pago;
        this.fechaInicioCobertura = fechaInicioCobertura;
        this.cotizaciones = cotizaciones;
        this.beneficio = beneficio;
        this.nroPoliza = nroPoliza;
        this.emision = emision;
        this.especificos = especificos;
        this.tipoMedioPago = tipoMedioPago;
        this.cantidadCuotas = cantidadCuotas;
        this.emitiendo = emitiendo;
        this.modalidadComision = modalidadComision;
        this.clausulaEstabilizacion = clausulaEstabilizacion;
        this.numeroReferencia = numeroReferencia;
    }

    public int compareTo(Venta venta) {
        return this.getFechaUltimaModificacion().compareTo(venta.getFechaUltimaModificacion());
    }

    public EstadoVenta getEstadoVenta() {
        if (this.emision != null) {
            if (this.nroPoliza != null)
                return EstadoVenta.EMITIDA;
            else if (this.emision.getRechazado())
                return EstadoVenta.RECHAZADA;
            else if (this.emitiendo != null && this.emitiendo)
                return EstadoVenta.EMITIENDO;
            else
                return EstadoVenta.EMISION_FALLIDA;
        }
        if (this.emitiendo != null && this.emitiendo){
            return EstadoVenta.EMITIENDO;
        }
        if (this.cotizaciones.stream().filter(cotizacion -> !cotizacion.getIdPlan().activo()).findAny().isPresent())
            return EstadoVenta.INHABILITADA;
        if (this.cliente!=null && (this.cliente.getApellido()!=null || this.cliente.getNombre()!=null || this.cliente.getRazonSocial()!=null)) {
            if ( this.cotizaciones != null && this.cotizaciones.stream().filter(cotizacion -> cotizacion.getEstado().equals(EstadoCotizacion.A_EMITIR)).findAny().isPresent())
                return EstadoVenta.A_EMITIR;
            if (this.cotizaciones != null && this.cotizaciones.stream().filter(cotizacion -> cotizacion.getFechaVencimiento().isAfter(LocalDate.now())).findAny().isPresent())
                return EstadoVenta.COTIZADA;
            return EstadoVenta.VENCIDA;
        }
        if ( this.cotizaciones != null && this.cotizaciones.stream().filter(cotizacion -> cotizacion.getEstado().equals(EstadoCotizacion.A_EMITIR)).findAny().isPresent())
            return EstadoVenta.BORRADOR_A_EMITIR;
        return EstadoVenta.BORRADOR;
    }

    public String getPlan() {
        if (this.cotizaciones != null) {
            Optional<Cotizacion> cotizacion = this.getCotizaciones()
                    .stream()
                    .filter(c -> c.getEstado().equals(EstadoCotizacion.EMITIDA) || c.getEstado().equals(EstadoCotizacion.A_EMITIR))
                    .findFirst();
            if (cotizacion.isPresent() && cotizacion.get().getNombrePlan() != null)
                return cotizacion.get().getNombrePlan();
            else if (tipoPlan != null)
                return tipoPlan.label();
        }
        return null;
    }

    @JsonProperty("cantidadCuotas")
    public String getCantidadCuotasValue() {
        return cantidadCuotas != null ? cantidadCuotas.value() : null;
    }

    @JsonProperty("canalVenta")
    public NegocioOption getCanalVentaOption() {
        return canalVenta != null ? new NegocioOption(canalVenta.toString(), canalVenta.label(),null) : null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Plan> getPlanes() {
        return planes;
    }

    public void setPlanes(List<Plan> planes) {
        this.planes = planes;
    }

    public LocalDate getFechaInicioCobertura() {
        return fechaInicioCobertura;
    }

    public void setFechaInicioCobertura(LocalDate fechaInicioCobertura) {
        this.fechaInicioCobertura = fechaInicioCobertura;
    }

    public TipoPlan getTipoPlan() {
        return tipoPlan;
    }

    public void setTipoPlan(TipoPlan tipoPlan) {
        this.tipoPlan = tipoPlan;
    }

    public LocalDateTime getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDateTime fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Vivienda getVivienda() {
        return vivienda;
    }

    public void setVivienda(Vivienda vivienda) {
        this.vivienda = vivienda;
    }

    public Pago getPago() {
        return pago;
    }

    public void setPago(Pago pago) {
        this.pago = pago;
    }

    public List<Cotizacion> getCotizaciones() {
        return cotizaciones;
    }

    public void setCotizaciones(List<Cotizacion> cotizaciones) {
        this.cotizaciones = cotizaciones;
    }

    public Emision getEmision() {
        return emision;
    }

    public void setEmision(Emision emision) {
        this.emision = emision;
    }

    public LocalDateTime getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(LocalDateTime fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    public ModalidadComision getModalidadComision() {
        return modalidadComision;
    }

    public void setModalidadComision(ModalidadComision modalidadComision) {
        this.modalidadComision = modalidadComision;
    }

    public Boolean getClausulaEstabilizacion() {
        return clausulaEstabilizacion;
    }

    public void setClausulaEstabilizacion(Boolean clausulaEstabilizacion) {
        this.clausulaEstabilizacion = clausulaEstabilizacion;
    }

    public String getPorcentajeEstabilizacion() {
        return porcentajeEstabilizacion;
    }

    public void setPorcentajeEstabilizacion(String porcentajeEstabilizacion) {
        this.porcentajeEstabilizacion = porcentajeEstabilizacion;
    }

    public Beneficio getBeneficio() {
        return beneficio;
    }

    public void setBeneficio(Beneficio beneficio) {
        this.beneficio = beneficio;
    }

    public String getNroPoliza() {
        return nroPoliza;
    }

    public void setNroPoliza(String nroPoliza) {
        this.nroPoliza = nroPoliza;
    }

    public List<Especifico> getEspecificos() {
        return especificos;
    }

    public void setEspecificos(List<Especifico> especificos) {
        this.especificos = especificos;
    }

    public TipoMedioPago getTipoMedioPago() {
        return tipoMedioPago;
    }

    public void setTipoMedioPago(TipoMedioPago tipoMedioPago) {
        this.tipoMedioPago = tipoMedioPago;
    }

    public Validacion getValidacionBeneficio() {
        return validacionBeneficio;
    }

    public void setValidacionBeneficio(Validacion validacionBeneficio) {
        this.validacionBeneficio = validacionBeneficio;
    }

    public String getNumeroReferencia() {
        return numeroReferencia;
    }

    public void setNumeroReferencia(String numeroReferencia) {
        this.numeroReferencia = numeroReferencia;
    }

    @JsonIgnore
    public CantidadCuotas getCantidadCuotas() {
        return cantidadCuotas;
    }

    public void setCantidadCuotas(CantidadCuotas cantidadCuotas) {
        this.cantidadCuotas = cantidadCuotas;
    }

    @JsonIgnore
    public CanalVenta getCanalVenta() {
        return canalVenta;
    }

    public void setCanalVenta(CanalVenta canalVenta) {
        this.canalVenta = canalVenta;
    }

    @JsonIgnore
    public Boolean getEmitiendo() {
        return emitiendo;
    }

    public void setEmitiendo(Boolean emitiendo) {
        this.emitiendo = emitiendo;
    }

    @JsonIgnore
    public VentaDb getVentaDb() {
        return ventaDb;
    }

    public void setVentaDb(VentaDb ventaDb) {
        this.ventaDb = ventaDb;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Venta)) return false;
        Venta venta = (Venta) o;
        return Objects.equals(id, venta.id) && Objects.equals(fechaCreacion, venta.fechaCreacion) && Objects.equals(fechaUltimaModificacion, venta.fechaUltimaModificacion) && canalVenta == venta.canalVenta && Objects.equals(vendedor, venta.vendedor) && Objects.equals(cliente, venta.cliente) && tipoPlan == venta.tipoPlan && Objects.equals(planes, venta.planes) && Objects.equals(vivienda, venta.vivienda) && Objects.equals(pago, venta.pago) && Objects.equals(fechaInicioCobertura, venta.fechaInicioCobertura) && Objects.equals(cotizaciones, venta.cotizaciones) && Objects.equals(especificos, venta.especificos) && tipoMedioPago == venta.tipoMedioPago && cantidadCuotas == venta.cantidadCuotas && beneficio == venta.beneficio && Objects.equals(nroPoliza, venta.nroPoliza) && Objects.equals(emision, venta.emision);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fechaCreacion, fechaUltimaModificacion, canalVenta, vendedor, cliente, tipoPlan, planes, vivienda, pago, fechaInicioCobertura, cotizaciones, especificos, tipoMedioPago, cantidadCuotas, beneficio, nroPoliza, emision);
    }
}
