package ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions;

import java.util.List;

public class ClienteRechazadoDuplicadoException extends  CoreBusinessException{

    public static String CODIGO_RECHAZO_GLM = "801";

    public ClienteRechazadoDuplicadoException(List<String> messages) {
        super(messages);
    }
}
