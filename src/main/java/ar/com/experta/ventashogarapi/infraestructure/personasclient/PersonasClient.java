package ar.com.experta.ventashogarapi.infraestructure.personasclient;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PersonasClient {
    private List<String> nominaDirectv;

    private List<String> nominaGrupoW;

    public PersonasClient() {
        nominaDirectv = new ArrayList<>();
        nominasCUIT.addall(nominaDirectv);
        nominasDNI.addall(nominaDirectv);
        nominaGrupoW = new ArrayList<>();
        nominasGWDNI.addall(nominaGrupoW);
    }

    public boolean existeNominaDirectv(String documentoCliente) {
        return nominaDirectv.contains(documentoCliente);
    }

    public boolean existeNominaGrupoW(String documentoCliente) {
        return nominaGrupoW.contains(documentoCliente);
    }
}
