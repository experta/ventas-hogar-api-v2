package ar.com.experta.ventashogarapi.infraestructure.negocioclient;

public enum ConsultasNegocio {

    GET_LOCALIZACIONES_OPTIONS_BY_CP("/localizaciones/opciones?codigoPostal={param}"),
    GET_BANCOS_OPTIONS ("/bancos/opciones"),
    GET_BANCOS_BY_CBU ("/bancos/opciones?cbu={param}"),
    GET_MARCAS_TARJETAS_CREDITO_OPTIONS ("/marcas-tarjetas-credito/opciones"),
    GET_ACTIVIDADES_TOMADOR_OPTIONS ("/actividades-tomador/opciones"),
    GET_NACIONALIDADES_OPTIONS ("/nacionalidades/opciones"),
    GET_TIPOS_PERSONA_OPTIONS ("/tipos-persona/opciones"),
    GET_CONDICIONES_IMPOSITIVAS_OPTIONS ("/condiciones-impositivas/opciones"),
    GET_CONDICIONES_IMPOSITIVAS_OPTIONS_BY_TIPO_PERSONA ("/condiciones-impositivas/opciones?tipoPersona={param}"),
    GET_TIPOS_DOCUMENTO_OPTIONS ("/tipos-documento/opciones"),
    GET_TIPOS_DOCUMENTO_OPTIONS_BY_CONDICION ("/tipos-documento/opciones?condicionImpositiva={param}"),
    GET_TIPOS_DOCUMENTO_OPTIONS_BY_PERSONA ("/tipos-documento/opciones?tipoPersona={param}"),
    GET_TIPOS_DOCUMENTO_OPTIONS_BY_CONDICION_AND_PERSONA ("/tipos-documento/opciones?condicionImpositiva={param1}&tipoPersona={param2}")
    ;

    private String uri;

    ConsultasNegocio(String uri) {
        this.uri = uri;
    }

    public String uri() {
        return uri;
    }

}
