package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ItemDTO {

    @JsonProperty("CodigoPostal")
    private String codigoPostal;
    @JsonProperty("SubCodigoPostal")
    private String subCodigoPostal;
    @JsonProperty("TipoRiesgoCodigo")
    private String tipoRiesgoCodigo;
    @JsonProperty("CalificacionRiesgo")
    private String calificacionRiesgo;
//    @JsonProperty("ClausulaAjuste")
//    private String clausulaAjuste;
    @JsonProperty("ProductosCotizar")
    private List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.ProductoCotizarDTO> productosCotizar;

    public ItemDTO() {
    }

    public ItemDTO(String codigoPostal, String subCodigoPostal, String tipoRiesgoCodigo, String calificacionRiesgo, String clausulaAjuste, List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.ProductoCotizarDTO> productosCotizar) {
        this.codigoPostal = codigoPostal;
        this.subCodigoPostal = subCodigoPostal;
        this.tipoRiesgoCodigo = tipoRiesgoCodigo;
        this.calificacionRiesgo = calificacionRiesgo;
        //TODO: Agregar cuando ande bien GLM
//        this.clausulaAjuste = clausulaAjuste;
        this.productosCotizar = productosCotizar;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getSubCodigoPostal() {
        return subCodigoPostal;
    }

    public void setSubCodigoPostal(String subCodigoPostal) {
        this.subCodigoPostal = subCodigoPostal;
    }

    public String getTipoRiesgoCodigo() {
        return tipoRiesgoCodigo;
    }

    public void setTipoRiesgoCodigo(String tipoRiesgoCodigo) {
        this.tipoRiesgoCodigo = tipoRiesgoCodigo;
    }

    public String getCalificacionRiesgo() {
        return calificacionRiesgo;
    }

    public void setCalificacionRiesgo(String calificacionRiesgo) {
        this.calificacionRiesgo = calificacionRiesgo;
    }

//    public String getClausulaAjuste() {
//        return clausulaAjuste;
//    }
//
//    public void setClausulaAjuste(String clausulaAjuste) {
//        this.clausulaAjuste = clausulaAjuste;
//    }

    public List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.ProductoCotizarDTO> getProductosCotizar() {
        return productosCotizar;
    }

    public void setProductosCotizar(List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.ProductoCotizarDTO> productosCotizar) {
        this.productosCotizar = productosCotizar;
    }
}
