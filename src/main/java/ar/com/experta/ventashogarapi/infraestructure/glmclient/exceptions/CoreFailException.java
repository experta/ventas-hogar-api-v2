package ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions;

import java.util.List;

public class CoreFailException extends ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.ErrorListableException {

    private List<String> errorList;

    public CoreFailException(List<String> errorList) {
        super();
        this.errorList = errorList;
    }

    public CoreFailException(String message) {
        super(message);
    }

    public List<String> getErrorList() {
        return errorList;
    }
}
