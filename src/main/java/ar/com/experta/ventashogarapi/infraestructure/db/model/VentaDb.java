package ar.com.experta.ventashogarapi.infraestructure.db.model;

import ar.com.experta.ventashogarapi.core.model.ModalidadComision;
import ar.com.experta.ventashogarapi.core.model.Vendedor;
import ar.com.experta.ventashogarapi.core.model.Venta;
import ar.com.experta.ventashogarapi.core.model.enums.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "ventas")
public class VentaDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "canal_venta")
    @Enumerated(EnumType.STRING)
    private CanalVenta canalVenta;

    @Column(name = "fecha_creacion")
    private LocalDateTime fechaCreacion;

    @Column(name = "fecha_ultima_modificacion")
    private LocalDateTime fechaUltimaModificacion;

    @Column(name = "vendedor_id")
    private String vendedorId;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_cliente")
    private ClienteDb cliente;

    @Column(name = "tipo_plan")
    @Enumerated(EnumType.STRING)
    private TipoPlan tipoPlan;

    @Column(name = "tipo_medio_pago")
    @Enumerated(EnumType.STRING)
    private TipoMedioPago tipoMedioPago;

    @Column(name = "cant_cuotas")
    private String cantidadCuotas;

    @Column(name = "modalidad_comision", length = 50)
    @Enumerated(EnumType.STRING)
    private ModalidadComision modalidadComision;

    @Column(name = "clausula_estabilizacion")
    private Boolean clausulaEstabilizacion;

    @OneToMany(targetEntity= PlanDb.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "id_venta", referencedColumnName = "id")
    @NotNull
    private List<PlanDb> planes;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_vivienda")
    private ViviendaDb vivienda;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pago")
    private PagoDb pago;

    @Column(name = "fecha_inicio_cobertura")
    private LocalDate fechaInicioCobertura;

    @OneToMany(targetEntity= CotizacionDb.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "id_venta", referencedColumnName = "id")
    @NotNull
    private List<CotizacionDb> cotizaciones;  //Llistado de

    @Column(name = "beneficio", length = 30)
    @Enumerated(EnumType.STRING)
    private Beneficio beneficio;

    @Column(name = "nro_poliza")
    private String numeroPoliza;

    @Column(name = "nro_referencia")
    private String numeroReferencia;

    @Column(name = "emitiendo")
    private Boolean emitiendo;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_emision")
    private EmisionDb emision;

    @OneToMany(targetEntity= EspecificoDb.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "id_venta", referencedColumnName = "id")
    private List<EspecificoDb> especificos;

    public VentaDb() {
    }

    public VentaDb(Venta venta) {
        this.id = venta.getId() != null ? Long.parseLong(venta.getId()) : null;
        this.canalVenta = venta.getCanalVenta();
        this.fechaCreacion = venta.getFechaCreacion() != null ? venta.getFechaCreacion() : LocalDateTime.now();
        this.fechaUltimaModificacion = venta.getFechaUltimaModificacion() != null ? venta.getFechaUltimaModificacion() : LocalDateTime.now();
        this.vendedorId = venta.getVendedor() != null ? venta.getVendedor().getId() : null;
        this.cliente = venta.getCliente() != null ? new ClienteDb(venta.getCliente()) : null;
        this.planes = venta.getPlanes() != null ?
                            venta.getPlanes()
                                    .stream()
                                    .map(plan -> new PlanDb(plan.getIdPlan()))
                                    .collect(Collectors.toList())
                        : null;
        this.vivienda = venta.getVivienda() != null ? new ViviendaDb(venta.getVivienda()) : null;
        this.pago = venta.getPago() != null ? new PagoDb(venta.getPago()) : null;
        this.fechaInicioCobertura = venta.getFechaInicioCobertura().isAfter(LocalDate.now()) || venta.getTipoPlan().vigenciaRetroactiva() ? venta.getFechaInicioCobertura() : LocalDate.now().plusDays(1);
        this.cotizaciones = venta.getCotizaciones() != null ?
                                venta.getCotizaciones()
                                        .stream()
                                        .map(cotizacion -> new CotizacionDb(cotizacion))
                                        .collect(Collectors.toList())
                                : null;
        this.numeroPoliza = venta.getNroPoliza();
        this.emision = venta.getEmision() != null ? new EmisionDb(venta.getEmision()) : null;
        this.especificos = venta.getEspecificos() != null ?
                                venta.getEspecificos()
                                        .stream()
                                        .map(especifico -> new EspecificoDb(especifico))
                                        .collect(Collectors.toList())
                                : null;
        this.beneficio = venta.getBeneficio();
        this.tipoPlan = venta.getTipoPlan();
        this.tipoMedioPago = venta.getTipoMedioPago();
        this.cantidadCuotas = venta.getCantidadCuotas() != null ? venta.getCantidadCuotas().value() : null;
        this.modalidadComision = venta.getModalidadComision();
        this.clausulaEstabilizacion = venta.getClausulaEstabilizacion();
        this.numeroReferencia = venta.getNumeroReferencia();
        this.emitiendo = venta.getEmitiendo();
    }

    public VentaDb updateVentaDb(Venta venta) {
        this.id = venta.getId() != null ? Long.parseLong(venta.getId()) : null;
        this.canalVenta = venta.getCanalVenta();
        this.fechaCreacion = venta.getFechaCreacion() != null ? venta.getFechaCreacion() : LocalDateTime.now();
        this.fechaUltimaModificacion = venta.getFechaUltimaModificacion() != null ? venta.getFechaUltimaModificacion() : LocalDateTime.now();
        this.vendedorId = venta.getVendedor() != null ? venta.getVendedor().getId() : null;
        this.fechaInicioCobertura = venta.getFechaInicioCobertura().isAfter(LocalDate.now()) || venta.getTipoPlan().vigenciaRetroactiva() ? venta.getFechaInicioCobertura() : LocalDate.now().plusDays(1);
        this.numeroPoliza = venta.getNroPoliza();
        this.beneficio = venta.getBeneficio();
        this.tipoPlan = venta.getTipoPlan();
        this.tipoMedioPago = venta.getTipoMedioPago();
        this.cantidadCuotas = venta.getCantidadCuotas() != null ? venta.getCantidadCuotas().value() : null;
        this.modalidadComision = venta.getModalidadComision();
        this.clausulaEstabilizacion = venta.getClausulaEstabilizacion();
        this.numeroReferencia = venta.getNumeroReferencia();
        this.emitiendo = venta.getEmitiendo();

        Venta ventaActual = this.mapToVenta();

        if (venta.getCliente() != null &&!venta.getCliente().equals(ventaActual.getCliente()))
            this.cliente = venta.getCliente() != null ? new ClienteDb(venta.getCliente()) : null;
        if (venta.getVivienda() != null &&!venta.getVivienda().equals(ventaActual.getVivienda()))
            this.vivienda = venta.getVivienda() != null ? new ViviendaDb(venta.getVivienda()) : null;
        if (venta.getPago() != null &&!venta.getPago().equals(ventaActual.getPago()))
            this.pago = venta.getPago() != null ? new PagoDb(venta.getPago()) : null;
        if (venta.getEmision() != null &&!venta.getEmision().equals(ventaActual.getEmision()))
            this.emision = venta.getEmision() != null ? new EmisionDb(venta.getEmision()) : null;

        if ((venta.getPlanes() != null && !venta.getPlanes().equals(ventaActual.getPlanes())) ||
                (venta.getPlanes() == null && ventaActual.getPlanes() != null)){
            if (this.planes != null) {
                this.planes.clear();
                this.planes.addAll(
                        venta.getPlanes()
                                .stream()
                                .map(plan -> new PlanDb(plan.getIdPlan()))
                                .collect(Collectors.toList()));
            }else
                this.planes = venta.getPlanes()
                        .stream()
                        .map(plan -> new PlanDb(plan.getIdPlan()))
                        .collect(Collectors.toList());
        }

        if ((venta.getCotizaciones() != null &&!venta.getCotizaciones().equals(ventaActual.getCotizaciones())) ||
                (venta.getCotizaciones() == null && ventaActual.getCotizaciones() != null)) {
            if (this.cotizaciones != null) {
                this.cotizaciones.clear();
                this.cotizaciones.addAll(venta.getCotizaciones() != null ?
                        venta.getCotizaciones()
                                .stream()
                                .map(cotizacion -> new CotizacionDb(cotizacion))
                                .collect(Collectors.toList()) : null);
            }else
                this.cotizaciones = venta.getCotizaciones() != null ?
                        venta.getCotizaciones()
                                .stream()
                                .map(cotizacion -> new CotizacionDb(cotizacion))
                                .collect(Collectors.toList())
                        : null;
        }

        if ((venta.getEspecificos() != null &&!venta.getEspecificos().equals(ventaActual.getEspecificos())) ||
                (venta.getEspecificos() == null && ventaActual.getEspecificos() != null)) {
            if (this.especificos != null) {
                this.especificos.clear();
                this.especificos.addAll(venta.getEspecificos() != null ?
                        venta.getEspecificos()
                                .stream()
                                .map(especifico -> new EspecificoDb(especifico))
                                .collect(Collectors.toList()) : null);
            }else
                this.especificos = venta.getEspecificos() != null ?
                        venta.getEspecificos()
                                .stream()
                                .map(especifico -> new EspecificoDb(especifico))
                                .collect(Collectors.toList())
                        : null;
        }

        venta.setVentaDb(this);
        return this;
    }

    public Venta mapToVenta() {
        Venta venta = new Venta(id != null ? id.toString() : null,
                canalVenta,
                fechaCreacion,
                fechaUltimaModificacion,
                vendedorId != null ? new Vendedor(vendedorId) : null,
                cliente != null ? cliente.mapToCliente() : null,
                tipoPlan,
                planes.size() > 0 ? planes.stream().map(planDb -> planDb.mapToPlan()).collect(Collectors.toList()) : null,
                vivienda != null ? vivienda.mapToVivienda() : null,
                pago != null ? pago.mapToPago() : null,
                fechaInicioCobertura.isAfter(LocalDate.now()) || tipoPlan.vigenciaRetroactiva()? fechaInicioCobertura : LocalDate.now().plusDays(1),
                cotizaciones.size() > 0 ? cotizaciones.stream().map(cotizacionDb -> cotizacionDb.mapToCotizacion()).collect(Collectors.toList()) : null,
                beneficio,
                numeroPoliza,
                emision != null ? emision.mapToEmision() : null,
                especificos != null ? especificos.stream().map(especificoDb -> especificoDb.mapToEspecifico()).collect(Collectors.toList()) : null,
                tipoMedioPago,
                cantidadCuotas != null ? CantidadCuotas.fromValue(cantidadCuotas) : null,
                modalidadComision,
                clausulaEstabilizacion,
                numeroReferencia,
                emitiendo != null ? emitiendo : Boolean.FALSE
                );

        venta.setVentaDb(this);
        return venta;
    }

    public Venta mapToVentaEncabezado() {
        Venta venta = new Venta(id != null ? id.toString() : null,
                canalVenta,
                fechaCreacion,
                fechaUltimaModificacion,
                vendedorId != null ? new Vendedor(vendedorId) : null,
                cliente != null ? cliente.mapToClienteEncabezado() : null,
                tipoPlan,
                planes.size() > 0 ? planes.stream().map(planDb -> planDb.mapToPlan()).collect(Collectors.toList()) : null,
                vivienda != null ? vivienda.mapToViviendaEncabezado() : null,
                null,
                fechaInicioCobertura.isAfter(LocalDate.now()) ? fechaInicioCobertura : LocalDate.now().plusDays(1),
                cotizaciones.size() > 0 ? cotizaciones.stream().map(cotizacionDb -> cotizacionDb.mapToCotizacion()).collect(Collectors.toList()) : null,
                beneficio,
                numeroPoliza,
                emision != null ? emision.mapToEmision() : null,
                null,
                tipoMedioPago,
                cantidadCuotas != null ? CantidadCuotas.fromValue(cantidadCuotas) : null,
                modalidadComision,
                clausulaEstabilizacion,
                numeroReferencia,
                emitiendo != null ? emitiendo : Boolean.FALSE
        );

        return venta;
    }

    public String getId() {
        return id != null ? id.toString() : null;
    }

    public void setId(String id) {
        this.id = id != null ? Long.parseLong(id) : null;
    }

    public LocalDateTime getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDateTime fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public LocalDateTime getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(LocalDateTime fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    public String getVendedorId() {
        return vendedorId;
    }

    public void setVendedorId(String vendedorId) {
        this.vendedorId = vendedorId;
    }

    public ClienteDb getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDb cliente) {
        this.cliente = cliente;
    }

    public List<PlanDb> getPlanes() {
        return planes;
    }

    public void setPlanes(List<PlanDb> planes) {
        this.planes = planes;
    }

    public ViviendaDb getVivienda() {
        return vivienda;
    }

    public void setVivienda(ViviendaDb viviendaDb) {
        this.vivienda = viviendaDb;
    }

    public PagoDb getPago() {
        return pago;
    }

    public void setPago(PagoDb pago) {
        this.pago = pago;
    }

    public LocalDate getFechaInicioCobertura() {
        return fechaInicioCobertura;
    }

    public void setFechaInicioCobertura(LocalDate fechaInicioCobertura) {
        this.fechaInicioCobertura = fechaInicioCobertura;
    }

    public List<CotizacionDb> getCotizaciones() {
        return cotizaciones;
    }

    public void setCotizaciones(List<CotizacionDb> cotizaciones) {
        this.cotizaciones = cotizaciones;
    }

    public String getNumeroPoliza() {
        return numeroPoliza;
    }

    public void setNumeroPoliza(String numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }

    public EmisionDb getEmision() {
        return emision;
    }

    public void setEmision(EmisionDb emision) {
        this.emision = emision;
    }

    public TipoPlan getTipoPlan() {
        return tipoPlan;
    }

    public void setTipoPlan(TipoPlan tipoPlan) {
        this.tipoPlan = tipoPlan;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CanalVenta getCanalVenta() {
        return canalVenta;
    }

    public void setCanalVenta(CanalVenta canalVenta) {
        this.canalVenta = canalVenta;
    }

    public Beneficio getBeneficio() {
        return beneficio;
    }

    public void setBeneficio(Beneficio beneficio) {
        this.beneficio = beneficio;
    }

    public List<EspecificoDb> getEspecificos() {
        return especificos;
    }

    public void setEspecificos(List<EspecificoDb> especificos) {
        this.especificos = especificos;
    }

    public TipoMedioPago getTipoMedioPago() {
        return tipoMedioPago;
    }

    public void setTipoMedioPago(TipoMedioPago tipoMedioPago) {
        this.tipoMedioPago = tipoMedioPago;
    }

    public String getCantidadCuotas() {
        return cantidadCuotas;
    }

    public ModalidadComision getModalidadComision() {
        return modalidadComision;
    }

    public void setModalidadComision(ModalidadComision modalidadComision) {
        this.modalidadComision = modalidadComision;
    }

    public Boolean getClausulaEstabilizacion() {
        return clausulaEstabilizacion;
    }

    public void setClausulaEstabilizacion(Boolean clausulaEstabilizacion) {
        this.clausulaEstabilizacion = clausulaEstabilizacion;
    }

    public void setCantidadCuotas(String cantidadCuotas) {
        this.cantidadCuotas = cantidadCuotas;
    }

    public Boolean getEmitiendo() {
        return emitiendo;
    }

    public void setEmitiendo(Boolean emitiendo) {
        this.emitiendo = emitiendo;
    }

    public String getNumeroReferencia() {
        return numeroReferencia;
    }

    public void setNumeroReferencia(String numeroReferencia) {
        this.numeroReferencia = numeroReferencia;
    }
}

