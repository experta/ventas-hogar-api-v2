package ar.com.experta.ventashogarapi.infraestructure.glmclient;

import ar.com.experta.ventashogarapi.core.model.ModalidadComision;
import ar.com.experta.ventashogarapi.core.model.Plan;
import ar.com.experta.ventashogarapi.core.model.enums.Beneficio;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.CoreBusinessException;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.CoreFailException;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.PorcentajesEstabilizacionResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Component
public class PorcentajesClient {
    private static final Logger logger = LoggerFactory.getLogger(PorcentajesClient.class);

    @Value("${urlglm}")
    private String hostGLM;

    private static final String RAMA_PRODUCTO = "15";
    private static final String URI_GET_ACTUALIZACION = "/ar.com.glmsa.seguros.comercial.apiglm.emision.awsporcentajeajustes";
    private static final String URI_GET_CLAUSULA_ESTABILIZACION  = "/ar.com.glmsa.seguros.comercial.apiglm.emision.awslistarclausuladeajuste";


    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    private static ObjectMapper objectMapper = new ObjectMapper();

    @Cacheable (value="cachePorcentajesEstabilizacion")
    public List<PorcentajesEstabilizacionResponse> getPorcentajesEstabilizacion(Plan plan, ModalidadComision modalidadComision, Beneficio beneficio) throws CoreFailException, CoreBusinessException {

//        PorcentajesEstabilizacionRequest porcentajesEstabilizacionRequest = new PorcentajesEstabilizacionRequest(
//                Integer.valueOf(RAMA_PRODUCTO),
//                PlanesMapper.getPlanComercial(plan, modalidadComision, beneficio)
//        );
//
//        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(hostGLM + URI_GET_CLAUSULA_ESTABILIZACION );
//        try {
//            return List.of(restTemplate.exchange(
//                    builder.toUriString(),
//                    HttpMethod.POST,
//                    new HttpEntity<>(porcentajesEstabilizacionRequest),
//                    PorcentajesEstabilizacionResponse[].class).getBody());
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new CoreFailException(e.getMessage());
//        }

        //TODO: Eliminar cuando se pase el servicio de GLM a PROD
        return Arrays.asList(new PorcentajesEstabilizacionResponse("10", "CLAUSULA AJUSTE 10%"));
    }

}
