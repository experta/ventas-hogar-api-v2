package ar.com.experta.ventashogarapi.infraestructure.valoresviviendaclient.model;

import java.math.BigDecimal;

public class ValoresVivienda {

    private BigDecimal valorDesde;
    private BigDecimal valorHasta;

    public ValoresVivienda() {
    }

    public ValoresVivienda(BigDecimal valorDesde, BigDecimal valorHasta) {
        this.valorDesde = valorDesde;
        this.valorHasta = valorHasta;
    }

    public BigDecimal getValorDesde() {
        return valorDesde;
    }

    public void setValorDesde(BigDecimal valorDesde) {
        this.valorDesde = valorDesde;
    }

    public BigDecimal getValorHasta() {
        return valorHasta;
    }

    public void setValorHasta(BigDecimal valorHasta) {
        this.valorHasta = valorHasta;
    }
}
