package ar.com.experta.ventashogarapi.infraestructure.impresoracontratosclient;

import ar.com.experta.ventashogarapi.commons.exceptions.badgateway.BadGatewayException;
import ar.com.experta.ventashogarapi.commons.exceptions.notfound.PolizaNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component
public class ImpresoraContratosClient {

    @Value("${url_impresora_contratos}")
    private String urlImpresoraContratos;
    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    private final static String RAMA = "HOGAR";

    public byte[] getImpresion(String nroPoliza) {

        restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());

        try {
            String url = urlImpresoraContratos + "/polizas/" + RAMA + "/" + nroPoliza;
            ResponseEntity<byte[]> response = restTemplate.exchange(url, HttpMethod.GET, null, byte[].class, "1");

            if (response.getStatusCodeValue() == HttpStatus.OK.value()) {
                byte[] bytesImpPoliza = response.getBody();
                return bytesImpPoliza;
            }
            if (response.getStatusCodeValue() == HttpStatus.NOT_FOUND.value())
                throw new PolizaNotFoundException();
            throw new BadGatewayException("Error inesperado descargando el PDF de la poliza", response.getStatusCode().getReasonPhrase());
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND))
                throw new PolizaNotFoundException();
            throw new BadGatewayException("Error inesperado descargando el PDF de la poliza", e.getMessage());
        }
    }

}
