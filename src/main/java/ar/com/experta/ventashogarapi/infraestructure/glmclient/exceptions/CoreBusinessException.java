package ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions;

import java.util.List;

public class CoreBusinessException extends Exception {

    private List<String> messages;

    public CoreBusinessException(List<String> messages) {
        this.messages = messages;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}
