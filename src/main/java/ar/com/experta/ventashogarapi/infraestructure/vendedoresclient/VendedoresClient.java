package ar.com.experta.ventashogarapi.infraestructure.vendedoresclient;

import ar.com.experta.ventashogarapi.commons.exceptions.badgateway.VendedorBadGatewayException;
import ar.com.experta.ventashogarapi.commons.exceptions.notfound.VendedorNotFoundException;
import ar.com.experta.ventashogarapi.core.model.Vendedor;
import ar.com.experta.ventashogarapi.core.model.enums.TipoProductos;
import ar.com.experta.ventashogarapi.infraestructure.vendedoresclient.model.VendedorDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class VendedoresClient {

    @Value("${url_vendedores}")
    private String HOST_API_VENDEDORES;
    private static final String GET_BY_ID = "/vendedores/{id}";

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    public Vendedor getVendedor(String id) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl((HOST_API_VENDEDORES + GET_BY_ID).replace("{id}", id));
            VendedorDTO vendedorDTO = restTemplate.getForObject(builder.toUriString(), VendedorDTO.class);
            List<TipoProductos> tiposProductos = Arrays.stream(TipoProductos.values()).filter(rol -> vendedorDTO.getTipoProductos().contains(rol.toString())).collect(Collectors.toList());
            return new Vendedor(vendedorDTO.getId(), vendedorDTO.getNombre(), tiposProductos, vendedorDTO.getCuponeraHabilitada());
        }catch (HttpClientErrorException e){
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND))
                throw new VendedorNotFoundException();
            throw new VendedorBadGatewayException(e);
        }catch (Exception e){
            throw new VendedorBadGatewayException(e);
        }
    }
}
