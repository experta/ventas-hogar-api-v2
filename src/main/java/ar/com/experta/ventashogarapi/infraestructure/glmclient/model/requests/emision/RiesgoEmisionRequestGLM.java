package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.emision;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RiesgoEmisionRequestGLM {

    @JsonProperty("DireccionLocalidadRef")
    private String direccionLocalidadRef;
    @JsonProperty("DistanciaLocalidadRef")
    private String distanciaLocalidadRef;
    @JsonProperty("LinderoSur")
    private String linderoSur;
    @JsonProperty("LinderoOeste")
    private String linderoOeste;

    public RiesgoEmisionRequestGLM() {
    }

    public RiesgoEmisionRequestGLM(String direccionLocalidadRef, String distanciaLocalidadRef, String linderoSur, String linderoOeste) {
        this.direccionLocalidadRef = direccionLocalidadRef;
        this.distanciaLocalidadRef = distanciaLocalidadRef;
        this.linderoSur = linderoSur;
        this.linderoOeste = linderoOeste;
    }

    public String getDireccionLocalidadRef() {
        return direccionLocalidadRef;
    }

    public void setDireccionLocalidadRef(String direccionLocalidadRef) {
        this.direccionLocalidadRef = direccionLocalidadRef;
    }

    public String getDistanciaLocalidadRef() {
        return distanciaLocalidadRef;
    }

    public void setDistanciaLocalidadRef(String distanciaLocalidadRef) {
        this.distanciaLocalidadRef = distanciaLocalidadRef;
    }

    public String getLinderoSur() {
        return linderoSur;
    }

    public void setLinderoSur(String linderoSur) {
        this.linderoSur = linderoSur;
    }

    public String getLinderoOeste() {
        return linderoOeste;
    }

    public void setLinderoOeste(String linderoOeste) {
        this.linderoOeste = linderoOeste;
    }
}
