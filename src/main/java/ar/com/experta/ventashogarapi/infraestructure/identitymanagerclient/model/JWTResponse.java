package ar.com.experta.ventashogarapi.infraestructure.identitymanagerclient.model;

public class JWTResponse {
    private String jwt;

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
