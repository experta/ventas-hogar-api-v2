package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


public class CotizacionIntegralesResponse {

    @JsonProperty("Rama")
    private String rama;
    @JsonProperty("Solicitud")
    private String solicitud;
    @JsonProperty("Instalacion")
    private String instalacion;
    @JsonProperty("DescripcionRiesgo")
    private String descripcionRiesgo;
    @JsonProperty("VigenciaHasta")
    private String vigenciaHasta;
    @JsonProperty("Productos")
    private List<ProductoResponse> productos;
    @JsonProperty("Excepciones")
    private List<ExcepcionesResponse> excepciones;
    @JsonProperty("Errores")
    private List<ErroresResponse> errores;

    public CotizacionIntegralesResponse() {
    }

    public CotizacionIntegralesResponse(String rama, String solicitud, String instalacion, String descripcionRiesgo, String vigenciaHasta, List<ProductoResponse> productos, List<ExcepcionesResponse> excepciones, List<ErroresResponse> errores) {
        this.rama = rama;
        this.solicitud = solicitud;
        this.instalacion = instalacion;
        this.descripcionRiesgo = descripcionRiesgo;
        this.vigenciaHasta = vigenciaHasta;
        this.productos = productos;
        this.excepciones = excepciones;
        this.errores = errores;
    }

    public String getRama() {
        return rama;
    }

    public void setRama(String rama) {
        this.rama = rama;
    }

    public String getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(String solicitud) {
        this.solicitud = solicitud;
    }

    public String getInstalacion() {
        return instalacion;
    }

    public void setInstalacion(String instalacion) {
        this.instalacion = instalacion;
    }

    public String getDescripcionRiesgo() {
        return descripcionRiesgo;
    }

    public void setDescripcionRiesgo(String descripcionRiesgo) {
        this.descripcionRiesgo = descripcionRiesgo;
    }

    public String getVigenciaHasta() {
        return vigenciaHasta;
    }

    public void setVigenciaHasta(String vigenciaHasta) {
        this.vigenciaHasta = vigenciaHasta;
    }

    public List<ProductoResponse> getProductos() {
        return productos;
    }

    public void setProductos(List<ProductoResponse> productos) {
        this.productos = productos;
    }

    public List<ExcepcionesResponse> getExcepciones() {
        return excepciones;
    }

    public void setExcepciones(List<ExcepcionesResponse> excepciones) {
        this.excepciones = excepciones;
    }

    public List<ErroresResponse> getErrores() {
        return errores;
    }

    public void setErrores(List<ErroresResponse> errores) {
        this.errores = errores;
    }
}
