package ar.com.experta.ventashogarapi.infraestructure.db.model;

import ar.com.experta.ventashogarapi.core.model.Plan;
import ar.com.experta.ventashogarapi.core.model.enums.IdPlan;

import javax.persistence.*;

@Entity
@Table(name = "ventas_planes")
public class PlanDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "id_plan", nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    private IdPlan idPlan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_venta", insertable=false, updatable=false)
    private VentaDb ventaDb;

    public PlanDb() {
    }

    public PlanDb(IdPlan idPlan) {
        this.idPlan = idPlan;
    }

    public Plan mapToPlan(){
        return new Plan(idPlan);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IdPlan getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(IdPlan idPlan) {
        this.idPlan = idPlan;
    }

    public VentaDb getVentaDb() {
        return ventaDb;
    }

    public void setVentaDb(VentaDb ventaDb) {
        this.ventaDb = ventaDb;
    }
}
