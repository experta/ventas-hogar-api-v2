package ar.com.experta.ventashogarapi.infraestructure.valoresviviendaclient;

import ar.com.experta.ventashogarapi.commons.exceptions.badgateway.ValoresViviendaBadGatewayException;
import ar.com.experta.ventashogarapi.core.model.enums.TipoVivienda;
import ar.com.experta.ventashogarapi.infraestructure.valoresviviendaclient.model.ValoresVivienda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.HashMap;

@Component
public class ValoresViviendaClient {

    @Value("${url_valores_vivienda}")
    private String HOST_API_VALORES_VIVIENDA;
    private static final String GET_BY_LOCALIDAD_TIPO_VIVIENDA = "/negocio/valorConstruccion/{idLocalizacion}/{tipoVivienda}";

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    private MathContext DEFAULT_MATH_CONTEXT = new MathContext(2, RoundingMode.HALF_UP);
    private BigDecimal MARGEN_DESFACE_DOLAR = new BigDecimal(1.65,DEFAULT_MATH_CONTEXT);
    //TODO: Eliminar con nueva sugerencia de precio
    private BigDecimal MARGEN_SOBRE_MAXIMO = new BigDecimal(1.25,DEFAULT_MATH_CONTEXT);
    private BigDecimal REDONDEO_MILLONES = new BigDecimal(1000,DEFAULT_MATH_CONTEXT);

    public ValoresVivienda getValores(String idLocalizacion, TipoVivienda tipoVivienda, Integer metrosCuadrados) {
        try {
            String url = HOST_API_VALORES_VIVIENDA + GET_BY_LOCALIDAD_TIPO_VIVIENDA;
            url = url.replace("{tipoVivienda}", tipoVivienda.toString());
            url = url.replace("{idLocalizacion}", idLocalizacion);
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
            HashMap<String,String> response =  restTemplate.getForObject(builder.toUriString(), HashMap.class);

            ValoresVivienda valoresVivienda = new ValoresVivienda(new BigDecimal(response.get("valorDesde ")), new BigDecimal(response.get("valorHasta")));

            valoresVivienda.setValorDesde(valoresVivienda.getValorDesde()
                    .multiply(BigDecimal.valueOf(metrosCuadrados),DEFAULT_MATH_CONTEXT)
                    .multiply(MARGEN_DESFACE_DOLAR,DEFAULT_MATH_CONTEXT)
                    .divide(REDONDEO_MILLONES,0, RoundingMode.HALF_UP)
                    .multiply(REDONDEO_MILLONES,DEFAULT_MATH_CONTEXT));
            valoresVivienda.setValorDesde(valoresVivienda.getValorDesde().setScale(0,RoundingMode.HALF_UP));
            valoresVivienda.setValorHasta(valoresVivienda.getValorHasta()
                    .multiply(BigDecimal.valueOf(metrosCuadrados),DEFAULT_MATH_CONTEXT)
                    .multiply(MARGEN_DESFACE_DOLAR,DEFAULT_MATH_CONTEXT)
                    .multiply(MARGEN_SOBRE_MAXIMO,DEFAULT_MATH_CONTEXT)
                    .divide(REDONDEO_MILLONES,0,RoundingMode.HALF_UP)
                    .multiply(REDONDEO_MILLONES,DEFAULT_MATH_CONTEXT));
            valoresVivienda.setValorHasta(valoresVivienda.getValorHasta().setScale(0,RoundingMode.HALF_UP));

            return valoresVivienda;

        }catch (Exception e){
            throw new ValoresViviendaBadGatewayException(e);
        }
    }

}