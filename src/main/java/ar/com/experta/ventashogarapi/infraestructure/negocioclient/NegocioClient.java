package ar.com.experta.ventashogarapi.infraestructure.negocioclient;


import ar.com.experta.ventashogarapi.commons.exceptions.badgateway.LocalizacionBadGatewayException;
import ar.com.experta.ventashogarapi.commons.exceptions.badgateway.NegocioBadGatewayException;
import ar.com.experta.ventashogarapi.core.model.Localizacion;
import ar.com.experta.ventashogarapi.core.model.NegocioOption;
import ar.com.experta.ventashogarapi.infraestructure.negocioclient.model.LocalizacionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.List;

@Component
public class NegocioClient {

    @Value("${url_negocio}")
    private String HOST_API_NEGOCIO;
    private static final String GET_LOCALIZACION_BY_ID = "/localizaciones/{id}";
    private static final String GET_LOCALIZACION_ZONA_RIESGO_BY_ID = "/localizaciones/{id}/zona-riesgo/HOGAR";


    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    public Localizacion getLocalizacion(String id) {
        try {
            UriComponentsBuilder builderLocalizacion = UriComponentsBuilder.fromHttpUrl((HOST_API_NEGOCIO + GET_LOCALIZACION_BY_ID).replace("{id}", id));
            LocalizacionDTO localizacionDTO = restTemplate.getForObject(builderLocalizacion.toUriString(), LocalizacionDTO.class);

            UriComponentsBuilder builderZonaRiesgo = UriComponentsBuilder.fromHttpUrl((HOST_API_NEGOCIO + GET_LOCALIZACION_ZONA_RIESGO_BY_ID).replace("{id}", id));
            String zonaRiesgo = restTemplate.getForObject(builderZonaRiesgo.toUriString(), String.class);

            return new Localizacion(localizacionDTO.getId(),
                    localizacionDTO.getCodigoPostal(),
                    localizacionDTO.getLocalidad(),
                    localizacionDTO.getProvincia(),
                    zonaRiesgo);
        }catch (Exception e){
            throw new LocalizacionBadGatewayException(e);
        }
    }

    public String getZonaRiesgo(String id) {
        try {
            UriComponentsBuilder builderZonaRiesgo = UriComponentsBuilder.fromHttpUrl((HOST_API_NEGOCIO + GET_LOCALIZACION_ZONA_RIESGO_BY_ID).replace("{id}", id));
            return restTemplate.getForObject(builderZonaRiesgo.toUriString(), String.class);
        }catch (Exception e){
            throw new LocalizacionBadGatewayException(e);
        }
    }

    public List<NegocioOption> getOptions(ConsultasNegocio consulta) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(HOST_API_NEGOCIO + consulta.uri());
            return Arrays.asList(restTemplate.getForObject(builder.toUriString(), NegocioOption[].class));
        }catch (Exception e){
            throw new NegocioBadGatewayException(e);
        }
    }

    public List<NegocioOption> getOptions(ConsultasNegocio consulta, String parametro) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl((HOST_API_NEGOCIO + consulta.uri()).replace("{param}", parametro));
            return Arrays.asList(restTemplate.getForObject(builder.toUriString(), NegocioOption[].class));
        }catch (Exception e){
            throw new NegocioBadGatewayException(e);
        }
    }

    public List<NegocioOption> getOptions(ConsultasNegocio consulta, String parametro1, String parametro2) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl((HOST_API_NEGOCIO + consulta.uri()).replace("{param1}", parametro1).replace("{param2}", parametro2));
            return Arrays.asList(restTemplate.getForObject(builder.toUriString(), NegocioOption[].class));
        }catch (Exception e){
            throw new NegocioBadGatewayException(e);
        }
    }

}
