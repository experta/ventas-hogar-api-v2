package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.porcentajeajuste;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PorcentajesEstabilizacionRequest {

    @JsonProperty("Rama")
    private Integer rama;
    @JsonProperty("Plan")
    private String planComercial;

    public PorcentajesEstabilizacionRequest() {
    }

    public PorcentajesEstabilizacionRequest(Integer rama, String planComercial) {
        this.rama = rama;
        this.planComercial = planComercial;
    }

    public Integer getRama() {
        return rama;
    }

    public void setRama(Integer rama) {
        this.rama = rama;
    }

    public String getPlanComercial() {
        return planComercial;
    }

    public void setPlanComercial(String planComercial) {
        this.planComercial = planComercial;
    }
}

