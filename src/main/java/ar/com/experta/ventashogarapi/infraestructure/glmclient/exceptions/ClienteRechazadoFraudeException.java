package ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions;

import java.util.List;

public class ClienteRechazadoFraudeException extends  CoreBusinessException{

    public static String CODIGO_RECHAZO_GLM = "803";

    public ClienteRechazadoFraudeException(List<String> messages) {
        super(messages);
    }
}
