package ar.com.experta.ventashogarapi.infraestructure.db.model;

import ar.com.experta.ventashogarapi.core.model.Cotizacion;
import ar.com.experta.ventashogarapi.core.model.enums.EstadoCotizacion;
import ar.com.experta.ventashogarapi.core.model.enums.IdPlan;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "ventas_cotizaciones")
public class CotizacionDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "plan_id", nullable = false, length = 100)
    @Enumerated(EnumType.STRING)
    private IdPlan idPlan;

    @OneToMany(targetEntity= CoberturaDb.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "id_cotizacion", referencedColumnName = "id")
    @NotNull
    private List<CoberturaDb> coberturas;

    @Column(name = "estado", length = 15)
    @Enumerated(EnumType.STRING)
    private EstadoCotizacion estado;

    @Column(name = "fecha_creacion")
    private LocalDate fechaCreacion;

    @Column(name = "fecha_vencimiento")
    private LocalDate fechaVencimiento;

    @Column(name = "fecha_a_emitir")
    private LocalDate fechaAEmitir;

    @Column(name = "prima")
    private BigDecimal prima;

    @Column(name = "premio")
    private BigDecimal premio;

    @Column(name = "impuestos")
    private BigDecimal impuestos;

    @Column(name = "cuotas")
    private Integer cuotas;

    @Column(name = "descuento")
    Integer descuento;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_venta", insertable=false, updatable=false)
    private VentaDb ventaDb;

    public CotizacionDb() {
    }

    public CotizacionDb(Cotizacion cotizacion) {
        this.id = cotizacion.getId() != null ? Long.parseLong(cotizacion.getId()) : null;
        this.idPlan = cotizacion.getIdPlan();
        this.coberturas = cotizacion.getCoberturas().size() > 0 ?
                                cotizacion.getCoberturas()
                                        .stream()
                                        .map(cobertura -> new CoberturaDb(cobertura))
                                        .collect(Collectors.toList())
                                : null;
        this.estado = cotizacion.getEstado();
        this.fechaCreacion = cotizacion.getFechaCreacion();
        this.fechaVencimiento = cotizacion.getFechaVencimiento();
        this.fechaAEmitir = cotizacion.getFechaAEmitir();
        this.prima = cotizacion.getPrima();
        this.premio = cotizacion.getPremio();
        this.impuestos = cotizacion.getImpuestos();
        this.cuotas = cotizacion.getCuotas();
        this.descuento = cotizacion.getDescuento();
    }

    public Cotizacion mapToCotizacion(){
        return new Cotizacion(id != null ? id.toString() : null,
                idPlan,
                coberturas.size() > 0 ?
                            coberturas.stream()
                                    .map(coberturaDb -> coberturaDb.mapToCobertura())
                                    .collect(Collectors.toList())
                            : null,
                fechaVencimiento != null && !fechaVencimiento.isAfter(LocalDate.now()) ? EstadoCotizacion.VENCIDA : estado,
                fechaCreacion,
                fechaVencimiento,
                fechaAEmitir,
                prima,
                premio,
                impuestos,
                cuotas,
                descuento);
    }

    public String getId() {
        return id != null ? id.toString() : null;
    }

    public void setId(String id) {
        this.id = id != null ? Long.parseLong(id) : null;
    }

    public IdPlan getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(IdPlan idPlan) {
        this.idPlan = idPlan;
    }

    public List<CoberturaDb> getCoberturas() {
        return coberturas;
    }

    public void setCoberturas(List<CoberturaDb> coberturas) {
        this.coberturas = coberturas;
//        this.coberturas.stream().forEach(coberturaDb -> coberturaDb.setCotizacionDb(this));
    }

    public EstadoCotizacion getEstado() {
        return estado;
    }

    public void setEstado(EstadoCotizacion estado) {
        this.estado = estado;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public LocalDate getFechaAEmitir() {
        return fechaAEmitir;
    }

    public void setFechaAEmitir(LocalDate fechaAEmitir) {
        this.fechaAEmitir = fechaAEmitir;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrima() {
        return prima;
    }

    public void setPrima(BigDecimal prima) {
        this.prima = prima;
    }

    public BigDecimal getPremio() {
        return premio;
    }

    public void setPremio(BigDecimal premio) {
        this.premio = premio;
    }

    public BigDecimal getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(BigDecimal impuestos) {
        this.impuestos = impuestos;
    }

    public VentaDb getVentaDb() {
        return ventaDb;
    }

    public void setVentaDb(VentaDb ventaDb) {
        this.ventaDb = ventaDb;
    }

    public Integer getDescuento() {
        return descuento;
    }

    public void setDescuento(Integer descuento) {
        this.descuento = descuento;
    }

    public Integer getCuotas() {
        return cuotas;
    }

    public void setCuotas(Integer cuotas) {
        this.cuotas = cuotas;
    }
}
