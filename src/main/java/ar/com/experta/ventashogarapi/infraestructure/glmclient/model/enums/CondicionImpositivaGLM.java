package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.enums;

public enum CondicionImpositivaGLM {
    RESPONSABLE_INSCRIPTO("1"),
    RESPONSABLE_NO_INSCRIPTO("2"),
    IVA_NO_RESPONSABLE("3"),
    EXENTO("4"),
    CONSUMIDOR_FINAL("5"),
    MONOTRIBUTISTA("6"),
    NO_CATEGORIZADO("7"),
    PROVEEDOR_EXTERIOR("8"),
    CLIENTE_EXTERIOR("9");

    private String codigoGlm;

    CondicionImpositivaGLM(String codigoGlm) {
        this.codigoGlm = codigoGlm;
    }

    public String codigoGlm() {
        return codigoGlm;
    }

}
