package ar.com.experta.ventashogarapi.infraestructure.impresoracotizacionclient.request;

import ar.com.experta.ventashogarapi.core.model.enums.TipoCobertura;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PlanRequest {

    private String titulo;
    private BigDecimal costo;
    private BigDecimal costoBonificado;
    private List<CoberturaRequest> coberturas;

    public PlanRequest(String titulo, BigDecimal costo, BigDecimal costoBonificado, List<CoberturaRequest> coberturas) {
        this.titulo = titulo;
        this.costo = costo;
        this.costoBonificado = costoBonificado;
//        this.coberturas = coberturas;
        //TODO: Sacar y hacer bien cuando en la web se pueda hacer que tome el fijo de un input (Cobertura con especifico)
        if (coberturas == null || coberturas.isEmpty()){
            this.coberturas = coberturas;
        }else{
            this.coberturas = new ArrayList<>();
            for (CoberturaRequest cobertura : coberturas){
                this.coberturas.add(cobertura);
                if (cobertura.getTipoCobertura().equals(TipoCobertura.ACCIDENTES_PERSONALES_PCP.label())) {
                    this.coberturas.add(
                      new CoberturaRequest(TipoCobertura.GASTOS_MEDICOS.label(), cobertura.getSumaAsegurada().multiply(new BigDecimal(0.025)).setScale(2, BigDecimal.ROUND_HALF_EVEN))
                    );
                }
            }
        }
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public BigDecimal getCosto() {
        return costo;
    }

    public void setCosto(BigDecimal costo) {
        this.costo = costo;
    }

    public BigDecimal getCostoBonificado() {
        return costoBonificado;
    }

    public void setCostoBonificado(BigDecimal costoBonificado) {
        this.costoBonificado = costoBonificado;
    }

    public List<CoberturaRequest> getCoberturas() {
        return coberturas;
    }

    public void setCoberturas(List<CoberturaRequest> coberturas) {
        this.coberturas = coberturas;
    }
}
