package ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions;

public class JsonParseException extends ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.CoreFailException {

    public JsonParseException() {
        super("Ha ocurrido un error al intentar parsear la respuesta");
    }
}
