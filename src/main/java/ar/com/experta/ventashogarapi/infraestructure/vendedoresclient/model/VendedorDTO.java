package ar.com.experta.ventashogarapi.infraestructure.vendedoresclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class VendedorDTO {

    private String id;
    private String nombre;
    @JsonProperty ("productos")
    private List<String> tipoProductos;
    private Boolean cuponeraHabilitada;

    public VendedorDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<String> getTipoProductos() {
        return tipoProductos;
    }

    public void setTipoProductos(List<String> tipoProductos) {
        this.tipoProductos = tipoProductos;
    }

    public Boolean getCuponeraHabilitada() {
        return cuponeraHabilitada;
    }

    public void setCuponeraHabilitada(Boolean cuponeraHabilitada) {
        this.cuponeraHabilitada = cuponeraHabilitada;
    }
}
