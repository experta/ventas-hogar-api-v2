package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PorcentajesEstabilizacionResponse {

    @JsonProperty("Codigo")
    private String codigo;
    @JsonProperty("Descripcion")
    private String descripcion;

    public PorcentajesEstabilizacionResponse() {
    }

    public PorcentajesEstabilizacionResponse(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
