package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.emision;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TomadorDTO {

    @JsonProperty("TipoDocumentoCodigo")
    private String tipoDocumentoCodigo;
    @JsonProperty("DocumentoNumero")
    private String documentoNumero;
    @JsonProperty("NacionalidadCodigo")
    private String nacionalidadCodigo;
    @JsonProperty("Nombre")
    private String nombre;
    @JsonProperty("CategoriaIVACodigo")
    private String categoriaIVACodigo;
    @JsonProperty("CalleNombre")
    private String calleNombre;
    @JsonProperty("CalleNumero")
    private String calleNumero;
    @JsonProperty("CallePiso")
    private String callePiso;
    @JsonProperty("CalleDepto")
    private String calleDepto;
    @JsonProperty("CodigoPostal")
    private String codigoPostal;
    @JsonProperty("SubCodigoPostal")
    private String subCodigoPostal;
    @JsonProperty("Telefono")
    private String telefono;
    @JsonProperty("EMail")
    private String eMail;
    @JsonProperty("FechaNacimiento")
    private String fechaNacimiento;
    @JsonProperty("Sexo")
    private String sexo;
    @JsonProperty("EstadoCivilCodigo")
    private String estadoCivilCodigo;
    @JsonProperty("LugarNacimiento")
    private String lugarNacimiento;
    @JsonProperty("DeclaraTitular")
    private String declaraTitular;
    @JsonProperty("Actividad")
    private String actividad;
    @JsonProperty("PEP")
    private String pep;
    @JsonProperty("Relacion")
    private String relacion;
    @JsonProperty("SujetoObligado")
    private String sujetoObligado;

    public TomadorDTO() {
    }

    public TomadorDTO(String tipoDocumentoCodigo, String documentoNumero, String nacionalidadCodigo, String nombre, String categoriaIVACodigo, String calleNombre, String calleNumero, String callePiso, String calleDepto, String codigoPostal, String subCodigoPostal, String telefono, String eMail, String fechaNacimiento, String sexo, String estadoCivilCodigo, String lugarNacimiento, String declaraTitular, String actividad, String pep, String relacion, String sujetoObligado) {
        this.tipoDocumentoCodigo = tipoDocumentoCodigo;
        this.documentoNumero = documentoNumero;
        this.nacionalidadCodigo = nacionalidadCodigo;
        this.nombre = nombre;
        this.categoriaIVACodigo = categoriaIVACodigo;
        this.calleNombre = calleNombre;
        this.calleNumero = calleNumero;
        this.callePiso = callePiso;
        this.calleDepto = calleDepto;
        this.codigoPostal = codigoPostal;
        this.subCodigoPostal = subCodigoPostal;
        this.telefono = telefono;
        this.eMail = eMail;
        this.fechaNacimiento = fechaNacimiento;
        this.sexo = sexo;
        this.estadoCivilCodigo = estadoCivilCodigo;
        this.lugarNacimiento = lugarNacimiento;
        this.declaraTitular = declaraTitular;
        this.actividad = actividad;
        this.pep = pep;
        this.relacion = relacion;
        this.sujetoObligado = sujetoObligado;
    }

    public String getTipoDocumentoCodigo() {
        return tipoDocumentoCodigo;
    }

    public void setTipoDocumentoCodigo(String tipoDocumentoCodigo) {
        this.tipoDocumentoCodigo = tipoDocumentoCodigo;
    }

    public String getDocumentoNumero() {
        return documentoNumero;
    }

    public void setDocumentoNumero(String documentoNumero) {
        this.documentoNumero = documentoNumero;
    }

    public String getNacionalidadCodigo() {
        return nacionalidadCodigo;
    }

    public void setNacionalidadCodigo(String nacionalidadCodigo) {
        this.nacionalidadCodigo = nacionalidadCodigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoriaIVACodigo() {
        return categoriaIVACodigo;
    }

    public void setCategoriaIVACodigo(String categoriaIVACodigo) {
        this.categoriaIVACodigo = categoriaIVACodigo;
    }

    public String getCalleNombre() {
        return calleNombre;
    }

    public void setCalleNombre(String calleNombre) {
        this.calleNombre = calleNombre;
    }

    public String getCalleNumero() {
        return calleNumero;
    }

    public void setCalleNumero(String calleNumero) {
        this.calleNumero = calleNumero;
    }

    public String getCallePiso() {
        return callePiso;
    }

    public void setCallePiso(String callePiso) {
        this.callePiso = callePiso;
    }

    public String getCalleDepto() {
        return calleDepto;
    }

    public void setCalleDepto(String calleDepto) {
        this.calleDepto = calleDepto;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getSubCodigoPostal() {
        return subCodigoPostal;
    }

    public void setSubCodigoPostal(String subCodigoPostal) {
        this.subCodigoPostal = subCodigoPostal;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEstadoCivilCodigo() {
        return estadoCivilCodigo;
    }

    public void setEstadoCivilCodigo(String estadoCivilCodigo) {
        this.estadoCivilCodigo = estadoCivilCodigo;
    }

    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    public void setLugarNacimiento(String lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }

    public String getDeclaraTitular() {
        return declaraTitular;
    }

    public void setDeclaraTitular(String declaraTitular) {
        this.declaraTitular = declaraTitular;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getPep() {
        return pep;
    }

    public void setPep(String pep) {
        this.pep = pep;
    }

    public String getRelacion() {
        return relacion;
    }

    public void setRelacion(String relacion) {
        this.relacion = relacion;
    }

    public String getSujetoObligado() {
        return sujetoObligado;
    }

    public void setSujetoObligado(String sujetoObligado) {
        this.sujetoObligado = sujetoObligado;
    }
}

