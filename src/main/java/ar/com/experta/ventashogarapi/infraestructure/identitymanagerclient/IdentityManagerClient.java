package ar.com.experta.ventashogarapi.infraestructure.identitymanagerclient;

import ar.com.experta.ventashogarapi.commons.exceptions.badgateway.IdentityManagerBadGatewayException;
import ar.com.experta.ventashogarapi.infraestructure.identitymanagerclient.model.JWTRequest;
import ar.com.experta.ventashogarapi.infraestructure.identitymanagerclient.model.JWTResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class IdentityManagerClient {
    @Value("${url_identity_manager}")
    private String HOST_IDENTITY_MANAGER;

    private static final String GET_APP_JWT = "/user/app/jwt";

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    public String getAccessToken() {

        JWTRequest jwtRequest = new JWTRequest();

        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(HOST_IDENTITY_MANAGER + GET_APP_JWT );
            JWTResponse jwtResponse = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.POST,
                    new HttpEntity<>(jwtRequest),
                    JWTResponse.class).getBody();

            return jwtResponse.getJwt();
        }catch (Exception e){
            throw new IdentityManagerBadGatewayException(e);
        }

    }

}
