package ar.com.experta.ventashogarapi.infraestructure.impresoracotizacionclient.mapper;

import ar.com.experta.ventashogarapi.core.model.Cotizacion;
import ar.com.experta.ventashogarapi.core.model.NegocioOptionClausulas;
import ar.com.experta.ventashogarapi.core.model.Plan;
import ar.com.experta.ventashogarapi.core.model.Venta;
import ar.com.experta.ventashogarapi.core.model.enums.IdPlan;
import ar.com.experta.ventashogarapi.core.model.enums.TipoCobertura;
import ar.com.experta.ventashogarapi.core.model.enums.TipoPlan;
import ar.com.experta.ventashogarapi.infraestructure.impresoracotizacionclient.request.CoberturaRequest;
import ar.com.experta.ventashogarapi.infraestructure.impresoracotizacionclient.request.CotizacionRequest;
import ar.com.experta.ventashogarapi.infraestructure.impresoracotizacionclient.request.PlanRequest;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class  PdfCotizacionRequestMapper {

    private static final String TEXTO_CLAUSULA_ESTABILZIACION = "Aplicable sobre todas las coberturas excepto Accidentes Personales y Responsabilidad Civil.";

    public CotizacionRequest mapToPdfCotizacionRequest(Venta venta, List<String> idsCotizaciones) {

        if (venta.getPlanes().get(0).getTipoPlan().equals(TipoPlan.ENLATADO_ALRIO))
            return mapToPdfCotizacionAlRioRequest(venta);

        Map<IdPlan, String> planMap = new HashMap<>();
        for (Plan plan : venta.getPlanes()){
            planMap.put(plan.getIdPlan(), plan.getNombrePlan());
        }

        //Fix para adicionales optativos de Incendio Contenido
        Boolean showAdicionalesIC = Boolean.FALSE;
        for (Cotizacion cotizacion : venta.getCotizaciones()
                .stream()
                .filter(cotizacion -> idsCotizaciones != null && !idsCotizaciones.isEmpty() ? idsCotizaciones.contains(cotizacion.getId()) : Boolean.TRUE)
                .collect(Collectors.toList())){
            if (cotizacion.getCoberturas().stream().filter(cobertura -> cobertura.getTipoCobertura().equals(TipoCobertura.INCENDIO_CONTENIDO)).findAny().isPresent()){
                showAdicionalesIC = Boolean.TRUE;
            }
        }
        Boolean finalShowAdicionalesIC = showAdicionalesIC;
        List<PlanRequest> planesRequest = venta.getCotizaciones()
                                        .stream()
                                        .filter(cotizacion -> idsCotizaciones != null && !idsCotizaciones.isEmpty() ? idsCotizaciones.contains(cotizacion.getId()) : Boolean.TRUE)
                                        .map(cotizacion -> new PlanRequest(planMap.get(cotizacion.getIdPlan()).toUpperCase(),
                                                                            cotizacion.getPremio(),
                                                                            cotizacion.getPremioBonificado(),
                                                                            cotizacion.getCoberturas()
                                                                                    .stream()
                                                                                    .filter(cobertura -> cobertura.getTipoCobertura().show() &&
                                                                                            ((!cobertura.getTipoCobertura().equals(TipoCobertura.AD_HVCT_IC) && !cobertura.getTipoCobertura().equals(TipoCobertura.AD_GRANIZO_CONTENIDO) || finalShowAdicionalesIC)))
                                                                                    .map(cobertura -> new CoberturaRequest( cobertura.getOpcion() != null ? cobertura.getOpcion().label() : cobertura.getTipoCobertura().label(),
                                                                                                                                    cobertura.getTipoCobertura().descriptionFranquicia(),
                                                                                                                                    cobertura.getMontoAsegurado()))
                                                                                    .collect(Collectors.toList())
                                        ))
                                        .collect(Collectors.toList());

        List<IdPlan> idPlanesServMantenimiento = venta.getCotizaciones()
                .stream()
                .filter(cotizacion -> idsCotizaciones != null && !idsCotizaciones.isEmpty() ? idsCotizaciones.contains(cotizacion.getId()) : Boolean.TRUE)
                .map(cotizacion -> cotizacion.getIdPlan())
                .filter( idPlan -> idPlan.servicioMantenimiento())
                .collect(Collectors.toList());

        List<NegocioOptionClausulas> serviciosEspeciales = venta.getPlanes().size() == 1 ?
                venta.getPlanes().get(0).getServiciosAsistencias() :
                venta.getPlanes().get(0).getServiciosAsistencias(venta.getPlanes()
                        .stream()
                        .filter(plan -> idPlanesServMantenimiento.contains(plan.getIdPlan()))
                        .map(plan -> plan.getNombrePlan())
                        .collect(Collectors.toList())
                );

        return new CotizacionRequest ( venta.getId(),
                                        venta.getFechaUltimaModificacion().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                                        venta.getCliente().getNombreCompleto() != null ? venta.getCliente().getNombreCompleto().toUpperCase() : venta.getCliente().getRazonSocial() != null ? venta.getCliente().getRazonSocial().toUpperCase() : null,
                                        venta.getVendedor().getNombre(),
                                        venta.getVivienda().getTipoVivienda().label().toUpperCase(),
                                        venta.getVivienda().getDireccion().getLocalizacion().getCodigoPostal(),
                                        venta.getVivienda().getDireccion().getLocalizacion().getLocalidad().toUpperCase(),
                                        venta.getVivienda().getDireccion().getLocalizacion().getProvincia().toUpperCase(),
                                        venta.getPlanes().get(0).getTipoPlan().label().toUpperCase(),
                                        venta.getCantidadCuotas().label() + " - Vigencia: Trimestral prorrogable",
                                        venta.getTipoMedioPago().label(),
                                        venta.getBeneficio() != null ? venta.getBeneficio().label().toUpperCase() : null,
                                        venta.getBeneficio() != null ? venta.getBeneficio().descuento() : null,
                                        venta.getClausulaEstabilizacion() != null && venta.getClausulaEstabilizacion() ? Integer.valueOf(venta.getPorcentajeEstabilizacion()) : null,
                                        venta.getClausulaEstabilizacion() != null && venta.getClausulaEstabilizacion() ? TEXTO_CLAUSULA_ESTABILZIACION : null,
                                        planesRequest,
                                        venta.getVivienda().getMedidasSeguridad(),
                                        serviciosEspeciales
                );
    }

    public CotizacionRequest mapToPdfCotizacionAlRioRequest(Venta venta) {
        return new CotizacionRequest ( venta.getId(),
                venta.getFechaUltimaModificacion().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                venta.getCliente().getNombreCompleto() != null ? venta.getCliente().getNombreCompleto().toUpperCase() : venta.getCliente().getRazonSocial().toUpperCase(),
                "ALRIO"
        );

    }

}
