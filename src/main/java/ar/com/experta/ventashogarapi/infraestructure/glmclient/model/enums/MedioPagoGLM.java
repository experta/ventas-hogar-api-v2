package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.enums;

public enum MedioPagoGLM {
    DEBITO("4"),
    CREDITO("3"),
    CUPONERA("0"),
    OP_BANCARIA_DTV("2")
    ;

    private String codigoGlm;

    private MedioPagoGLM(final String codigoGlm) {
        this.codigoGlm = codigoGlm;
    }

    public String codigoGlm() {
        return codigoGlm;
    }
}
