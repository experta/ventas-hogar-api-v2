package ar.com.experta.ventashogarapi.infraestructure.db.model;

import ar.com.experta.ventashogarapi.core.model.*;
import ar.com.experta.ventashogarapi.core.model.enums.CondicionImpositiva;
import ar.com.experta.ventashogarapi.core.model.enums.Sexo;
import ar.com.experta.ventashogarapi.core.model.enums.TipoDocumento;
import ar.com.experta.ventashogarapi.core.model.enums.TipoPersona;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "ventas_clientes")
public class ClienteDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "documento_tipo", length = 15)
    @Enumerated(EnumType.STRING)
    private TipoDocumento documentoTipo;

    @Column(name = "documento_numero", length = 20)
    private String documentoNumero;

    @Column(name = "nombre", length = 100)
    private String nombre;

    @Column(name = "apellido", length = 100)
    private String apellido;

    @Column(name = "razonSocial", length = 200)
    private String razonSocial;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "nacionalidad", length = 50)
    private String nacionalidad;

    @Column(name = "fecha_nacimiento", length = 10)
    private LocalDate fechaNacimiento;

    @Column(name = "sexo", length = 15)
    @Enumerated(EnumType.STRING)
    private Sexo sexo;

    @Column(name = "actividad", length = 500)
    private String actividad;

    @Column(name = "pep", length = 1)
    private Boolean pep;

    @Column(name = "motivo_pep", length = 500)
    private String motivoPep;

    @Column(name = "sujeto_obligado", length = 1)
    private Boolean sujetoObligado;

    @Column(name = "telefono_prefijo", length = 11)
    private String telefonoPrefijo;

    @Column(name = "telefono_numero", length = 11)
    private String telefonoNumero;

    @Column(name = "telefono_texto", length = 100)
    private String telefonoTexto;

    @Column(name = "direccion_calle", length = 100)
    private String direccionCalle;

    @Column(name = "direccion_numero", length = 20)
    private String direccionNumero;

    @Column(name = "direccion_piso", length = 20)
    private String direccionPiso;

    @Column(name = "direccion_departamento", length = 20)
    private String direccionDepartamento;

    @Column(name = "direccion_localizacion_id", length = 20)
    private String direccionLocalizacionId;

    @Column(name = "tipo_persona", length = 50)
    @Enumerated(EnumType.STRING)
    private TipoPersona tipoPersona;

    @Column(name = "condicion_iva", length = 50)
    @Enumerated(EnumType.STRING)
    private CondicionImpositiva condicionImpositiva;

    public ClienteDb() {
    }

    public ClienteDb(Cliente cliente) {
        this.documentoTipo = cliente.getDocumento() != null ? cliente.getDocumento().getTipo() : null;
        this.documentoNumero = cliente.getDocumento() != null ? cliente.getDocumento().getNumero() : null;
        this.nombre = cliente.getNombre();
        this.apellido = cliente.getApellido();
        this.razonSocial = cliente.getRazonSocial();
        this.email = cliente.getEmail();
        this.nacionalidad = cliente.getNacionalidad();
        this.fechaNacimiento = cliente.getFechaNacimiento();
        this.sexo = cliente.getSexo();
        this.actividad = cliente.getActividad();
        this.pep = cliente.getPep();
        this.motivoPep = cliente.getMotivoPep();
        this.sujetoObligado = cliente.getSujetoObligado();
        this.telefonoPrefijo = cliente.getTelefono() != null ? cliente.getTelefono().getPrefijo() : null;
        this.telefonoNumero = cliente.getTelefono() != null ? cliente.getTelefono().getNumero() : null;
        this.telefonoTexto = cliente.getTelefonoTexto();
        this.direccionCalle = cliente.getDireccion() != null ? cliente.getDireccion().getCalle() : null;
        this.direccionNumero = cliente.getDireccion() != null ? cliente.getDireccion().getNumero() : null;
        this.direccionPiso = cliente.getDireccion() != null ? cliente.getDireccion().getPiso() : null;
        this.direccionDepartamento = cliente.getDireccion() != null ? cliente.getDireccion().getDepartamento() : null;
        this.direccionLocalizacionId = cliente.getDireccion() != null && cliente.getDireccion().getLocalizacion() != null ? cliente.getDireccion().getLocalizacion().getId() : null;
        this.tipoPersona = cliente.getTipoPersona();
        this.condicionImpositiva = cliente.getCondicionImpositiva();
    }

    public Cliente mapToCliente(){
        return new Cliente(
                documentoTipo != null || documentoNumero != null ?
                        new Documento(documentoTipo, documentoNumero)
                        : null,
                nombre,
                apellido,
                razonSocial,
                email,
                nacionalidad,
                fechaNacimiento,
                sexo,
                actividad,
                pep,
                motivoPep,
                sujetoObligado,
                telefonoPrefijo != null || telefonoNumero  != null ?
                            new Telefono(telefonoPrefijo, telefonoNumero)
                            : null,
                this.telefonoTexto,
                direccionCalle  != null ||
                        direccionCalle != null ||
                        direccionPiso != null ||
                        direccionPiso  != null ||
                        direccionLocalizacionId  != null ?
                            new Direccion(direccionCalle,
                                            direccionNumero,
                                            direccionPiso,
                                            direccionDepartamento,
                                            direccionLocalizacionId != null ? new Localizacion(direccionLocalizacionId) : null)
                            : null,
                tipoPersona,
                condicionImpositiva);
    }

    public Cliente mapToClienteEncabezado(){
        return new Cliente(
                documentoTipo != null || documentoNumero != null ?
                        new Documento(documentoTipo, documentoNumero)
                        : null,
                nombre,
                apellido,
                razonSocial,
                email,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                telefonoPrefijo != null || telefonoNumero  != null ?
                        new Telefono(telefonoPrefijo, telefonoNumero)
                        : null,
                this.telefonoTexto,
                direccionCalle  != null ||
                        direccionCalle != null ||
                        direccionPiso != null ||
                        direccionPiso  != null ||
                        direccionLocalizacionId  != null ?
                        new Direccion(direccionCalle,
                                direccionNumero,
                                direccionPiso,
                                direccionDepartamento,
                                direccionLocalizacionId != null ? new Localizacion(direccionLocalizacionId) : null)
                        : null,
                tipoPersona,
                condicionImpositiva);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoDocumento getDocumentoTipo() {
        return documentoTipo;
    }

    public void setDocumentoTipo(TipoDocumento documentoTipo) {
        this.documentoTipo = documentoTipo;
    }

    public String getDocumentoNumero() {
        return documentoNumero;
    }

    public void setDocumentoNumero(String documentoNumero) {
        this.documentoNumero = documentoNumero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public Boolean getPep() {
        return pep;
    }

    public void setPep(Boolean pep) {
        this.pep = pep;
    }

    public String getMotivoPep() {
        return motivoPep;
    }

    public void setMotivoPep(String motivoPep) {
        this.motivoPep = motivoPep;
    }

    public Boolean getSujetoObligado() {
        return sujetoObligado;
    }

    public void setSujetoObligado(Boolean sujetoObligado) {
        this.sujetoObligado = sujetoObligado;
    }

    public String getTelefonoPrefijo() {
        return telefonoPrefijo;
    }

    public void setTelefonoPrefijo(String telefonoPrefijo) {
        this.telefonoPrefijo = telefonoPrefijo;
    }

    public String getTelefonoNumero() {
        return telefonoNumero;
    }

    public void setTelefonoNumero(String telefonoNumero) {
        this.telefonoNumero = telefonoNumero;
    }

    public String getDireccionCalle() {
        return direccionCalle;
    }

    public void setDireccionCalle(String direccionCalle) {
        this.direccionCalle = direccionCalle;
    }

    public String getDireccionNumero() {
        return direccionNumero;
    }

    public void setDireccionNumero(String direccionNumero) {
        this.direccionNumero = direccionNumero;
    }

    public String getDireccionPiso() {
        return direccionPiso;
    }

    public void setDireccionPiso(String direccionPiso) {
        this.direccionPiso = direccionPiso;
    }

    public String getDireccionDepartamento() {
        return direccionDepartamento;
    }

    public void setDireccionDepartamento(String direccionDepartamento) {
        this.direccionDepartamento = direccionDepartamento;
    }

    public String getDireccionLocalizacionId() {
        return direccionLocalizacionId;
    }

    public void setDireccionLocalizacionId(String direccionLocalizacionId) {
        this.direccionLocalizacionId = direccionLocalizacionId;
    }

    public TipoPersona getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(TipoPersona tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public CondicionImpositiva getCondicionImpositiva() {
        return condicionImpositiva;
    }

    public void setCondicionImpositiva(CondicionImpositiva condicionImpositiva) {
        this.condicionImpositiva = condicionImpositiva;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getApellidoNombre() {
        return this.apellido != null && this.nombre != null ?
                this.apellido + " " + this.nombre : null;
    }

    public String getNombreCompleto() {
        return this.apellido != null && this.nombre != null ?
                this.nombre + " " + this.apellido : null;
    }
}

