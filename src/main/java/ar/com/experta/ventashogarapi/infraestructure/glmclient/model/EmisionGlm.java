package ar.com.experta.ventashogarapi.infraestructure.glmclient.model;

public class EmisionGlm {

    private String numeroPoliza;

    public EmisionGlm() {
    }

    public EmisionGlm(String numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }

    public String getNumeroPoliza() {
        return numeroPoliza;
    }

    public void setNumeroPoliza(String numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }
}
