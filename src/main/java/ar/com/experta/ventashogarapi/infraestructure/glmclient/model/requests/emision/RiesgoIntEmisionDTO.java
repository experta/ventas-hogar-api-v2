package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.emision;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RiesgoIntEmisionDTO {

    @JsonProperty("DomicilioIgualTomador")
    private String domicilioIgualTomador;
    @JsonProperty("DomicilioCalle")
    private String domicilioCalle;
    @JsonProperty("DomicilioNro")
    private String domicilioNro;
    @JsonProperty("DomicilioPiso")
    private String domicilioPiso;
    @JsonProperty("DomicilioDpto")
    private String domicilioDpto;
    @JsonProperty("CodigoPostal")
    private String codigoPostal;
    @JsonProperty("SubCodigoPostal")
    private String subCodigoPostal;

    public RiesgoIntEmisionDTO() {

    }

    public RiesgoIntEmisionDTO(String domicilioIgualTomador, String domicilioCalle, String domicilioNro, String domicilioPiso, String domicilioDpto, String codigoPostal, String subCodigoPostal) {
        this.domicilioIgualTomador = domicilioIgualTomador;
        this.domicilioCalle = domicilioCalle;
        this.domicilioNro = domicilioNro;
        this.domicilioPiso = domicilioPiso;
        this.domicilioDpto = domicilioDpto;
        this.codigoPostal = codigoPostal;
        this.subCodigoPostal = subCodigoPostal;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getSubCodigoPostal() {
        return subCodigoPostal;
    }

    public void setSubCodigoPostal(String subCodigoPostal) {
        this.subCodigoPostal = subCodigoPostal;
    }

    public String getDomicilioIgualTomador() {
        return domicilioIgualTomador;
    }

    public void setDomicilioIgualTomador(String domicilioIgualTomador) {
        this.domicilioIgualTomador = domicilioIgualTomador;
    }

    public String getDomicilioCalle() {
        return domicilioCalle;
    }

    public void setDomicilioCalle(String domicilioCalle) {
        this.domicilioCalle = domicilioCalle;
    }

    public String getDomicilioNro() {
        return domicilioNro;
    }

    public void setDomicilioNro(String domicilioNro) {
        this.domicilioNro = domicilioNro;
    }

    public String getDomicilioPiso() {
        return domicilioPiso;
    }

    public void setDomicilioPiso(String domicilioPiso) {
        this.domicilioPiso = domicilioPiso;
    }

    public String getDomicilioDpto() {
        return domicilioDpto;
    }

    public void setDomicilioDpto(String domicilioDpto) {
        this.domicilioDpto = domicilioDpto;
    }
}
