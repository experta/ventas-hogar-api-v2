package ar.com.experta.ventashogarapi.infraestructure.db.model;

import ar.com.experta.ventashogarapi.core.model.Direccion;
import ar.com.experta.ventashogarapi.core.model.Localizacion;
import ar.com.experta.ventashogarapi.core.model.Vivienda;
import ar.com.experta.ventashogarapi.core.model.enums.TipoVivienda;

import javax.persistence.*;

@Entity
@Table(name = "ventas_viviendas")
public class ViviendaDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "tipo_vivienda", length = 100)
    @Enumerated(EnumType.STRING)
    private TipoVivienda tipoVivienda;

    @Column(name = "direccion_tomador", length = 10)
    private Boolean direccionTomador;

    @Column(name = "direccion_calle", length = 100)
    private String direccionCalle;

    @Column(name = "direccion_numero", length = 100)
    private String direccionNumero;

    @Column(name = "direccion_piso", length = 100)
    private String direccionPiso;

    @Column(name = "direccion_departamento", length = 100)
    private String direccionDepartamento;

    @Column(name = "direccion_localizacion_id", length = 100)
    private String direccionLocalizacionId;

    @Column(name = "tiene_rejas")
    private Boolean tieneRejas;

    @Column(name = "metros_cuadrados")
    private Integer metrosCuadrados;


    public ViviendaDb() {
    }

    public ViviendaDb(Vivienda vivienda) {
        this.tipoVivienda = vivienda.getTipoVivienda();
        this.direccionTomador = vivienda.getDireccionTomador();
        this.direccionCalle = vivienda.getDireccion() != null ? vivienda.getDireccion().getCalle() : null;
        this.direccionNumero = vivienda.getDireccion() != null ? vivienda.getDireccion().getNumero() : null;
        this.direccionPiso = vivienda.getDireccion() != null ? vivienda.getDireccion().getPiso() : null;
        this.direccionDepartamento = vivienda.getDireccion() != null ? vivienda.getDireccion().getDepartamento() : null;
        this.direccionLocalizacionId = vivienda.getDireccion() != null && vivienda.getDireccion().getLocalizacion() != null ? vivienda.getDireccion().getLocalizacion().getId() : null;
        this.tieneRejas = vivienda.getTieneRejas();
        this.metrosCuadrados = vivienda.getMetrosCuadrados();
    }

    public Vivienda mapToVivienda(){
        return new Vivienda(tipoVivienda,
                direccionTomador,
                direccionCalle  != null ||
                        direccionCalle != null ||
                        direccionPiso != null ||
                        direccionPiso  != null ||
                        direccionLocalizacionId  != null ?
                        new Direccion(direccionCalle,
                                direccionNumero,
                                direccionPiso,
                                direccionDepartamento,
                                direccionLocalizacionId != null ? new Localizacion(direccionLocalizacionId) : null)
                        : null,
                tieneRejas,
                metrosCuadrados);
    }

    public Vivienda mapToViviendaEncabezado(){
        return new Vivienda(tipoVivienda,
                null,
                direccionCalle  != null ||
                        direccionCalle != null ||
                        direccionPiso != null ||
                        direccionPiso  != null ||
                        direccionLocalizacionId  != null ?
                        new Direccion(direccionCalle,
                                direccionNumero,
                                direccionPiso,
                                direccionDepartamento,
                                direccionLocalizacionId != null ? new Localizacion(direccionLocalizacionId) : null)
                        : null,
                null,
                metrosCuadrados);
    }

    public Boolean getDireccionTomador() {
        return direccionTomador;
    }

    public void setDireccionTomador(Boolean direccionTomador) {
        this.direccionTomador = direccionTomador;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoVivienda getTipoVivienda() {
        return tipoVivienda;
    }

    public void setTipoVivienda(TipoVivienda tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    public String getDireccionCalle() {
        return direccionCalle;
    }

    public void setDireccionCalle(String direccionCalle) {
        this.direccionCalle = direccionCalle;
    }

    public String getDireccionNumero() {
        return direccionNumero;
    }

    public void setDireccionNumero(String direccionNumero) {
        this.direccionNumero = direccionNumero;
    }

    public String getDireccionPiso() {
        return direccionPiso;
    }

    public void setDireccionPiso(String direccionPiso) {
        this.direccionPiso = direccionPiso;
    }

    public String getDireccionDepartamento() {
        return direccionDepartamento;
    }

    public void setDireccionDepartamento(String direccionDepartamento) {
        this.direccionDepartamento = direccionDepartamento;
    }

    public String getDireccionLocalizacionId() {
        return direccionLocalizacionId;
    }

    public void setDireccionLocalizacionId(String direccionLocalizacionId) {
        this.direccionLocalizacionId = direccionLocalizacionId;
    }

    public Boolean getTieneRejas() {
        return tieneRejas;
    }

    public void setTieneRejas(Boolean tieneRejas) {
        this.tieneRejas = tieneRejas;
    }

    public Integer getMetrosCuadrados() {
        return metrosCuadrados;
    }

    public void setMetrosCuadrados(Integer metrosCuadrados) {
        this.metrosCuadrados = metrosCuadrados;
    }
}
