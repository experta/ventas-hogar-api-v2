package ar.com.experta.ventashogarapi.infraestructure.db;

import ar.com.experta.ventashogarapi.commons.exceptions.NotImplementedException;
import ar.com.experta.ventashogarapi.commons.exceptions.notfound.PlanNotFoundException;
import ar.com.experta.ventashogarapi.core.model.Plan;
import ar.com.experta.ventashogarapi.core.model.enums.IdPlan;
import ar.com.experta.ventashogarapi.core.model.enums.TipoPlan;
import ar.com.experta.ventashogarapi.core.model.enums.TipoProductos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PlanDAO {

    @Autowired
    PlanConfiguration planConfiguration;

    public PlanDAO() {
    }

    public List<Plan> getPlanes() {
        return planConfiguration.getPlanes();
    }

    public List<Plan> getPlanes(List<TipoProductos> roles, TipoPlan tipoPlan) {
        //TODO: reemplazar por DB
        return  this.getPlanes()
                .stream()
                .filter(plan -> roles == null || CollectionUtils.containsAny(plan.getTiposProductos(),roles))
                .filter(plan -> tipoPlan == null || plan.getTipoPlan().equals(tipoPlan))
                .collect(Collectors.toList());
    }

    public Plan getPlan(IdPlan idPlan) {
        //TODO: reemplazar por DB
        return this.getPlanes()
                .stream()
                .filter(plan -> plan.getIdPlan().equals(idPlan))
                .findAny()
                .orElseThrow(PlanNotFoundException::new);
    }

    public Plan save(Plan plan){
        //TODO: implementar
        throw new NotImplementedException();
    }


}