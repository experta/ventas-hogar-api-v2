package ar.com.experta.ventashogarapi.infraestructure.glmclient.model;

import java.math.BigDecimal;

public class CotizacionGLM {

    private BigDecimal prima;
    private BigDecimal premio;
    private BigDecimal impuestos;
    private Integer cuotas;
    private String numeroSolicitud;
    private String codigoProducto;

    public CotizacionGLM() {
    }

    public CotizacionGLM(BigDecimal prima, BigDecimal premio, BigDecimal impuestos, Integer cuotas, String numeroSolicitud, String codigoProducto) {
        this.prima = prima;
        this.premio = premio;
        this.impuestos = impuestos;
        this.numeroSolicitud = numeroSolicitud;
        this.codigoProducto = codigoProducto;
        this.cuotas = cuotas;
    }

    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public BigDecimal getPrima() {
        return prima;
    }

    public void setPrima(BigDecimal prima) {
        this.prima = prima;
    }

    public BigDecimal getPremio() {
        return premio;
    }

    public void setPremio(BigDecimal premio) {
        this.premio = premio;
    }

    public BigDecimal getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(BigDecimal impuestos) {
        this.impuestos = impuestos;
    }

    public Integer getCuotas() {
        return cuotas;
    }

    public void setCuotas(Integer cuotas) {
        this.cuotas = cuotas;
    }
}
