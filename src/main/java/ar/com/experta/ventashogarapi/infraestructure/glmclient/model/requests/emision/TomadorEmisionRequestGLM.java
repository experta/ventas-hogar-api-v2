package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.emision;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TomadorEmisionRequestGLM {

    @JsonProperty("tipodocumentocodigo")
    private String tipoDocumentoCodigo;
    @JsonProperty("documentonumero")
    private String documentoNumero;
    @JsonProperty("nacionalidadcodigo")
    private String nacionalidadCodigo;
    @JsonProperty("nombre")
    private String nombre;
    @JsonProperty("categoriaivacodigo")
    private String categoriaIvaCodigo;
    @JsonProperty("callenombre")
    private String calleNombre;
    @JsonProperty("callenumero")
    private String calleNumero;
    @JsonProperty("callepiso")
    private String callePiso;
    @JsonProperty("calledepto")
    private String calleDepto;
    @JsonProperty("codigopostal")
    private String codigoPostal;
    @JsonProperty("subcodigopostal")
    private String subCodigoPostal;
    @JsonProperty("telefono")
    private String telefono;
    @JsonProperty("email")
    private String email;
    @JsonProperty("fechanacimiento")
    private String fechaNacimiento;
    @JsonProperty("sexo")
    private String sexo;
    @JsonProperty("estadocivilcodigo")
    private String estadoCivilCodigo;
    @JsonProperty("lugarnacimiento")
    private String lugarNacimiento;
    @JsonProperty("PEP")
    private String personaExpuestaPoliticamente;
    @JsonProperty("DeclaraTitular")
    private String declaraTitular;
    @JsonProperty("cargo")
    private String cargo;
    @JsonProperty("organismo")
    private String organismo;
    @JsonProperty("relacion")
    private String relacion;

    public TomadorEmisionRequestGLM() {
    }

    public TomadorEmisionRequestGLM(String tipoDocumentoCodigo, String documentoNumero, String nacionalidadCodigo, String nombre, String categoriaIvaCodigo, String calleNombre, String calleNumero, String callePiso, String calleDepto, String codigoPostal, String subCodigoPostal, String telefono, String email, String fechaNacimiento, String sexo, String estadoCivilCodigo, String lugarNacimiento, String personaExpuestaPoliticamente, String cargo, String organismo, String relacion, String declaraTitular) {
        this.tipoDocumentoCodigo = tipoDocumentoCodigo;
        this.documentoNumero = documentoNumero;
        this.nacionalidadCodigo = nacionalidadCodigo;
        this.nombre = nombre;
        this.categoriaIvaCodigo = categoriaIvaCodigo;
        this.calleNombre = calleNombre;
        this.calleNumero = calleNumero;
        this.callePiso = callePiso;
        this.calleDepto = calleDepto;
        this.codigoPostal = codigoPostal;
        this.subCodigoPostal = subCodigoPostal;
        this.telefono = telefono;
        this.email = email;
        this.fechaNacimiento = fechaNacimiento;
        this.sexo = sexo;
        this.estadoCivilCodigo = estadoCivilCodigo;
        this.lugarNacimiento = lugarNacimiento;
        this.personaExpuestaPoliticamente = personaExpuestaPoliticamente;
        this.cargo = cargo;
        this.organismo = organismo;
        this.relacion = relacion;
        this.declaraTitular = declaraTitular;
    }

    public String getTipoDocumentoCodigo() {
        return tipoDocumentoCodigo;
    }

    public void setTipoDocumentoCodigo(String tipoDocumentoCodigo) {
        this.tipoDocumentoCodigo = tipoDocumentoCodigo;
    }

    public String getDocumentoNumero() {
        return documentoNumero;
    }

    public void setDocumentoNumero(String documentoNumero) {
        this.documentoNumero = documentoNumero;
    }

    public String getNacionalidadCodigo() {
        return nacionalidadCodigo;
    }

    public void setNacionalidadCodigo(String nacionalidadCodigo) {
        this.nacionalidadCodigo = nacionalidadCodigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoriaIvaCodigo() {
        return categoriaIvaCodigo;
    }

    public void setCategoriaIvaCodigo(String categoriaIvaCodigo) {
        this.categoriaIvaCodigo = categoriaIvaCodigo;
    }

    public String getCalleNombre() {
        return calleNombre;
    }

    public void setCalleNombre(String calleNombre) {
        this.calleNombre = calleNombre;
    }

    public String getCalleNumero() {
        return calleNumero;
    }

    public void setCalleNumero(String calleNumero) {
        this.calleNumero = calleNumero;
    }

    public String getCallePiso() {
        return callePiso;
    }

    public void setCallePiso(String callePiso) {
        this.callePiso = callePiso;
    }

    public String getCalleDepto() {
        return calleDepto;
    }

    public void setCalleDepto(String calleDepto) {
        this.calleDepto = calleDepto;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getSubCodigoPostal() {
        return subCodigoPostal;
    }

    public void setSubCodigoPostal(String subCodigoPostal) {
        this.subCodigoPostal = subCodigoPostal;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEstadoCivilCodigo() {
        return estadoCivilCodigo;
    }

    public void setEstadoCivilCodigo(String estadoCivilCodigo) {
        this.estadoCivilCodigo = estadoCivilCodigo;
    }

    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    public void setLugarNacimiento(String lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }

    public String getPersonaExpuestaPoliticamente() {
        return personaExpuestaPoliticamente;
    }

    public void setPersonaExpuestaPoliticamente(String personaExpuestaPoliticamente) {
        this.personaExpuestaPoliticamente = personaExpuestaPoliticamente;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getOrganismo() {
        return organismo;
    }

    public void setOrganismo(String organismo) {
        this.organismo = organismo;
    }

    public String getRelacion() {
        return relacion;
    }

    public void setRelacion(String relacion) {
        this.relacion = relacion;
    }

    public String getDeclaraTitular() {
        return declaraTitular;
    }

    public void setDeclaraTitular(String declaraTitular) {
        this.declaraTitular = declaraTitular;
    }
}
