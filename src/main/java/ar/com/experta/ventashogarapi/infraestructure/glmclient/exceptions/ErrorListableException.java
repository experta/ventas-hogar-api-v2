package ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions;

import java.util.List;

public abstract class ErrorListableException extends Exception{

    public ErrorListableException() {
    }

    public ErrorListableException(String message) {
        super(message);
    }

    public abstract List<String> getErrorList();

}
