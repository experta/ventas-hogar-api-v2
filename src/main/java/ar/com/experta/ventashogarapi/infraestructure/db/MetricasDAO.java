package ar.com.experta.ventashogarapi.infraestructure.db;

import ar.com.experta.ventashogarapi.core.model.enums.Beneficio;
import ar.com.experta.ventashogarapi.core.model.enums.CanalVenta;
import ar.com.experta.ventashogarapi.core.model.enums.TipoPlan;
import ar.com.experta.ventashogarapi.infraestructure.db.model.VentaDb;
import ar.com.experta.ventashogarapi.infraestructure.db.repository.MetricasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class MetricasDAO {

    @Autowired
    private MetricasRepository metricasRepository;

    public Integer getCotizacionesTotales(LocalDate fechaDesde, LocalDate fechaHasta, TipoPlan tipoPlan, CanalVenta canalVenta, Beneficio beneficio) {
        return metricasRepository.countCotizacionesTotales(
                fechaDesde.atStartOfDay(),
                fechaHasta.atTime(23,59),
                tipoPlan,
                beneficio != null ? "TRUE" : ( tipoPlan != null ? "FALSE" : null),
                beneficio,
                canalVenta
        );
    }

    public Integer getEmisionesTotales(LocalDate fechaDesde, LocalDate fechaHasta, TipoPlan tipoPlan, CanalVenta canalVenta, Beneficio beneficio) {
        return metricasRepository.countEmisionesTotales(
                fechaDesde.atStartOfDay(),
                fechaHasta.atTime(23,59),
                tipoPlan,
                beneficio != null ? "TRUE" : ( tipoPlan != null ? "FALSE" : null),
                beneficio,
                canalVenta
        );
    }

    public Map<String, Integer> getCotizacionesPorDia(LocalDate fechaDesde, LocalDate fechaHasta, TipoPlan tipoPlan, CanalVenta canalVenta, Beneficio beneficio) {
        return metricasRepository.getCotizacionesPorDia(
                        fechaDesde,
                        fechaHasta,
                        tipoPlan != null ? tipoPlan.toString() : null,
                        beneficio != null ? "TRUE" : ( tipoPlan != null ? "FALSE" : null),
                        beneficio != null ? beneficio.toString() : null,
                        canalVenta != null ? canalVenta.toString() : null
                )
                .stream()
                .collect(Collectors.toMap(MetricasRepository.MetricaGraficoStats::getNombre, MetricasRepository.MetricaGraficoStats::getTotal));
    }

    public Map<String, Integer> getCotizacionesPorMes(LocalDate fechaDesde, LocalDate fechaHasta, TipoPlan tipoPlan, CanalVenta canalVenta, Beneficio beneficio) {
        return metricasRepository.getCotizacionesPorMes(
                        fechaDesde,
                        fechaHasta,
                        tipoPlan != null ? tipoPlan.toString() : null,
                        beneficio != null ? "TRUE" : ( tipoPlan != null ? "FALSE" : null),
                        beneficio != null ? beneficio.toString() : null,
                        canalVenta != null ? canalVenta.toString() : null
                )
                .stream()
                .collect(Collectors.toMap(MetricasRepository.MetricaGraficoStats::getNombre, MetricasRepository.MetricaGraficoStats::getTotal));
    }

    public Map<String, Integer> getEmisionesPorDia(LocalDate fechaDesde, LocalDate fechaHasta, TipoPlan tipoPlan, CanalVenta canalVenta, Beneficio beneficio) {
        return metricasRepository.getEmisionesPorDia(
                        fechaDesde,
                        fechaHasta,
                        tipoPlan != null ? tipoPlan.toString() : null,
                        beneficio != null ? "TRUE" : ( tipoPlan != null ? "FALSE" : null),
                        beneficio != null ? beneficio.toString() : null,
                        canalVenta != null ? canalVenta.toString() : null
                )
                .stream()
                .collect(Collectors.toMap(MetricasRepository.MetricaGraficoStats::getNombre, MetricasRepository.MetricaGraficoStats::getTotal));
    }

    public Map<String, Integer> getEmisionesPorMes(LocalDate fechaDesde, LocalDate fechaHasta, TipoPlan tipoPlan, CanalVenta canalVenta, Beneficio beneficio) {
        return metricasRepository.getEmisionesPorMes(
                        fechaDesde,
                        fechaHasta,
                        tipoPlan != null ? tipoPlan.toString() : null,
                        beneficio != null ? "TRUE" : ( tipoPlan != null ? "FALSE" : null),
                        beneficio != null ? beneficio.toString() : null,
                        canalVenta != null ? canalVenta.toString() : null
                )
                .stream()
                .collect(Collectors.toMap(MetricasRepository.MetricaGraficoStats::getNombre, MetricasRepository.MetricaGraficoStats::getTotal));
    }

    public List<VentaDb> getVentasReporteCotizaciones(LocalDate fechaDesde, LocalDate fechaHasta, TipoPlan tipoPlan, CanalVenta canalVenta, Beneficio beneficio) {
        return metricasRepository.getVentasReporteCotizaciones(
                fechaDesde.atStartOfDay(),
                fechaHasta.atTime(23,59),
                tipoPlan,
                beneficio != null ? "TRUE" : ( tipoPlan != null ? "FALSE" : null),
                beneficio,
                canalVenta
        );
    }
}
