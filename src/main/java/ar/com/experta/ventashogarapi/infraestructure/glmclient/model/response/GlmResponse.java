package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response;

import ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.JsonParseException;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GlmResponse {


    private String status;
    @JsonProperty("Description")
    private String description;
    private String detail;
    @JsonProperty("JsonResult")
    private String jsonResult;

    public GlmResponse(){}

    public GlmResponse(String status, String description, String detail, String jsonResult) {
        this.status = status;
        this.description = description;
        this.detail = detail;
        this.jsonResult = jsonResult;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getJsonResult() {
        return jsonResult;
    }

    public void setJsonResult(String jsonResult) {
        this.jsonResult = jsonResult;
    }



    public ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.CrearEmisionResponse jsonResponseToEmisionResponse(ObjectMapper objectMapper) throws JsonParseException {
        if(jsonResult == null || jsonResult.isEmpty())
            return new ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.CrearEmisionResponse();
        try {
            return objectMapper.readValue(jsonResult, ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.CrearEmisionResponse.class);
        } catch (Exception e) {
            throw new JsonParseException();
        }

    }

    public ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.CotizacionIntegralesResponse jsonResponseToCotizarIntegralesResponse(ObjectMapper objectMapper) throws JsonParseException {
        if(jsonResult == null || jsonResult.isEmpty())
            return new ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.CotizacionIntegralesResponse();
        try {
            return objectMapper.readValue(jsonResult, ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.CotizacionIntegralesResponse.class);
        } catch (Exception e) {
            throw new JsonParseException();
        }

    }



}
