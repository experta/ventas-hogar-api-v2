package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.emision;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmitirCotizacionDTO {

    @JsonProperty("Rama")
    private String rama;
    @JsonProperty("Solicitud")
    private String solicitud;
    @JsonProperty("Instalacion")
    private String instalacion;
    @JsonProperty("Tomador")
    private TomadorDTO tomador;
    @JsonProperty("AseguradoEsTomador")
    private String aseguradoEsTomador;
    @JsonProperty("ProductoSeleccionado")
    private String productoSeleccionado;
    @JsonProperty("FormaPago")
    private String formaPago;
    @JsonProperty("FormaPagoTarjetaCodigo")
    private String formaPagoTarjetaCodigo;
    @JsonProperty("FormaPagoTarjetaNumero")
    private String formaPagoTarjetaNumero;
    @JsonProperty("FormaPagoTarjetaBancoCodigo")
    private String formaPagoTarjetaBancoCodigo;
    @JsonProperty("FormaPagoTarjetaVencimiento")
    private String formaPagoTarjetaVencimiento;
    @JsonProperty("FormaPagoDebitoCBU")
    private String formaPagoDebitoCBU;
    @JsonProperty("FormaPagoDebitoBancoCodigo")
    private String formaPagoDebitoBancoCodigo;
    @JsonProperty("FormaPagoOBBancoCodigo")
    private String formaPagoOBBancoCodigo;
    @JsonProperty("FormaPagoOBOperatoriaId")
    private String formaPagoOBOperatoriaId;

    @JsonProperty("FormaPagoOBNumeroCuenta")
    private String formaPagoOBNumeroCuenta;
    @JsonProperty("PolizaElectronicaAceptar")
    private String polizaElectronicaAceptar;
    @JsonProperty("PolizaElectronicaEmail")
    private String polizaElectronicaEmail;
    @JsonProperty("IdPorcentaje")
    private String idPorcentaje;
    @JsonProperty("Porcentaje")
    private String porcentaje;
    @JsonProperty("Frecuencia")
    private String frecuencia;
    @JsonProperty("RiesgoINT")
    private RiesgoIntEmisionDTO riesgoINT;

    public EmitirCotizacionDTO() {
    }

    public EmitirCotizacionDTO(String rama, String solicitud, String instalacion, TomadorDTO tomador, String aseguradoEsTomador, String productoSeleccionado, String formaPago, String formaPagoTarjetaCodigo, String formaPagoTarjetaNumero, String formaPagoTarjetaBancoCodigo, String formaPagoTarjetaVencimiento, String formaPagoDebitoCBU, String formaPagoDebitoBancoCodigo, String formaPagoOBBancoCodigo, String formaPagoOBOperatoriaId, String formaPagoOBNumeroCuenta, String polizaElectronicaAceptar,String polizaElectronicaEmail, String idPorcentaje, String porcentaje, String frecuencia, RiesgoIntEmisionDTO riesgoINT ) {
        this.rama = rama;
        this.solicitud = solicitud;
        this.instalacion = instalacion;
        this.tomador = tomador;
        this.aseguradoEsTomador = aseguradoEsTomador;
        this.productoSeleccionado = productoSeleccionado;
        this.formaPago = formaPago;
        this.formaPagoTarjetaCodigo = formaPagoTarjetaCodigo;
        this.formaPagoTarjetaNumero = formaPagoTarjetaNumero;
        this.formaPagoTarjetaBancoCodigo = formaPagoTarjetaBancoCodigo;
        this.formaPagoTarjetaVencimiento = formaPagoTarjetaVencimiento;
        this.formaPagoDebitoCBU = formaPagoDebitoCBU;
        this.formaPagoDebitoBancoCodigo = formaPagoDebitoBancoCodigo;
        this.formaPagoOBBancoCodigo = formaPagoOBBancoCodigo;
        this.formaPagoOBOperatoriaId = formaPagoOBOperatoriaId;
        this.formaPagoOBNumeroCuenta = formaPagoOBNumeroCuenta;
        this.polizaElectronicaAceptar = polizaElectronicaAceptar;
        this.polizaElectronicaEmail = polizaElectronicaEmail;
        this.idPorcentaje = idPorcentaje;
        this.porcentaje = porcentaje;
        this.frecuencia = frecuencia;
        this.riesgoINT = riesgoINT;
    }

    public String getRama() {
        return rama;
    }

    public void setRama(String rama) {
        this.rama = rama;
    }

    public String getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(String solicitud) {
        this.solicitud = solicitud;
    }

    public String getInstalacion() {
        return instalacion;
    }

    public void setInstalacion(String instalacion) {
        this.instalacion = instalacion;
    }

    public TomadorDTO getTomador() {
        return tomador;
    }

    public void setTomador(TomadorDTO tomador) {
        this.tomador = tomador;
    }

    public String getAseguradoEsTomador() {
        return aseguradoEsTomador;
    }

    public void setAseguradoEsTomador(String aseguradoEsTomador) {
        this.aseguradoEsTomador = aseguradoEsTomador;
    }

    public String getProductoSeleccionado() {
        return productoSeleccionado;
    }

    public void setProductoSeleccionado(String productoSeleccionado) {
        this.productoSeleccionado = productoSeleccionado;
    }

    public RiesgoIntEmisionDTO getRiesgoINT() {
        return riesgoINT;
    }

    public void setRiesgoINT(RiesgoIntEmisionDTO riesgoINT) {
        this.riesgoINT = riesgoINT;
    }

    public String getFormaPagoTarjetaCodigo() {
        return formaPagoTarjetaCodigo;
    }

    public void setFormaPagoTarjetaCodigo(String formaPagoTarjetaCodigo) {
        this.formaPagoTarjetaCodigo = formaPagoTarjetaCodigo;
    }

    public String getFormaPagoTarjetaNumero() {
        return formaPagoTarjetaNumero;
    }

    public void setFormaPagoTarjetaNumero(String formaPagoTarjetaNumero) {
        this.formaPagoTarjetaNumero = formaPagoTarjetaNumero;
    }

    public String getFormaPagoTarjetaBancoCodigo() {
        return formaPagoTarjetaBancoCodigo;
    }

    public void setFormaPagoTarjetaBancoCodigo(String formaPagoTarjetaBancoCodigo) {
        this.formaPagoTarjetaBancoCodigo = formaPagoTarjetaBancoCodigo;
    }

    public String getFormaPagoTarjetaVencimiento() {
        return formaPagoTarjetaVencimiento;
    }

    public void setFormaPagoTarjetaVencimiento(String formaPagoTarjetaVencimiento) {
        this.formaPagoTarjetaVencimiento = formaPagoTarjetaVencimiento;
    }

    public String getFormaPagoDebitoCBU() {
        return formaPagoDebitoCBU;
    }

    public void setFormaPagoDebitoCBU(String formaPagoDebitoCBU) {
        this.formaPagoDebitoCBU = formaPagoDebitoCBU;
    }

    public String getFormaPagoDebitoBancoCodigo() {
        return formaPagoDebitoBancoCodigo;
    }

    public void setFormaPagoDebitoBancoCodigo(String formaPagoDebitoBancoCodigo) {
        this.formaPagoDebitoBancoCodigo = formaPagoDebitoBancoCodigo;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    public String getPolizaElectronicaAceptar() {
        return polizaElectronicaAceptar;
    }

    public void setPolizaElectronicaAceptar(String polizaElectronicaAceptar) {
        this.polizaElectronicaAceptar = polizaElectronicaAceptar;
    }

    public String getPolizaElectronicaEmail() {
        return polizaElectronicaEmail;
    }

    public void setPolizaElectronicaEmail(String polizaElectronicaEmail) {
        this.polizaElectronicaEmail = polizaElectronicaEmail;
    }

    public String getIdPorcentaje() {
        return idPorcentaje;
    }

    public void setIdPorcentaje(String idPorcentaje) {
        this.idPorcentaje = idPorcentaje;
    }

    public String getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(String porcentaje) {
        this.porcentaje = porcentaje;
    }

    public String getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }
}
