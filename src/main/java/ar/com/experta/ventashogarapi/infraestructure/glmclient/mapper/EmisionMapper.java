package ar.com.experta.ventashogarapi.infraestructure.glmclient.mapper;


import ar.com.experta.ventashogarapi.core.model.*;
import ar.com.experta.ventashogarapi.core.model.enums.MedioPago;
import ar.com.experta.ventashogarapi.core.model.enums.TipoCobertura;
import ar.com.experta.ventashogarapi.core.model.enums.TipoPersona;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.ClienteRechazadoDuplicadoException;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.ClienteRechazadoFraudeException;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.CoreBusinessException;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.EmisionGlm;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.enums.*;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.emision.EmitirCotizacionDTO;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.emision.RiesgoIntEmisionDTO;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.emision.TomadorDTO;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.CrearEmisionResponse;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.ErroresResponse;

import java.time.LocalDate;
import java.util.Locale;
import java.util.stream.Collectors;

public class EmisionMapper {

    public static final String NACIONALIDAD_DEFAULT = "1"; //No informado
    private static final SexoGLM SEXO_DEFAULT = SexoGLM.FEMENINO;
    private static final String ESTADO_CIVIL_DEFAULT = "99";
    private static final String DECLARA_TITULAR_DEFAULT = "S";
    private static final String ES_PEP = "S";
    private static final String NO_PEP = "N";
    private static final String ES_SUJETO_OBLIGADO = "S";
    private static final String NO_SUJETO_OBLIGADO = "N";
    private static final String RAMA_PRODUCTO = "15";
    public static final String INSTALACION = "0";
    public static final String BANCO_DEFAULT_PAGO_TARJETA = "999";
    public static final String OP_BANCARIA_DTV_BANCO = "007";
    public static final String OP_BANCARIA_DTV_ID = "2";
    public static final String OP_BANCARIA_DTV_CUENTA = "1430001713001895370012";

    public static final String POLIZA_ELECTRONICA = "S";

    public static EmitirCotizacionDTO toEmitirCotizacionDTO(String numeroCotizacion, String codigoProducto, Vivienda vivienda, Cliente cliente, Pago pago) {

        TomadorDTO tomador = new TomadorDTO(
                TipoDocumentoGLM.valueOf(cliente.getDocumento().getTipo().toString()).codigoGlm(),
                cliente.getDocumento().getNumero(),
                cliente.getNacionalidad() != null ? cliente.getNacionalidad() : NACIONALIDAD_DEFAULT,
                cliente.getTipoPersona().equals(TipoPersona.FISICA) ? cliente.getApellidoNombre() : cliente.getRazonSocial(),
                CondicionImpositivaGLM.valueOf(cliente.getCondicionImpositiva().toString()).codigoGlm(),
                cliente.getDireccion().getCalle(),
                cliente.getDireccion().getNumero(),
                cliente.getDireccion().getPiso(),
                cliente.getDireccion().getDepartamento(),
                cliente.getDireccion().getLocalizacion().getCodigoPostal(),
                cliente.getDireccion().getLocalizacion().getSubcp(),
                cliente.getTelefono() != null ? cliente.getTelefono().getNumeroCompleto() : cliente.getTelefonoTexto(),
                cliente.getEmail() != null ? cliente.getEmail().toLowerCase() : "",
                cliente.getFechaNacimiento() != null ? cliente.getFechaNacimiento().toString() : "",
                cliente.getSexo() != null ? SexoGLM.valueOf(cliente.getSexo().toString()).codigoGlm() : SEXO_DEFAULT.codigoGlm(),
                ESTADO_CIVIL_DEFAULT,
                null,
                DECLARA_TITULAR_DEFAULT,
                cliente.getActividad() != null ? cliente.getActividad() : "",
                cliente.getPep() != null && cliente.getPep() ? ES_PEP : NO_PEP,
                cliente.getPep() != null && cliente.getPep() && cliente.getMotivoPep() != null ? cliente.getMotivoPep() : "",
                cliente.getSujetoObligado() != null && cliente.getSujetoObligado() ? ES_SUJETO_OBLIGADO : NO_SUJETO_OBLIGADO
        );


        Direccion direccionVivienda = vivienda.getDireccionTomador() ? cliente.getDireccion() : vivienda.getDireccion();

        RiesgoIntEmisionDTO riesgoInt = new RiesgoIntEmisionDTO(
                vivienda.getDireccionTomador()  ? "S" : "N",
                direccionVivienda.getCalle(),
                direccionVivienda.getNumero(),
                direccionVivienda.getPiso(),
                direccionVivienda.getDepartamento(),
                direccionVivienda.getLocalizacion().getCodigoPostal(),
                direccionVivienda.getLocalizacion().getSubcp()
        );

        return new EmitirCotizacionDTO(RAMA_PRODUCTO,
                numeroCotizacion,
                INSTALACION,
                tomador,
                "S",
                codigoProducto,
                MedioPagoGLM.valueOf(pago.getMedioPago().toString()).codigoGlm(),
                pago.getMedioPago().equals(MedioPago.CREDITO) ?
                        ((PagoCredito)pago).getEmpresaTarjeta()
                        : null,
                pago.getMedioPago().equals(MedioPago.CREDITO) ?
                        ((PagoCredito)pago).getNumeroTarjeta()
                        : null,
                pago.getMedioPago().equals(MedioPago.CREDITO) ?
                        BANCO_DEFAULT_PAGO_TARJETA
                        : null,
                pago.getMedioPago().equals(MedioPago.CREDITO) ?
                        formatFechaVencimientoGLM(((PagoCredito)pago).getAnioVencimiento(), ((PagoCredito)pago).getMesVencimiento())
                        : null,
                pago.getMedioPago().equals(MedioPago.DEBITO) ?
                        ((PagoDebito)pago).getCbu()
                        : null,
                pago.getMedioPago().equals(MedioPago.DEBITO) ?
                        ((PagoDebito)pago).getBanco()
                        : null,
                pago.getMedioPago().equals(MedioPago.OP_BANCARIA_DTV) ?
                        OP_BANCARIA_DTV_BANCO
                        : null,
                pago.getMedioPago().equals(MedioPago.OP_BANCARIA_DTV) ?
                        OP_BANCARIA_DTV_ID
                        : null,
                pago.getMedioPago().equals(MedioPago.OP_BANCARIA_DTV) ?
                        OP_BANCARIA_DTV_CUENTA
                        : null,
                POLIZA_ELECTRONICA,
                cliente.getEmail() != null ? cliente.getEmail().toLowerCase() : "",
                "1",
                "30",
                "1",
                riesgoInt);

    }


    private static String formatFechaVencimientoGLM(Integer anio, Integer mes){
        LocalDate date = LocalDate.of(anio, mes, 1);
        return date.withDayOfMonth(date.lengthOfMonth()).toString();

    }

    public EmisionGlm mapResponseFromGLM(CrearEmisionResponse crearEmisionResponse) throws CoreBusinessException {
        if (crearEmisionResponse.getErrores() != null && !crearEmisionResponse.getErrores().isEmpty()){

            if (crearEmisionResponse.getErrores()
                    .stream()
                    .filter(error -> error.getCodigo().equals(ClienteRechazadoFraudeException.CODIGO_RECHAZO_GLM))
                    .findAny().isPresent()) {
                throw new ClienteRechazadoFraudeException(crearEmisionResponse.getErrores()
                        .stream()
                        .map(errorGlm -> mapErrorFinal(errorGlm))
                        .collect(Collectors.toList()));
            }else if (crearEmisionResponse.getErrores()
                    .stream()
                    .filter(error -> error.getCodigo().equals(ClienteRechazadoDuplicadoException.CODIGO_RECHAZO_GLM))
                    .findAny().isPresent()) {
                throw new ClienteRechazadoDuplicadoException(crearEmisionResponse.getErrores()
                        .stream()
                        .map(errorGlm -> mapErrorFinal(errorGlm))
                        .collect(Collectors.toList()));
            }else {
                throw new CoreBusinessException(crearEmisionResponse.getErrores()
                        .stream()
                        .map(errorGlm -> mapErrorFinal(errorGlm))
                        .collect(Collectors.toList()));
            }
        }

        return new EmisionGlm(crearEmisionResponse.getPoliza());
    }

    private String mapErrorFinal (ErroresResponse errorGlm) {
        switch(errorGlm.getCodigo()) {
            case "801": // Documento Duplicado en más de una Persona
                return "No se puede emitir la póliza con el tipo de documento ingresado para el cliente. Por favor comunicate con tu ejecutivo";
            case "803": // No se pudo emitir la operación. Tiene excepciones condicionantes: Persona alcanzada por Padrón Terroristas / Persona con Marca Fraude
                return "El cliente no es apto para emitir en Experta Seguros. Para mas informacion comunicate con tu ejecutivo.";
        }
        for (TipoCoberturaGLM tipoCoberturaGLM : TipoCoberturaGLM.values()){
            if (errorGlm.getDescripcion().toLowerCase(Locale.ROOT).contains( "cobertura " + tipoCoberturaGLM.codigoGlm())) {
                return errorGlm.getDescripcion().replace(" " +tipoCoberturaGLM.codigoGlm(), " " + TipoCobertura.valueOf(tipoCoberturaGLM.toString()).label()).trim().replaceAll("\\s{2,}", " ");
            }
        }

        return errorGlm.getDescripcion();
    }


}
