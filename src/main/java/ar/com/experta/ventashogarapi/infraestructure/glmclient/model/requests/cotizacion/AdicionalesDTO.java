package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdicionalesDTO {

    @JsonProperty("Tipo")
    private String Tipo;

    @JsonProperty("Codigo")
    private String Codigo;

    @JsonProperty("ValorAsegurado")
    private String ValorAsegurado;

    public AdicionalesDTO() {
    }

    public AdicionalesDTO(String tipo, String codigo, String valorAsegurado) {
        Tipo = tipo;
        Codigo = codigo;
        ValorAsegurado = valorAsegurado;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String tipo) {
        Tipo = tipo;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getValorAsegurado() {
        return ValorAsegurado;
    }

    public void setValorAsegurado(String valorAsegurado) {
        ValorAsegurado = valorAsegurado;
    }
}
