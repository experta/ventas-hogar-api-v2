package ar.com.experta.ventashogarapi.infraestructure.db.model;

import ar.com.experta.ventashogarapi.core.model.Pago;
import ar.com.experta.ventashogarapi.core.model.PagoCredito;
import ar.com.experta.ventashogarapi.core.model.PagoCuponera;
import ar.com.experta.ventashogarapi.core.model.PagoDebito;
import ar.com.experta.ventashogarapi.core.model.enums.MedioPago;

import javax.persistence.*;

@Entity
@Table(name = "ventas_pagos")
public class PagoDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "medio_pago", length = 50)
    @Enumerated(EnumType.STRING)
    private MedioPago medioPago;

    @Column(name = "tarjeta_empresa", length = 100)
    private String empresaTarjeta;

    @Column(name = "tarjeta_numero", length = 20)
    private String numeroTarjeta;

    @Column(name = "tarjeta_vencimiento_anio", length = 10)
    private Integer anioVencimiento;

    @Column(name = "tarjeta_vencimiento_mes", length = 10)
    private Integer mesVencimiento;

    @Column(name = "banco", length = 100)
    private String banco;

    @Column(name = "cbu", length = 50)
    private String cbu;

    public PagoDb() {
    }

    public PagoDb(Pago pago) {
        this.medioPago = pago.getMedioPago();
        this.empresaTarjeta = pago.getMedioPago().equals(MedioPago.CREDITO) ? ((PagoCredito)pago).getEmpresaTarjeta() : null;
        this.numeroTarjeta = pago.getMedioPago().equals(MedioPago.CREDITO) ? ((PagoCredito)pago).getNumeroTarjeta() : null;
        this.anioVencimiento = pago.getMedioPago().equals(MedioPago.CREDITO) ? ((PagoCredito)pago).getAnioVencimiento() : null;
        this.mesVencimiento = pago.getMedioPago().equals(MedioPago.CREDITO) ? ((PagoCredito)pago).getMesVencimiento() : null;
        this.banco = pago.getMedioPago().equals(MedioPago.DEBITO) ? ((PagoDebito)pago).getBanco() : null;
        this.cbu = pago.getMedioPago().equals(MedioPago.DEBITO) ? ((PagoDebito)pago).getCbu() : null;
    }

    public Pago mapToPago(){
        if (medioPago != null && medioPago.equals(MedioPago.CREDITO)){
            return new PagoCredito(empresaTarjeta,numeroTarjeta,anioVencimiento,mesVencimiento);
        }
        if (medioPago != null && medioPago.equals(MedioPago.DEBITO)){
            return new PagoDebito(banco,cbu);
        }
        if (medioPago != null && medioPago.equals(MedioPago.CUPONERA)){
            return new PagoCuponera();
        }
        return null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MedioPago getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(MedioPago medioPago) {
        this.medioPago = medioPago;
    }

    public String getEmpresaTarjeta() {
        return empresaTarjeta;
    }

    public void setEmpresaTarjeta(String empresaTarjeta) {
        this.empresaTarjeta = empresaTarjeta;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public Integer getAnioVencimiento() {
        return anioVencimiento;
    }

    public void setAnioVencimiento(Integer anioVencimiento) {
        this.anioVencimiento = anioVencimiento;
    }

    public Integer getMesVencimiento() {
        return mesVencimiento;
    }

    public void setMesVencimiento(Integer mesVencimiento) {
        this.mesVencimiento = mesVencimiento;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getCbu() {
        return cbu;
    }

    public void setCbu(String cbu) {
        this.cbu = cbu;
    }
}

