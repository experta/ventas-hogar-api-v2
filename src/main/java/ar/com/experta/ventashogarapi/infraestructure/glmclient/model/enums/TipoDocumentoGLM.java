package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.enums;

public enum TipoDocumentoGLM {

    DNI("96"),
    CUIT("80"),
    CUIL("86"),
    LIBRETA_CIVICA("90"),
    LIBRETA_ENROLAMIENTO("91"),
    DOCUMENTO_PARA_EXTRANJEROS("92");

    private String codigoGlm;

    private TipoDocumentoGLM(final String codigoGlm) {
        this.codigoGlm = codigoGlm;
    }

    public String codigoGlm() {
        return codigoGlm;
    }
}
