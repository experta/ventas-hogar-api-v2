package ar.com.experta.ventashogarapi.infraestructure.keycloakclient.model;

import java.util.List;

public class UsuarioKeycloak {
    private String access_token;
    private String refresh_token;
    private List<String> perfiles;
    private String pwid;
    private String dni;
    private String mail;
    private String nombre;
    private String apellido;
    private String status_auto;
    private List<Object> roles;

    public UsuarioKeycloak(){}

    public UsuarioKeycloak(String access_token, String expires_in, String refresh_expires_in, String refresh_token, String token_type, String session_state, String scope, String not_before_policy){
        this.access_token = access_token;
        this.refresh_token = refresh_token;
    }


    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public List<String> getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(List<String> perfiles) {
        this.perfiles = perfiles;
    }

    public String getPwid() {
        return pwid;
    }

    public void setPwid(String pwid) {
        this.pwid = pwid;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getStatus_auto() {
        return status_auto;
    }

    public void setStatus_auto(String status_auto) {
        this.status_auto = status_auto;
    }

    public List<Object> getRoles() {return roles;}

    public void setRoles(List<Object> roles) {this.roles = roles;}
}
