package ar.com.experta.ventashogarapi.infraestructure.impresoracotizacionclient;

import ar.com.experta.ventashogarapi.commons.exceptions.badgateway.BadGatewayException;
import ar.com.experta.ventashogarapi.commons.exceptions.badrequest.CotizacionBadRequestException;
import ar.com.experta.ventashogarapi.core.model.Venta;
import ar.com.experta.ventashogarapi.infraestructure.impresoracotizacionclient.mapper.PdfCotizacionRequestMapper;
import ar.com.experta.ventashogarapi.infraestructure.impresoracotizacionclient.request.CotizacionRequest;
import ar.com.experta.ventashogarapi.infraestructure.impresoracotizacionclient.request.ImpresoraRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.List;

@Component
public class ImpresoraCotizacionClient {

    @Value("${url_impresora}")
    private String HOST_API_IMPRESORA;
    private static final String POST_IMPRESION = "/1.0/REPORTE_COTIZACION_HOGAR.PDF";

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    private ObjectMapper objectMapper;

    private PdfCotizacionRequestMapper pdfCotizacionRequestMapper = new PdfCotizacionRequestMapper();

    private int MAX_PLANES_X_COTIZACION = 6;

    public ImpresoraCotizacionClient() {
        this.objectMapper = new ObjectMapper();
        this.objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    public byte[] getImpresion(Venta venta, List<String> ids) {

        if( (( ids == null || ids.isEmpty()) && venta.getCotizaciones().size() > MAX_PLANES_X_COTIZACION )
                || (ids != null && ids.size() > MAX_PLANES_X_COTIZACION) )
            throw new CotizacionBadRequestException(String.valueOf(MAX_PLANES_X_COTIZACION));

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(HOST_API_IMPRESORA + POST_IMPRESION);
        if (ids != null)
            Collections.sort(ids);
        CotizacionRequest cotizacionRequest = pdfCotizacionRequestMapper.mapToPdfCotizacionRequest(venta,  ids ) ;
        restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());

        try {
            ImpresoraRequest impresoraRequest = new ImpresoraRequest(objectMapper.writeValueAsString(cotizacionRequest));
            ResponseEntity<byte[]> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, new HttpEntity<>(impresoraRequest), byte[].class, "1");

            if (response.getStatusCodeValue() == HttpStatus.OK.value()) {
                byte[] bytesImpPoliza = response.getBody();
                return bytesImpPoliza;
            }
            throw new BadGatewayException("Error inesperado descargando el PDF de la cotizacion", response.getStatusCode().getReasonPhrase());
        } catch (HttpClientErrorException | JsonProcessingException e) {
            throw new BadGatewayException("Error inesperado descargando el PDF de la cotizacion", e.getMessage());
        }
    }

}
