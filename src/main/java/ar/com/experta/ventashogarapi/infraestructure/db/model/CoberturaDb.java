package ar.com.experta.ventashogarapi.infraestructure.db.model;

import ar.com.experta.ventashogarapi.core.model.Cobertura;
import ar.com.experta.ventashogarapi.core.model.enums.TipoCobertura;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "ventas_cotizaciones_coberturas")
public class CoberturaDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "tipo_cobertura", nullable = false, length = 50)
    @Enumerated(EnumType.STRING)
    private TipoCobertura tipoCobertura;

    @Column(name = "monto_asegurado", nullable = false)
    private BigDecimal montoAsegurado;

    @Column(name = "opcion", length = 50)
    @Enumerated(EnumType.STRING)
    private TipoCobertura opcion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cotizacion", insertable=false, updatable=false)
    private CotizacionDb cotizacionDb;

    public CoberturaDb() {
    }

    public CoberturaDb(Cobertura cobertura) {
        this.tipoCobertura = cobertura.getTipoCobertura();
        this.montoAsegurado = cobertura.getMontoAsegurado();
        this.opcion = cobertura.getOpcion();
    }

    public Cobertura mapToCobertura(){
        return new Cobertura(tipoCobertura,montoAsegurado,opcion);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoCobertura getTipoCobertura() {
        return tipoCobertura;
    }

    public void setTipoCobertura(TipoCobertura tipoCobertura) {
        this.tipoCobertura = tipoCobertura;
    }

    public BigDecimal getMontoAsegurado() {
        return montoAsegurado;
    }

    public void setMontoAsegurado(BigDecimal montoAsegurado) {
        this.montoAsegurado = montoAsegurado;
    }

    public TipoCobertura getOpcion() {
        return opcion;
    }

    public void setOpcion(TipoCobertura opcion) {
        this.opcion = opcion;
    }

    public CotizacionDb getCotizacionDb() {
        return cotizacionDb;
    }

    public void setCotizacionDb(CotizacionDb cotizacionDb) {
        this.cotizacionDb = cotizacionDb;
    }
}
