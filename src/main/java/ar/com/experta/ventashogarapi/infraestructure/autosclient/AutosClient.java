package ar.com.experta.ventashogarapi.infraestructure.autosclient;

import ar.com.experta.ventashogarapi.commons.exceptions.badgateway.AutosBadGatewayException;
import ar.com.experta.ventashogarapi.core.model.Documento;
import ar.com.experta.ventashogarapi.infraestructure.autosclient.model.PolizaAutos;
import ar.com.experta.ventashogarapi.infraestructure.identitymanagerclient.IdentityManagerClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.List;

@Component
public class AutosClient {
    private static final Logger logger = LoggerFactory.getLogger(AutosClient.class);

    @Value("${url_autos}")
    private String HOST_API_AUTOS;

    private static final String GET_POLIZA_DOCUMENTO = "/polizas/documento?numeroDocumento={numero}&tipoDocumento={tipo}";

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    private IdentityManagerClient identityManagerClient;

    public List<PolizaAutos> getGetPolizasAutos(Documento documento) {

        try {

            String url = HOST_API_AUTOS + GET_POLIZA_DOCUMENTO;
            url = url.replace("{numero}", documento.getNumero());
            switch (documento.getTipo()){
                case CUIT: url = url.replace("{tipo}", "CUIT");
                case DNI: url = url.replace("{tipo}", "DU");
            }

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+identityManagerClient.getAccessToken());
            HttpEntity<String> entity = new HttpEntity<String>(headers);

            PolizaAutos[] polizasAutos = restTemplate.exchange(builder.toUriString(),
                HttpMethod.GET,
                entity,
                PolizaAutos[].class).getBody();
            logger.info("Polizas Autos: " + Arrays.toString(polizasAutos));
            return Arrays.asList(polizasAutos);

        }catch (Exception e){
            logger.error("Error consultando Autos: " + e.getMessage());
            throw new AutosBadGatewayException(e);
        }

    }

}
