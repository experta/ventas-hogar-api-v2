package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ErroresResponse {

    @JsonProperty("Codigo")
    private String codigo;
    @JsonProperty("Descripcion")
    private String descripcion;
    @JsonProperty("Nivel")
    private String nivel;

    public ErroresResponse() {
    }

    public ErroresResponse(String codigo, String descripcion, String nivel) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.nivel = nivel;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }
}
