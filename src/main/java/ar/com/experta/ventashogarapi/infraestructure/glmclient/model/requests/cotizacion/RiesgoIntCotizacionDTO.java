package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RiesgoIntCotizacionDTO {

    @JsonProperty("Item")
    private ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.ItemDTO item;

    public RiesgoIntCotizacionDTO() {
    }

    public RiesgoIntCotizacionDTO(ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.ItemDTO item) {
        this.item = item;
    }

    public ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.ItemDTO getItem() {
        return item;
    }

    public void setItem(ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.ItemDTO item) {
        this.item = item;
    }
}
