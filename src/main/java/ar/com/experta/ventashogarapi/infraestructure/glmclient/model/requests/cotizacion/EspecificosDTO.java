package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EspecificosDTO {

    @JsonProperty("Tipo")
    private String tipo;
    @JsonProperty("ValorAsegurado")
    private String valorAsegurado;

    @JsonProperty("Codigo")
    private String codigo;
    @JsonProperty("Cantidad")
    private String cantidad;
    @JsonProperty("Identificacion")
    private String identificacion;

    @JsonProperty("ApellidoNombre")
    private String nombreApellido;
    @JsonProperty("TipoDocumento")
    private String tipoDocumento;
    @JsonProperty("NumeroDocumento")
    private String numeroDocumento;


    public EspecificosDTO() {
    }

    public EspecificosDTO(String tipo, String valorAsegurado, String codigo, String cantidad, String identificacion, String nombreApellido, String tipoDocumento, String numeroDocumento) {
        this.tipo = tipo;
        this.valorAsegurado = valorAsegurado;
        this.codigo = codigo;
        this.cantidad = cantidad;
        this.identificacion = identificacion;
        this.nombreApellido = nombreApellido;
        this.tipoDocumento = tipoDocumento;
        this.numeroDocumento = numeroDocumento;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getValorAsegurado() {
        return valorAsegurado;
    }

    public void setValorAsegurado(String valorAsegurado) {
        this.valorAsegurado = valorAsegurado;
    }

    public String getNombreApellido() {
        return nombreApellido;
    }

    public void setNombreApellido(String nombreApellido) {
        this.nombreApellido = nombreApellido;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }
}
