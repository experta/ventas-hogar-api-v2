package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


public class ProductoResponse {


    @JsonProperty("Item")
    private String Item;
    @JsonProperty("ProductoCodigo")
    private String ProductoCodigo;
    @JsonProperty("DescripcionProducto")
    private String DescripcionProducto;
    @JsonProperty("Prima")
    private String Prima;
    @JsonProperty("PorcBonificacion")
    private String ProcBonificacion;
    @JsonProperty("Bonificacion")
    private String Bonificacion;
    @JsonProperty("PorcRecargoAdministrativo")
    private String ProcRecargoAdministrativo;
    @JsonProperty("RecargoAdministrativo")
    private String RecargoAdministrativo;
    @JsonProperty("RecargoFinanciero")
    private String RecargoFinanciero;
    @JsonProperty("DerechoEmision")
    private String DerechoEmision;
    @JsonProperty("Impuestos")
    private String Impuestos;
    @JsonProperty("Premio")
    private String Premio;
    @JsonProperty("ImporteCuota1")
    private String ImporteCuota1;
    @JsonProperty("ImporteRestoCuotas")
    private String ImporteRestoCuotas;
    @JsonProperty("Comision")
    private String Comision;
    @JsonProperty("CantidadCuotas")
    private String CantidadCuotas;
    @JsonProperty("Coberturas")
    private List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.CoberturaResponse> coberturas;

    public ProductoResponse() {
    }

    public ProductoResponse(String item, String productoCodigo, String descripcionProducto, String prima, String procBonificacion, String bonificacion, String procRecargoAdministrativo, String recargoAdministrativo, String recargoFinanciero, String derechoEmision, String impuestos, String premio, String importeCuota1, String importeRestoCuotas, String comision, String cantidadCuotas, List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.CoberturaResponse> coberturas) {
        Item = item;
        ProductoCodigo = productoCodigo;
        DescripcionProducto = descripcionProducto;
        Prima = prima;
        ProcBonificacion = procBonificacion;
        Bonificacion = bonificacion;
        ProcRecargoAdministrativo = procRecargoAdministrativo;
        RecargoAdministrativo = recargoAdministrativo;
        RecargoFinanciero = recargoFinanciero;
        DerechoEmision = derechoEmision;
        Impuestos = impuestos;
        Premio = premio;
        ImporteCuota1 = importeCuota1;
        ImporteRestoCuotas = importeRestoCuotas;
        Comision = comision;
        CantidadCuotas = cantidadCuotas;
        this.coberturas = coberturas;
    }

    public String getCantidadCuotas() {
        return CantidadCuotas;
    }

    public void setCantidadCuotas(String cantidadCuotas) {
        CantidadCuotas = cantidadCuotas;
    }

    public String getItem() {
        return Item;
    }

    public void setItem(String item) {
        Item = item;
    }

    public String getProductoCodigo() {
        return ProductoCodigo;
    }

    public void setProductoCodigo(String productoCodigo) {
        ProductoCodigo = productoCodigo;
    }

    public String getDescripcionProducto() {
        return DescripcionProducto;
    }

    public void setDescripcionProducto(String descripcionProducto) {
        DescripcionProducto = descripcionProducto;
    }

    public String getPrima() {
        return Prima;
    }

    public void setPrima(String prima) {
        Prima = prima;
    }

    public String getProcBonificacion() {
        return ProcBonificacion;
    }

    public void setProcBonificacion(String procBonificacion) {
        ProcBonificacion = procBonificacion;
    }

    public String getBonificacion() {
        return Bonificacion;
    }

    public void setBonificacion(String bonificacion) {
        Bonificacion = bonificacion;
    }

    public String getProcRecargoAdministrativo() {
        return ProcRecargoAdministrativo;
    }

    public void setProcRecargoAdministrativo(String procRecargoAdministrativo) {
        ProcRecargoAdministrativo = procRecargoAdministrativo;
    }

    public String getRecargoAdministrativo() {
        return RecargoAdministrativo;
    }

    public void setRecargoAdministrativo(String recargoAdministrativo) {
        RecargoAdministrativo = recargoAdministrativo;
    }

    public String getRecargoFinanciero() {
        return RecargoFinanciero;
    }

    public void setRecargoFinanciero(String recargoFinanciero) {
        RecargoFinanciero = recargoFinanciero;
    }

    public String getDerechoEmision() {
        return DerechoEmision;
    }

    public void setDerechoEmision(String derechoEmision) {
        DerechoEmision = derechoEmision;
    }

    public String getImpuestos() {
        return Impuestos;
    }

    public void setImpuestos(String impuestos) {
        Impuestos = impuestos;
    }

    public String getPremio() {
        return Premio;
    }

    public void setPremio(String premio) {
        Premio = premio;
    }

    public String getImporteCuota1() {
        return ImporteCuota1;
    }

    public void setImporteCuota1(String importeCuota1) {
        ImporteCuota1 = importeCuota1;
    }

    public String getImporteRestoCuotas() {
        return ImporteRestoCuotas;
    }

    public void setImporteRestoCuotas(String importeRestoCuotas) {
        ImporteRestoCuotas = importeRestoCuotas;
    }

    public String getComision() {
        return Comision;
    }

    public void setComision(String comision) {
        Comision = comision;
    }

    public List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.CoberturaResponse> getCoberturas() {
        return coberturas;
    }

    public void setCoberturas(List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.CoberturaResponse> coberturas) {
        this.coberturas = coberturas;
    }
}
