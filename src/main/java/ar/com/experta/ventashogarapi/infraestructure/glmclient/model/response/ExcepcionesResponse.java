package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response;


import com.fasterxml.jackson.annotation.JsonProperty;

public class ExcepcionesResponse {
    @JsonProperty("Item")
    private String item;
    @JsonProperty("Tipo")
    private String tipo;
    @JsonProperty("Motivo")
    private String motivo;
    @JsonProperty("Detalle")
    private String detalle;
    @JsonProperty("Observaciones")
    private String observaciones;

    @JsonProperty("Estado")
    private String estado;


    public ExcepcionesResponse() {
    }

    public ExcepcionesResponse(String item, String tipo, String motivo, String detalle, String observaciones, String estado) {
        this.item = item;
        this.tipo = tipo;
        this.motivo = motivo;
        this.detalle = detalle;
        this.observaciones = observaciones;
        this.estado = estado;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
}
