package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.enums;

public enum TipoBienEspecificoGLM {

    NOTEBOOK("NOTEBOOK"),
    BICICLETA_BG("BICI"),
    ARTIC_DEPORTIVOS_BG("DEPO"),
    INSTR_MUSICALES_BG("INST_MUSIC"),
    NOTEBOOK_BG("NOTEBOOK"),
    PC_ELEC("PCESC"),
    AUDIO_ELEC("AUDIO"),
    CAVA_ELEC("CAVA"),
    TV_ELEC("TV"),
    NOTEBOOK_POR("NOTEBOOK"),
    FILMADORAS_POR("FILMADORA"),
    ARTIC_DEPORTIVOS_POR("DEPO"),
    TABLETS_POR("TABLET"),
    IPADS_POR("IPAD"),
    CAMARAS_POR("CAMARA"),
    BICICLETA("BICI");

    private String codigoGlm;

    TipoBienEspecificoGLM(String codigoGlm) {
        this.codigoGlm = codigoGlm;
    }

    public String codigoGlm() {
        return codigoGlm;
    }


}
