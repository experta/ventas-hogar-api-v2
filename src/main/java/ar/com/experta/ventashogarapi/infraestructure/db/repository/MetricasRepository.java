package ar.com.experta.ventashogarapi.infraestructure.db.repository;

import ar.com.experta.ventashogarapi.core.model.enums.Beneficio;
import ar.com.experta.ventashogarapi.core.model.enums.CanalVenta;
import ar.com.experta.ventashogarapi.core.model.enums.TipoPlan;
import ar.com.experta.ventashogarapi.infraestructure.db.model.VentaDb;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface MetricasRepository extends CrudRepository<VentaDb, Long> {

    @Query( " SELECT COUNT(v) FROM VentaDb v " +
            " WHERE v.fechaCreacion between :fechadesde AND :fechahasta " +
            "   AND ( :tipoPlan IS NULL OR v.tipoPlan = :tipoPlan) " +
            "   AND ( :filtroBeneficio IS NULL " +
            "        OR ( :filtroBeneficio = 'FALSE' AND v.beneficio IS NULL) " +
            "        OR ( :filtroBeneficio = 'TRUE' AND v.beneficio = :beneficio) ) " +
            "   AND ( :canalventa IS NULL OR v.canalVenta = :canalventa) " )
    Integer countCotizacionesTotales ( @Param("fechadesde") LocalDateTime fechaDesde,
                                       @Param("fechahasta") LocalDateTime fechaHasta,
                                       @Param("tipoPlan") TipoPlan tipoPlan,
                                       @Param("filtroBeneficio") String filtroBeneficio,
                                       @Param("beneficio") Beneficio beneficio,
                                       @Param("canalventa") CanalVenta canalVenta
    );

    @Query( " SELECT COUNT(v) FROM VentaDb v " +
            " WHERE v.fechaUltimaModificacion between :fechadesde AND :fechahasta " +
            "   AND ( :tipoPlan IS NULL OR v.tipoPlan = :tipoPlan) " +
            "   AND ( :filtroBeneficio IS NULL " +
            "        OR ( :filtroBeneficio = 'FALSE' AND v.beneficio IS NULL) " +
            "        OR ( :filtroBeneficio = 'TRUE' AND v.beneficio = :beneficio) ) " +
            "   AND ( :canalventa IS NULL OR v.canalVenta = :canalventa) " +
            "   AND v.numeroPoliza IS NOT NULL " )
    Integer countEmisionesTotales ( @Param("fechadesde") LocalDateTime fechaDesde,
                                    @Param("fechahasta") LocalDateTime fechaHasta,
                                    @Param("tipoPlan") TipoPlan tipoPlan,
                                    @Param("filtroBeneficio") String filtroBeneficio,
                                    @Param("beneficio") Beneficio beneficio,
                                    @Param("canalventa") CanalVenta canalVenta
    );

    @Query( " FROM VentaDb v " +
            " WHERE v.fechaCreacion between :fechadesde AND :fechahasta " +
            "   AND ( :tipoPlan IS NULL OR v.tipoPlan = :tipoPlan) " +
            "   AND ( :canalventa IS NULL OR v.canalVenta = :canalventa) " +
            "   AND ( :filtroBeneficio IS NULL " +
            "        OR ( :filtroBeneficio = 'FALSE' AND v.beneficio IS NULL) " +
            "        OR ( :filtroBeneficio = 'TRUE' AND v.beneficio = :beneficio) ) " )
    List<VentaDb> getVentasReporteCotizaciones(@Param("fechadesde") LocalDateTime fechaDesde,
                                               @Param("fechahasta") LocalDateTime fechaHasta,
                                               @Param("tipoPlan") TipoPlan tipoPlan,
                                               @Param("filtroBeneficio") String filtroBeneficio,
                                               @Param("beneficio") Beneficio beneficio,
                                               @Param("canalventa") CanalVenta canalVenta
    );

    interface MetricaGraficoStats{
        String getNombre();
        Integer getTotal();
    }

    @Query( value = " SELECT TO_CHAR(v.fecha_creacion, 'DD/MM') nombre, COUNT(*) total FROM ventas v " +
                    " WHERE v.fecha_creacion between :fechadesde AND :fechahasta " +
                    "   AND ( :tipoPlan IS NULL OR v.tipo_plan = :tipoPlan) " +
                    "   AND ( :filtroBeneficio IS NULL " +
                    "        OR ( :filtroBeneficio = 'FALSE' AND v.beneficio IS NULL) " +
                    "        OR ( :filtroBeneficio = 'TRUE' AND v.beneficio = :beneficio) ) " +
                    "   AND ( :canalventa IS NULL OR v.canal_venta = :canalventa) " +
                    " GROUP BY TO_CHAR(v.fecha_creacion, 'DD/MM')", nativeQuery = true)
    List<MetricaGraficoStats> getCotizacionesPorDia( @Param("fechadesde") LocalDate fechaDesde,
                                                     @Param("fechahasta") LocalDate fechaHasta,
                                                     @Param("tipoPlan") String tipoPlan,
                                                     @Param("filtroBeneficio") String filtroBeneficio,
                                                     @Param("beneficio") String beneficio,
                                                     @Param("canalventa") String canalVenta
    );

    @Query( value = " SELECT TO_CHAR(v.fecha_creacion, 'MM/YYYY') nombre, COUNT(*) total FROM ventas v " +
            " WHERE v.fecha_creacion between :fechadesde AND :fechahasta " +
            "   AND ( :tipoPlan IS NULL OR v.tipo_plan = :tipoPlan) " +
            "   AND ( :filtroBeneficio IS NULL " +
            "        OR ( :filtroBeneficio = 'FALSE' AND v.beneficio IS NULL) " +
            "        OR ( :filtroBeneficio = 'TRUE' AND v.beneficio = :beneficio) ) " +
            "   AND ( :canalventa IS NULL OR v.canal_venta = :canalventa) " +
            " GROUP BY TO_CHAR(v.fecha_creacion, 'MM/YYYY')", nativeQuery = true)
    List<MetricaGraficoStats> getCotizacionesPorMes( @Param("fechadesde") LocalDate fechaDesde,
                                                     @Param("fechahasta") LocalDate fechaHasta,
                                                     @Param("tipoPlan") String tipoPlan,
                                                     @Param("filtroBeneficio") String filtroBeneficio,
                                                     @Param("beneficio") String beneficio,
                                                     @Param("canalventa") String canalVenta
    );

    @Query( value = " SELECT TO_CHAR(v.fecha_ultima_modificacion, 'DD/MM') nombre, COUNT(*) total FROM ventas v " +
            " WHERE v.fecha_ultima_modificacion between :fechadesde AND :fechahasta " +
            "   AND ( :tipoPlan IS NULL OR v.tipo_plan = :tipoPlan) " +
            "   AND ( :filtroBeneficio IS NULL " +
            "        OR ( :filtroBeneficio = 'FALSE' AND v.beneficio IS NULL) " +
            "        OR ( :filtroBeneficio = 'TRUE' AND v.beneficio = :beneficio) ) " +
            "   AND ( :canalventa IS NULL OR v.canal_venta = :canalventa) " +
            "   AND v.nro_poliza IS NOT NULL " +
            " GROUP BY TO_CHAR(v.fecha_ultima_modificacion, 'DD/MM')", nativeQuery = true)
    List<MetricaGraficoStats> getEmisionesPorDia( @Param("fechadesde") LocalDate fechaDesde,
                                                  @Param("fechahasta") LocalDate fechaHasta,
                                                  @Param("tipoPlan") String tipoPlan,
                                                  @Param("filtroBeneficio") String filtroBeneficio,
                                                  @Param("beneficio") String beneficio,
                                                  @Param("canalventa") String canalVenta
    );

    @Query( value = " SELECT TO_CHAR(v.fecha_ultima_modificacion, 'MM/YYYY') nombre, COUNT(*) total FROM ventas v " +
            " WHERE v.fecha_ultima_modificacion between :fechadesde AND :fechahasta " +
            "   AND ( :tipoPlan IS NULL OR v.tipo_plan = :tipoPlan) " +
            "   AND ( :filtroBeneficio IS NULL " +
            "        OR ( :filtroBeneficio = 'FALSE' AND v.beneficio IS NULL) " +
            "        OR ( :filtroBeneficio = 'TRUE' AND v.beneficio = :beneficio) ) " +
            "   AND ( :canalventa IS NULL OR v.canal_venta = :canalventa) " +
            "   AND v.nro_poliza IS NOT NULL " +
            " GROUP BY TO_CHAR(v.fecha_ultima_modificacion, 'MM/YYYY')", nativeQuery = true)
    List<MetricaGraficoStats> getEmisionesPorMes( @Param("fechadesde") LocalDate fechaDesde,
                                                  @Param("fechahasta") LocalDate fechaHasta,
                                                  @Param("tipoPlan") String tipoPlan,
                                                  @Param("filtroBeneficio") String filtroBeneficio,
                                                  @Param("beneficio") String beneficio,
                                                  @Param("canalventa") String canalVenta
    );


}
