package ar.com.experta.ventashogarapi.infraestructure.db.model;

import ar.com.experta.ventashogarapi.core.model.Emision;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "ventas_emisiones")
public class EmisionDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "motivo_rechazo")
    private String motivoError;

    @Column(name = "reintentos_emision")
    private Integer reintentos;

    @Column(name = "timestamp_emision")
    private LocalDateTime timestamp;

    @Column(name = "rechazo_final")
    private String rechazado;


    public EmisionDb() {
    }

    public EmisionDb(Emision emision) {
        this.motivoError = emision.getMotivoError();
        this.reintentos = emision.getReintentos();
        this.timestamp = emision.getTimestamp();
        this.rechazado = emision.getRechazado() ? "S" : null;
    }

    public Emision mapToEmision(){
        return new Emision(motivoError, reintentos, timestamp, rechazado !=null && rechazado.equals("S"));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMotivoError() {
        return motivoError;
    }

    public void setMotivoError(String motivoError) {
        this.motivoError = motivoError;
    }


    public Integer getReintentos() {
        return reintentos;
    }

    public void setReintentos(Integer reintentos) {
        this.reintentos = reintentos;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}
