package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CrearEmisionResponse {

    @JsonProperty("IdServicioEmision")
    private String idServicioEmision;
    @JsonProperty("Rama")
    private String rama;
    @JsonProperty("Poliza")
    private String poliza;
    @JsonProperty("Endoso")
    private String endoso;
    @JsonProperty("EstadoSolicitud")
    private String estadoSolicitud;
    @JsonProperty("Impuestos")
    private String impuestos;
    @JsonProperty("Premio")
    private String premio;
    @JsonProperty("Excepciones")
    private List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.ExcepcionesResponse> excepciones;
    @JsonProperty("Errores")
    private List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.ErroresResponse> errores;

    public CrearEmisionResponse() {
    }

    public CrearEmisionResponse(String idServicioEmision, String rama, String poliza, String endoso, String estadoSolicitud, String impuestos, List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.ExcepcionesResponse> excepciones, List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.ErroresResponse> errores) {
        this.idServicioEmision = idServicioEmision;
        this.rama = rama;
        this.poliza = poliza;
        this.endoso = endoso;
        this.estadoSolicitud = estadoSolicitud;
        this.impuestos = impuestos;
        this.excepciones = excepciones;
        this.errores = errores;
    }

    public String getIdServicioEmision() {
        return idServicioEmision;
    }

    public void setIdServicioEmision(String idServicioEmision) {
        this.idServicioEmision = idServicioEmision;
    }

    public String getRama() {
        return rama;
    }

    public void setRama(String rama) {
        this.rama = rama;
    }

    public String getPoliza() {
        return poliza;
    }

    public void setPoliza(String poliza) {
        this.poliza = poliza;
    }

    public String getEndoso() {
        return endoso;
    }

    public void setEndoso(String endoso) {
        this.endoso = endoso;
    }

    public String getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(String estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
    }

    public String getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(String impuestos) {
        this.impuestos = impuestos;
    }

    public List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.ExcepcionesResponse> getExcepciones() {
        return excepciones;
    }

    public void setExcepciones(List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.ExcepcionesResponse> excepciones) {
        this.excepciones = excepciones;
    }

    public List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.ErroresResponse> getErrores() {
        return errores;
    }

    public void setErrores(List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.ErroresResponse> errores) {
        this.errores = errores;
    }

    public String getPremio() {
        return premio;
    }

    public void setPremio(String premio) {
        this.premio = premio;
    }
}
