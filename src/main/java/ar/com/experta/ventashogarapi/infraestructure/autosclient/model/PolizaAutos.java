package ar.com.experta.ventashogarapi.infraestructure.autosclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class PolizaAutos {

    @JsonProperty("numPol1")
    private String numeroPoliza;
    @JsonProperty("codRamo")
    private String ramo;
    @JsonProperty("fechaVigPol")
    private String fechaInicioVigencia;
    @JsonProperty("fechaVencPol")
    private String fechaFinVigencia;

    public String getNumeroPoliza() {
        return numeroPoliza;
    }

    public void setNumeroPoliza(String numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }

    public String getRamo() {
        return ramo;
    }

    public void setRamo(String ramo) {
        this.ramo = ramo;
    }

    public LocalDate getFechaInicioVigencia() {
        return LocalDate.parse(fechaInicioVigencia.substring(0,10));
    }

    public void setFechaInicioVigencia(String fechaInicioVigencia) {
        this.fechaInicioVigencia = fechaInicioVigencia;
    }

    public LocalDate getFechaFinVigencia() {
        return LocalDate.parse(fechaFinVigencia.substring(0,10));
    }

    public void setFechaFinVigencia(String fechaFinVigencia) {
        this.fechaFinVigencia = fechaFinVigencia;
    }

    @Override
    public String toString() {
        return "PolizaAutos{" +
                "numeroPoliza='" + numeroPoliza + '\'' +
                ", ramo='" + ramo + '\'' +
                ", fechaInicioVigencia='" + fechaInicioVigencia + '\'' +
                ", fechaFinVigencia='" + fechaFinVigencia + '\'' +
                '}';
    }
}
