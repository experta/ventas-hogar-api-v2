package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.enums;

public enum TipoViviendaGLM {
    CASA("1"),
    COUNTRY("2"),
    EDIFICIO_PB("1"),
    EDIFICIO_PA("2"),
    VIVIENDA_PERMANENTE("3"),
    VIVIENDA_PERMANENTE_ASEGURALO("5"),

    ;


    private String codigoGlm;

    private TipoViviendaGLM(final String codigoGlm) {
        this.codigoGlm = codigoGlm;
    }

    public String codigoGlm() {
        return codigoGlm;
    }

}
