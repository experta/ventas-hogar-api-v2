package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.enums;

public enum TipoPagoGLM {
    DEBITO("4"),
    CREDITO("3");

    private String codigoGlm;

    private TipoPagoGLM(final String codigoGlm) {
        this.codigoGlm = codigoGlm;
    }

    public String codigoGlm() {
        return codigoGlm;
    }

}
