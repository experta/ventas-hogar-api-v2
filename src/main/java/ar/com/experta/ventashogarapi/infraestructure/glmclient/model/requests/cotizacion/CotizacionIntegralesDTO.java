package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CotizacionIntegralesDTO {

    @JsonProperty("SistemaOrigen")
    private String sistemaOrigen;
    @JsonProperty("Rama")
    private String rama;
    @JsonProperty("TipoPolizaCodigo")
    private String tipoPolizaCodigo;
    @JsonProperty("TomadorNombre")
    private String tomadorNombre;
//    @JsonProperty("TomadorCUIT")
//    private String tomadorCUIT;
    @JsonProperty("TomadorTipoPersona")
    private String tomadorTipoPersona;
    @JsonProperty("TomadorCategoriaIVACodigo")
    private String tomadorCategoriaIVACodigo;
    @JsonProperty("TomadorIIBBCodigo")
    private String tomadorIIBBCodigo;
    @JsonProperty("VigenciaDesde")
    private String vigenciaDesde;
    @JsonProperty("ProductorCodigo")
    private String productorCodigo;
    @JsonProperty("MonedaCodigo")
    private String monedaCodigo;
    @JsonProperty("PlanComercialCodigo")
    private String planComercialCodigo;
    @JsonProperty("FormaPagoCodigo")
    private String formaPagoCodigo;
    @JsonProperty("ModoFacturacionCodigo")
    private String modoFacturacionCodigo;
    @JsonProperty("CondicionPagoCodigo")
    private String condicionPagoCodigo;
    @JsonProperty("GastosExplotacion")
    private String gastosExplotacion;
    @JsonProperty("GastosAdquisicion")
    private String gastosAdquisicion;
    @JsonProperty("RiesgoINT")
    private RiesgoIntCotizacionDTO riesgoInt;
    @JsonProperty("NroIdentificacion")
    private String nroIdentificacion;

    public CotizacionIntegralesDTO() {
    }

    public CotizacionIntegralesDTO(String sistemaOrigen, String rama, String tipoPolizaCodigo, String tomadorNombre, String tomadorTipoPersona, String tomadorCategoriaIVACodigo, String tomadorIIBBCodigo, String vigenciaDesde, String productorCodigo, String monedaCodigo, String planComercialCodigo, String formaPagoCodigo, String modoFacturacionCodigo, String condicionPagoCodigo, String gastosExplotacion, String gastosAdquisicion, String nroIdentificacion, RiesgoIntCotizacionDTO riesgoInt) {
        this.sistemaOrigen = sistemaOrigen;
        this.rama = rama;
        this.tipoPolizaCodigo = tipoPolizaCodigo;
        this.tomadorNombre = tomadorNombre;
        this.tomadorTipoPersona = tomadorTipoPersona;
        this.tomadorCategoriaIVACodigo = tomadorCategoriaIVACodigo;
        this.tomadorIIBBCodigo = tomadorIIBBCodigo;
        this.vigenciaDesde = vigenciaDesde;
        this.productorCodigo = productorCodigo;
        this.monedaCodigo = monedaCodigo;
        this.planComercialCodigo = planComercialCodigo;
        this.formaPagoCodigo = formaPagoCodigo;
        this.modoFacturacionCodigo = modoFacturacionCodigo;
        this.condicionPagoCodigo = condicionPagoCodigo;
        this.gastosExplotacion = gastosExplotacion;
        this.gastosAdquisicion = gastosAdquisicion;
        this.nroIdentificacion = nroIdentificacion;
        this.riesgoInt = riesgoInt;

    }

    public String getSistemaOrigen() {
        return sistemaOrigen;
    }

    public void setSistemaOrigen(String sistemaOrigen) {
        this.sistemaOrigen = sistemaOrigen;
    }

    public String getRama() {
        return rama;
    }

    public void setRama(String rama) {
        this.rama = rama;
    }

    public String getTipoPolizaCodigo() {
        return tipoPolizaCodigo;
    }

    public void setTipoPolizaCodigo(String tipoPolizaCodigo) {
        this.tipoPolizaCodigo = tipoPolizaCodigo;
    }

    public String getTomadorNombre() {
        return tomadorNombre;
    }

    public void setTomadorNombre(String tomadorNombre) {
        this.tomadorNombre = tomadorNombre;
    }

    public String getTomadorTipoPersona() {
        return tomadorTipoPersona;
    }

    public void setTomadorTipoPersona(String tomadorTipoPersona) {
        this.tomadorTipoPersona = tomadorTipoPersona;
    }

    public String getTomadorCategoriaIVACodigo() {
        return tomadorCategoriaIVACodigo;
    }

    public void setTomadorCategoriaIVACodigo(String tomadorCategoriaIVACodigo) {
        this.tomadorCategoriaIVACodigo = tomadorCategoriaIVACodigo;
    }

    public String getTomadorIIBBCodigo() {
        return tomadorIIBBCodigo;
    }

    public void setTomadorIIBBCodigo(String tomadorIIBBCodigo) {
        this.tomadorIIBBCodigo = tomadorIIBBCodigo;
    }

    public String getVigenciaDesde() {
        return vigenciaDesde;
    }

    public void setVigenciaDesde(String vigenciaDesde) {
        this.vigenciaDesde = vigenciaDesde;
    }

    public String getProductorCodigo() {
        return productorCodigo;
    }

    public void setProductorCodigo(String productorCodigo) {
        this.productorCodigo = productorCodigo;
    }

    public String getMonedaCodigo() {
        return monedaCodigo;
    }

    public void setMonedaCodigo(String monedaCodigo) {
        this.monedaCodigo = monedaCodigo;
    }

    public String getPlanComercialCodigo() {
        return planComercialCodigo;
    }

    public void setPlanComercialCodigo(String planComercialCodigo) {
        this.planComercialCodigo = planComercialCodigo;
    }

    public String getFormaPagoCodigo() {
        return formaPagoCodigo;
    }

    public void setFormaPagoCodigo(String formaPagoCodigo) {
        this.formaPagoCodigo = formaPagoCodigo;
    }

    public String getModoFacturacionCodigo() {
        return modoFacturacionCodigo;
    }

    public void setModoFacturacionCodigo(String modoFacturacionCodigo) {
        this.modoFacturacionCodigo = modoFacturacionCodigo;
    }

    public String getCondicionPagoCodigo() {
        return condicionPagoCodigo;
    }

    public void setCondicionPagoCodigo(String condicionPagoCodigo) {
        this.condicionPagoCodigo = condicionPagoCodigo;
    }

    public String getGastosExplotacion() {
        return gastosExplotacion;
    }

    public void setGastosExplotacion(String gastosExplotacion) {
        this.gastosExplotacion = gastosExplotacion;
    }

    public String getGastosAdquisicion() {
        return gastosAdquisicion;
    }

    public void setGastosAdquisicion(String gastosAdquisicion) {
        this.gastosAdquisicion = gastosAdquisicion;
    }

    public String getNroIdentificacion() {
        return nroIdentificacion;
    }

    public void setNroIdentificacion(String nroIdentificacion) {
        this.nroIdentificacion = nroIdentificacion;
    }

    public ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.RiesgoIntCotizacionDTO getRiesgoInt() {
        return riesgoInt;
    }

    public void setRiesgoInt(ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.RiesgoIntCotizacionDTO riesgoInt) {
        this.riesgoInt = riesgoInt;
    }
}
