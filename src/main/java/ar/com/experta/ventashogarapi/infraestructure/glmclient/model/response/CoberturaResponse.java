package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CoberturaResponse {


    @JsonProperty("Codigo")
    private String Codigo;
    @JsonProperty("Descripcion")
    private String Descripcion;
    @JsonProperty("Capital")
    private String Capital;

    public CoberturaResponse() {
    }

    public CoberturaResponse(String codigo, String descripcion, String capital) {
        Codigo = codigo;
        Descripcion = descripcion;
        Capital = capital;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getCapital() {
        return Capital;
    }

    public void setCapital(String capital) {
        Capital = capital;
    }
}
