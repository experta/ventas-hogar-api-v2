package ar.com.experta.ventashogarapi.infraestructure.impresoracotizacionclient.request;

public class ImpresoraRequest {

    private String parametros;

    public ImpresoraRequest() {
    }

    public ImpresoraRequest(String parametros) {
        this.parametros = parametros;
    }

    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }
}
