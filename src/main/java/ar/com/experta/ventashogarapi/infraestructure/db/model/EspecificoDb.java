package ar.com.experta.ventashogarapi.infraestructure.db.model;

import ar.com.experta.ventashogarapi.core.model.BienEspecifico;
import ar.com.experta.ventashogarapi.core.model.Especifico;
import ar.com.experta.ventashogarapi.core.model.Personal;
import ar.com.experta.ventashogarapi.core.model.enums.TipoBienEspecifico;
import ar.com.experta.ventashogarapi.core.model.enums.TipoCobertura;
import ar.com.experta.ventashogarapi.core.model.enums.TipoEspecifico;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "ventas_especificos")
public class EspecificoDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "tipo_cobertura", length = 30)
    @Enumerated(EnumType.STRING)
    private TipoCobertura tipoCobertura;

    @Column(name = "tipo_especifico", length = 30)
    @Enumerated(EnumType.STRING)
    private TipoEspecifico tipoEspecifico;

    @Column(name = "monto_asegurado")
    private BigDecimal montoAsegurado;

    @Column(name = "tipo_bien_especifico", length = 50)
    @Enumerated(EnumType.STRING)
    private TipoBienEspecifico tipoBienEspecifico;

    @Column(name = "modelo", length = 100)
    private String modelo;

    @Column(name = "numero_serie", length = 100)
    private String numeroSerie;

    @Column(name = "dni", length = 10)
    private String dni;

    @Column(name = "nombre", length = 100)
    private String nombre;

    @Column(name = "apellido", length = 100)
    private String apellido;

    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_venta", insertable=false, updatable=false)
    private VentaDb ventaDb;

    public EspecificoDb() {
    }

    public EspecificoDb(Especifico especifico) {
        this.tipoCobertura = especifico.getTipoCobertura();
        this.tipoEspecifico = especifico.getTipoEspecifico();
        this.montoAsegurado = especifico.getMontoAsegurado();
        if (especifico.getTipoEspecifico().equals(TipoEspecifico.BIEN_ESPECIFICO)) {
            BienEspecifico bienEspecifico = (BienEspecifico) especifico;
            this.tipoBienEspecifico = bienEspecifico.getTipoBienEspecifico();
            this.modelo = bienEspecifico.getModelo();
            this.numeroSerie = bienEspecifico.getNumeroSerie();
        }
        if (especifico.getTipoEspecifico().equals(TipoEspecifico.PERSONAL)) {
            Personal personal = (Personal) especifico;
            this.dni = personal.getDni();
            this.nombre = personal.getNombre();
            this.apellido = personal.getApellido();
            this.fechaNacimiento = personal.getFechaNacimiento();
        }
    }

    public Especifico mapToEspecifico(){
        if (this.tipoEspecifico.equals(TipoEspecifico.BIEN_ESPECIFICO))
            return new BienEspecifico(this.tipoCobertura,
                                        this.montoAsegurado,
                                        this.tipoBienEspecifico,
                                        this.modelo,
                                        this.numeroSerie);
        if (this.tipoEspecifico.equals(TipoEspecifico.PERSONAL))
            return new Personal(this.tipoCobertura,
                                this.montoAsegurado,
                                this.dni,
                                this.nombre,
                                this.apellido,
                                this.fechaNacimiento);
        return null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoCobertura getTipoCobertura() {
        return tipoCobertura;
    }

    public void setTipoCobertura(TipoCobertura tipoCobertura) {
        this.tipoCobertura = tipoCobertura;
    }

    public TipoEspecifico getTipoEspecifico() {
        return tipoEspecifico;
    }

    public void setTipoEspecifico(TipoEspecifico tipoEspecifico) {
        this.tipoEspecifico = tipoEspecifico;
    }

    public BigDecimal getMontoAsegurado() {
        return montoAsegurado;
    }

    public void setMontoAsegurado(BigDecimal montoAsegurado) {
        this.montoAsegurado = montoAsegurado;
    }

    public TipoBienEspecifico getTipoBienEspecifico() {
        return tipoBienEspecifico;
    }

    public void setTipoBienEspecifico(TipoBienEspecifico tipoBienEspecifico) {
        this.tipoBienEspecifico = tipoBienEspecifico;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public VentaDb getVentaDb() {
        return ventaDb;
    }

    public void setVentaDb(VentaDb ventaDb) {
        this.ventaDb = ventaDb;
    }
}
