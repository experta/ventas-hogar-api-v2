package ar.com.experta.ventashogarapi.infraestructure.glmclient.mapper;


import ar.com.experta.ventashogarapi.core.model.*;
import ar.com.experta.ventashogarapi.core.model.enums.*;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.CoreBusinessException;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.CotizacionGLM;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.enums.*;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.*;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.CotizacionIntegralesResponse;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.ProductoResponse;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class CotizacionMapper {

    private static final String CANTIDAD_ESPECIFICO_VALUE = "1";
    private static final String TIPO_BIEN_ESPECIFICO = "ESF";
    private static final String TIPO_PERSONAL_ESPECIFICO = "ESF";
    private static final String CODIGO_TIPO_DOCUMENTO_DNI = "96";
    private static final TipoCobertura COBERTURA_REFERENCIA_CONFIGURABLE = TipoCobertura.INCENDIO_EDIFICIO;
    private static final String TIPO_ADICIONAL = "ADI";
    private static final String SISTEMA_ORIGEN_GLM = "PORTAL";
    private static final String RAMA_PRODUCTO = "15";
    private static final String CODIGO_GLM_PERSONA_FISICA = "1";
    private static final String CODIGO_GLM_PERSONA_JURIDICA = "2";
    private static final String TOMADOR_IIBB_COD = "";
    private static final String MONEDA_DEFAULT_PESOS = null; //Pesos
    private static final String MEDIO_PAGO_DEFAULT_MANUAL = "0"; //Cobranza Manual
    private static final String MEDIO_PAGO_AUTOMATICO = "3"; //Cobranza Credito
    private static final Integer REFACTURACIONES = 1;
    private static final Integer CUOTAS_BONIFICADAS_DTV = 2;


    public static class Secuencia {
        private Integer valor;

        public Secuencia() {
            this.valor = Integer.valueOf(0);
        }

        public String next(){
            valor++;
            return valor.toString();
        }
    }

    public static CotizacionIntegralesDTO toCotizacionIntegralesDTO(String idVendedor, Plan plan, List<Cobertura> coberturas, Vivienda vivienda, List<Especifico> especificos, Cliente cliente, Pago pago, LocalDate fechaInicioCobertura, TipoMedioPago tipoMedioPago, CantidadCuotas cantidadCuotas, ModalidadComision modalidadComision, Boolean clausulaEstabilizacion, String porcentajeEstabilizacion, Beneficio beneficio, String numeroReferencia, Boolean especificosDefault) throws CoreBusinessException {

        List<CoberturasDTO> coberturasGLM = new ArrayList<>();

        for (Cobertura cobertura : coberturas) {
            if (!TipoCoberturaGLM.valueOf(cobertura.getTipoCobertura().toString()).esAdicional() &&
                    cobertura.getMontoAsegurado().compareTo(new BigDecimal(0)) > 0) {
                Map<TipoBienEspecifico, Secuencia> mapIndices = new HashMap<>();
                for (TipoBienEspecifico tipo : TipoBienEspecifico.values()) {
                    mapIndices.put(tipo, new Secuencia());
                }

                List<EspecificosDTO> especificosDTOS = especificos != null ? especificos.stream()
                        .filter(especifico -> especifico.getTipoCobertura().equals(cobertura.getTipoCobertura()))
                        .map(especifico -> new EspecificosDTO(
                                especifico.getTipoEspecifico().equals(TipoEspecifico.BIEN_ESPECIFICO) ? TIPO_BIEN_ESPECIFICO : TIPO_PERSONAL_ESPECIFICO,
                                especifico.getMontoAsegurado().toString(),
                                especifico.getTipoEspecifico().equals(TipoEspecifico.BIEN_ESPECIFICO) ?
                                        TipoBienEspecificoGLM.valueOf(((BienEspecifico) especifico).getTipoBienEspecifico().toString()).codigoGlm() + mapIndices.get(((BienEspecifico) especifico).getTipoBienEspecifico()).next() : null,
                                CANTIDAD_ESPECIFICO_VALUE,
                                especifico.getTipoEspecifico().equals(TipoEspecifico.BIEN_ESPECIFICO) ? ((BienEspecifico) especifico).getDescripcionCompleta() : null,
                                especifico.getTipoEspecifico().equals(TipoEspecifico.PERSONAL) ? ((Personal) especifico).getNombreCompleto() : null,
                                especifico.getTipoEspecifico().equals(TipoEspecifico.PERSONAL) ? CODIGO_TIPO_DOCUMENTO_DNI : null,
                                especifico.getTipoEspecifico().equals(TipoEspecifico.PERSONAL) ? ((Personal) especifico).getDni() : null))
                        .collect(Collectors.toList())
                        : null;

                // Agregado para enviar especifico por default cuando se cotiza
                Optional<PlanItem> planItem = plan.getItems().stream().filter(p -> p.getTipoEspecificoCobertura() != null && p.getTipoCobertura().equals(cobertura.getTipoCobertura())).findAny();

                if (especificosDefault && especificos == null && planItem.isPresent()) {
                    especificosDTOS = Arrays.asList(new EspecificosDTO(
                            planItem.get().getTipoEspecificoCobertura().equals(TipoEspecifico.BIEN_ESPECIFICO) ? TIPO_BIEN_ESPECIFICO : TIPO_PERSONAL_ESPECIFICO,
                            cobertura.getMontoAsegurado().toString(),
                            planItem.get().getTipoEspecificoCobertura().equals(TipoEspecifico.BIEN_ESPECIFICO) ?
                                    TipoBienEspecificoGLM.valueOf(Arrays.stream(TipoBienEspecifico.values()).filter( t -> t.categoriaBienEspecifico().equals(planItem.get().getCategoriaBienEspecifico())).findFirst().get().toString()).codigoGlm() + "0" : null,
                            CANTIDAD_ESPECIFICO_VALUE,
                            "-",
                            planItem.get().getTipoEspecificoCobertura().equals(TipoEspecifico.PERSONAL) ? "-" : null,
                            planItem.get().getTipoEspecificoCobertura().equals(TipoEspecifico.PERSONAL) ? CODIGO_TIPO_DOCUMENTO_DNI : null,
                            planItem.get().getTipoEspecificoCobertura().equals(TipoEspecifico.PERSONAL) ? "12345678" : null
                    ));
                }

                List<AdicionalesDTO> adicionalesDTOS = TipoCoberturaGLM.valueOf(cobertura.getTipoCobertura().toString()).tieneAdicional() ? coberturas.stream()
                        .filter( c -> TipoCoberturaGLM.valueOf(c.getTipoCobertura().toString()).esAdicional() && TipoCoberturaGLM.valueOf(c.getTipoCobertura().toString()).adicional().toString().equals(cobertura.getTipoCobertura().toString()))
                        .map(c -> new AdicionalesDTO(TIPO_ADICIONAL, TipoCoberturaGLM.valueOf(c.getTipoCobertura().toString()).codigoGlm(), null))
                        .collect(Collectors.toList())
                        : null;

                coberturasGLM.add(new CoberturasDTO(
                        cobertura.getTipoCobertura().equals(COBERTURA_REFERENCIA_CONFIGURABLE) ?
                                TipoCoberturaGLM.valueOf(cobertura.getOpcion().toString()).codigoGlm() :
                                TipoCoberturaGLM.valueOf(cobertura.getTipoCobertura().toString()).codigoGlm(),
                        cobertura.getMontoAsegurado().toString(),
                        adicionalesDTOS,
                        especificosDTOS != null && !especificosDTOS.isEmpty() ? especificosDTOS : null
                ));

                //TODO: Sacar y hacer bien cuando en la web se pueda hacer que tome el fijo de un input (Cobertura con especifico)
                if (cobertura.getTipoCobertura().equals(TipoCobertura.ACCIDENTES_PERSONALES_PCP)) {
                    coberturasGLM.add(new CoberturasDTO(TipoCoberturaGLM.GASTOS_MEDICOS.codigoGlm(), cobertura.getMontoAsegurado().multiply(new BigDecimal(0.025)).toString(),
                            null, null));
                }
            }
        }

        ProductoCotizarDTO productoCotizarDTO = new ProductoCotizarDTO(PlanesMapper.getCodigoProducto(plan, vivienda.getDireccion().getLocalizacion().getZonaHogar(), coberturas, clausulaEstabilizacion), coberturasGLM);
        List<ProductoCotizarDTO> productosCotizar = new ArrayList<>();
        productosCotizar.add(productoCotizarDTO);

         ItemDTO item = new ItemDTO(
                            vivienda.getDireccion().getLocalizacion().getCodigoPostal(),
                            vivienda.getDireccion().getLocalizacion().getSubcp(),
                            plan.getTipoPlan().equals(TipoPlan.ENLATADO_ALIANZA_ASEGURALO) ?
                                 TipoViviendaGLM.VIVIENDA_PERMANENTE_ASEGURALO.codigoGlm() :
                                 TipoViviendaGLM.valueOf(vivienda.getTipoVivienda().toString()).codigoGlm(),
                            null,
                            clausulaEstabilizacion != null && clausulaEstabilizacion ? porcentajeEstabilizacion : null,
                            productosCotizar);

        RiesgoIntCotizacionDTO riesgoInt = new RiesgoIntCotizacionDTO(item);

        String nombreTomador = null;
        if(cliente.getNombre() != null)
            //Se corta el tamaño del nombre del tomador a un maximo de 59 caracteres porque GLM solo acepta hasta 60 caracteres
            nombreTomador = cliente.getNombre()
                    .substring(0, Math.min(cliente.getNombre().length(), 59));

        return new CotizacionIntegralesDTO(
                SISTEMA_ORIGEN_GLM,
                RAMA_PRODUCTO,
                PlanesMapper.getTipoPoliza(clausulaEstabilizacion),
                nombreTomador,
                cliente.getTipoPersona().equals(TipoPersona.FISICA) ? CODIGO_GLM_PERSONA_FISICA : CODIGO_GLM_PERSONA_JURIDICA,
                CondicionImpositivaGLM.valueOf(cliente.getCondicionImpositiva().toString()).codigoGlm(),
                TOMADOR_IIBB_COD,
                fechaInicioCobertura.toString(),
                idVendedor,
                MONEDA_DEFAULT_PESOS,
                PlanesMapper.getPlanComercial(plan, beneficio),
                pago != null && pago.getMedioPago() != null ? MedioPagoGLM.valueOf(pago.getMedioPago().toString()).codigoGlm() :
                        ( tipoMedioPago != null && tipoMedioPago.equals(TipoMedioPago.AUTOMATICO) ? MEDIO_PAGO_AUTOMATICO :
                                plan.getTipoPlan().equals(TipoPlan.ENLATADO_ALIANZA_DTV) ? MedioPagoGLM.OP_BANCARIA_DTV.codigoGlm() :MEDIO_PAGO_DEFAULT_MANUAL),
                PlanesMapper.getModoFacturacion(plan),
                PlanesMapper.getCodigoCantidadCuotas(plan, cantidadCuotas),
                PlanesMapper.getGastosExplotacion(plan.getIdPlan(), tipoMedioPago, modalidadComision, beneficio),
                PlanesMapper.getGastosAdquisicion(plan.getIdPlan(), modalidadComision, beneficio),
                numeroReferencia != null ? numeroReferencia : "-",
                riesgoInt
        );
    }

    public CotizacionGLM toCotizacionGlm(CotizacionIntegralesResponse cotizacionIntegralesResponse, TipoPlan tipoPlan, String codigoProducto) throws CoreBusinessException {
        if (cotizacionIntegralesResponse.getErrores() != null && !cotizacionIntegralesResponse.getErrores().isEmpty())
            throw new CoreBusinessException(cotizacionIntegralesResponse.getErrores()
                                                                        .stream()
                                                                        .map(erroresResponse -> Arrays.asList(erroresResponse.getDescripcion().split("\\r\\n")))
                                                                        .flatMap(List::stream)
                                                                        .map (error -> mapError(error))
                                                                        .collect(Collectors.toList())
            );

        ProductoResponse productoResponse = cotizacionIntegralesResponse.getProductos().get(0);

        Integer cuotas = Integer.valueOf(productoResponse.getCantidadCuotas()) * REFACTURACIONES;

        BigDecimal prima = new BigDecimal(productoResponse.getPrima());
        BigDecimal premio = new BigDecimal(productoResponse.getPremio());
        BigDecimal impuesto = new BigDecimal(productoResponse.getImpuestos());

        BigDecimal cuotasBD = new BigDecimal(productoResponse.getCantidadCuotas().equals("0")?"1":productoResponse.getCantidadCuotas());

        return new CotizacionGLM(   prima.divide(cuotasBD, 0, RoundingMode.HALF_UP),
                                    premio.divide(cuotasBD, 0, RoundingMode.HALF_UP),
                                    impuesto.divide(cuotasBD,  0, RoundingMode.HALF_UP),
                                    tipoPlan.equals(TipoPlan.ENLATADO_CDTV) || tipoPlan.equals(TipoPlan.CONFIGURABLE_CDTV) ? cuotas - CUOTAS_BONIFICADAS_DTV : cuotas,
                                    cotizacionIntegralesResponse.getSolicitud(),
                                    codigoProducto);
    }

    private String mapError(String errorGlm) {
        if (errorGlm.equals("El productor no está habilitado para el plan comercial"))
            return "El productor no está configurado para emitir el producto deseado.";

        String errorGLMlc = errorGlm.toLowerCase(Locale.ROOT);
        for (TipoCoberturaGLM tipoCoberturaGLM : TipoCoberturaGLM.values()){
            if (errorGLMlc.contains( "cobertura " + tipoCoberturaGLM.codigoGlm()) || errorGLMlc.contains( "cobertura  " + tipoCoberturaGLM.codigoGlm())) {
                errorGlm = errorGlm.replace(" " +tipoCoberturaGLM.codigoGlm(), " " + TipoCobertura.valueOf(tipoCoberturaGLM.toString()).label()).trim().replaceAll("\\s{2,}", " ");
            }
        }

        return errorGlm;
    }

}
