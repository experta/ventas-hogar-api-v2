package ar.com.experta.ventashogarapi.infraestructure.dtvhubclient.model;

public class AsignacionRequest {

    private String numeroPoliza;

    public AsignacionRequest() {
    }

    public AsignacionRequest(String numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }

    public String getNumeroPoliza() {
        return numeroPoliza;
    }

    public void setNumeroPoliza(String numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }
}
