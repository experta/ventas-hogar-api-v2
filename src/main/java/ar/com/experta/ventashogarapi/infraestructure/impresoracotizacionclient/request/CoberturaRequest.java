package ar.com.experta.ventashogarapi.infraestructure.impresoracotizacionclient.request;

import java.math.BigDecimal;

public class CoberturaRequest {

    private String tipoCobertura;
    private String franquicia;
    private BigDecimal sumaAsegurada;


    public CoberturaRequest(String tipoCobertura, BigDecimal sumaAsegurada) {
        this.tipoCobertura = tipoCobertura;
        this.sumaAsegurada = sumaAsegurada;
    }

    public CoberturaRequest(String tipoCobertura, String franquicia, BigDecimal sumaAsegurada) {
        this.tipoCobertura = tipoCobertura;
        this.franquicia = franquicia;
        this.sumaAsegurada = sumaAsegurada;
    }

    public String getFranquicia() {
        return franquicia;
    }

    public void setFranquicia(String franquicia) {
        this.franquicia = franquicia;
    }

    public String getTipoCobertura() {
        return tipoCobertura;
    }

    public void setTipoCobertura(String tipoCobertura) {
        this.tipoCobertura = tipoCobertura;
    }

    public BigDecimal getSumaAsegurada() {
        return sumaAsegurada;
    }

    public void setSumaAsegurada(BigDecimal sumaAsegurada) {
        this.sumaAsegurada = sumaAsegurada;
    }
}
