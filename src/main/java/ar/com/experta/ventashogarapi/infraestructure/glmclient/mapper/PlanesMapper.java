package ar.com.experta.ventashogarapi.infraestructure.glmclient.mapper;

import ar.com.experta.ventashogarapi.core.model.Cobertura;
import ar.com.experta.ventashogarapi.core.model.ModalidadComision;
import ar.com.experta.ventashogarapi.core.model.Plan;
import ar.com.experta.ventashogarapi.core.model.enums.*;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.CoreBusinessException;

import java.util.Arrays;
import java.util.List;

public class PlanesMapper {

    private static final String TIPO_POLIZA_GRAL = "CF01";
    private static final String TIPO_POLIZA_CON_ESTABILIZACION = "CF03";

    private static final TipoCobertura COBERTURA_REFERENCIA_CONFIGURABLE = TipoCobertura.INCENDIO_EDIFICIO;

    private static final String PLAN_COMERCIAL_CONF = "PREMI";
    private static final String PLAN_COMERCIAL_CONF_BENEFICIO_AUTOS = "PD30I";
    private static final String PLAN_COMERCIAL_CONF_BENEFICIO_GRUPOW = "PGWI";
    private static final String PLAN_COMERCIAL_CONF_BENEFICIO_EMPLEADO_DTV = "PDIRI";
    private static final String CODIGO_PLAN_COMERCIAL_ENLATADO_1 = "21";
    private static final String CODIGO_PLAN_COMERCIAL_ENLATADO_2 = "22";
    private static final String CODIGO_PLAN_COMERCIAL_ENLATADO_3 = "23";
    private static final String CODIGO_PLAN_COMERCIAL_ENLATADO_4 = "24";
    private static final String CODIGO_PLAN_COMERCIAL_ENLATADO_5 = "25";
    private static final String CODIGO_PLAN_COMERCIAL_ENLATADO_6 = "26";
    private static final String AGREGADO_PLAN_COMERCIAL_ENLATADO_BENEFICIO_GRUPOW = "GW";
    private static final String PLAN_COMERCIAL_ENLATADO_ELEBAR = "ELEB1";
    private static final String PLAN_COMERCIAL_ENLATADO_REBA_1 = "R1";
    private static final String PLAN_COMERCIAL_ENLATADO_REBA_2 = "R2";
    private static final String PLAN_COMERCIAL_ENLATADO_REBA_3 = "R3";
    private static final String PLAN_COMERCIAL_AL_RIO = "ALRIO";
    private static final String PLAN_COMERCIAL_CONFIGURABLE_CLIENTES_DTV = "DTVC";
    private static final String PLAN_COMERCIAL_ENLATADO_CLIENTES_DTV = "DTVP";
    private static final String PLAN_COMERCIAL_CONFIGURABLE_AFINIDAD = "PGAI";
    private static final String AGREGADO_PLAN_COMERCIAL_ENLATADO_AFINIDAD = "GA";
    private static final String PLAN_COMERCIAL_CONFIGURABLE_GH = "PGH";
    private static final String AGREGADO_PLAN_COMERCIAL_ENLATADO_GH = "GH";
    private static final String PLAN_COMERCIAL_ALIANZA_DTV = "PRDTV";

    private static final String PLAN_COMERCIAL_ALIANZA_COMPARAENCASA = "CCH";
    private static final String PLAN_COMERCIAL_ALIANZA_ASEGURALO = "ASEG";
    private static final String PLAN_COMERCIAL_ALIANZA_BTF = "BTF";
    private static final String PLAN_COMERCIAL_ALIANZA_DGO_LITE = "DGO1I";
    private static final String PLAN_COMERCIAL_ALIANZA_DGO_FULL = "DGO2I";

    private static final String PRODUCTO_ELEBAR_AHORRO_SN = "ELEB2";
    private static final String PRODUCTO_ELEBAR_AHORRO_CN = "ELEB1";
    private static final String PRODUCTO_ELEBAR_BALANCE_SN = "ELEB4";
    private static final String PRODUCTO_ELEBAR_BALANCE_CN = "ELEB3";
    private static final String PRODUCTO_ALIANZA_DTV = "PRDTV";

    private static final String PRODUCTO_COMPARAENCASA_1 = "CCM1";
    private static final String PRODUCTO_COMPARAENCASA_2 = "CCM2";
    private static final String PRODUCTO_COMPARAENCASA_3 = "CCM3";

    private static final String PRODUCTO_ASEGURALO_1 = "AS1";
    private static final String PRODUCTO_ASEGURALO_2 = "AS2";
    private static final String PRODUCTO_ASEGURALO_3 = "AS3";
    private static final String PRODUCTO_ASEGURALO_4 = "AS4";

    private static final String PRODUCTO_BTF_1 = "BTF1";
    private static final String PRODUCTO_BTF_2 = "BTF2";
    private static final String PRODUCTO_BTF_3 = "BTF3";
    private static final String PRODUCTO_BTF_4 = "BTF4";
    private static final String PRODUCTO_ALIANZA_DGO_LITE = "DG1";
    private static final String PRODUCTO_ALIANZA_DGO_FULL = "DG2";

    private static final String CANTIDAD_CUOTAS_1 = "01";
    private static final String CANTIDAD_CUOTAS_2 = "02";
    private static final String CANTIDAD_CUOTAS_3 = "03";
    private static final String CANTIDAD_CUOTAS_DTV = "07";

//    private static final String CANTIDAD_CUOTAS_2 = "01";
//    private static final String CANTIDAD_CUOTAS_4 = "02";
//    private static final String CANTIDAD_CUOTAS_6 = "03";
//    private static final String CANTIDAD_CUOTAS_8 = "04";
//    private static final String CANTIDAD_CUOTAS_10 = "05";
//    private static final String CANTIDAD_CUOTAS_12 = "06";

    private static final String GASTOS_EXPLOTACION_AUTOMATICO = "10";
    private static final String GASTOS_EXPLOTACION_AUTOMATICO_CROSSELING_AUTOS = "0";
    private static final String GASTOS_EXPLOTACION_AUTOMATICO_AFINIDAD = "10";
    private static final String GASTOS_EXPLOTACION_MANUAL = "15.45";
    private static final String GASTOS_EXPLOTACION_MANUAL_CROSSELING_AUTOS = "8.20";
    private static final String GASTOS_EXPLOTACION_MANUAL_AFINIDAD = "16.82";

    private static final String GASTOS_ADQUISICION = "30";
    private static final String GASTOS_ADQUISICION_CROSSELING_AUTOS = "10";
    private static final String GASTOS_ADQUISICION_AFINIDAD = "15";

    private static final String MODO_FAC_CODIGO = "03"; //Trimestral prorrogable
    private static final String MODO_FAC_CODIGO_DTV = "02"; //Trimestral prorrogable

    public static String getTipoPoliza(Boolean clausulaEstabilizacion) {
        if (clausulaEstabilizacion != null && clausulaEstabilizacion)
            return TIPO_POLIZA_CON_ESTABILIZACION;
        return TIPO_POLIZA_GRAL;
    }

    public static String getPlanComercial(Plan plan, Beneficio beneficio) throws CoreBusinessException {

        switch (plan.getTipoPlan()) {
            case CONFIGURABLE: {
                if (beneficio == null)
                    return PLAN_COMERCIAL_CONF;
                else if (beneficio.equals(Beneficio.AUTOS_HOGAR))
                    return PLAN_COMERCIAL_CONF_BENEFICIO_AUTOS;
                else if (beneficio.equals(Beneficio. EMPLEADOS_GW))
                    return PLAN_COMERCIAL_CONF_BENEFICIO_GRUPOW;
                else if (beneficio.equals(Beneficio. EMPLEADOS_DIRECTV))
                    return PLAN_COMERCIAL_CONF_BENEFICIO_EMPLEADO_DTV;
                else
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
            }
            case ENLATADO:{
                if (beneficio == null)
                    return PlanesMapper.getCodigoEnlatado(plan.getIdPlan()) + "I";
                else if (beneficio.equals(Beneficio. EMPLEADOS_GW))
                    return PlanesMapper.getCodigoEnlatado(plan.getIdPlan()) + AGREGADO_PLAN_COMERCIAL_ENLATADO_BENEFICIO_GRUPOW + "I";
                else
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
            }
            case ENLATADO_ECOMMERCE:
                if (beneficio != null)
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
                return PlanesMapper.getCodigoEnlatado(plan.getIdPlan()) + "I";
            case ENLATADO_ELEBAR:
                if (beneficio != null)
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
                return PLAN_COMERCIAL_ENLATADO_ELEBAR;
            case ENLATADO_REBA:
                if (beneficio != null)
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
                switch (plan.getIdPlan()){
                    case REBANKING_AHORRO:
                        return PLAN_COMERCIAL_ENLATADO_REBA_1;
                    case REBANKING_BALANCE:
                        return PLAN_COMERCIAL_ENLATADO_REBA_2;
                    case REBANKING_SEGURIDAD:
                        return PLAN_COMERCIAL_ENLATADO_REBA_3;
                    default:
                        throw new CoreBusinessException(Arrays.asList("El plan " + plan.getIdPlan() + " es incorrecto para el producto " + TipoPlan.ENLATADO_REBA.label()));
                }
            case ENLATADO_ALRIO: {
                if (beneficio != null)
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
                return PLAN_COMERCIAL_AL_RIO;
            }
            case CONFIGURABLE_CDTV:
                if (beneficio != null)
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
                return PLAN_COMERCIAL_CONFIGURABLE_CLIENTES_DTV;
            case ENLATADO_CDTV:
                if (beneficio != null)
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
                return PLAN_COMERCIAL_ENLATADO_CLIENTES_DTV;
            case CONFIGURABLE_AFINIDAD:
                if (beneficio != null)
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
                return PLAN_COMERCIAL_CONFIGURABLE_AFINIDAD;
            case ENLATADO_AFINIDAD:{
                if (beneficio != null)
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
                return PlanesMapper.getCodigoEnlatado(plan.getIdPlan()) + AGREGADO_PLAN_COMERCIAL_ENLATADO_AFINIDAD;
            }
            case CONFIGURABLE_GH:
                if (beneficio != null)
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
                return PLAN_COMERCIAL_CONFIGURABLE_GH;
            case ENLATADO_GH:{
                if (beneficio != null)
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
                return PlanesMapper.getCodigoEnlatado(plan.getIdPlan()) + AGREGADO_PLAN_COMERCIAL_ENLATADO_GH;
            }
            case ENLATADO_ALIANZA_DTV: {
                if (beneficio != null)
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
                return PLAN_COMERCIAL_ALIANZA_DTV;
            }
            case ENLATADO_ALIANZA_COMPARAENCASA: {
                if (beneficio != null)
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
                return PLAN_COMERCIAL_ALIANZA_COMPARAENCASA;
            }
            case ENLATADO_ALIANZA_ASEGURALO: {
                if (beneficio != null)
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
                return PLAN_COMERCIAL_ALIANZA_ASEGURALO;
            }
            case ENLATADO_ALIANZA_BTF: {
                if (beneficio != null)
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
                return PLAN_COMERCIAL_ALIANZA_BTF;
            }
            case ENLATADO_HOGAR_DGO: {
                if (beneficio != null)
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir planes de " + plan.getTipoPlan().label() + " con el beneficio " + beneficio.label()));
                if (plan.getIdPlan() == null || !(plan.getIdPlan().equals(IdPlan.DGO_LITE) || plan.getIdPlan().equals(IdPlan.DGO_FULL)) )
                    throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir el plan de " + plan.getIdPlan().toString()));
                return plan.getIdPlan().equals(IdPlan.DGO_LITE) ? PLAN_COMERCIAL_ALIANZA_DGO_LITE : PLAN_COMERCIAL_ALIANZA_DGO_FULL;
            }

            default:
                throw new CoreBusinessException(Arrays.asList("El tipo de plan " + plan.getTipoPlan() + " no tiene definida una configuracion para cotizar"));
        }
    }

    public static String getCodigoProducto(Plan plan, String zona, List<Cobertura> coberturas, Boolean clausulaEstabilizacion) throws CoreBusinessException {
        switch (plan.getTipoPlan()) {
            case CONFIGURABLE:
            case CONFIGURABLE_CDTV:
            case CONFIGURABLE_AFINIDAD:
            case CONFIGURABLE_GH: {
                Cobertura coberturaReferencia = coberturas.stream()
                        .filter(cobertura -> cobertura.getTipoCobertura().equals(COBERTURA_REFERENCIA_CONFIGURABLE))
                        .findFirst()
                        .get();

                String codigoProductoConfigurable = "";

                if (coberturaReferencia.getOpcion().equals(TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO))
                    codigoProductoConfigurable += "PA";
                else if (coberturaReferencia.getOpcion().equals(TipoCobertura.INCENDIO_EDIFICIO_PRORRATA))
                    codigoProductoConfigurable += "PP";
                else
                    throw new CoreBusinessException(Arrays.asList("El tipo de cobertura " + coberturaReferencia.getOpcion() + " es incorrecto para el producto configurable"));

                return codigoProductoConfigurable +
                        PlanesMapper.getCodigoZona(zona) +
                        (clausulaEstabilizacion != null && clausulaEstabilizacion ? "CE" : "C"); //TODO: Eliminar cuando ande bien GLM
            }
            case ENLATADO:
            case ENLATADO_ECOMMERCE:
            case ENLATADO_CDTV:
            case ENLATADO_AFINIDAD:
            case ENLATADO_GH:
                return PlanesMapper.getCodigoEnlatado(plan.getIdPlan())
                        + PlanesMapper.getCodigoZona(zona)
                        + "C";
            case ENLATADO_ELEBAR:{
                switch (plan.getIdPlan()){
                    case ELEBAR_3_1:
                        return PRODUCTO_ELEBAR_AHORRO_SN;
                    case ELEBAR_3_2:
                        return PRODUCTO_ELEBAR_AHORRO_CN;
                    case ELEBAR_3_3:
                        return PRODUCTO_ELEBAR_BALANCE_SN;
                    case ELEBAR_3_4:
                        return PRODUCTO_ELEBAR_BALANCE_CN;
                }
            }
            case ENLATADO_REBA: {
                return PlanesMapper.getCodigoEnlatado(plan.getIdPlan())
                        + "_" + PlanesMapper.getCodigoZonaReba(zona);
            }
            case ENLATADO_ALRIO: {
                return plan.getIdPlan().toString();
            }
            case ENLATADO_ALIANZA_DTV:{
                return PRODUCTO_ALIANZA_DTV;
            }
            case ENLATADO_ALIANZA_COMPARAENCASA: {
                switch (plan.getIdPlan()){
                    case COMPARAENCASA_1:
                        return PRODUCTO_COMPARAENCASA_1;
                    case COMPARAENCASA_2:
                        return PRODUCTO_COMPARAENCASA_2;
                    case COMPARAENCASA_3:
                        return PRODUCTO_COMPARAENCASA_3;
                }
            }
            case ENLATADO_ALIANZA_ASEGURALO: {
                switch (plan.getIdPlan()){
                    case ASEGURALO_1:
                        return PRODUCTO_ASEGURALO_1;
                    case ASEGURALO_2:
                        return PRODUCTO_ASEGURALO_2;
                    case ASEGURALO_3:
                        return PRODUCTO_ASEGURALO_3;
                    case ASEGURALO_4:
                        return PRODUCTO_ASEGURALO_4;
                }
            }
            case ENLATADO_ALIANZA_BTF: {
                switch (plan.getIdPlan()){
                    case BTF_1:
                        return PRODUCTO_BTF_1;
                    case BTF_2:
                        return PRODUCTO_BTF_2;
                    case BTF_3:
                        return PRODUCTO_BTF_3;
                    case BTF_4:
                        return PRODUCTO_BTF_4;
                }
            }
            case ENLATADO_HOGAR_DGO: {
                if (plan == null || plan.getIdPlan() == null || !(plan.getIdPlan().equals(IdPlan.DGO_LITE) || plan.getIdPlan().equals(IdPlan.DGO_FULL)) )
                    throw new CoreBusinessException(Arrays.asList("No hay producto configurado para emitir el plan de " + plan.getIdPlan().toString()));
                return plan.getIdPlan().equals(IdPlan.DGO_LITE) ? PRODUCTO_ALIANZA_DGO_LITE + PlanesMapper.getCodigoZona(zona) : PRODUCTO_ALIANZA_DGO_FULL + PlanesMapper.getCodigoZona(zona);
            }

            default:
                throw new CoreBusinessException(Arrays.asList("El tipo de plan " + plan.getTipoPlan() + " no tiene definida una configuracion de producto para cotizar"));
            }
        }


    public static String getCodigoCantidadCuotas(Plan plan, CantidadCuotas cantidadCuotas) throws CoreBusinessException {
        if (plan.getTipoPlan().equals(TipoPlan.ENLATADO_ALIANZA_DTV))
            return CANTIDAD_CUOTAS_DTV;
        switch (cantidadCuotas){
//            case CUOTAS2: return CANTIDAD_CUOTAS_2;
//            case CUOTAS4: return CANTIDAD_CUOTAS_4;
//            case CUOTAS6: return CANTIDAD_CUOTAS_6;
//            case CUOTAS8: return CANTIDAD_CUOTAS_8;
//            case CUOTAS10: return CANTIDAD_CUOTAS_10;
//            case CUOTAS12: return CANTIDAD_CUOTAS_12;
            case CUOTAS1: return CANTIDAD_CUOTAS_1;
            case CUOTAS2: return CANTIDAD_CUOTAS_2;
            case CUOTAS3: return CANTIDAD_CUOTAS_3;
        }
        throw new CoreBusinessException(Arrays.asList("La cantidad de cuotas " + cantidadCuotas.label() + " es incorrecto para el plan seleccionado"));
    }

    private static String getCodigoEnlatado(IdPlan idPlan) throws CoreBusinessException {
        switch (idPlan) {
            case CERCANIA_2:
            case CERCANIA_CDTV:
            case CERCANIA_AFINIDAD:
            case CERCANIA_GH:
                return CODIGO_PLAN_COMERCIAL_ENLATADO_1;
            case PROTECCION_2:
            case PROTECCION_CDTV:
            case PROTECCION_AFINIDAD:
            case PROTECCION_GH:
                return CODIGO_PLAN_COMERCIAL_ENLATADO_2;
            case SEGURIDAD_0_2:
            case SEGURIDAD_0_CDTV:
            case SEGURIDAD_0_AFINIDAD:
            case SEGURIDAD_0_GH:
                return CODIGO_PLAN_COMERCIAL_ENLATADO_3;
            case SEGURIDAD_1_2:
            case SEGURIDAD_1_CDTV:
            case SEGURIDAD_1_AFINIDAD:
            case SEGURIDAD_1_GH:
                return CODIGO_PLAN_COMERCIAL_ENLATADO_4;
            case SEGURIDAD_2_2:
            case SEGURIDAD_2_CDTV:
            case SEGURIDAD_2_AFINIDAD:
            case SEGURIDAD_2_GH:
                return CODIGO_PLAN_COMERCIAL_ENLATADO_5;
            case SEGURIDAD_3_2:
            case SEGURIDAD_3_CDTV:
            case SEGURIDAD_3_AFINIDAD:
            case SEGURIDAD_3_GH:
                return CODIGO_PLAN_COMERCIAL_ENLATADO_6;
        }
        throw new CoreBusinessException(Arrays.asList("No hay plan comercial configurado para emitir el plan enlatado " + idPlan));
    }

    private static String getCodigoZona (String zona) throws CoreBusinessException {
        switch (zona){
            case "1":
            case "2":
                return "Z1";
            case "3":
            case "4":
            case "5":
                return "Z2";
            case "6":
            case "7":
            case "8":
            case "9":
                return "Z3";
            case "10":
                return "Z4";
        }
        throw new CoreBusinessException(Arrays.asList("La zona " + zona + " es incorrecta"));
    }

    private static String getCodigoZonaReba(String zona) throws CoreBusinessException {
        switch (zona){
            case "1": case "2": case "3": case "4": case "5": case "6": case "7": case "8": case "9": case "10":
                return "Z"+zona;
        }
        throw new CoreBusinessException(Arrays.asList("La zona " + zona + " es incorrecta para el producto enlatado especial"));
    }

    public static String getGastosExplotacion(IdPlan idPlan, TipoMedioPago tipoMedioPago, ModalidadComision modalidadComision, Beneficio beneficio) {
        if (idPlan.equals(IdPlan.PAS_CONFIGURABLE_AFINIDAD) ||
                idPlan.equals(IdPlan.CERCANIA_AFINIDAD)||
                idPlan.equals(IdPlan.PROTECCION_AFINIDAD)||
                idPlan.equals(IdPlan.SEGURIDAD_0_AFINIDAD)||
                idPlan.equals(IdPlan.SEGURIDAD_1_AFINIDAD)||
                idPlan.equals(IdPlan.SEGURIDAD_2_AFINIDAD)||
                idPlan.equals(IdPlan.SEGURIDAD_3_AFINIDAD))
            return tipoMedioPago.equals(TipoMedioPago.AUTOMATICO) ? GASTOS_EXPLOTACION_AUTOMATICO_AFINIDAD : GASTOS_EXPLOTACION_MANUAL_AFINIDAD;

        return tipoMedioPago.equals(TipoMedioPago.AUTOMATICO) ?
                (beneficio != null && beneficio.equals(Beneficio.AUTOS_HOGAR) ? GASTOS_EXPLOTACION_AUTOMATICO_CROSSELING_AUTOS : GASTOS_EXPLOTACION_AUTOMATICO) :
                (beneficio != null && beneficio.equals(Beneficio.AUTOS_HOGAR) ? GASTOS_EXPLOTACION_MANUAL_CROSSELING_AUTOS : GASTOS_EXPLOTACION_MANUAL);
    }

    public static String getGastosAdquisicion(IdPlan idPlan, ModalidadComision modalidadComision, Beneficio beneficio) {
        if (idPlan.equals(IdPlan.PAS_CONFIGURABLE_AFINIDAD) ||
                idPlan.equals(IdPlan.CERCANIA_AFINIDAD)||
                idPlan.equals(IdPlan.PROTECCION_AFINIDAD)||
                idPlan.equals(IdPlan.SEGURIDAD_0_AFINIDAD)||
                idPlan.equals(IdPlan.SEGURIDAD_1_AFINIDAD)||
                idPlan.equals(IdPlan.SEGURIDAD_2_AFINIDAD)||
                idPlan.equals(IdPlan.SEGURIDAD_3_AFINIDAD))
            return GASTOS_ADQUISICION_AFINIDAD;
        return beneficio != null && beneficio.equals(Beneficio.AUTOS_HOGAR) ? GASTOS_ADQUISICION_CROSSELING_AUTOS : GASTOS_ADQUISICION;
    }

    public static String getModoFacturacion(Plan plan) {
        return plan.getTipoPlan().equals(TipoPlan.ENLATADO_ALIANZA_DTV) ? MODO_FAC_CODIGO_DTV : MODO_FAC_CODIGO;
    }
}
