package ar.com.experta.ventashogarapi.infraestructure.db;

import ar.com.experta.ventashogarapi.core.model.NegocioOption;
import ar.com.experta.ventashogarapi.core.model.NegocioOptionOpcionCobertura;
import ar.com.experta.ventashogarapi.core.model.Plan;
import ar.com.experta.ventashogarapi.core.model.PlanItem;
import ar.com.experta.ventashogarapi.core.model.enums.*;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class PlanConfiguration {

    public PlanConfiguration() {
    }

    @Cacheable(value="cachePlanes")
    public List<Plan> getPlanes() {
        //TODO: Si se modifican los nombres de los planes o se agregan nuevos, agregar a la api de polizas (polizas-seguros-api)
        List<Plan> planes = new ArrayList<>();
        List<PlanItem> items;

        //CONFIGURABLE
        int orden = 1;

        List<NegocioOption> opcionesIncendioEdificio = new ArrayList<>();
        opcionesIncendioEdificio.add(new NegocioOptionOpcionCobertura(TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO.toString(),"1er Riesgo Absoluto", TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO.description(), new BigDecimal(200000000), Boolean.TRUE));
        opcionesIncendioEdificio.add(new NegocioOptionOpcionCobertura(TipoCobertura.INCENDIO_EDIFICIO_PRORRATA.toString(),"A Prorrata", TipoCobertura.INCENDIO_EDIFICIO_PRORRATA.description(), new BigDecimal(400000000)));

        items = new ArrayList<>();
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO, opcionesIncendioEdificio,Boolean.TRUE,Boolean.FALSE,new BigDecimal(80000000),new BigDecimal(20000000),new BigDecimal(200000000),null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE,Boolean.TRUE,Boolean.FALSE,null,null,null,TipoCobertura.INCENDIO_EDIFICIO,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.FALSE,null,null,null,TipoCobertura.INCENDIO_EDIFICIO,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(4000000),new BigDecimal(1000000),new BigDecimal(20000000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(5),new BigDecimal(5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(4000000),new BigDecimal(1000000),new BigDecimal(20000000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(5),new BigDecimal(5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1000000),new BigDecimal(500000),new BigDecimal(1000000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(2.5),new BigDecimal(2.5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.FALSE,Boolean.FALSE,new BigDecimal(3000000),new BigDecimal(3000000),new BigDecimal(125000000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(0),new BigDecimal(50),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC,Boolean.TRUE, Boolean.FALSE,null,null,null,TipoCobertura.INCENDIO_CONTENIDO,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE, Boolean.FALSE,null,null,null,TipoCobertura.INCENDIO_CONTENIDO,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,new BigDecimal(150000),new BigDecimal(150000),new BigDecimal(6250000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(5),new BigDecimal(5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.FALSE,Boolean.FALSE,new BigDecimal(450000),new BigDecimal(50000),new BigDecimal(7000000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(25),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,new BigDecimal(20000),new BigDecimal(20000),new BigDecimal(20000),TipoCobertura.ROBO,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,new BigDecimal(225000),new BigDecimal(25000),new BigDecimal(3500000),TipoCobertura.ROBO,new BigDecimal(50),new BigDecimal(50),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.BIENES_ESPECIFICOS,Boolean.FALSE,Boolean.FALSE,new BigDecimal(50000),new BigDecimal(50000),new BigDecimal(2000000),TipoCobertura.ROBO,new BigDecimal(0),new BigDecimal(20),TipoEspecifico.BIEN_ESPECIFICO,CategoriaBienEspecifico.BIENES_GENERALES));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO_PORTATILES,Boolean.FALSE,Boolean.FALSE,new BigDecimal(50000),new BigDecimal(50000),new BigDecimal(2000000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(15),TipoEspecifico.BIEN_ESPECIFICO,CategoriaBienEspecifico.PORTATILES));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO_BICICLETA,Boolean.FALSE,Boolean.FALSE,new BigDecimal(50000),new BigDecimal(50000),new BigDecimal(2000000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(15),TipoEspecifico.BIEN_ESPECIFICO,CategoriaBienEspecifico.BICICLETAS));
        items.add( new PlanItem(orden++, TipoCobertura.TR_ELECTRO_NOTEBOOK,Boolean.FALSE,Boolean.FALSE,new BigDecimal(100000),new BigDecimal(100000),new BigDecimal(3000000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(15),TipoEspecifico.BIEN_ESPECIFICO,CategoriaBienEspecifico.NOTEBOOK));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO,Boolean.FALSE,Boolean.FALSE,new BigDecimal(50000),new BigDecimal(50000),new BigDecimal(6000000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(20),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.FALSE,new BigDecimal(1000000),new BigDecimal(1000000),new BigDecimal(25000000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(0),new BigDecimal(100),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.FALSE,null,null,null,TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.FALSE,Boolean.FALSE,new BigDecimal(1000000),new BigDecimal(1000000),new BigDecimal(25000000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(0),new BigDecimal(100),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.FALSE,new BigDecimal(250000),new BigDecimal(10000),new BigDecimal(1250000),null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES_PCP,Boolean.FALSE,Boolean.FALSE,new BigDecimal(400000),new BigDecimal(50000),new BigDecimal(2000000),null,null,null,TipoEspecifico.PERSONAL,null));
//        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_MEDICOS,Boolean.FALSE,Boolean.TRUE, Boolean.TRUE,new BigDecimal(10000),new BigDecimal(1250),new BigDecimal(50000),TipoCobertura.ACCIDENTES_PERSONALES_PCP,new BigDecimal(2.5),new BigDecimal(2.5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA_EDIFICIO,Boolean.FALSE,Boolean.FALSE,new BigDecimal(300000),new BigDecimal(10000),new BigDecimal(2225000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(10),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1500000),new BigDecimal(500000),new BigDecimal(1500000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(2.5),new BigDecimal(2.5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.PALOS_GOLF,Boolean.FALSE,Boolean.FALSE,new BigDecimal(300000),new BigDecimal(50000),new BigDecimal(1500000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.FALSE,Boolean.FALSE,new BigDecimal(5000),new BigDecimal(5000),new BigDecimal(60000),null,null,null,null,null));
        
        planes.add(new Plan(IdPlan.PAS_CONFIGURABLE_2024,
                "A tu medida",
                TipoPlan.CONFIGURABLE,
                Arrays.asList(TipoProductos.HOGAR_EXPERTA_PRODUCTORES, TipoProductos.HOGAR_BENEFICIO_WERTHEIN, TipoProductos.HOGAR_BENEFICIO_DIRECTV),
                items));
        planes.add(new Plan(IdPlan.PAS_CONFIGURABLE_CDTV_2024,
                "A tu medida",
                TipoPlan.CONFIGURABLE_CDTV,
                Arrays.asList(TipoProductos.HOGAR_CLIENTES_DTV_VAAS_ALIANZA),
                items));
        planes.add(new Plan(IdPlan.PAS_CONFIGURABLE_AFINIDAD,
                "A tu medida (Afinidad Grupo Werthein)",
                TipoPlan.CONFIGURABLE_AFINIDAD,
                Arrays.asList(TipoProductos.HOGAR_BENEFICIO_AFINIDAD_GW),
                items));
        planes.add(new Plan(IdPlan.PAS_CONFIGURABLE_GH,
                "A tu medida (Promoción Gran Hermano 2024)",
                TipoPlan.CONFIGURABLE_GH,
                Arrays.asList(TipoProductos.HOGAR_PROMOCION_GH),
                items));

        //PAQUETES

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(10500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(525000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(525000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(262500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(4200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(210000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(245000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(122500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(245000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(700000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(700000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(36750),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(350000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE,new BigDecimal(42000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(262500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(19250),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.CERCANIA_2,
                "Cercanía",
                TipoPlan.ENLATADO,
                Arrays.asList(TipoProductos.HOGAR_EXPERTA_PRODUCTORES, TipoProductos.HOGAR_CALLCENTER_ALIANZA, TipoProductos.HOGAR_BENEFICIO_WERTHEIN),
                items));
        planes.add(new Plan(IdPlan.CERCANIA_2,
                "Cercanía",
                TipoPlan.ENLATADO_ECOMMERCE,
                Arrays.asList(TipoProductos.HOGAR_EXPERTA_ECOMMERCE),
                items));
        planes.add(new Plan(IdPlan.CERCANIA_CDTV,
                "Cercanía",
                TipoPlan.ENLATADO_CDTV,
                Arrays.asList(TipoProductos.HOGAR_CLIENTES_DTV_VAAS_ALIANZA),
                items));
        planes.add(new Plan(IdPlan.CERCANIA_AFINIDAD,
                "Cercanía",
                TipoPlan.ENLATADO_AFINIDAD,
                Arrays.asList(TipoProductos.HOGAR_BENEFICIO_AFINIDAD_GW),
                items));
        planes.add(new Plan(IdPlan.CERCANIA_GH,
                "Cercanía",
                TipoPlan.ENLATADO_GH,
                Arrays.asList(TipoProductos.HOGAR_PROMOCION_GH),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(16800000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(840000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(840000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(420000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(6720000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(336000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(380000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(190000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(380000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(58800),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(460000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE,new BigDecimal(67200),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(420000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(24000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.PROTECCION_2,
                "Protección",
                TipoPlan.ENLATADO,
                Arrays.asList(TipoProductos.HOGAR_EXPERTA_PRODUCTORES, TipoProductos.HOGAR_EXPERTA_ECOMMERCE, TipoProductos.HOGAR_CALLCENTER_ALIANZA, TipoProductos.HOGAR_BENEFICIO_WERTHEIN),
                items));
        planes.add(new Plan(IdPlan.PROTECCION_2,
                "Protección",
                TipoPlan.ENLATADO_ECOMMERCE,
                Arrays.asList(TipoProductos.HOGAR_EXPERTA_ECOMMERCE),
                items));
        planes.add(new Plan(IdPlan.PROTECCION_CDTV,
                "Protección",
                TipoPlan.ENLATADO_CDTV,
                Arrays.asList(TipoProductos.HOGAR_CLIENTES_DTV_VAAS_ALIANZA),
                items));
        planes.add(new Plan(IdPlan.PROTECCION_AFINIDAD,
                "Protección",
                TipoPlan.ENLATADO_AFINIDAD,
                Arrays.asList(TipoProductos.HOGAR_BENEFICIO_AFINIDAD_GW),
                items));
        planes.add(new Plan(IdPlan.PROTECCION_GH,
                "Protección",
                TipoPlan.ENLATADO_GH,
                Arrays.asList(TipoProductos.HOGAR_PROMOCION_GH),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(21150000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1057500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1057500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(8460000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(423000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(481500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(240750),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(481500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1305000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1305000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(74025),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(585000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE,new BigDecimal(84600),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(528750),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(29250),null,null,null,null,null,null,null));

        //Si se modifica este plan modificar en gateway-ecommerce-hogar para ver en ecommerce Servicio de Mantenimientos
        planes.add(new Plan(IdPlan.SEGURIDAD_0_2,
                "Seguridad",
                TipoPlan.ENLATADO,
                Arrays.asList(TipoProductos.HOGAR_EXPERTA_PRODUCTORES, TipoProductos.HOGAR_EXPERTA_ECOMMERCE, TipoProductos.HOGAR_CALLCENTER_ALIANZA, TipoProductos.HOGAR_BENEFICIO_WERTHEIN),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_0_2,
                "Seguridad",
                TipoPlan.ENLATADO_ECOMMERCE,
                Arrays.asList(TipoProductos.HOGAR_EXPERTA_ECOMMERCE),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_0_CDTV,
                "Seguridad",
                TipoPlan.ENLATADO_CDTV,
                Arrays.asList(TipoProductos.HOGAR_CLIENTES_DTV_VAAS_ALIANZA),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_0_AFINIDAD,
                "Seguridad",
                TipoPlan.ENLATADO_AFINIDAD,
                Arrays.asList(TipoProductos.HOGAR_BENEFICIO_AFINIDAD_GW),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_0_GH,
                "Seguridad",
                TipoPlan.ENLATADO_GH,
                Arrays.asList(TipoProductos.HOGAR_PROMOCION_GH),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(26500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1325000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1325000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(10600000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(530000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(600000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(300000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(600000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1575000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1575000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(92750),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(725000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE,new BigDecimal(106000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(662500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(37500),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.SEGURIDAD_1_2,
                "Seguridad 1.1",
                TipoPlan.ENLATADO,
                Arrays.asList(TipoProductos.HOGAR_EXPERTA_PRODUCTORES, TipoProductos.HOGAR_CALLCENTER_ALIANZA, TipoProductos.HOGAR_BENEFICIO_WERTHEIN),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_1_CDTV,
                "Seguridad 1.1",
                TipoPlan.ENLATADO_CDTV,
                Arrays.asList(TipoProductos.HOGAR_CLIENTES_DTV_VAAS_ALIANZA),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_1_AFINIDAD,
                "Seguridad 1.1",
                TipoPlan.ENLATADO_AFINIDAD,
                Arrays.asList(TipoProductos.HOGAR_BENEFICIO_AFINIDAD_GW),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_1_GH,
                "Seguridad 1.1",
                TipoPlan.ENLATADO_GH,
                Arrays.asList(TipoProductos.HOGAR_PROMOCION_GH),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(34100000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1705000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1705000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(13640000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(682000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(742500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(371250),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(742500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1925000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1925000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(119350),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(797500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE,new BigDecimal(136400),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(46750),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.SEGURIDAD_2_2,
                "Seguridad 1.2",
                TipoPlan.ENLATADO,
                Arrays.asList(TipoProductos.HOGAR_EXPERTA_PRODUCTORES, TipoProductos.HOGAR_CALLCENTER_ALIANZA, TipoProductos.HOGAR_BENEFICIO_WERTHEIN),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_2_CDTV,
                "Seguridad 1.2",
                TipoPlan.ENLATADO_CDTV,
                Arrays.asList(TipoProductos.HOGAR_CLIENTES_DTV_VAAS_ALIANZA),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_2_AFINIDAD,
                "Seguridad 1.2",
                TipoPlan.ENLATADO_AFINIDAD,
                Arrays.asList(TipoProductos.HOGAR_BENEFICIO_AFINIDAD_GW),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_2_GH,
                "Seguridad 1.2",
                TipoPlan.ENLATADO_GH,
                Arrays.asList(TipoProductos.HOGAR_PROMOCION_GH),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(42000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2100000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2100000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(16800000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(840000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1080000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(540000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1080000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2520000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2520000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(147000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1020000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE,new BigDecimal(168000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(60000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.SEGURIDAD_3_2,
                "Seguridad 1.3",
                TipoPlan.ENLATADO,
                Arrays.asList(TipoProductos.HOGAR_EXPERTA_PRODUCTORES, TipoProductos.HOGAR_CALLCENTER_ALIANZA, TipoProductos.HOGAR_BENEFICIO_WERTHEIN),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_3_CDTV,
                "Seguridad 1.3",
                TipoPlan.ENLATADO_CDTV,
                Arrays.asList(TipoProductos.HOGAR_CLIENTES_DTV_VAAS_ALIANZA),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_3_AFINIDAD,
                "Seguridad 1.3",
                TipoPlan.ENLATADO_AFINIDAD,
                Arrays.asList(TipoProductos.HOGAR_BENEFICIO_AFINIDAD_GW),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_3_GH,
                "Seguridad 1.3",
                TipoPlan.ENLATADO_GH,
                Arrays.asList(TipoProductos.HOGAR_PROMOCION_GH),
                items));


        // HOGAR + DirecTV Go

        items = new ArrayList<>();
        orden = 1;

        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(85000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(4250000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(4250000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(8500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(425000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1125000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(20000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(562500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1125000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(150000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(30000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.DGO_LITE,
                "Hogar + DGO Lite",
                TipoPlan.ENLATADO_HOGAR_DGO,
                Arrays.asList(TipoProductos.HOGAR_DGO_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;

        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(156000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(7800000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(7800000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(15600000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(780000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2250000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(20000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1125000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2250000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(6000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(6000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(275000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(70000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.DGO_FULL,
                "Hogar + DGO Full",
                TipoPlan.ENLATADO_HOGAR_DGO,
                Arrays.asList(TipoProductos.HOGAR_DGO_ALIANZA),
                items));

        //PAQUETES ELEBAR

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(4500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(225000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(225000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1350000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(101250),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO_ELEBAR_2,Boolean.TRUE,Boolean.TRUE, new BigDecimal(67500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.TR_ELECTRO_NOTEBOOK_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(0),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(337500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(337500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(90000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(13500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(33750),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(33750),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(6750),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(50625),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ELEBAR_3_1,
                "Ahorro sin Notebook",
                TipoPlan.ENLATADO_ELEBAR,
                Arrays.asList(TipoProductos.HOGAR_ELEBAR_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(4500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(225000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(225000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1350000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(67500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO_ELEBAR_2,Boolean.TRUE,Boolean.TRUE, new BigDecimal(67500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.TR_ELECTRO_NOTEBOOK_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(67500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(337500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(337500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(90000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(13500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(33750),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(33750),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(6750),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(33750),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ELEBAR_3_2,
                "Ahorro con Notebook",
                TipoPlan.ENLATADO_ELEBAR,
                Arrays.asList(TipoProductos.HOGAR_ELEBAR_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(7200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(360000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(360000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1800000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(135000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO_ELEBAR_2,Boolean.TRUE,Boolean.TRUE, new BigDecimal(90000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.TR_ELECTRO_NOTEBOOK_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(0),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(540000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(540000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(144000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(21600),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(54000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(54000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(10800),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(26695),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ELEBAR_3_3,
                "Balance sin Notebook",
                TipoPlan.ENLATADO_ELEBAR,
                Arrays.asList(TipoProductos.HOGAR_ELEBAR_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(7200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(360000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(360000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1800000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(90000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO_ELEBAR_2,Boolean.TRUE,Boolean.TRUE, new BigDecimal(90000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.TR_ELECTRO_NOTEBOOK_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(90000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(540000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(540000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(144000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(21600),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(54000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(54000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(10800),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(36583),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ELEBAR_3_4,
                "Balance con Notebook",
                TipoPlan.ENLATADO_ELEBAR,
                Arrays.asList(TipoProductos.HOGAR_ELEBAR_ALIANZA),
                items));

        //PAQUETES REBA

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(50000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(50000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGOS_NATURALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(50000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(400000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(30000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(15000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(110000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(110000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(110000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(40000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(3000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(10000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(10000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(15000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.REBANKING_AHORRO,
                "Ahorro 2.1",
                TipoPlan.ENLATADO_REBA,
                Arrays.asList(TipoProductos.HOGAR_REBANKING_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1400000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(70000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(70000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGOS_NATURALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1400000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(70000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(560000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(55000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(37000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(130000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(130000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(130000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(50000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(4500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(15000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(15000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(3000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(27500),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.REBANKING_BALANCE,
                "Balance 2.1",
                TipoPlan.ENLATADO_REBA,
                Arrays.asList(TipoProductos.HOGAR_REBANKING_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2400000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(120000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(120000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGOS_NATURALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2400000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(120000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(960000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(70000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(55000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(160000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(160000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(160000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(100000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(5500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(26000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(26000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(5500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(35000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.REBANKING_SEGURIDAD,
                "Seguridad 2.1",
                TipoPlan.ENLATADO_REBA,
                Arrays.asList(TipoProductos.HOGAR_REBANKING_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(5000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(0),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(0),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(0),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(0),null,null,null,null,null,null,null));

        //PAQUETES AL RIO

        planes.add(new Plan(IdPlan.ALRIO1,
                "Al Río Básico",
                TipoPlan.ENLATADO_ALRIO,
                Arrays.asList(TipoProductos.HOGAR_ALRIO_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(6000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2400000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(50000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(5000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(15000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ALRIO2,
                "Al Río 50m²",
                TipoPlan.ENLATADO_ALRIO,
                Arrays.asList(TipoProductos.HOGAR_ALRIO_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(8500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(3400000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(250000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(65000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(10000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(20000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ALRIO3,
                "Al Río 70m²",
                TipoPlan.ENLATADO_ALRIO,
                Arrays.asList(TipoProductos.HOGAR_ALRIO_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(12000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(4800000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(350000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(100000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(12000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(30000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ALRIO4,
                "Al Río 100m²",
                TipoPlan.ENLATADO_ALRIO,
                Arrays.asList(TipoProductos.HOGAR_ALRIO_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(15000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(6000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1800000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1800000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(400000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(135000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(18000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(45000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ALRIO5,
                "Al Río 120m²",
                TipoPlan.ENLATADO_ALRIO,
                Arrays.asList(TipoProductos.HOGAR_ALRIO_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(25000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(10000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(175000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(20000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(55000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ALRIO6,
                "Al Río 200m²",
                TipoPlan.ENLATADO_ALRIO,
                Arrays.asList(TipoProductos.HOGAR_ALRIO_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(37000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(14800000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(650000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(23000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(60000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ALRIO7,
                "Al Río 300m²",
                TipoPlan.ENLATADO_ALRIO,
                Arrays.asList(TipoProductos.HOGAR_ALRIO_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(50000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(20000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(250000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(25000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(85000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ALRIO8,
                "Al Río 400m²",
                TipoPlan.ENLATADO_ALRIO,
                Arrays.asList(TipoProductos.HOGAR_ALRIO_ALIANZA),
                items));

        //PAQUETE DIRECTV

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(13000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(6500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(650000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(650000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(325000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(650000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(325000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(325000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(180000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(90000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(22000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(650000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        planes.add(new Plan(IdPlan.DTV0,
                "PACK DTV",
                TipoPlan.ENLATADO_ALIANZA_DTV,
                Arrays.asList(TipoProductos.HOGAR_CLIENTES_DTV_ALIANZA),
                items));

        //PAQUETES COMPARA EN CASA

        items = new ArrayList<>();
        orden = 1;

        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(75000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(4000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(300000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.COMPARAENCASA_3,
                "Plan Compara en Casa Estandar",
                TipoPlan.ENLATADO_ALIANZA_COMPARAENCASA,
                Arrays.asList(TipoProductos.HOGAR_COMPARAENCASA_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;

        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(75000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(250000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(250000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.COMPARAENCASA_2,
                "Plan Compara en Casa Plus",
                TipoPlan.ENLATADO_ALIANZA_COMPARAENCASA,
                Arrays.asList(TipoProductos.HOGAR_COMPARAENCASA_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;

        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(75000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(7500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(30000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(375000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(300000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.COMPARAENCASA_1,
                "Plan Compara en Casa Premium",
                TipoPlan.ENLATADO_ALIANZA_COMPARAENCASA,
                Arrays.asList(TipoProductos.HOGAR_COMPARAENCASA_ALIANZA),
                items));

        //PAQUETES ASEGURALO

        items = new ArrayList<>();
        orden = 1;

        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(21000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(8400000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1260000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(30000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1050000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(420000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1050000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(525000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(525000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(420000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(20000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(210000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(420000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(30000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE,new BigDecimal(150000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1260000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(510000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ASEGURALO_1,
                "Plan Aseguralo Basico",
                TipoPlan.ENLATADO_ALIANZA_ASEGURALO,
                Arrays.asList(TipoProductos.HOGAR_ASEGURALO_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;

        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(31500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(12600000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1890000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(45000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1575000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(630000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1575000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(787500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(787500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(630000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(20000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(315000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(630000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(45000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE,new BigDecimal(225000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1890000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(765000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ASEGURALO_2,
                "Plan Aseguralo Estandar",
                TipoPlan.ENLATADO_ALIANZA_ASEGURALO,
                Arrays.asList(TipoProductos.HOGAR_ASEGURALO_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;

        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(42000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(16800000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2520000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(60000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2100000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(840000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2100000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1050000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1050000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(840000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(20000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(420000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(840000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(60000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE,new BigDecimal(300000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2520000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1020000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ASEGURALO_3,
                "Plan Aseguralo Plus",
                TipoPlan.ENLATADO_ALIANZA_ASEGURALO,
                Arrays.asList(TipoProductos.HOGAR_ASEGURALO_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;

        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(63000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(25200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3780000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(90000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3150000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1260000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3150000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1575000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1575000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1260000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(20000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(630000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1260000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(90000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE,new BigDecimal(450000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3780000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1530000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ASEGURALO_4,
                "Plan Aseguralo Premium",
                TipoPlan.ENLATADO_ALIANZA_ASEGURALO,
                Arrays.asList(TipoProductos.HOGAR_ASEGURALO_ALIANZA),
                items));

        //PAQUETES BANCO DE TIERRA DEL FUEGO
        items = new ArrayList<>();
        orden = 1;

        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(21000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1050000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1050000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(8400000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(420000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(540000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(270000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(540000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1260000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1260000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(150000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(510000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE,new BigDecimal(150000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(525000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(30000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.BTF_1,
                "Plan Banco de Tierra del Fuego Básico",
                TipoPlan.ENLATADO_ALIANZA_BTF,
                Arrays.asList(TipoProductos.HOGAR_BTF_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;

        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(31500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1575000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1575000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(12600000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(630000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(810000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(405000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(810000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1890000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1890000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(225000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(765000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE,new BigDecimal(225000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(45000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.BTF_2,
                "Plan Banco de Tierra del Fuego Estandar",
                TipoPlan.ENLATADO_ALIANZA_BTF,
                Arrays.asList(TipoProductos.HOGAR_BTF_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;

        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(42000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2100000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2100000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(16800000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(840000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1080000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(540000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1080000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2520000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(2520000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(300000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1020000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE,new BigDecimal(300000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(60000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.BTF_3,
                "Plan Banco de Tierra del Fuego Plus",
                TipoPlan.ENLATADO_ALIANZA_BTF,
                Arrays.asList(TipoProductos.HOGAR_BTF_ALIANZA),
                items));

        items = new ArrayList<>();
        orden = 1;

        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(63000000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3150000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3150000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(25200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC_ABSOLUTO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1260000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1620000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(5000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(810000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1620000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3780000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE,null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(3780000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(450000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1530000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE,new BigDecimal(450000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(750000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE,new BigDecimal(90000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.BTF_4,
                "Plan Banco de Tierra del Fuego Premium",
                TipoPlan.ENLATADO_ALIANZA_BTF,
                Arrays.asList(TipoProductos.HOGAR_BTF_ALIANZA),
                items));

















        // DEPRECADOS

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO, opcionesIncendioEdificio,Boolean.TRUE,Boolean.FALSE,new BigDecimal(28000000),new BigDecimal(1500000),new BigDecimal(140000000),null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE,Boolean.TRUE,Boolean.FALSE,null,null,null,TipoCobertura.INCENDIO_EDIFICIO,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.FALSE,null,null,null,TipoCobertura.INCENDIO_EDIFICIO,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1400000),new BigDecimal(75000),new BigDecimal(7000000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(5),new BigDecimal(5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE,new BigDecimal(1400000),new BigDecimal(75000),new BigDecimal(7000000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(5),new BigDecimal(5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.HONORARIOS_PROFESIONALES,Boolean.TRUE,Boolean.TRUE,new BigDecimal(700000),new BigDecimal(37500),new BigDecimal(500000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(2.5),new BigDecimal(2.5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.FALSE,Boolean.FALSE,new BigDecimal(5600000),new BigDecimal(200000),new BigDecimal(70000000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(0),new BigDecimal(50),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC,Boolean.TRUE,Boolean.FALSE,null,null,null,TipoCobertura.INCENDIO_CONTENIDO,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_GRANIZO_CONTENIDO,Boolean.TRUE,Boolean.FALSE,null,null,null,TipoCobertura.INCENDIO_CONTENIDO,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS_CONTENIDO,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,new BigDecimal(280000),new BigDecimal(10000),new BigDecimal(3500000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(5),new BigDecimal(5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.FALSE,Boolean.FALSE,new BigDecimal(840000),new BigDecimal(25000),new BigDecimal(3500000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(30),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DOCUMENTOS_PERSONALES,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,new BigDecimal(15000),new BigDecimal(15000),new BigDecimal(15000),TipoCobertura.ROBO,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.FALSE,Boolean.TRUE,Boolean.TRUE,new BigDecimal(420000),new BigDecimal(12500),new BigDecimal(1750000),TipoCobertura.ROBO,new BigDecimal(50),new BigDecimal(50),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.BIENES_ESPECIFICOS,Boolean.FALSE,Boolean.FALSE,new BigDecimal(100000),new BigDecimal(5000),new BigDecimal(750000),TipoCobertura.ROBO,new BigDecimal(0),new BigDecimal(20),TipoEspecifico.BIEN_ESPECIFICO,CategoriaBienEspecifico.BIENES_GENERALES));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO_PORTATILES,Boolean.FALSE,Boolean.FALSE,new BigDecimal(100000),new BigDecimal(5000),new BigDecimal(1000000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(25),TipoEspecifico.BIEN_ESPECIFICO,CategoriaBienEspecifico.PORTATILES));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO_BICICLETA,Boolean.FALSE,Boolean.FALSE,new BigDecimal(100000),new BigDecimal(5000),new BigDecimal(1000000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(25),TipoEspecifico.BIEN_ESPECIFICO,CategoriaBienEspecifico.BICICLETAS));
        items.add( new PlanItem(orden++, TipoCobertura.TR_ELECTRO_NOTEBOOK,Boolean.FALSE,Boolean.FALSE,new BigDecimal(100000),new BigDecimal(25000),new BigDecimal(1200000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(25),TipoEspecifico.BIEN_ESPECIFICO,CategoriaBienEspecifico.NOTEBOOK));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO,Boolean.FALSE,Boolean.FALSE,new BigDecimal(100000),new BigDecimal(50000),new BigDecimal(1800000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(25),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.FALSE,new BigDecimal(1000000),new BigDecimal(150000),new BigDecimal(10000000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(0),new BigDecimal(100),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.FALSE,null,null,null,TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.FALSE,Boolean.FALSE,new BigDecimal(1000000),new BigDecimal(250000),new BigDecimal(10000000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(0),new BigDecimal(100),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.FALSE,new BigDecimal(100000),new BigDecimal(5000),new BigDecimal(500000),null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES_PCP,Boolean.FALSE,Boolean.FALSE,new BigDecimal(160000),new BigDecimal(10000),new BigDecimal(800000),null,null,null,TipoEspecifico.PERSONAL,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA_EDIFICIO,Boolean.FALSE,Boolean.FALSE,new BigDecimal(100000),new BigDecimal(10000),new BigDecimal(1216800),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(10),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE,new BigDecimal(700000),new BigDecimal(37500),new BigDecimal(750000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(2.5),new BigDecimal(2.5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.PALOS_GOLF,Boolean.FALSE,Boolean.FALSE,new BigDecimal(100000),new BigDecimal(5000),new BigDecimal(500000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(5),new BigDecimal(5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.FALSE,Boolean.FALSE,new BigDecimal(4000),new BigDecimal(2000),new BigDecimal(40000),null,null,null,null,null));

        planes.add(new Plan(IdPlan.PAS_CONFIGURABLE_2023,
                "A tu medida",
                TipoPlan.CONFIGURABLE,
                Arrays.asList(),
                items));
        planes.add(new Plan(IdPlan.PAS_CONFIGURABLE_CDTV,
                "A tu medida",
                TipoPlan.CONFIGURABLE_CDTV,
                Arrays.asList(),
                items));


        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO, opcionesIncendioEdificio,Boolean.TRUE,Boolean.FALSE, new BigDecimal(16000000),new BigDecimal(1500000),new BigDecimal(80000000),null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.FALSE, new BigDecimal(1500000),new BigDecimal(100000),new BigDecimal(30000000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(0),new BigDecimal(300),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.FALSE, new BigDecimal(350000),new BigDecimal(45000),new BigDecimal(3000000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(5),new BigDecimal(45),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.BIENES_ESPECIFICOS,Boolean.FALSE,Boolean.FALSE, new BigDecimal(4500),new BigDecimal(4500),new BigDecimal(135000),TipoCobertura.ROBO,new BigDecimal(0),new BigDecimal(10),TipoEspecifico.BIEN_ESPECIFICO,CategoriaBienEspecifico.BIENES_GENERALES));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO,Boolean.TRUE,Boolean.FALSE, new BigDecimal(180000),new BigDecimal(25000),new BigDecimal(750000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(1),new BigDecimal(25),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.TR_ELECTRO_DETALLE,Boolean.FALSE,Boolean.FALSE, new BigDecimal(25000),new BigDecimal(25000),new BigDecimal(750000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(25),TipoEspecifico.BIEN_ESPECIFICO,CategoriaBienEspecifico.ELECTRODOMESTICOS));
        items.add( new PlanItem(orden++, TipoCobertura.TR_ELECTRO_NOTEBOOK,Boolean.FALSE,Boolean.FALSE, new BigDecimal(25000),new BigDecimal(25000),new BigDecimal(750000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(25),TipoEspecifico.BIEN_ESPECIFICO,CategoriaBienEspecifico.NOTEBOOK));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.FALSE, new BigDecimal(1500000),new BigDecimal(250000),new BigDecimal(5000000),null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.FALSE,Boolean.FALSE, new BigDecimal(375000),new BigDecimal(375000),new BigDecimal(3000000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(0),new BigDecimal(25),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES_PCP,Boolean.FALSE,Boolean.FALSE, new BigDecimal(10000),new BigDecimal(10000),new BigDecimal(300000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(20),TipoEspecifico.PERSONAL,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_MEDICOS,Boolean.FALSE,Boolean.TRUE, new BigDecimal(1500),new BigDecimal(1500),new BigDecimal(45000),TipoCobertura.ACCIDENTES_PERSONALES,new BigDecimal(2.5),new BigDecimal(2.5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.FALSE, new BigDecimal(25000),new BigDecimal(5000),new BigDecimal(300000),null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.PALOS_GOLF,Boolean.FALSE,Boolean.FALSE, new BigDecimal(5000),new BigDecimal(5000),new BigDecimal(750000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA_EDIFICIO,Boolean.FALSE,Boolean.FALSE, new BigDecimal(10000),new BigDecimal(10000),new BigDecimal(300000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(10),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.FALSE,Boolean.FALSE, new BigDecimal(75000),new BigDecimal(75000),new BigDecimal(1500000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(0),new BigDecimal(5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.FALSE,Boolean.FALSE, new BigDecimal(2500),new BigDecimal(2500),new BigDecimal(10000),TipoCobertura.INCENDIO_CONTENIDO,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGOS_NATURALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1500000),new BigDecimal(1500000),new BigDecimal(30000000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(100),new BigDecimal(100),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(75000),new BigDecimal(75000),new BigDecimal(1500000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(5),new BigDecimal(5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(75000),new BigDecimal(75000),new BigDecimal(1500000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(5),new BigDecimal(5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(75000),new BigDecimal(75000),new BigDecimal(1500000),TipoCobertura.INCENDIO_EDIFICIO,new BigDecimal(5),new BigDecimal(5),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(175000),new BigDecimal(50000),new BigDecimal(1500000),TipoCobertura.ROBO,new BigDecimal(50),new BigDecimal(50),null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO_PORTATILES,Boolean.FALSE,Boolean.FALSE, new BigDecimal(25000),new BigDecimal(25000),new BigDecimal(750000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(25),TipoEspecifico.BIEN_ESPECIFICO,CategoriaBienEspecifico.PORTATILES));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO_BICICLETA,Boolean.FALSE,Boolean.FALSE, new BigDecimal(25000),new BigDecimal(25000),new BigDecimal(750000),TipoCobertura.INCENDIO_CONTENIDO,new BigDecimal(0),new BigDecimal(25),TipoEspecifico.BIEN_ESPECIFICO,CategoriaBienEspecifico.BICICLETAS));

        planes.add(new Plan(IdPlan.PAS_CONFIGURABLE,
                "A tu medida",
                TipoPlan.CONFIGURABLE,
                Arrays.asList(),
                items));


        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2400000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(120000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(120000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(120000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(960000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(70000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(55000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(160000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(160000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(100000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(5500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(26000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(26000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(5500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(35000),null,null,null,null,null,null,null));
        planes.add(new Plan(IdPlan.CERCANIA,
                "Cercanía",
                TipoPlan.ENLATADO,
                Arrays.asList(),
                items));
        planes.add(new Plan(IdPlan.CERCANIA,
                "Cercanía",
                TipoPlan.ENLATADO_ECOMMERCE,
                Arrays.asList(),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(3300000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(165000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(165000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(165000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1320000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(95000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(75000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(115000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(6000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(30000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(30000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(6000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(47500),null,null,null,null,null,null,null));
        planes.add(new Plan(IdPlan.PROTECCION,
                "Protección",
                TipoPlan.ENLATADO,
                Arrays.asList(),
                items));
        planes.add(new Plan(IdPlan.PROTECCION,
                "Protección",
                TipoPlan.ENLATADO_ECOMMERCE,
                Arrays.asList(),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(3700000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(185000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(185000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(185000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1480000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(107000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(85000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(225000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(225000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(130000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(6500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(34000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(34000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(6700),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(53500),null,null,null,null,null,null,null));
        planes.add(new Plan(IdPlan.SEGURIDAD_0,
                "Seguridad",
                TipoPlan.ENLATADO,
                Arrays.asList(),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_0,
                "Seguridad",
                TipoPlan.ENLATADO_ECOMMERCE,
                Arrays.asList(),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(4100000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(205000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(205000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(205000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1640000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(120000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(95000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(240000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(240000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(145000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(7500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(38000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(38000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(7000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(60000),null,null,null,null,null,null,null));
        planes.add(new Plan(IdPlan.SEGURIDAD_1,
                "Seguridad 1.1",
                TipoPlan.ENLATADO,
                Arrays.asList(),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_1_CDTV,
                "Seguridad 1.1",
                TipoPlan.ENLATADO_CDTV,
                Arrays.asList(),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(4900000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(245000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(245000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(245000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1960000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(135000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(105000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(270000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(270000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(160000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(8500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(45000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(45000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(7500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(67500),null,null,null,null,null,null,null));
        planes.add(new Plan(IdPlan.SEGURIDAD_2,
                "Seguridad 1.2",
                TipoPlan.ENLATADO,
                Arrays.asList(),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_2_CDTV,
                "Seguridad 1.2",
                TipoPlan.ENLATADO_CDTV,
                Arrays.asList(),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(5500000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IE,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(275000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(275000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(275000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_HVCT_IC,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(180000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ELECTRO_ESPECIFICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(120000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(330000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.AD_RESPONSABILIDAD_CIVIL_ANIMALES,Boolean.TRUE,Boolean.TRUE, null,null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(330000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(170000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(10000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(50000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(50000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(8500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(90000),null,null,null,null,null,null,null));
        planes.add(new Plan(IdPlan.SEGURIDAD_3,
                "Seguridad 1.3",
                TipoPlan.ENLATADO,
                Arrays.asList(),
                items));
        planes.add(new Plan(IdPlan.SEGURIDAD_3_CDTV,
                "Seguridad 1.3",
                TipoPlan.ENLATADO_CDTV,
                Arrays.asList(),
                items));


        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(60000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(60000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGOS_NATURALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1200000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(60000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(400000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(30000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(15000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(110000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(110000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(40000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(3000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(10000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(10000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(15000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ELEBAR_1_1,
                "Seguridad 1.1 Elebar",
                TipoPlan.ENLATADO_ELEBAR,
                Arrays.asList(),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1680000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(84000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(84000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGOS_NATURALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1680000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(84000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(560000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(55000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(37000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(130000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(130000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(50000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(4500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(15000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(15000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(3000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(27500),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ELEBAR_1_2,
                "Seguridad 1.2 Elebar",
                TipoPlan.ENLATADO_ELEBAR,
                Arrays.asList(),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2880000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(144000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(144000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGOS_NATURALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2880000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GRANIZO_EDIFICIO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(144000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(960000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(70000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(55000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(160000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(160000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(100000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(5500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(26000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(26000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(5500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(35000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ELEBAR_1_3,
                "Seguridad 1.3 Elebar",
                TipoPlan.ENLATADO_ELEBAR,
                Arrays.asList(),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1680000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(84000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(84000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGOS_NATURALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1680000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(560000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(42000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(20000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(110000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(110000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(40000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(3000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(10000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(10000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(21000),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ELEBAR_2_1,
                "Seguridad 2.1 Elebar",
                TipoPlan.ENLATADO_ELEBAR,
                Arrays.asList(),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2352000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(117600),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(117600),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGOS_NATURALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(2352000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(784000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(77000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(42000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(130000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(130000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(50000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(4500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(15000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(15000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(3000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(38500),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ELEBAR_2_2,
                "Seguridad 2.2 Elebar",
                TipoPlan.ENLATADO_ELEBAR,
                Arrays.asList(),
                items));

        items = new ArrayList<>();
        orden = 1;
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_EDIFICIO_ABSOLUTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(4176000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.GASTOS_ALOJAMIENTO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(208800),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RETIRO_ESCOMBROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(208800),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGOS_NATURALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(4176000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.INCENDIO_CONTENIDO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(1392000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ROBO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(101500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RIESGO_ELECTRO_ELEBAR,Boolean.TRUE,Boolean.TRUE, new BigDecimal(60000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_PRIVADOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(160000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.RESPONSABILIDAD_CIVIL_LINDEROS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(160000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ACCIDENTES_PERSONALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(100000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.CRISTALES,Boolean.TRUE,Boolean.TRUE, new BigDecimal(5500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_AGUA,Boolean.TRUE,Boolean.TRUE, new BigDecimal(26000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.DANIOS_ESTETICOS,Boolean.TRUE,Boolean.TRUE, new BigDecimal(26000),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.ALIMENTOS_FREEZER,Boolean.TRUE,Boolean.TRUE, new BigDecimal(5500),null,null,null,null,null,null,null));
        items.add( new PlanItem(orden++, TipoCobertura.VIVIENDA_VERANO,Boolean.TRUE,Boolean.TRUE, new BigDecimal(50750),null,null,null,null,null,null,null));

        planes.add(new Plan(IdPlan.ELEBAR_2_3,
                "Seguridad 2.3 Elebar",
                TipoPlan.ENLATADO_ELEBAR,
                Arrays.asList(),
                items));

        return planes;

    }

}