package ar.com.experta.ventashogarapi.infraestructure.dtvhubclient;

import ar.com.experta.ventashogarapi.commons.exceptions.badgateway.DtvHubApiBadGatewayException;
import ar.com.experta.ventashogarapi.commons.exceptions.notfound.NotFoundException;
import ar.com.experta.ventashogarapi.commons.exceptions.unprocessable.CodigoDGONotFoudException;
import ar.com.experta.ventashogarapi.core.model.enums.IdPlan;
import ar.com.experta.ventashogarapi.infraestructure.dtvhubclient.model.AsignacionRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class DtvHubClient {

    @Value("${url_dtvhub}")
    private String HOST_API_HUB_DTV;
    private static final String GET_CODIGO_DTVGO = "/codigos/proximolibre?plan={plan}";
    private static final String PUT_LOCALIZACION_ZONA_RIESGO_BY_ID = "/codigos/{codigo}/poliza";
    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    private static final Logger logger = LoggerFactory.getLogger(DtvHubClient.class);

    public String getCodigoDTVGO(IdPlan idPlan) {
        try {
            String plan;
            switch (idPlan) {
                case DGO_LITE:{
                    plan = "LITE";
                    break;
                }
                case DGO_FULL:{
                    plan = "FULL";
                    break;
                }
                default:
                    return null;
            }
            UriComponentsBuilder builderZonaRiesgo = UriComponentsBuilder.fromHttpUrl((HOST_API_HUB_DTV + GET_CODIGO_DTVGO).replace("{plan}", plan));
            return restTemplate.getForObject(builderZonaRiesgo.toUriString(), String.class);
        }catch (NotFoundException ex){
            logger.error(ex.getMessage());
            throw new CodigoDGONotFoudException();
        }catch (Exception ex){
            logger.error(ex.getMessage());
            throw new DtvHubApiBadGatewayException(ex);
        }
    }

    public void asignarPolizaCodigoDTVGO(String codigoDGO, String numeroPoliza) {

        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl((HOST_API_HUB_DTV + PUT_LOCALIZACION_ZONA_RIESGO_BY_ID).replace("{codigo}", codigoDGO));
            AsignacionRequest asignacionRequest = new AsignacionRequest(numeroPoliza);

            ResponseEntity<byte[]> response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, new HttpEntity<>(asignacionRequest), byte[].class, "1");

            if (!response.getStatusCode().equals(HttpStatus.NO_CONTENT))
                throw new DtvHubApiBadGatewayException("Response: " + response.getStatusCode());

        } catch (Exception e) {
            throw new DtvHubApiBadGatewayException(e);
        }

    }

}
