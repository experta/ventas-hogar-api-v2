package ar.com.experta.ventashogarapi.infraestructure.glmclient;

import ar.com.experta.ventashogarapi.core.model.*;
import ar.com.experta.ventashogarapi.core.model.enums.Beneficio;
import ar.com.experta.ventashogarapi.core.model.enums.CantidadCuotas;
import ar.com.experta.ventashogarapi.core.model.enums.TipoMedioPago;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.CoreBusinessException;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.CoreFailException;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.exceptions.JsonParseException;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.mapper.CotizacionMapper;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.mapper.EmisionMapper;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.CotizacionGLM;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.EmisionGlm;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.CotizacionIntegralesDTO;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.emision.EmitirCotizacionDTO;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.CotizacionIntegralesResponse;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.CrearEmisionResponse;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.GlmResponse;
import ar.com.experta.ventashogarapi.infraestructure.glmclient.model.response.PorcentajesEstabilizacionResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Component
public class GlmHogarClient {
    private static final Logger logger = LoggerFactory.getLogger(GlmHogarClient.class);

    @Value("${urlglm}")
    private String hostGLM;

    private static final String URI_COTIZAR = "/ar.com.glmsa.seguros.comercial.apiglm.emision.integrales.awscotizarintegrales";
    private static final String URI_EMITIR  = "/ar.com.glmsa.seguros.comercial.apiglm.emision.awsemitircotizacion";

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;
    @Autowired
    PorcentajesClient porcentajesClient;
    private CotizacionMapper cotizacionMapper = new CotizacionMapper();
    private EmisionMapper emisionMapper = new EmisionMapper();
    private static ObjectMapper objectMapper = new ObjectMapper();

    @Cacheable (value="cacheEnlatados",
            key="{#plan.idPlan, #vivienda.direccion.localizacion.zonaHogar, #vivienda.tipoVivienda, #cantidadCuotas, #tipoMedioPago}",
            condition = "#plan.tipoPlan.enlatado"
    )
    public CotizacionGLM cotizar (String idVendedor, Plan plan, List<Cobertura> coberturas, Vivienda vivienda, List<Especifico> especificos, Cliente cliente, Pago pago, LocalDate fechaInicioCobertura, TipoMedioPago tipoMedioPago, CantidadCuotas cantidadCuotas, ModalidadComision modalidadComision, Boolean clausulaEstabilizacion) throws CoreBusinessException, CoreFailException {
        return this.cotizarGLM(idVendedor, plan, coberturas, vivienda, especificos, cliente, pago, fechaInicioCobertura, tipoMedioPago, cantidadCuotas, modalidadComision, clausulaEstabilizacion, null, null, Boolean.TRUE);
    }

    private CotizacionGLM cotizarGLM(String idVendedor, Plan plan, List<Cobertura> coberturas, Vivienda vivienda, List<Especifico> especificos, Cliente cliente, Pago pago, LocalDate fechaInicioCobertura,TipoMedioPago tipoMedioPago, CantidadCuotas cantidadCuotas, ModalidadComision modalidadComision, Boolean clausulaEstabilizacion, Beneficio beneficio, String numeroReferencia, Boolean especificosDefault) throws CoreBusinessException, CoreFailException {
        String porcentajeEstabilizacion = this.getPorcentajesEstabilizacion(plan,modalidadComision,beneficio).get(0).getCodigo();
        CotizacionIntegralesDTO cotizarRequestGLM = cotizacionMapper.toCotizacionIntegralesDTO(idVendedor, plan, coberturas, vivienda, especificos, cliente, pago, fechaInicioCobertura, tipoMedioPago, cantidadCuotas, modalidadComision, clausulaEstabilizacion, porcentajeEstabilizacion, beneficio, numeroReferencia, especificosDefault);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(hostGLM + URI_COTIZAR );
        GlmResponse glmResponse = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                new HttpEntity<>(cotizarRequestGLM),
                GlmResponse.class).getBody();
        logCotizacion(cotizarRequestGLM, glmResponse);
        if (glmResponse.getStatus().equals("200")) {
            try {
                return cotizacionMapper.toCotizacionGlm(objectMapper.readValue(glmResponse.getJsonResult(), CotizacionIntegralesResponse.class), plan.getTipoPlan(), cotizarRequestGLM.getRiesgoInt().getItem().getProductosCotizar().get(0).getProductoCod());
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                throw new JsonParseException();
            }
        }

        throw new CoreFailException(Arrays.asList(glmResponse.getDetail()));
    }

    private void logCotizacion(CotizacionIntegralesDTO cotizarRequestGLM, GlmResponse glmResponse){
        if (!glmResponse.getStatus().equals("200") || glmResponse.getJsonResult().contains("Errores")){
            if (glmResponse.getJsonResult().contains("El productor no está habilitado para el plan comercial"))
                logger.error("Cotizacion Glm Response (400): " + glmResponse.getJsonResult() + " Vendedor: " + cotizarRequestGLM.getProductorCodigo()+ " Producto: "+ cotizarRequestGLM.getRiesgoInt().getItem().getProductosCotizar().get(0).getProductoCod());
            else
                logger.error("Cotizacion Glm Response (400): " + glmResponse.getJsonResult());
        }else{
            logger.info("Cotizacion Glm Response (200): " + glmResponse.getJsonResult());
        }

    }

    public EmisionGlm emitir (String idVendedor, Plan plan, List<Cobertura> coberturas, Vivienda vivienda, List<Especifico> especificos, Cliente cliente, Pago pago, LocalDate fechaInicioCobertura, TipoMedioPago tipoMedioPago, CantidadCuotas cantidadCuotas, ModalidadComision modalidadComision, Boolean clausulaEstabilizacion, Beneficio beneficio, String numeroReferencia) throws CoreBusinessException, CoreFailException {
        CotizacionGLM cotizacionGLM = this.cotizarGLM(idVendedor, plan, coberturas, vivienda, especificos, cliente, pago, fechaInicioCobertura, tipoMedioPago, cantidadCuotas, modalidadComision, clausulaEstabilizacion, beneficio, numeroReferencia, Boolean.FALSE);

        EmitirCotizacionDTO emisionRequestGLM = EmisionMapper.toEmitirCotizacionDTO(cotizacionGLM.getNumeroSolicitud(),
                cotizacionGLM.getCodigoProducto(),
                vivienda,
                cliente,
                pago);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(hostGLM + URI_EMITIR );
        GlmResponse glmResponse = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                new HttpEntity<>(emisionRequestGLM),
                GlmResponse.class).getBody();

        logEmision(emisionRequestGLM, glmResponse);
        if (glmResponse.getStatus().equals("200")){
            try {
                return emisionMapper.mapResponseFromGLM(objectMapper.readValue(glmResponse.getJsonResult(), CrearEmisionResponse.class));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                throw new JsonParseException();
            }
        }
        throw new CoreFailException(Arrays.asList(glmResponse.getDetail()));

    }

    private void logEmision(EmitirCotizacionDTO emisionRequestGLM, GlmResponse glmResponse){
        if (!glmResponse.getStatus().equals("200") || !glmResponse.getJsonResult().contains("\"Errores\":[]")){
            if (glmResponse.getJsonResult().contains("\"Codigo\":801") || glmResponse.getJsonResult().contains("\"Codigo\":803")){
                logger.error("Emision Glm Response: (400)" + glmResponse.getJsonResult() + " Documento: " +  emisionRequestGLM.getTomador().getDocumentoNumero());
            }else{
                logger.error("Emision Glm Response: (400)" + glmResponse.getJsonResult());
            }
        }else{
            logger.info("Emision Glm Response (200): " + glmResponse.getJsonResult());
        }

    }

    public List<PorcentajesEstabilizacionResponse> getPorcentajesEstabilizacion(Plan plan, ModalidadComision modalidadComision, Beneficio beneficio) throws CoreFailException, CoreBusinessException {
        return porcentajesClient.getPorcentajesEstabilizacion(plan, modalidadComision, beneficio);
    }


}
