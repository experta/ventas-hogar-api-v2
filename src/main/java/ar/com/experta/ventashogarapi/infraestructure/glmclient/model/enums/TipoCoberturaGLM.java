package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.enums;

public enum TipoCoberturaGLM {

    INCENDIO_EDIFICIO("200", Boolean.FALSE, Boolean.TRUE, null),
    INCENDIO_EDIFICIO_PRORRATA("199", Boolean.FALSE, Boolean.FALSE, null),
    INCENDIO_EDIFICIO_ABSOLUTO("200", Boolean.FALSE, Boolean.FALSE, null),
    INCENDIO_CONTENIDO("201", Boolean.FALSE, Boolean.TRUE, null),
    RIESGOS_NATURALES("202", Boolean.FALSE, Boolean.FALSE, null),
    GRANIZO_EDIFICIO("206", Boolean.FALSE, Boolean.FALSE, null),
    RESPONSABILIDAD_CIVIL_LINDEROS("212", Boolean.FALSE, Boolean.FALSE, null),
    ALIMENTOS_FREEZER("213", Boolean.FALSE, Boolean.FALSE, null),
    RETIRO_ESCOMBROS("215", Boolean.FALSE, Boolean.FALSE, null),
    RETIRO_ESCOMBROS_CONTENIDO("216", Boolean.FALSE, Boolean.FALSE, null),
    GASTOS_ALOJAMIENTO("218", Boolean.FALSE, Boolean.FALSE, null),
    DANIOS_ESTETICOS("221", Boolean.FALSE, Boolean.FALSE, null),
    ROBO("300", Boolean.FALSE, Boolean.FALSE, null),
    ROBO_ELEBAR("300", Boolean.FALSE, Boolean.FALSE, null),
    VIVIENDA_VERANO("321", Boolean.FALSE, Boolean.FALSE, null),
    RIESGO_ELECTRO("501", Boolean.FALSE, Boolean.FALSE, null),
    RIESGO_ELECTRO_ELEBAR("501", Boolean.FALSE, Boolean.FALSE, null),
    RIESGO_ELECTRO_ELEBAR_2("501", Boolean.FALSE, Boolean.FALSE, null),
    BIENES_ESPECIFICOS("502", Boolean.FALSE, Boolean.FALSE, null),
    TR_ELECTRO_NOTEBOOK("528", Boolean.FALSE, Boolean.FALSE, null),
    TR_ELECTRO_NOTEBOOK_ELEBAR("528", Boolean.FALSE, Boolean.FALSE, null),
    ELECTRO_ESPECIFICOS("598", Boolean.FALSE, Boolean.FALSE, null),
    TR_ELECTRO_DETALLE("599", Boolean.FALSE, Boolean.FALSE, null),
    CRISTALES("600", Boolean.FALSE, Boolean.FALSE, null),
    CRISTALES_ELEBAR("600", Boolean.FALSE, Boolean.FALSE, null),
    DANIOS_AGUA("700", Boolean.FALSE, Boolean.FALSE, null),
    DANIOS_AGUA_EDIFICIO("704", Boolean.FALSE, Boolean.FALSE, null),
    RESPONSABILIDAD_CIVIL_PRIVADOS("800", Boolean.FALSE, Boolean.TRUE, null),
    RESPONSABILIDAD_CIVIL_ANIMALES("803", Boolean.FALSE, Boolean.FALSE, null),
    PALOS_GOLF("900", Boolean.FALSE, Boolean.FALSE, null),
    ACCIDENTES_PERSONALES("1000", Boolean.FALSE, Boolean.FALSE, null),
    ACCIDENTES_PERSONALES_PCP("1100", Boolean.FALSE, Boolean.FALSE, null),
    GASTOS_MEDICOS("1102", Boolean.FALSE, Boolean.FALSE, null),
    ROBO_PORTATILES("505", Boolean.FALSE, Boolean.FALSE, null),
    ROBO_BICICLETA("506", Boolean.FALSE, Boolean.FALSE, null),
    AD_HVCT_IE("001", Boolean.TRUE, Boolean.TRUE, INCENDIO_EDIFICIO),
    AD_HVCT_IE_PRORRATA("001", Boolean.TRUE, Boolean.TRUE, INCENDIO_EDIFICIO),
    AD_HVCT_IE_ABSOLUTO("001", Boolean.TRUE, Boolean.TRUE, INCENDIO_EDIFICIO),
    AD_HVCT_IC("001", Boolean.TRUE, Boolean.TRUE, INCENDIO_CONTENIDO),
    AD_HVCT_IC_PRORRATA("001", Boolean.TRUE, Boolean.TRUE, INCENDIO_CONTENIDO),
    AD_HVCT_IC_ABSOLUTO("001", Boolean.TRUE, Boolean.TRUE, INCENDIO_CONTENIDO),
    AD_GRANIZO_EDIFICIO("002", Boolean.TRUE, Boolean.TRUE, INCENDIO_EDIFICIO),
    AD_GRANIZO_CONTENIDO("002", Boolean.TRUE, Boolean.TRUE, INCENDIO_EDIFICIO),
    AD_RESPONSABILIDAD_CIVIL_ANIMALES("003", Boolean.TRUE, Boolean.TRUE, RESPONSABILIDAD_CIVIL_PRIVADOS),
    HONORARIOS_PROFESIONALES("219", Boolean.FALSE, Boolean.FALSE, null),
    DOCUMENTOS_PERSONALES("316", Boolean.FALSE, Boolean.FALSE, null),
    BIENES_BAULERAS("304", Boolean.FALSE, Boolean.FALSE, null)
    ;

    private String codigoGlm;
    private Boolean esAdicional;
    private Boolean tieneAdicional;
    private TipoCoberturaGLM adicional;

    TipoCoberturaGLM(String codigoGlm, Boolean esAdicional, Boolean tieneAdicional, TipoCoberturaGLM adicional) {
        this.codigoGlm = codigoGlm;
        this.adicional = adicional;
        this.tieneAdicional = tieneAdicional;
        this.esAdicional = esAdicional;
    }

    public String codigoGlm() {
        return codigoGlm;
    }

    public Boolean esAdicional() {
        return esAdicional;
    }

    public Boolean tieneAdicional() {
        return tieneAdicional;
    }

    public TipoCoberturaGLM adicional() {
        return adicional;
    }


}
