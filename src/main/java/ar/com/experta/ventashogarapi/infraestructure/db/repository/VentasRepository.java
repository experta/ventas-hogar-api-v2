package ar.com.experta.ventashogarapi.infraestructure.db.repository;

import ar.com.experta.ventashogarapi.core.model.enums.EstadoCotizacion;
import ar.com.experta.ventashogarapi.infraestructure.db.model.VentaDb;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Repository
@Transactional
public interface VentasRepository extends CrudRepository<VentaDb, Long> {

    @Query("from VentaDb v join v.cliente c where v.vendedorId = :vendedorId and v.fechaUltimaModificacion between :fechaDesde and :fechaHasta and (c.nombre IS NOT NULL OR c.apellido IS NOT NULL OR c.razonSocial IS NOT NULL) ")
    List<VentaDb> findByVendedorIdAndFechaUltimaModificacionBetween(@Param("vendedorId") String vendedorId,
                                                                    @Param("fechaDesde") LocalDateTime fechaDesde,
                                                                    @Param("fechaHasta") LocalDateTime fechaHasta);

    @Modifying
    @Query("update VentaDb v set v.emitiendo = 1, v.fechaUltimaModificacion = SYSDATE where v.id = :id")
    void updateEmitiendo(@Param(value = "id") Long id);

    @Modifying
    @Query("update VentaDb v set v.numeroPoliza = :numeroPoliza, v.fechaUltimaModificacion = SYSDATE, v.emitiendo = 0 where v.id = :id")
    void updateVentaEmitida(@Param(value = "id") Long id,
                            @Param(value = "numeroPoliza") String numeroPoliza);

    @Modifying
    @Query("update CotizacionDb c set c.estado = :estado where c.id = :id")
    void updateCotizacionEmitida(@Param(value = "id") long id,
                                 @Param(value = "estado") EstadoCotizacion estado);

    @Query( value= " SELECT v.emitiendo FROM ventas v WHERE v.id = :id ", nativeQuery = true)
    BigDecimal getEmitiendo(String id);
}
