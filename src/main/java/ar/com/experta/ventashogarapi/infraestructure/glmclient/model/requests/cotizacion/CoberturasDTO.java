package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoberturasDTO {

    @JsonProperty("Codigo")
    private String codigo;
    @JsonProperty("Capital")
    private String capital;
    @JsonProperty("Adicionales")
    private List<AdicionalesDTO> adicionales;
    @JsonProperty("Especificos")
    private List<EspecificosDTO> especificos;


    public CoberturasDTO() {
    }

    public CoberturasDTO(String codigo, String capital, List<AdicionalesDTO> adicionales, List<EspecificosDTO> especificos) {
        this.codigo = codigo;
        this.capital = capital;
        this.adicionales = adicionales;
        this.especificos = especificos;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public List<AdicionalesDTO> getAdicionales() {
        return adicionales;
    }

    public void setAdicionales(List<AdicionalesDTO> adicionales) {
        this.adicionales = adicionales;
    }

    public List<EspecificosDTO> getEspecificos() {
        return especificos;
    }

    public void setEspecificos(List<EspecificosDTO> especificos) {
        this.especificos = especificos;
    }
}
