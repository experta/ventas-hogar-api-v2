package ar.com.experta.ventashogarapi.infraestructure.impresoracotizacionclient.request;

import ar.com.experta.ventashogarapi.core.model.NegocioOptionClausulas;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CotizacionRequest {
    private String numeroCotizacion;
    private String fechaCotizacion;
    private String cliente;
    private String productor;
    private String tipoVivienda;
    private String cp;
    private String localidad;
    private String provincia;
    private String tipoPlan;
    private String beneficio;
    private String cuotas;
    private String medioPago;
    private Integer descuento;
    private Integer clausulaEstabilizacion;
    private String textoClausulaEstabilizacion;
    private List<PlanRequest> planes;
    private List<NegocioOptionClausulas> medidasSeguridad;
    private List<NegocioOptionClausulas> servicios;

    public CotizacionRequest(String numeroCotizacion, String fechaCotizacion, String cliente, String tipoPlan) {
        this.numeroCotizacion = numeroCotizacion;
        this.fechaCotizacion = fechaCotizacion;
        this.cliente = cliente;
        this.tipoPlan = tipoPlan;
    }

    public CotizacionRequest(String numeroCotizacion, String fechaCotizacion, String cliente, String productor, String tipoVivienda, String cp, String localidad, String provincia, String tipoPlan, String cuotas, String medioPago, String beneficio, Integer descuento, Integer clausulaEstabilizacion, String textoClausulaEstabilizacion, List<PlanRequest> planes, List<NegocioOptionClausulas> medidasSeguridad, List<NegocioOptionClausulas> servicios) {
        this.numeroCotizacion = numeroCotizacion;
        this.fechaCotizacion = fechaCotizacion;
        this.cliente = cliente;
        this.productor = productor;
        this.tipoVivienda = tipoVivienda;
        this.cp = cp;
        this.localidad = localidad;
        this.provincia = provincia;
        this.tipoPlan = tipoPlan;
        this.beneficio = beneficio;
        this.descuento = descuento;
        this.planes = planes;
        this.medidasSeguridad = medidasSeguridad;
        this.medioPago = medioPago;
        this.cuotas = cuotas;
        this.servicios = servicios;
        this.clausulaEstabilizacion = clausulaEstabilizacion;
        this.textoClausulaEstabilizacion = textoClausulaEstabilizacion;
    }

    public String getNumeroCotizacion() {
        return numeroCotizacion;
    }

    public void setNumeroCotizacion(String numeroCotizacion) {
        this.numeroCotizacion = numeroCotizacion;
    }

    public String getFechaCotizacion() {
        return fechaCotizacion;
    }

    public void setFechaCotizacion(String fechaCotizacion) {
        this.fechaCotizacion = fechaCotizacion;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getProductor() {
        return productor;
    }

    public void setProductor(String productor) {
        this.productor = productor;
    }

    public String getTipoVivienda() {
        return tipoVivienda;
    }

    public void setTipoVivienda(String tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getTipoPlan() {
        return tipoPlan;
    }

    public void setTipoPlan(String tipoPlan) {
        this.tipoPlan = tipoPlan;
    }

    public String getBeneficio() {
        return beneficio;
    }

    public void setBeneficio(String beneficio) {
        this.beneficio = beneficio;
    }

    public List<PlanRequest> getPlanes() {
        return planes;
    }

    public void setPlanes(List<PlanRequest> planes) {
        this.planes = planes;
    }

    public Integer getDescuento() {
        return descuento;
    }

    public void setDescuento(Integer descuento) {
        this.descuento = descuento;
    }

    public List<NegocioOptionClausulas> getMedidasSeguridad() {
        return medidasSeguridad;
    }

    public void setMedidasSeguridad(List<NegocioOptionClausulas> medidasSeguridad) {
        this.medidasSeguridad = medidasSeguridad;
    }

    public String getCuotas() {
        return cuotas;
    }

    public void setCuotas(String cuotas) {
        this.cuotas = cuotas;
    }

    public String getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(String medioPago) {
        this.medioPago = medioPago;
    }

    public List<NegocioOptionClausulas> getServicios() {
        return servicios;
    }

    public void setServicios(List<NegocioOptionClausulas> servicios) {
        this.servicios = servicios;
    }

    public Integer getClausulaEstabilizacion() {
        return clausulaEstabilizacion;
    }

    public void setClausulaEstabilizacion(Integer clausulaEstabilizacion) {
        this.clausulaEstabilizacion = clausulaEstabilizacion;
    }

    public String getTextoClausulaEstabilizacion() {
        return textoClausulaEstabilizacion;
    }

    public void setTextoClausulaEstabilizacion(String textoClausulaEstabilizacion) {
        this.textoClausulaEstabilizacion = textoClausulaEstabilizacion;
    }
}
