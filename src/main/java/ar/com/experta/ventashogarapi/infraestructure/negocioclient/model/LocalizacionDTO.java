package ar.com.experta.ventashogarapi.infraestructure.negocioclient.model;

public class LocalizacionDTO {
    private String id;
    private String codigoPostal;
    private String localidad;
    private String provincia;

    public LocalizacionDTO() {
    }

    public LocalizacionDTO(String id, String codigoPostal, String localidad, String provincia) {
        this.id = id;
        this.codigoPostal = codigoPostal;
        this.localidad = localidad;
        this.provincia = provincia;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }
}
