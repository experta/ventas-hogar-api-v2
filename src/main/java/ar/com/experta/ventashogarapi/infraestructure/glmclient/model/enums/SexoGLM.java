package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.enums;

public enum SexoGLM {
    MASCULINO("1"),
    FEMENINO("2");

    private String codigoGlm;

    private SexoGLM(final String codigoGlm) {
        this.codigoGlm = codigoGlm;
    }

    public String codigoGlm() {
        return codigoGlm;
    }
}
