package ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductoCotizarDTO {

    @JsonProperty("ProductoCodigo")
    private String productoCod;
    @JsonProperty("Coberturas")
    private List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.CoberturasDTO> coberturas;

    public ProductoCotizarDTO() {
    }

    public ProductoCotizarDTO(String productoCod, List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.CoberturasDTO> coberturas) {
        this.productoCod = productoCod;
        this.coberturas = coberturas;
    }

    public String getProductoCod() {
        return productoCod;
    }

    public void setProductoCod(String productoCod) {
        this.productoCod = productoCod;
    }

    public List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.CoberturasDTO> getCoberturas() {
        return coberturas;
    }

    public void setCoberturas(List<ar.com.experta.ventashogarapi.infraestructure.glmclient.model.requests.cotizacion.CoberturasDTO> coberturas) {
        this.coberturas = coberturas;
    }
}
