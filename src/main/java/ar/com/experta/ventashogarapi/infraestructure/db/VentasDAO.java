package ar.com.experta.ventashogarapi.infraestructure.db;

import ar.com.experta.ventashogarapi.commons.exceptions.notfound.VentaNotFoundException;
import ar.com.experta.ventashogarapi.core.model.Venta;
import ar.com.experta.ventashogarapi.core.model.enums.EstadoCotizacion;
import ar.com.experta.ventashogarapi.core.model.enums.EstadoVenta;
import ar.com.experta.ventashogarapi.infraestructure.db.model.VentaDb;
import ar.com.experta.ventashogarapi.infraestructure.db.repository.VentasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class VentasDAO  {

    @Autowired
    private VentasRepository ventasRepository;

    public VentasDAO() {
    }

    public Venta save(Venta venta) {
        VentaDb ventaDb = venta.getVentaDb() != null ? venta.getVentaDb().updateVentaDb(venta) : new VentaDb(venta);
        ventaDb.setFechaUltimaModificacion(LocalDateTime.now());
        ventasRepository.save(ventaDb);
        venta.setFechaUltimaModificacion(LocalDateTime.now());
        venta.setId(ventaDb.getId());
        return venta;
    }

    public Venta get(String idVenta) {
        long id;
        try{
            id = Long.parseLong(idVenta);
        }catch (Exception e){
            throw new VentaNotFoundException(idVenta);
        }
        Optional<VentaDb> ventaDb =  ventasRepository.findById(id);
        if (ventaDb.isPresent())
            return ventaDb.get().mapToVenta();
        throw new VentaNotFoundException(idVenta);
    }

    public List<Venta> search(String idVendedor, LocalDate fechaDesde, LocalDate fechaHasta, Optional<String> numero, Optional<String> nombre, Optional<String> email, Optional<EstadoVenta> estadoVenta) {
        if (numero.isPresent() && !numero.get().isEmpty())
            return Arrays.asList(this.get(numero.get()));

        List<VentaDb> ventasDb =  ventasRepository.findByVendedorIdAndFechaUltimaModificacionBetween(idVendedor, fechaDesde.atStartOfDay(), fechaHasta.atTime(23,59));
        List<Venta> ventas = new ArrayList<>();
        if (ventasDb != null && !ventasDb.isEmpty()) {
            if ((nombre.isPresent() && !nombre.get().isEmpty()) || (email.isPresent() && !email.get().isEmpty())) {
                ventas = ventasDb.stream()
                        .filter(ventaDb -> (nombre.isEmpty() ||
                                            nombre.get().isEmpty() ||
                                            (ventaDb.getCliente().getNombre() != null
                                                    && ventaDb.getCliente().getNombre().equalsIgnoreCase(nombre.get().toUpperCase())) ||
                                            (ventaDb.getCliente().getApellido() != null
                                                    && ventaDb.getCliente().getApellido().equalsIgnoreCase(nombre.get())) ||
                                            (ventaDb.getCliente().getRazonSocial() != null
                                                    && ventaDb.getCliente().getRazonSocial().equalsIgnoreCase(nombre.get().toUpperCase())) ||
                                            (ventaDb.getCliente().getApellidoNombre() != null
                                                    && ventaDb.getCliente().getApellidoNombre().equalsIgnoreCase(nombre.get().toUpperCase())) ||
                                            (ventaDb.getCliente().getNombreCompleto() != null
                                                    && ventaDb.getCliente().getNombreCompleto().equalsIgnoreCase(nombre.get().toUpperCase()))
                                            )&&
                                            (email.isEmpty() ||
                                                    (ventaDb.getCliente().getEmail() != null
                                                            && ventaDb.getCliente().getEmail().equalsIgnoreCase(email.get().toUpperCase()))
                                            )
                                )
                        .map(VentaDb::mapToVenta).collect(Collectors.toList());
            } else {
                ventas = ventasDb.stream().map(VentaDb::mapToVentaEncabezado).collect(Collectors.toList());
            }
            if (estadoVenta.isPresent())
                return ventas.stream()
                        .filter(venta -> venta.getEstadoVenta() != EstadoVenta.BORRADOR &&
                                venta.getEstadoVenta() != EstadoVenta.BORRADOR_A_EMITIR &&
                                venta.getEstadoVenta().equals(estadoVenta.get()))
                        .collect(Collectors.toList());
        }
       return ventas;
    }

    public void saveEmitiendo(String idVenta) {
        Long id;
        try{
            id = Long.parseLong(idVenta);
        }catch (Exception e){
            throw new VentaNotFoundException(idVenta);
        }
        ventasRepository.updateEmitiendo(id);
    }

    public Boolean getEmitiendo(String id) {
        BigDecimal number = ventasRepository.getEmitiendo(id);
        return number != null && number.equals(new BigDecimal(1));
    }

    public void saveVentaEmitida(String idVenta, String numeroPoliza) {
        Long id;
        try{
            id = Long.parseLong(idVenta);
        }catch (Exception e){
            throw new VentaNotFoundException(idVenta);
        }
        ventasRepository.updateVentaEmitida(id, numeroPoliza);
    }

    public void saveCotizacionEmitida(String idCotizacion, EstadoCotizacion estadoCotizacion) {
        Long id;
        try{
            id = Long.parseLong(idCotizacion);
        }catch (Exception e){
            throw new VentaNotFoundException(idCotizacion);
        }
        ventasRepository.updateCotizacionEmitida(id, estadoCotizacion);
    }


}
