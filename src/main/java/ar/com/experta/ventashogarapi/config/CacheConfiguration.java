package ar.com.experta.ventashogarapi.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableCaching
public class CacheConfiguration implements CachingConfigurer {
    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager("cacheEnlatados", "cachePlanes", "cachePorcentajesEstabilizacion");
    }

    @CacheEvict(allEntries = true, value = {"cacheEnlatados", "cachePlanes", "cachePorcentajesEstabilizacion"})
    @Scheduled(fixedDelay = 24 * 60 * 60 * 1000 ,  initialDelay = 500)
    public void reportCacheEvict() {}

}
