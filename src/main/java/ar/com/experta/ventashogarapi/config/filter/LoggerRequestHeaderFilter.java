package ar.com.experta.ventashogarapi.config.filter;


import ar.com.experta.web.filter.request.RequestIdHeaderFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

@Component
public class LoggerRequestHeaderFilter extends RequestIdHeaderFilter {

    @Value("${request.header}")
    private String requestHeader;

    private static final Logger logger = LoggerFactory.getLogger(LoggerRequestHeaderFilter.class);

    @Override
    public void beforeRequest(ServletRequest request, ServletResponse response) {
    }

    @Override
    public void afterRequest(ServletRequest request, ServletResponse response) {
        MDC.remove(this.requestHeader);
//        logger.info("Response: {} {}",((ResponseFacade) response).getStatus(), HttpStatus.valueOf(((ResponseFacade) response).getStatus()).getReasonPhrase());
    }

    @Override
    public void afterGettingRequestId(String requestId) {
        MDC.put(this.requestHeader, requestId);
    }
}
