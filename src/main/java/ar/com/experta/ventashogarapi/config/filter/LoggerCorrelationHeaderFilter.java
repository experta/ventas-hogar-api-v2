package ar.com.experta.ventashogarapi.config.filter;

import ar.com.experta.web.filter.correlation.CorrelationIdHeaderFilter;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

@Component
public class LoggerCorrelationHeaderFilter extends CorrelationIdHeaderFilter {

    @Value("${correlation.header}")
    private String correlationHeader;

    @Override
    public void beforeRequest(ServletRequest servletRequest, ServletResponse servletResponse) {

    }

    @Override
    public void afterRequest(ServletRequest servletRequest, ServletResponse servletResponse) {
        MDC.remove(this.correlationHeader);
    }

    @Override
    public void afterGettingCorrelationId(String correlationId) {

        MDC.put(this.correlationHeader, correlationId);
    }
}
