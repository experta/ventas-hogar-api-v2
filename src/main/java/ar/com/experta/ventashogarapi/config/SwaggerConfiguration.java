package ar.com.experta.ventashogarapi.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;


@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Value("${swagger.host}")
    private String host;
    @Value("${swagger.environment}")
    private String environment;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .host(host)
                .select()
                .apis(RequestHandlerSelectors.basePackage("ar.com.experta.ventashogarapi"))
                .build()
                .apiInfo(getApiInformation());
    }

    private ApiInfo getApiInformation() {
        return new ApiInfo("API Ventas Hogar"  + ( !environment.equals("Produccion") ? " (" + environment + ")" : ""),
                "Api de venta de pólizas de Seguros de Combinado Familiar (Hogar) de Experta Seguros",
                "2.0",
                null,
                new Contact("Grupo Werthein - Equipo de Canales Digitales", null, "desa.teamCD@experta.com.ar"),
                null,
                null,
                Collections.emptyList()
        );
    }
}
