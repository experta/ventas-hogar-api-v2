package ar.com.experta.ventashogarapi.config.filter.httpclient;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class RestTemplateCorrelationInterceptor implements ClientHttpRequestInterceptor {
    @Value("${correlation.header}")
    private String correlationHeader;

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {

        request.getHeaders().add(this.correlationHeader, MDC.get(this.correlationHeader));

        ClientHttpResponse response = execution.execute(request, body);

        return response;
    }
}
