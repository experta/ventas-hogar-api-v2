package ar.com.experta.ventashogarapi.config;

import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
public class FailoverDatasourceConfig {

	@Value("${spring.datasource.url}")
	private String url;
	@Value("${spring.datasource.username}")
	private String username;
	@Value("${spring.datasource.password}")
	private String password;
	@Value("${oracle.ucp.initialPoolSize}")
	private int initialPoolSize;
	@Value("${oracle.ucp.minPoolSize}")
	private int minPoolSize;
	@Value("${oracle.ucp.maxPoolSize}")
	private int maxPoolSize;
	@Value("${spring.datasource.driver.class}")
	private String driverClassName;
	@Value("${oracle.ucp.connectionPoolName}")
	private String connectionPoolName;
	@Value("${oracle.ucp.fastConnectionFailoverEnabled}")
	private Boolean fastConnectionFailoverEnabled;

	@Bean(name = "OracleUniversalConnectionPool")
	@Primary
	public DataSource getDataSource() throws SQLException {
		PoolDataSource dataSource = PoolDataSourceFactory.getPoolDataSource();
		dataSource.setUser(username);
		dataSource.setPassword(password);
		dataSource.setConnectionFactoryClassName(driverClassName);
		dataSource.setURL(url);
		dataSource.setFastConnectionFailoverEnabled(fastConnectionFailoverEnabled);
		dataSource.setInitialPoolSize(initialPoolSize);
		dataSource.setMinPoolSize(minPoolSize);
		dataSource.setMaxPoolSize(maxPoolSize);
		dataSource.setConnectionPoolName(connectionPoolName);
		return dataSource;
	}
}
