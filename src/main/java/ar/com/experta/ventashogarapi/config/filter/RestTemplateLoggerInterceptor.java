package ar.com.experta.ventashogarapi.config.filter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class RestTemplateLoggerInterceptor
        implements ClientHttpRequestInterceptor {

    private static final Logger logger = LoggerFactory.getLogger(RestTemplateLoggerInterceptor.class);

    @Value("${authorization.key}")
    private String AUTHORIZATION;

    @Override
    public ClientHttpResponse intercept(
            HttpRequest request,
            byte[] body,
            ClientHttpRequestExecution execution) throws IOException {

        if (!request.getHeaders().containsKey("Authorization"))
            request.getHeaders().add("Authorization", "Bearer " + AUTHORIZATION);

        String stringifiedBody = new String(body, "UTF-8").replaceAll("(&password=)[^&]*(&)", "$1*****$2");;
        if (StringUtils.isNotEmpty(stringifiedBody)) {
            this.logger.info("Request: {} {} Body: {}", request.getMethod().toString(),request.getURI(), stringifiedBody);
        }else{
            this.logger.info("Request: {} {}", request.getMethod().toString(),request.getURI());
        }

        ClientHttpResponse response = execution.execute(request, body);

        //logueamos la respuesta si el response tiene agun error
        if (!response.getStatusCode().is2xxSuccessful()) {
            this.logger.error("Response: {} ,StatusText : {}", response.getStatusCode(), response.getStatusText());
        }else{
            this.logger.info("Response: {} ", response.getStatusCode());
        }

        return response;
    }
}