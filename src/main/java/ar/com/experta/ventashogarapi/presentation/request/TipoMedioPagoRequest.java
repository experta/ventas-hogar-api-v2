package ar.com.experta.ventashogarapi.presentation.request;

import ar.com.experta.ventashogarapi.core.model.enums.TipoMedioPago;

import javax.validation.constraints.NotNull;

public class TipoMedioPagoRequest {

    @NotNull
    private TipoMedioPago tipoMedioPago;

    public TipoMedioPagoRequest() {
    }

    public TipoMedioPagoRequest(TipoMedioPago tipoMedioPago) {
        this.tipoMedioPago = tipoMedioPago;
    }

    public TipoMedioPago getTipoMedioPago() {
        return tipoMedioPago;
    }

    public void setTipoMedioPago(TipoMedioPago tipoMedioPago) {
        this.tipoMedioPago = tipoMedioPago;
    }
}
