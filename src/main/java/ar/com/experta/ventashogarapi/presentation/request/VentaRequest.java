package ar.com.experta.ventashogarapi.presentation.request;

import ar.com.experta.ventashogarapi.core.model.*;
import ar.com.experta.ventashogarapi.core.model.enums.*;
import ar.com.experta.ventashogarapi.presentation.validations.Numeric;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class VentaRequest {

    public static class VendedorVentaRequest {
        @NotBlank(message = "El id del vendedor no puede estar vacío")
        @Numeric(message = "El id del vendedor no es un valor numérico válido")
        private String id;

        public VendedorVentaRequest() {}

        public VendedorVentaRequest(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class LocalizacionRequest{
        @NotNull (message = "El id de localización no puede ser nulo")
        private String id;

        public LocalizacionRequest() {
        }

        public LocalizacionRequest(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class DireccionVentaRequest{
        @Valid
        @NotNull @NotNull (message = "La localización no puede ser nula")
        private LocalizacionRequest localizacion;

        public DireccionVentaRequest() {
        }

        public DireccionVentaRequest(LocalizacionRequest localizacion) {
            this.localizacion = localizacion;
        }

        public LocalizacionRequest getLocalizacion() {
            return localizacion;
        }

        public void setLocalizacion(LocalizacionRequest localizacion) {
            this.localizacion = localizacion;
        }
    }

    public static class ViviendaVentaRequest {

        @NotNull (message = "El tipo de vivienda no puede ser nulo")
        private TipoVivienda tipoVivienda;
        @Valid @NotNull (message = "La dirección no puede ser nula")
        private DireccionVentaRequest direccion;

        private Integer metrosCuadrados;

        public ViviendaVentaRequest() {
        }

        public ViviendaVentaRequest(@NotNull TipoVivienda tipoVivienda, @NotNull DireccionVentaRequest direccion, @NotNull Integer metrosCuadrados) {
            this.tipoVivienda = tipoVivienda;
            this.direccion = direccion;
            this.metrosCuadrados = metrosCuadrados;
        }

        public Integer getMetrosCuadrados() {
            return metrosCuadrados;
        }

        public TipoVivienda getTipoVivienda() {
            return tipoVivienda;
        }

        public void setTipoVivienda(TipoVivienda tipoVivienda) {
            this.tipoVivienda = tipoVivienda;
        }

        public DireccionVentaRequest getDireccion() {
            return direccion;
        }

        public void setDireccion(DireccionVentaRequest direccion) {
            this.direccion = direccion;
        }

        public void setMetrosCuadrados(Integer metrosCuadrados) {
            this.metrosCuadrados = metrosCuadrados;
        }
    }

    public static class DocumentoRequest{
        private String numero;
        private TipoDocumento tipo;

        public DocumentoRequest() {
        }

        public TipoDocumento getTipo() {
            return tipo;
        }

        public void setTipo(TipoDocumento tipo) {
            this.tipo = tipo;
        }

        public String getNumero() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero = numero;
        }
    }

    public static class TelefonoClienteRequest {

        @NotBlank (message = "El prefijo del teléfono del cliente no puede estar vacío")
        @Numeric (message = "El prefijo del teléfono del cliente  debe ser numérico")
        private String prefijo;
        @NotBlank (message = "El número del teléfono del cliente no puede estar vacío")
        @Numeric (message = "El número del teléfono del cliente  debe ser numérico")
        private String numero;

        public TelefonoClienteRequest() {
        }

        public String getPrefijo() {
            return prefijo;
        }

        public void setPrefijo(String prefijo) {
            this.prefijo = prefijo;
        }

        public String getNumero() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero = numero;
        }
    }

    public class ClienteVentaRequest {

        private DocumentoRequest documento;
        @Size(min = 2, max = 50, message = "El nombre debe tener entre 10 and 50 caracteres")
        private String nombre;
        @Size(min = 2, max = 50, message = "El apellido debe tener entre 10 and 50 caracteres")
        private String apellido;
        @Email (message = "El email debe tener un formato válido (ejemplo@dominio.ej)")
        @Size(min = 7, max = 50, message = "El email debe tener entre 7 and 50 caracteres")
        private String email;
        @Valid
        private TelefonoClienteRequest telefono;
        private String telefonoTexto;
        @NotNull (message = "El tipo de persona no puede ser nulo")
        TipoPersona tipoPersona;
        @NotNull (message = "El tipo de condición del IVA no puede ser nulo")
        CondicionImpositiva condicionImpositiva;

        public ClienteVentaRequest() {
        }

        public ClienteVentaRequest(DocumentoRequest documento, @Size(min = 10, max = 50, message = "El nombre debe tener entre 10 and 50 characters") String nombre, @Size(min = 10, max = 50, message = "El apellido debe tener entre 10 and 50 characters") String apellido, @Email @Size(min = 10, max = 50, message = "El email debe tener entre 10 and 50 characters") String email, TelefonoClienteRequest telefono, @NotNull TipoPersona tipoPersona, @NotNull CondicionImpositiva condicionImpositiva) {
            this.documento = documento;
            this.nombre = nombre;
            this.apellido = apellido;
            this.email = email;
            this.telefono = telefono;
            this.tipoPersona = tipoPersona;
            this.condicionImpositiva = condicionImpositiva;
        }

        public DocumentoRequest getDocumento() {
            return documento;
        }

        public void setDocumento(DocumentoRequest documento) {
            this.documento = documento;
        }

        public CondicionImpositiva getCondicionImpositiva() {
            return condicionImpositiva;
        }

        public void setCondicionImpositiva(CondicionImpositiva condicionImpositiva) {
            this.condicionImpositiva = condicionImpositiva;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getApellido() {
            return apellido;
        }

        public void setApellido(String apellido) {
            this.apellido = apellido;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public TelefonoClienteRequest getTelefono() {
            return telefono;
        }

        public String getTelefonoTexto() {
            return telefonoTexto;
        }

        public void setTelefono(TelefonoClienteRequest telefono) {
            this.telefono = telefono;
        }

        public TipoPersona getTipoPersona() {
            return tipoPersona;
        }

        public void setTipoPersona(TipoPersona tipoPersona) {
            this.tipoPersona = tipoPersona;
        }
    }

    @Valid
    @NotNull (message = "El vendedor no puede ser nulo")
    private VendedorVentaRequest vendedor;
    @NotNull (message = "El tipo de plan no puede ser nulo")
    private TipoPlan tipoPlan;
    @Valid
    private ViviendaVentaRequest vivienda;
    @Valid
    @NotNull (message = "El cliente no puede ser nulo")
    private ClienteVentaRequest cliente;
    @NotNull (message = "La fecha de inicio de cobertura no puede ser nula")
    private LocalDate fechaInicioCobertura;
    private PagoRequest pagoRequest;
    private TipoMedioPago tipoMedioPago;
    private String cantidadCuotas;
    private ModalidadComision modalidadComision;
    private Boolean clausulaEstabilizacion;
    private Beneficio beneficio;

    public VentaRequest() {
    }

    public VentaRequest(VendedorVentaRequest vendedor, TipoPlan tipoPlan, ViviendaVentaRequest vivienda, ClienteVentaRequest cliente, LocalDate fechaInicioCobertura, PagoRequest pagoRequest, TipoMedioPago tipoMedioPago, String cantidadCuotas, Beneficio beneficio) {
        this.vendedor = vendedor;
        this.tipoPlan = tipoPlan;
        this.vivienda = vivienda;
        this.cliente = cliente;
        this.fechaInicioCobertura = fechaInicioCobertura;
        this.pagoRequest = pagoRequest;
        this.tipoMedioPago = tipoMedioPago;
        this.cantidadCuotas = cantidadCuotas;
        this.beneficio = beneficio;
    }

    public void setTipoPlan(TipoPlan tipoPlan) {
        this.tipoPlan = tipoPlan;
    }

    public void setVendedor(VendedorVentaRequest vendedor) {
        this.vendedor = vendedor;
    }

    public void setVivienda(ViviendaVentaRequest vivienda) {
        this.vivienda = vivienda;
    }

    public void setCliente(ClienteVentaRequest cliente) {
        this.cliente = cliente;
    }

    public void setPagoRequest(PagoRequest pagoRequest) {
        this.pagoRequest = pagoRequest;
    }

    public void setTipoMedioPago(TipoMedioPago tipoMedioPago) {
        this.tipoMedioPago = tipoMedioPago;
    }

    public void setCantidadCuotas(String cantidadCuotas) {
        this.cantidadCuotas = cantidadCuotas;
    }

    public void setFechaInicioCobertura(LocalDate fechaInicioCobertura) {
        this.fechaInicioCobertura = fechaInicioCobertura;
    }

    public VendedorVentaRequest getVendedor(){
        return this.vendedor;
    }

    public Cliente getCliente() {
        return new Cliente(
                cliente.getDocumento() != null ? new Documento(cliente.getDocumento().getTipo(), cliente.getDocumento().getNumero()) : null,
                cliente.getNombre(),
                cliente.getApellido(),
                null,
                cliente.getEmail(),
                cliente.getTelefono() != null ? new Telefono(cliente.getTelefono().getPrefijo(), cliente.getTelefono().getNumero()) : null,
                cliente.getTelefonoTexto(),
                cliente.getTipoPersona(),
                cliente.getCondicionImpositiva());
    }

    public Vivienda getVivienda() {
        return new Vivienda(vivienda.getTipoVivienda(),
                new Direccion(new Localizacion(vivienda.getDireccion().getLocalizacion().getId())),
                vivienda.getMetrosCuadrados());
    }

    public LocalDate getFechaInicioCobertura() {
        return fechaInicioCobertura;
    }

    public TipoPlan getTipoPlan() {
        return tipoPlan;
    }

    public Beneficio getBeneficio() {
        return beneficio;
    }

    public TipoMedioPago getTipoMedioPago(){return tipoMedioPago;}

    public PagoRequest getPagoRequest() {
        return pagoRequest;
    }

    public CantidadCuotas getCantidadCuotas() {
        return cantidadCuotas != null ? CantidadCuotas.fromValue(cantidadCuotas) : null;
    }

    public ModalidadComision getModalidadComision() {
        return modalidadComision;
    }

    public void setModalidadComision(ModalidadComision modalidadComision) {
        this.modalidadComision = modalidadComision;
    }

    public Boolean getClausulaEstabilizacion() {
        return clausulaEstabilizacion;
    }

    public void setClausulaEstabilizacion(Boolean clausulaEstabilizacion) {
        this.clausulaEstabilizacion = clausulaEstabilizacion;
    }

    public void setBeneficio(Beneficio beneficio) {
        this.beneficio = beneficio;
    }


}
