package ar.com.experta.ventashogarapi.presentation.request;

import ar.com.experta.ventashogarapi.core.model.enums.CantidadCuotas;

import javax.validation.constraints.NotNull;

public class CantidadCuotasRequest {

    @NotNull
    private String cantidadCuotas;

    public CantidadCuotasRequest() {
    }

    public CantidadCuotasRequest(String cantidadCuotas) {
        this.cantidadCuotas = cantidadCuotas;
    }

    public CantidadCuotas getCantidadCuotas() {
        return cantidadCuotas != null ? CantidadCuotas.fromValue(cantidadCuotas) : null;
    }

    public void setCantidadCuotas(String cantidadCuotas) {
        this.cantidadCuotas = cantidadCuotas;
    }
}
