package ar.com.experta.ventashogarapi.presentation.request;


import ar.com.experta.ventashogarapi.core.model.enums.TipoCobertura;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.ResolvableDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class CotizacionRequest {

    public static final class CoberturaRequestDeserializer extends StdDeserializer<CoberturaRequest>
            implements ResolvableDeserializer {
        private JsonDeserializer<Object> underlyingDeserializer;

        public CoberturaRequestDeserializer() {
            super(CoberturaRequest.class);
        }

        @Override
        public void resolve(DeserializationContext ctxt) throws JsonMappingException {
            underlyingDeserializer = ctxt
                    .findRootValueDeserializer(ctxt.getTypeFactory().constructType(CoberturaRequest.class));
        }

        @Override
        public CoberturaRequest deserialize(JsonParser p, DeserializationContext ctxt)
                throws IOException, JsonProcessingException {
            JsonStreamContext ourContext = p.getParsingContext();
            JsonStreamContext listContext = ourContext.getParent();
            JsonStreamContext containerContext = listContext.getParent();
            CotizacionRequest container = (CotizacionRequest) containerContext.getCurrentValue();
            CoberturaRequest value = container.new CoberturaRequest();
            // note use of three-argument deserialize method to specify instance to populate
            underlyingDeserializer.deserialize(p, ctxt, value);
            return value;
        }
    }

    public class CoberturaRequest {

        @NotNull (message = "El tipo de cobertura no puede ser nulo")
        private TipoCobertura tipoCobertura;
        private BigDecimal montoAsegurado;

        private TipoCobertura opcion;

        public CoberturaRequest() {
        }

        public CoberturaRequest(TipoCobertura tipoCobertura, BigDecimal montoAsegurado) {
            this.tipoCobertura = tipoCobertura;
            this.montoAsegurado = montoAsegurado;
        }

        public CoberturaRequest(@NotNull TipoCobertura tipoCobertura, @NotNull BigDecimal montoAsegurado, TipoCobertura opcion) {
            this.tipoCobertura = tipoCobertura;
            this.montoAsegurado = montoAsegurado;
            this.opcion = opcion;
        }

        public TipoCobertura getTipoCobertura() {
            return tipoCobertura;
        }

        public void setTipoCobertura(TipoCobertura tipoCobertura) {
            this.tipoCobertura = tipoCobertura;
        }

        public BigDecimal getMontoAsegurado() {
            return montoAsegurado;
        }

        public void setMontoAsegurado(BigDecimal montoAsegurado) {
            this.montoAsegurado = montoAsegurado;
        }

        public TipoCobertura getOpcion() {
            return opcion;
        }

        public void setOpcion(TipoCobertura opcion) {
            this.opcion = opcion;
        }
    }

    @Valid
    @NotNull (message = "El listado de coberturas no puede ser nulo")
    @JsonDeserialize(contentUsing = CoberturaRequestDeserializer.class)
    private List<CoberturaRequest> coberturas;

    public CotizacionRequest() {
    }

    public CotizacionRequest(@NotNull @Valid List<CoberturaRequest> coberturas) {
        this.coberturas = coberturas;
    }

    public List<CoberturaRequest> getCoberturas() {
        return coberturas;
    }

    public void setCoberturas(List<CoberturaRequest> coberturas) {
        this.coberturas = coberturas;
    }
}
