package ar.com.experta.ventashogarapi.presentation.exporter;

import ar.com.experta.ventashogarapi.infraestructure.db.model.VentaDb;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExporterExcelCotizacionesSimples {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<VentaDb> ventaDbList;
    private Map<String,Integer> mapaVendedoresCotizaciones;
    private Map<String,Integer> mapaVendedoresEmisiones;


    public ExporterExcelCotizacionesSimples(List<VentaDb> ventaDbList) {
        workbook = new XSSFWorkbook();
        this.ventaDbList = ventaDbList;
        this.mapaVendedoresCotizaciones = new HashMap<>();
        this.mapaVendedoresEmisiones = new HashMap<>();
    }

    private void writeHeaderLine() {
        try {
            sheet = workbook.createSheet("Reporte de Cotizaciones");
            Row row = sheet.createRow(0);

            CellStyle style = workbook.createCellStyle();
            XSSFFont font = workbook.createFont();
            font.setBold(true);
            font.setFontHeight(12);
            style.setFont(font);

            createCell(row, 0, "PRODUCTOR", style);
            createCell(row, 1, "COTIZACIONES", style);
            createCell(row, 2, "EMISIONES", style);

        }catch(Exception e){
            new Exception(e.getMessage());
        }
    }
    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        final int[] rowCount = {1};

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        CellStyle styleCurrency = workbook.createCellStyle();
        styleCurrency.setDataFormat((short)8);

        for (VentaDb ventaDb : this.ventaDbList) {
            if (!mapaVendedoresCotizaciones.containsKey(ventaDb.getVendedorId())) {
                mapaVendedoresCotizaciones.put(ventaDb.getVendedorId(), 1);
                mapaVendedoresEmisiones.put(ventaDb.getVendedorId(), ventaDb.getNumeroPoliza() != null ? 1 : 0);
            }else {
                mapaVendedoresCotizaciones.put(ventaDb.getVendedorId(), mapaVendedoresCotizaciones.get(ventaDb.getVendedorId()) + 1);
                if (ventaDb.getNumeroPoliza() != null)
                    mapaVendedoresEmisiones.put(ventaDb.getVendedorId(), mapaVendedoresEmisiones.get(ventaDb.getVendedorId()) + 1);
            }

        }

        for (String vendedorId : mapaVendedoresCotizaciones.keySet()){
            Row row = sheet.createRow(rowCount[0]++);
            row.createCell(0).setCellValue(vendedorId);
            row.createCell(1).setCellValue(mapaVendedoresCotizaciones.get(vendedorId));
            row.createCell(2).setCellValue(mapaVendedoresEmisiones.get(vendedorId));
        }
        for (int j = 0; j < 3; j++) {
            sheet.autoSizeColumn(j);
        }
    }
    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);

        workbook.close();
        outputStream.close();
    }

    public Map<String, Integer> getMapaVendedoresCotizaciones() {
        return mapaVendedoresCotizaciones;
    }

    public void setMapaVendedoresCotizaciones(Map<String, Integer> mapaVendedoresCotizaciones) {
        this.mapaVendedoresCotizaciones = mapaVendedoresCotizaciones;
    }

    public Map<String, Integer> getMapaVendedoresEmisiones() {
        return mapaVendedoresEmisiones;
    }

    public void setMapaVendedoresEmisiones(Map<String, Integer> mapaVendedoresEmisiones) {
        this.mapaVendedoresEmisiones = mapaVendedoresEmisiones;
    }
}
