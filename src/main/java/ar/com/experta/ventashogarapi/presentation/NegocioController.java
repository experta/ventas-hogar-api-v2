package ar.com.experta.ventashogarapi.presentation;

import ar.com.experta.ventashogarapi.core.KeycloakService;
import ar.com.experta.ventashogarapi.core.NegocioService;
import ar.com.experta.ventashogarapi.core.PlanService;
import ar.com.experta.ventashogarapi.core.model.NegocioOption;
import ar.com.experta.ventashogarapi.core.model.NegocioOptionClausulas;
import ar.com.experta.ventashogarapi.core.model.NegocioOptionTipoPlan;
import ar.com.experta.ventashogarapi.core.model.enums.*;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

@Validated
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/negocio")
@Api(tags="Negocio", description="Endpoints con información de objetos de negocio")
public class NegocioController {

    @Autowired
    private NegocioService negocioService;
    @Autowired
    private KeycloakService keycloakService;


    @Autowired
    PlanService planService;

    @GetMapping("/planes")
    @ApiOperation(value = "Obtener los planes configurados en el sistema")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity buscarPlanesEncabezado(@RequestParam(required = false) @ApiParam(name = "idVendedor", value = "ID del vendedor para el cual se desean obtener los planes", required = false) String idVendedor,
                                                 @RequestParam(required = false) @ApiParam(name = "tipoPlan", value = "Tipo plan para el cual se desean obtener los planes", required = false) TipoPlan tipoPlan,
             @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(planService.getPlanes(idVendedor, tipoPlan));
    }

    @GetMapping("/planes/{idPlan}")
    @ApiOperation(value = "Obtener el plan detallado")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación OK"),
            @ApiResponse(code = 501, message = "Not implemented")
    })
    public ResponseEntity obtenerPlanDetalle( @ApiParam(name = "idPlan", value = "ID de plan que se desea obtener", required = true) @PathVariable IdPlan idPlan,
             @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(planService.getPlan(idPlan));
    }

    @GetMapping("/localidades/opciones")
    @ApiOperation(value = "Tipos de Vivienda")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación OK")
    })
    public ResponseEntity<List<NegocioOption>> getLocalidades(
            @RequestParam @ApiParam(name = "codigoPostal", value = "Codigo Postal por el cual se filtra las localidades", required = true) String codigoPostal,
            @RequestParam(required = false) @ApiParam(name = "tipoPlan", value = "Tipo plan para el cual se desean obtener los planes", required = false) TipoPlan tipoPlan,
            @RequestHeader(name = "Authorization", required = false) String authHeader
    ){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getLocalidades(codigoPostal));
    }

    @GetMapping("/canales-venta/opciones")
    @ApiOperation(value = "Canales de Venta")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación OK")
    })
    public ResponseEntity<List<NegocioOption>> getCanalesVenta(@RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getCanalesVenta());
    }

    @GetMapping("/tipos-planes")
    @ApiOperation(value = "Tipos de Planes")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación OK")
    })
    public ResponseEntity<List<NegocioOptionTipoPlan>> getTiposPlanes(@RequestParam @ApiParam(name = "idVendedor", value = "ID del vendedor para el cual se desean obtener los planes", required = false) Optional<String> idVendedor,
                                                                      @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getTiposPlanes(idVendedor, this.keycloakService.getCanalVenta(authHeader)));
    }

    @GetMapping("/tipos-vivienda/opciones")
    @ApiOperation(value = "Tipos de Vivienda")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación OK")
    })
    public ResponseEntity<List<NegocioOption>> getTiposVivida(
            @ApiParam(name = "tipoPlan", value = "Tipo plan para el cual se desean obtener las viviendas", required = false) @RequestParam(required = false) TipoPlan tipoPlan,
            @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getTiposVivienda(tipoPlan));
    }

    @GetMapping("/medidas-seguridad/opciones")
    @ApiOperation(value = "Medidas de seguridad")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación OK")
    })
    public ResponseEntity<List<NegocioOptionClausulas>> getMedidasSeguridad(@RequestParam(required = false) @ApiParam(name = "localizacionId", value = "Localidad en la cual se encuentra la vivienda que se desea asegurar", required = false) Optional<String> localizacionId,
                                                                            @RequestParam(required = false) @ApiParam(name = "tipoVivienda", value = "Tipo de vivienda que se desea asegurar", required = false) Optional<TipoVivienda> tipoVivienda,
                                                                            @RequestParam(required = false) @ApiParam(name = "tipoPlan", value = "Tipo plan para el cual se desean obtener las viviendas", required = false) Optional<TipoPlan> tipoPlan,
                                                                            @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getMedidasSeguridad(
                localizacionId.isPresent() ? localizacionId.get() : null,
                tipoVivienda.isPresent() ? tipoVivienda.get() : null,
                tipoPlan.isPresent() ? tipoPlan.get() : null)
        );
    }

    @GetMapping("/tipos-cobertura/opciones")
    @ApiOperation(value = "Tipos de Coberturas")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación OK")
    })
    public ResponseEntity<List<NegocioOption>> getTiposCoberturas(@RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getTiposCoberturas());
    }

    @GetMapping("/condiciones-impositivas/opciones")
    @ApiOperation(value = "Tipos de Condiciones Impositivas")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación OK")
    })
    public ResponseEntity<List<NegocioOption>> getCondicionesImpositivas( @ApiParam(name = "tipoPersona", value = "Tipo de persona que es el tomador (Fisica o Juridica)", required = false) @RequestParam(required = false) TipoPersona tipoPersona,
                                                                          @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getCondicionesImpositivas(tipoPersona));
    }

    @GetMapping("/tipos-documento/opciones")
    @ApiOperation(value = "Tipos de Documento de un Tomador")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity<List<NegocioOption>> getTiposDocumento( @ApiParam(name = "tipoPersona", value = "Tipo de persona que es el tomador", required = false) @RequestParam(required = false) TipoPersona tipoPersona,
                                                                  @ApiParam(name = "condicionImpositiva", value = "Condicion impositiva de la persona que Tomador", required = false) @RequestParam(required = false) CondicionImpositiva condicionImpositiva,
                                                                  @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getTiposDocumento(tipoPersona,condicionImpositiva));
    }

    @GetMapping(value={"/tipos-bienes-especificos/opciones","/tipos-bienes-especificos"})
    @ApiOperation(value = "Obtener los tipos de bienes especificos segun categoria (definida en cada tipo de cobertura)")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity<List<NegocioOption>> getTiposBienesEspecificos(@RequestParam @ApiParam(name = "categoriaBienEspecifico", value = "ID del tipo de detalle para el cual se desean obtener los tipos de bienes que se deben detallar para emitir ciertas coberturas", required = true) CategoriaBienEspecifico categoriaBienEspecifico,
                                                                         @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getTiposBienesEspecificos(categoriaBienEspecifico));
    }

    @GetMapping("/medios-pago/opciones")
    @ApiOperation(value = "Obtener el listado de bancos configurados en el sistema")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity<List<NegocioOption>> getMediosPago(@RequestParam @ApiParam(name = "idPlan", value = "ID de plan que se desea obtener", required = true)  IdPlan idPlan,
                                                             @RequestParam @ApiParam(name = "idVendedor", value = "ID del vendedor para el cual se desean obtener los planes", required = true) String idVendedor,
                                                             @RequestParam(required = false) @ApiParam(name = "tipoMedioPago", value = "Tipo de medio de pago de la venta", required = false) TipoMedioPago tipoMedioPago,
                                                             @RequestHeader(name = "Authorization", required = false) String authHeader) {
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getMediosPago(idPlan, idVendedor, tipoMedioPago));
    }

    @GetMapping("/tipos-medio-pago/opciones")
    @ApiOperation(value = "Obtener el listado de tipos de medio de pagos soportados por el sistema")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })

    public ResponseEntity getTiposMedioPago(@RequestHeader(name = "Authorization", required = false) String authHeader,
                                            @RequestParam @ApiParam(name = "tipoPlan", value = "Tipo de plan que se desea obtener", required = true)  TipoPlan tipoPlan,
                                            @RequestParam @ApiParam(name = "idVendedor", value = "ID del vendedor para el cual se desean obtener los planes", required = true) String idVendedor ){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getTiposMedioPago(tipoPlan, idVendedor));
    }

    @GetMapping("/cantidad-cuotas/opciones")
    @ApiOperation(value = "Obtener el listado de cantidad de cuotas soportados por una venta")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })

    public ResponseEntity getCantidadCuotas(@RequestHeader(name = "Authorization", required = false) String authHeader,
                                            @RequestParam @ApiParam(name = "tipoPlan", value = "Tipo de plan que se desea obtener", required = true)  TipoPlan tipoPlan){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getCantidadCuotas(tipoPlan));
    }

    @GetMapping("/bancos/opciones")
    @ApiOperation(value = "Obtener el listado de bancos configurados en el sistema")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity<List<NegocioOption>> getBancos(@RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getBancos());
    }

    @GetMapping("/marcas-tarjetas-credito/opciones")
    @ApiOperation(value = "Obtener el listado de bancos configurados en el sistema")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity<List<NegocioOption>> getMarcasTarjetasCredito(@RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getMarcasTarjetasCredito());
    }

    @GetMapping("/nacionalidades/opciones")
    @ApiOperation(value = "Obtener el listado de las nacionalidades configuradas en el sistema")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity<List<NegocioOption>> getNacionalidades(@RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getNacionalidades());
    }

    @GetMapping("/actividades-tomador/opciones")
    @ApiOperation(value = "Obtener el listado de las actividades laborales configuradas en el sistema")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity<List<NegocioOption>> getActividades(@RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getActividadesTomador());
    }


    @GetMapping("/estados-venta/opciones")
    @ApiOperation(value = "Tipos de Coberturas")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación OK")
    })
    public ResponseEntity<List<NegocioOption>> getEstadosVenta(@RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getEstadosVenta());
    }

    @GetMapping("/beneficios/opciones")
    @ApiOperation(value = "Tipos de Beneficios")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación OK")
    })
    public ResponseEntity<List<NegocioOption>> buscarBeneficios(@RequestHeader(name = "Authorization", required = false) String authHeader,
                                                                @RequestParam @ApiParam(name = "idVendedor", value = "ID del vendedor para el cual se desean obtener los planes", required = false) Optional<String> idVendedor,
                                                                @RequestParam @ApiParam(name = "tipoPlan", value = "Tipo de plan para el cual se desean obtener los planes", required = false) Optional<TipoPlan> tipoPlan){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.getBeneficios(idVendedor, tipoPlan));
    }


    @GetMapping("/validaciones/beneficios/{beneficio}")
    @ApiOperation(value = "Tipos de Beneficios")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación OK")
    })
    public ResponseEntity validarBeneficio(@ApiParam(name = "beneficio", value = "Id del beneficio que se desea valida", required = true) @PathVariable Beneficio beneficio,
                                           @ApiParam(name = "documento-cliente", value = "Documento del cliente que quiero validar si aplica el descuento", required = true) @RequestParam(name="documento-cliente") String documentoCliente,
                                           @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(negocioService.validarBeneficios(beneficio, documentoCliente));
    }

    @GetMapping(value = "/manuales/{tipoPlan}")
    public ResponseEntity getManual(@ApiParam(name = "tipoPlan", value = "Tipo de plan para el cual se quiere descargar el manual", required = true) @PathVariable TipoPlan tipoPlan,
                                    @RequestHeader(name = "Authorization", required = false) String authHeader) throws IOException {
        this.keycloakService.validateJwt(authHeader);
        InputStream file = negocioService.getManualStream(tipoPlan);
        ContentDisposition contentDisposition = ContentDisposition.builder("inline")
                .filename(negocioService.getManualNombre(tipoPlan))
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentDisposition(contentDisposition);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        return ResponseEntity.ok()
                .headers(headers)
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(file))
                ;
    }
}
