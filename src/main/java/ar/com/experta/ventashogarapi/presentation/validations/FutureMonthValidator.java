package ar.com.experta.ventashogarapi.presentation.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class FutureMonthValidator implements ConstraintValidator<FutureMonth, Object> {
    private FutureMonth futureMonth;

    @Override
    public void initialize(FutureMonth constraintAnnotation) {
        this.futureMonth = constraintAnnotation;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        // Check the state of the Adminstrator.
        if (value == null) {
            return true;
        }

        // Initialize it.
        LocalDate data = LocalDate.parse(String.valueOf(value));
        return data.isAfter(LocalDate.now()) && data.isBefore(LocalDate.now().plusMonths(1));
    }

}