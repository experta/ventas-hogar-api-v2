package ar.com.experta.ventashogarapi.presentation.request;

import javax.validation.constraints.NotNull;

public class ClausulaEstabilizacionRequest {
    @NotNull
    private Boolean clausulaEstabilizacion;

    public ClausulaEstabilizacionRequest() {
    }

    public ClausulaEstabilizacionRequest(Boolean clausulaEstabilizacion) {
        this.clausulaEstabilizacion = clausulaEstabilizacion;
    }

    public Boolean getClausulaEstabilizacion() {
        return clausulaEstabilizacion;
    }

    public void setClausulaEstabilizacion(Boolean clausulaEstabilizacion) {
        this.clausulaEstabilizacion = clausulaEstabilizacion;
    }
}
