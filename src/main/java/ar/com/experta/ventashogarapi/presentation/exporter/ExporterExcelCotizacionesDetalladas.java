package ar.com.experta.ventashogarapi.presentation.exporter;

import ar.com.experta.ventashogarapi.core.model.enums.EstadoCotizacion;
import ar.com.experta.ventashogarapi.core.model.enums.TipoCobertura;
import ar.com.experta.ventashogarapi.infraestructure.db.model.CoberturaDb;
import ar.com.experta.ventashogarapi.infraestructure.db.model.CotizacionDb;
import ar.com.experta.ventashogarapi.infraestructure.db.model.VentaDb;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExporterExcelCotizacionesDetalladas {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<VentaDb> ventaDbList;
    private Map<TipoCobertura,Integer> mapaColumnas;
    private static final Integer OFFSET_COLUMNAS_COBERTURAS = 8;

    public ExporterExcelCotizacionesDetalladas(List<VentaDb> ventaDbList) {
        workbook = new XSSFWorkbook();
        this.ventaDbList = ventaDbList;
        this.mapaColumnas = new HashMap<>();
        Integer position = OFFSET_COLUMNAS_COBERTURAS;
        for (VentaDb ventaDb : ventaDbList){
            for (CotizacionDb cotizacionDb : ventaDb.getCotizaciones()){
                for (CoberturaDb coberturaDb : cotizacionDb.getCoberturas()){
                    if (!mapaColumnas.containsKey(coberturaDb.getTipoCobertura())){
                        mapaColumnas.put(coberturaDb.getTipoCobertura(), position);
                        position++;
                    }
                }
            }
        }
    }

    private void writeHeaderLine() {
        try {
            sheet = workbook.createSheet("Reporte de Cotizaciones");
            Row row = sheet.createRow(0);

            CellStyle style = workbook.createCellStyle();
            XSSFFont font = workbook.createFont();
            font.setBold(true);
            font.setFontHeight(12);
            style.setFont(font);

            createCell(row, 0, "NRO VENTA", style);
            createCell(row, 1, "VENDEDOR", style);
            createCell(row, 2, "CANAL VENTA", style);
            createCell(row, 3, "TIPO PLAN", style);
            createCell(row, 4, "TIPO RIESGO", style);
            createCell(row, 5, "CANTIDAD CUOTAS", style);
            createCell(row, 6, "MEDIO PAGO", style);
            createCell(row, 7, "EMITIDA", style);
            createCell(row, 8, "PREMIO", style);
            createCell(row, 9, "PRIMA", style);

            //Hay que sumar una columna por cada cobertura
            for(TipoCobertura tipoCobertura : mapaColumnas.keySet()){
                createCell(row, mapaColumnas.get(tipoCobertura), tipoCobertura.label(), style);
            }

        }catch(Exception e){
            new Exception(e.getMessage());
        }
    }
    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        final int[] rowCount = {1};

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        CellStyle styleCurrency = workbook.createCellStyle();
        styleCurrency.setDataFormat((short)8);

        for (VentaDb ventaDb : this.ventaDbList) {
            CotizacionDb cotizacionDb = ventaDb.getCotizaciones()
                    .stream()
                    .filter(c -> c.getEstado().equals(EstadoCotizacion.EMITIDA))
                    .findFirst()
                    .orElse(null);
            if (cotizacionDb == null)
                cotizacionDb = ventaDb.getCotizaciones().get(0);

            Row row = sheet.createRow(rowCount[0]++);
            row.createCell(0).setCellValue(ventaDb.getId());
            row.createCell(1).setCellValue(ventaDb.getVendedorId());
            row.createCell(2).setCellValue(ventaDb.getCanalVenta() != null ? ventaDb.getCanalVenta().label() : "");
            row.createCell(3).setCellValue(ventaDb.getTipoPlan() != null ? ventaDb.getTipoPlan().label() : "");
            row.createCell(4).setCellValue(ventaDb.getVivienda() != null ? ventaDb.getVivienda().getTipoVivienda().toString() : "");
            row.createCell(5).setCellValue(cotizacionDb != null ? cotizacionDb.getCuotas().toString() : "");
            row.createCell(6).setCellValue(ventaDb.getPago() != null && ventaDb.getPago().getMedioPago() != null ? ventaDb.getPago().getMedioPago().label() : "");
            row.createCell(7).setCellValue(ventaDb.getNumeroPoliza() != null);
            row.createCell(8).setCellValue(cotizacionDb != null && cotizacionDb.getPremio() != null ? cotizacionDb.getPremio().toString() : "");
            row.createCell(9).setCellValue(cotizacionDb != null && cotizacionDb.getPrima() != null ? cotizacionDb.getPrima().toString() : "");

            for(CoberturaDb coberturaDb : cotizacionDb.getCoberturas()){
                row.createCell(mapaColumnas.get(coberturaDb.getTipoCobertura())).setCellValue(coberturaDb.getMontoAsegurado() != null ? coberturaDb.getMontoAsegurado().toString() : "x");
            }

        }
        for (int j = 0; j < OFFSET_COLUMNAS_COBERTURAS + mapaColumnas.size(); j++) {
            sheet.autoSizeColumn(j);
        }
    }
    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);

        workbook.close();
        outputStream.close();
    }

    public List<VentaDb> getVentaDbList() {
        return ventaDbList;
    }

    public void setVentaDbList(List<VentaDb> ventaDbList) {
        this.ventaDbList = ventaDbList;
    }

    public Map<TipoCobertura, Integer> getMapaColumnas() {
        return mapaColumnas;
    }

    public void setMapaColumnas(Map<TipoCobertura, Integer> mapaColumnas) {
        this.mapaColumnas = mapaColumnas;
    }
}
