package ar.com.experta.ventashogarapi.presentation;

import ar.com.experta.ventashogarapi.core.MetricasService;
import ar.com.experta.ventashogarapi.core.model.enums.Beneficio;
import ar.com.experta.ventashogarapi.core.model.enums.CanalVenta;
import ar.com.experta.ventashogarapi.core.model.enums.TipoPlan;
import ar.com.experta.ventashogarapi.infraestructure.db.model.VentaDb;
import ar.com.experta.ventashogarapi.presentation.exporter.ExporterExcelCotizacionesDetalladas;
import ar.com.experta.ventashogarapi.presentation.exporter.ExporterExcelCotizacionesSimples;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Validated
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/metricas")
@Api(tags="Métricas", description="Endpoints con información de métricas de operación")
public class MetricasController {

    @Autowired
    private MetricasService metricasService;
    private ExporterExcelCotizacionesSimples exporterExcelCotizacionesSimples;
    private ExporterExcelCotizacionesDetalladas exporterExcelCotizacionesDetalladas;
    private List<VentaDb> ventaDbList;
    private Date fechaActual;

    @GetMapping("/ventas/dias")
    @ApiOperation(value = "Obtener cotizaciones por dia")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity getVentasPorDia(
            @ApiParam(name = "fechaDesde", value = "Fecha de ultima modificacion desde", required = true) @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaDesde,
            @ApiParam(name = "fechaHasta", value = "Fecha de ultima modificacion hasta", required = false) @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaHasta,
            @ApiParam(name = "tipoPlan", value = "Tipo de plan", required = false) @RequestParam(required = false) TipoPlan tipoPlan,
            @ApiParam(name = "beneficio", value = "Beneficio de descuento", required = false) @RequestParam(required = false) Beneficio beneficio,
            @ApiParam(name = "canalVenta", value = "Canal de venta", required = false) @RequestParam(required = false) CanalVenta canalVenta){
        return ResponseEntity.ok(metricasService.getVentasPorDia(fechaDesde, fechaHasta, tipoPlan, canalVenta, beneficio));
    }

    @GetMapping("/ventas/mes")
    @ApiOperation(value = "Obtener cotizaciones por mes")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity getVentasPorMes(
            @ApiParam(name = "fechaDesde", value = "Fecha de ultima modificacion desde", required = true) @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaDesde,
            @ApiParam(name = "fechaHasta", value = "Fecha de ultima modificacion hasta", required = false) @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaHasta,
            @ApiParam(name = "tipoPlan", value = "Tipo de plan", required = false) @RequestParam(required = false) TipoPlan tipoPlan,
            @ApiParam(name = "beneficio", value = "Beneficio de descuento", required = false) @RequestParam(required = false) Beneficio beneficio,
            @ApiParam(name = "canalVenta", value = "Canal de venta", required = false) @RequestParam(required = false) CanalVenta canalVenta){
        return ResponseEntity.ok(metricasService.getVentasPorMes(fechaDesde, fechaHasta, tipoPlan, canalVenta, beneficio));
    }

    @GetMapping("/totales")
    @ApiOperation(value = "Obtener cotizaciones totales")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity getTotales(
            @ApiParam(name = "fechaDesde", value = "Fecha de ultima modificacion desde", required = true) @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaDesde,
            @ApiParam(name = "fechaHasta", value = "Fecha de ultima modificacion hasta", required = false) @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaHasta
            ){
        return ResponseEntity.ok(metricasService.getTotales(fechaDesde, fechaHasta));
    }

    @GetMapping("/totales/cotizaciones/tipos-plan")
    @ApiOperation(value = "Obtener cotizaciones totales")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity getTotalesCotizacionesTipoPlan(
            @ApiParam(name = "fechaDesde", value = "Fecha de ultima modificacion desde", required = true) @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaDesde,
            @ApiParam(name = "fechaHasta", value = "Fecha de ultima modificacion hasta", required = false) @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaHasta
    ){
        return ResponseEntity.ok(metricasService.getTotalesCotizacionesTipoPlan(fechaDesde, fechaHasta));
    }

    @GetMapping("/totales/emisiones/tipos-plan")
    @ApiOperation(value = "Obtener cotizaciones totales")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity getTotalesEmisionesTipoPlan(
            @ApiParam(name = "fechaDesde", value = "Fecha de ultima modificacion desde", required = true) @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaDesde,
            @ApiParam(name = "fechaHasta", value = "Fecha de ultima modificacion hasta", required = false) @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaHasta
    ){
        return ResponseEntity.ok(metricasService.getTotalesEmisionesTipoPlan(fechaDesde, fechaHasta));
    }

    @GetMapping("/totales/cotizaciones/canales-venta")
    @ApiOperation(value = "Obtener cotizaciones totales")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity getTotalesCotizacionesCanalesVenta(
            @ApiParam(name = "fechaDesde", value = "Fecha de ultima modificacion desde", required = true) @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaDesde,
            @ApiParam(name = "fechaHasta", value = "Fecha de ultima modificacion hasta", required = false) @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaHasta
    ){
        return ResponseEntity.ok(metricasService.getTotalesCotizacionesCanalesVenta(fechaDesde, fechaHasta));
    }

    @GetMapping("/totales/emisiones/canales-venta")
    @ApiOperation(value = "Obtener cotizaciones totales")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity getTotalesEmisionesCanalesVenta(
            @ApiParam(name = "fechaDesde", value = "Fecha de ultima modificacion desde", required = true) @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaDesde,
            @ApiParam(name = "fechaHasta", value = "Fecha de ultima modificacion hasta", required = false) @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaHasta
    ){
        return ResponseEntity.ok(metricasService.getTotalesEmisionesCanalesVenta(fechaDesde, fechaHasta));
    }

    @GetMapping("/totales/cotizaciones/beneficios")
    @ApiOperation(value = "Obtener cotizaciones totales")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity getTotalesCotizacionesBeneficios(
            @ApiParam(name = "fechaDesde", value = "Fecha de ultima modificacion desde", required = true) @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaDesde,
            @ApiParam(name = "fechaHasta", value = "Fecha de ultima modificacion hasta", required = false) @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaHasta
    ){
        return ResponseEntity.ok(metricasService.getTotalesCotizacionesBeneficios(fechaDesde, fechaHasta));
    }

    @GetMapping("/totales/emisiones/beneficios")
    @ApiOperation(value = "Obtener cotizaciones totales")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity getTotalesEmisionesBeneficios(
            @ApiParam(name = "fechaDesde", value = "Fecha de ultima modificacion desde", required = true) @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaDesde,
            @ApiParam(name = "fechaHasta", value = "Fecha de ultima modificacion hasta", required = false) @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaHasta
    ){
        return ResponseEntity.ok(metricasService.getTotalesEmisionesBeneficios(fechaDesde, fechaHasta));
    }

    @GetMapping(value = "/reporte/cotizaciones-simples") //, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE
    @ApiOperation(value = "Obtener reporte de cotizaciones")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public void getReporteCotizacionesSimples(
            @ApiParam(name = "fechaDesde",value = "Fecha de última modificación desde",required = true) @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaDesde,
            @ApiParam(name = "fechaHasta",value = "Fecha de última modificación hasta",required = false) @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaHasta,
            @ApiParam(name = "tipoPlan",value = "Tipo de Plan",required = false) @RequestParam(required = false) TipoPlan tipoPlan,
            @ApiParam(name = "canalVenta",value = "Canal de Venta",required = false) @RequestParam(required = false) CanalVenta canalVenta,
            @ApiParam(name = "beneficio", value = "Beneficio de descuento", required = false) @RequestParam(required = false) Beneficio beneficio,
            HttpServletResponse response) throws IOException {

        response.setContentType("application/octet-stream");
        String headerKey = "Content-Disposition";
        String headerValue = this.nombreArchivoHorafechaActual("COMERCIO","SIMPLE");
        response.setHeader(headerKey, headerValue);

        ventaDbList=metricasService.getReporteCotizaciones(fechaDesde,fechaHasta,tipoPlan,canalVenta,beneficio);
        exporterExcelCotizacionesSimples = new ExporterExcelCotizacionesSimples(ventaDbList);
        exporterExcelCotizacionesSimples.export(response);

    }

    @GetMapping(value = "/reporte/cotizaciones-detalladas") //, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE
    @ApiOperation(value = "Obtener reporte de cotizaciones")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public void getReporteCotizacionesDetalladas(
            @ApiParam(name = "fechaDesde",value = "Fecha de última modificación desde",required = true) @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaDesde,
            @ApiParam(name = "fechaHasta",value = "Fecha de última modificación hasta",required = false) @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaHasta,
            @ApiParam(name = "tipoPlan",value = "Tipo de Plan",required = false) @RequestParam(required = false) TipoPlan tipoPlan,
            @ApiParam(name = "canalVenta",value = "Canal de Venta",required = false) @RequestParam(required = false) CanalVenta canalVenta,
            @ApiParam(name = "beneficio", value = "Beneficio de descuento", required = false) @RequestParam(required = false) Beneficio beneficio,
            HttpServletResponse response) throws IOException {


        response.setContentType("application/octet-stream");
        String headerKey = "Content-Disposition";
        String headerValue = this.nombreArchivoHorafechaActual("COMERCIO","DETALLADO");
        response.setHeader(headerKey, headerValue);

        ventaDbList=metricasService.getReporteCotizaciones(fechaDesde,fechaHasta,tipoPlan,canalVenta,beneficio);
        exporterExcelCotizacionesDetalladas= new ExporterExcelCotizacionesDetalladas(ventaDbList);
        exporterExcelCotizacionesDetalladas.export(response);
    }

    private String nombreArchivoHorafechaActual(String origen,String tipoInforme) {

        String nombreArchivoConFechaHora = null;

        if (origen.equalsIgnoreCase("COMERCIO")) {

            fechaActual = new Date();

            nombreArchivoConFechaHora = null;
            String nombreArchivoSinExtension = null;
            String headerValue = null;

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

            String fechaFormateada = dateFormat.format(fechaActual);
            String horaFormateada = timeFormat.format(fechaActual);

            if (tipoInforme.equalsIgnoreCase("DETALLADO")) {
                headerValue = "attachment; filename=reporteCotizacionesVentasComercioDetallado.xlsx";
            } else {
                headerValue = "attachment; filename=reporteCotizacionesVentasComercioSimple.xlsx";
            }

            int puntoIndex = headerValue.lastIndexOf('.');

            if (puntoIndex != -1) {
                nombreArchivoSinExtension = headerValue.substring(0, puntoIndex);
                nombreArchivoConFechaHora = nombreArchivoSinExtension + "_" + fechaFormateada + "_" + horaFormateada + ".xlsx";
            } else {
                nombreArchivoConFechaHora = headerValue + "_" + fechaFormateada + "_" + horaFormateada;
            }
        }


        return nombreArchivoConFechaHora;
    }
}
