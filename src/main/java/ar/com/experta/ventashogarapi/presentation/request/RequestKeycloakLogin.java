package ar.com.experta.ventashogarapi.presentation.request;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class RequestKeycloakLogin {


    @NotNull
    @Valid
    private String username;
    @NotNull
    @Valid
    private String password;


    public RequestKeycloakLogin(){}

    public RequestKeycloakLogin(@NotNull @Valid String username, @NotNull @Valid String password) {

        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
