package ar.com.experta.ventashogarapi.presentation.validations;

import ar.com.experta.ventashogarapi.presentation.request.VentaRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class FutureMonthByProductValidator implements ConstraintValidator<FutureMonthByProduct, Object> {
    private FutureMonthByProduct futureMonthByProduct;

    @Override
    public void initialize(FutureMonthByProduct constraintAnnotation) {
        this.futureMonthByProduct = constraintAnnotation;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        // Check the state of the Adminstrator.
        if (value == null) {
            return true;
        }

        VentaRequest ventaRequest = (VentaRequest) value;
        LocalDate data = ventaRequest.getFechaInicioCobertura();
        if (!ventaRequest.getTipoPlan().vigenciaRetroactiva())
            return data.isAfter(LocalDate.now()) && data.isBefore(LocalDate.now().plusMonths(1));
        return Boolean.TRUE;
    }

}