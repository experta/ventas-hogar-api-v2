package ar.com.experta.ventashogarapi.presentation.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.Period;

public class NotAge65Validator implements ConstraintValidator<NotAge65, Object> {
    private NotAge65 notAge65;
    private final static int AGE = 65;

    @Override
    public void initialize(NotAge65 constraintAnnotation) {
        this.notAge65 = constraintAnnotation;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        // Check the state of the Adminstrator.
        if (value == null) {
            return true;
        }

        // Initialize it.
        LocalDate date = LocalDate.parse(String.valueOf(value));
        return Period.between(date, LocalDate.now()).getYears() < AGE;
    }

}