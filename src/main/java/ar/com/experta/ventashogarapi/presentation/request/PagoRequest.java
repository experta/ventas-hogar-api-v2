package ar.com.experta.ventashogarapi.presentation.request;

import ar.com.experta.ventashogarapi.commons.exceptions.badrequest.PagoCreditoValidationException;
import ar.com.experta.ventashogarapi.commons.exceptions.badrequest.PagoDebitoValidationException;
import ar.com.experta.ventashogarapi.commons.exceptions.badrequest.TipoPagoInvalidoException;
import ar.com.experta.ventashogarapi.core.model.*;
import ar.com.experta.ventashogarapi.core.model.enums.MedioPago;

import javax.validation.constraints.NotNull;

public class PagoRequest {

    private final static int LENGTH_NUMERO_AMEX = 15;
    private final static int LENGTH_NUMERO = 16;

    @NotNull (message = "El tipo de pago no puede ser nulo")
    private MedioPago tipo;
    private String banco;
    private String cbu;
    private String empresaTarjeta;
    private Long numeroTarjeta;
    private Integer anioVencimiento;
    private Integer mesVencimiento;

    public Pago toPago() {
        if (tipo.equals(MedioPago.DEBITO) && (banco == null || cbu == null))
            throw new PagoDebitoValidationException();
        if (tipo.equals(MedioPago.CREDITO) && (empresaTarjeta == null || numeroTarjeta == null || anioVencimiento == null || mesVencimiento == null))
            throw new PagoCreditoValidationException(PagoCreditoValidationException.MotivoRechazo.DATOS_INCOMPLETOS);

        if (tipo.equals(MedioPago.CREDITO) && empresaTarjeta.equals("AMEX") && numeroTarjeta.toString().length() != LENGTH_NUMERO_AMEX)
            throw new PagoCreditoValidationException(PagoCreditoValidationException.MotivoRechazo.NUMERO_DIGITO_INCOMPLETO_AMEX);
        else if (tipo.equals(MedioPago.CREDITO) && !empresaTarjeta.equals("AMEX") && numeroTarjeta.toString().length() != LENGTH_NUMERO)
            throw new PagoCreditoValidationException(PagoCreditoValidationException.MotivoRechazo.NUMERO_DIGITO_INCOMPLETO);

        switch (tipo){
            case DEBITO: return new PagoDebito(banco, cbu != null && cbu.isEmpty() ? null : cbu);
            case CREDITO: return new PagoCredito(empresaTarjeta,numeroTarjeta.toString(),anioVencimiento, mesVencimiento);
            case CUPONERA: return new PagoCuponera();
            case OP_BANCARIA_DTV: return new PagoOpBancariaDTV();
        }
        throw new TipoPagoInvalidoException();

    }

    public PagoRequest() {
    }

    public PagoRequest(MedioPago tipo, String banco, String cbu, String empresaTarjeta, Long numeroTarjeta, Integer anioVencimiento, Integer mesVencimiento) {
        this.tipo = tipo;
        this.banco = banco;
        this.cbu = cbu;
        this.empresaTarjeta = empresaTarjeta;
        this.numeroTarjeta = numeroTarjeta;
        this.anioVencimiento = anioVencimiento;
        this.mesVencimiento = mesVencimiento;
    }

    public void setTipo(MedioPago tipo) {
        this.tipo = tipo;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public void setCbu(String cbu) {
        this.cbu = cbu;
    }

    public void setEmpresaTarjeta(String empresaTarjeta) {
        this.empresaTarjeta = empresaTarjeta;
    }

    public void setNumeroTarjeta(Long numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public void setAnioVencimiento(Integer anioVencimiento) {
        this.anioVencimiento = anioVencimiento;
    }

    public void setMesVencimiento(Integer mesVencimiento) {
        this.mesVencimiento = mesVencimiento;
    }

    public MedioPago getTipo() {
        return tipo;
    }

    public String getBanco() {
        return banco;
    }

    public String getCbu() {
        return cbu;
    }

    public String getEmpresaTarjeta() {
        return empresaTarjeta;
    }

    public Long getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public Integer getAnioVencimiento() {
        return anioVencimiento;
    }

    public Integer getMesVencimiento() {
        return mesVencimiento;
    }
}
