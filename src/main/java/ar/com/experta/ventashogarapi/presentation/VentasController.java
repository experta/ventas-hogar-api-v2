package ar.com.experta.ventashogarapi.presentation;

import ar.com.experta.ventashogarapi.core.KeycloakService;
import ar.com.experta.ventashogarapi.core.VentasService;
import ar.com.experta.ventashogarapi.core.model.Cobertura;
import ar.com.experta.ventashogarapi.core.model.Venta;
import ar.com.experta.ventashogarapi.core.model.enums.EstadoVenta;
import ar.com.experta.ventashogarapi.presentation.request.*;
import ar.com.experta.ventashogarapi.presentation.validations.FutureMonthByProduct;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Validated
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/ventas")
@Api(tags="Venta de Seguros de Hogar", description="Endpoints para comercializacion de seguros de hogar (combinado familiar) de Experta")
@EnableAsync
public class VentasController {

    @Autowired
    private VentasService ventasService;

    @Autowired
    private KeycloakService keycloakService;

    @Value("${url_local}")
    private String HOST_LOCAL;

    @PostMapping(value = "/")
    @ApiOperation(value = "Crear Venta")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Venta creada"),
            @ApiResponse(code = 400, message = "Solicitud inválida"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity postVenta(@Valid @RequestBody @FutureMonthByProduct(message = "La fecha de inicio de cobertura debe ser posterior a hoy, hasta un mes") VentaRequest ventaRequest, @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        Venta venta = ventasService.crearVenta(
                this.keycloakService.getCanalVenta(authHeader),
                ventaRequest.getVendedor().getId(),
                ventaRequest.getTipoPlan(),
                ventaRequest.getCliente(),
                ventaRequest.getVivienda(),
                ventaRequest.getFechaInicioCobertura(),
                ventaRequest.getTipoMedioPago(),
                ventaRequest.getCantidadCuotas(),
                ventaRequest.getModalidadComision(),
                ventaRequest.getClausulaEstabilizacion(),
                ventaRequest.getBeneficio()
        );

        URI uri = ServletUriComponentsBuilder
                .fromHttpUrl(HOST_LOCAL)
                .path("/ventas/{id}")
                .buildAndExpand(venta.getId())
                .toUri();

        return ResponseEntity.created(uri).body("{\"Location\":\""+uri+"\"}");

    }


    @GetMapping("/{idVenta}")
    @ApiOperation(value = "Obtener venta ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity getVenta(@ApiParam(name = "idVenta", value = "Id de la venta que se desea obtener", required = true) @PathVariable String idVenta,  @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        Venta venta = this.ventasService.getVenta(idVenta);

        return ResponseEntity.ok(venta);
    }

    @GetMapping("/")
    @ApiOperation(value = "Buscar ventas, cotizaciones y emisiones ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity searchVentas( @ApiParam(name = "idVendedor", value = "Id de Vendedor", required = true) @RequestParam String idVendedor,
                                        @ApiParam(name = "fechaDesde", value = "Fecha de ultima modificacion desde", required = true) @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaDesde,
                                        @ApiParam(name = "fechaHasta", value = "Fecha de ultima modificacion hasta", required = false) @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fechaHasta,
                                        @ApiParam(name = "numero", value = "Numero de cotizacion", required = false) @RequestParam(required = false) String  numero,
                                        @ApiParam(name = "nombre", value = "Nombre del cliente", required = false) @RequestParam(required = false) String nombre,
                                        @ApiParam(name = "email", value = "Email del cliente", required = false) @RequestParam(required = false) String email,
                                        @ApiParam(name = "estadoVenta", value = "Estado de Venta", required = false) @RequestParam(required = false) EstadoVenta estadoVenta,
                                          @RequestHeader(name = "Authorization", required = false) String authHeader ){
        this.keycloakService.validateJwt(authHeader);
        List<Venta> lista = this.ventasService.searchVentas(idVendedor,
                fechaDesde,
                fechaHasta != null ? Optional.of(fechaHasta) : Optional.empty(),
                numero != null ? Optional.of(numero) : Optional.empty(),
                nombre != null ? Optional.of(nombre) : Optional.empty(),
                email != null ? Optional.of(email) : Optional.empty(),
                estadoVenta != null ? Optional.of(estadoVenta) : Optional.empty());
        return ResponseEntity.ok(lista);
    }


    @PutMapping("/{idVenta}/cotizaciones/{idCotizacion}")
    @ApiOperation(value = "Recotizar una cotizacion")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Cotizacion guardada"),
            @ApiResponse(code = 400, message = "Solicitud inválida"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")

    })
    public ResponseEntity putCotizacion(@ApiParam(name = "idVenta", value = "Id de la venta que se desea recotizar", required = true) @PathVariable String idVenta,
                                        @ApiParam(name = "idCotizacion", value = "Id de la cotizacion que se desea recotizar", required = true) @PathVariable String idCotizacion,
                                        @Valid @RequestBody CotizacionRequest cotizacionRequest,
                                        @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        ventasService.putCotizacion(idVenta,
                idCotizacion,
                cotizacionRequest.getCoberturas().stream().map(c -> new Cobertura(c.getTipoCobertura(),c.getMontoAsegurado(), c.getOpcion())).collect(Collectors.toList())
        );
        return ResponseEntity.noContent().build();
    }


    @GetMapping("/{idVenta}/cotizaciones/{idCotizacion}")
    @ApiOperation(value = "Obtener una cotizacion ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity getCotizacion(@ApiParam(name = "idVenta", value = "Id de la venta que se desea obtener", required = true) @PathVariable String idVenta,
                                        @ApiParam(name = "idCotizacion", value = "Id de la cotizacion que se desea obtener", required = true) @PathVariable String idCotizacion,
                                        @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(this.ventasService.getCotizacion(idVenta,idCotizacion));
    }

    @PutMapping("/{idVenta}/fechaInicioCobertura")
    @ApiOperation(value = "Guardar la fecha de inicio de cobertura")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Se guardo la informacion"),
            @ApiResponse(code = 400, message = "Solicitud inválida"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")

    })
    public ResponseEntity putFechaInicioCobertura(@ApiParam(name = "idVenta", value = "Id de la venta que se desea modificar", required = true) @PathVariable String idVenta,
                                     @Valid @RequestBody FechaRequest fechaRequest,
                                     @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        ventasService.putFechaInicioCobertura(idVenta, fechaRequest.getFechaInicioCobertura());
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{idVenta}/tipo-medio-pago")
    @ApiOperation(value = "Cambiar el tipo de medio de pago")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Se guardo la informacion"),
            @ApiResponse(code = 400, message = "Solicitud inválida"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")

    })
    public ResponseEntity putTipoMedioPago(@ApiParam(name = "idVenta", value = "Id de la venta que se desea modificar", required = true) @PathVariable String idVenta,
                                           @Valid @RequestBody TipoMedioPagoRequest tipoMedioPagoRequest,
                                           @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        ventasService.putTipoMedioPago(idVenta, tipoMedioPagoRequest.getTipoMedioPago());
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{idVenta}/cantidad-cuotas")
    @ApiOperation(value = "Cambiar la cantidad de cuotas de la poliza")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Se guardo la informacion"),
            @ApiResponse(code = 400, message = "Solicitud inválida"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")

    })
    public ResponseEntity putCantidadCuotas(@ApiParam(name = "idVenta", value = "Id de la venta que se desea modificar", required = true) @PathVariable String idVenta,
                                           @Valid @RequestBody CantidadCuotasRequest cantidadCuotasRequest,
                                           @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        ventasService.putCantidadCuotas(idVenta, cantidadCuotasRequest.getCantidadCuotas());
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{idVenta}/beneficio")
    @ApiOperation(value = "Guardar el beneficio que se desea activar a la venta ")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Se guardo la informacion"),
            @ApiResponse(code = 400, message = "Solicitud inválida"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")

    })
    public ResponseEntity putBeneficio(@ApiParam(name = "idVenta", value = "Id de la venta que se desea modificar", required = true) @PathVariable String idVenta,
                                                  @Valid @RequestBody BeneficioRequest beneficioRequest,
                                       @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        ventasService.putBeneficio(idVenta, beneficioRequest.getBeneficio());
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{idVenta}/beneficio")
    @ApiOperation(value = "Eliminar el beneficio de la venta")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Se guardo la informacion"),
            @ApiResponse(code = 400, message = "Solicitud inválida"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")

    })
    public ResponseEntity deleteBeneficio(@ApiParam(name = "idVenta", value = "Id de la venta que se desea modificar", required = true) @PathVariable String idVenta,
                                          @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        ventasService.deleteBeneficio(idVenta);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{idVenta}/vendedor")
    @ApiOperation(value = "Guardar datos del vendedor ")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Se guardo la informacion"),
            @ApiResponse(code = 400, message = "Solicitud inválida"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")

    })
    public ResponseEntity putCliente(@ApiParam(name = "idVenta", value = "Id de la venta que se desea modificar", required = true) @PathVariable String idVenta,
                                     @Valid @RequestBody VendedorRequest vendedorRequest,
                                     @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        ventasService.putVendedor(idVenta, vendedorRequest.toVendedor());
        return ResponseEntity.noContent().build();
    }


    @PutMapping("/{idVenta}/cliente")
    @ApiOperation(value = "Guardar datos del cliente ")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Se guardo la informacion"),
            @ApiResponse(code = 400, message = "Solicitud inválida"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")

    })
    public ResponseEntity putCliente(@ApiParam(name = "idVenta", value = "Id de la venta que se desea modificar", required = true) @PathVariable String idVenta,
                                     @Valid @RequestBody ClienteRequest clienteRequest,
                                     @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        ventasService.putCliente(idVenta, clienteRequest.toCliente());
        return ResponseEntity.noContent().build();
    }


    @PutMapping("/{idVenta}/vivienda")
    @ApiOperation(value = "Guardar datos de la vivienda ")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Se guardo la informacion"),
            @ApiResponse(code = 400, message = "Solicitud inválida"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")

    })
    public ResponseEntity putVivienda(@ApiParam(name = "idVenta", value = "Id de la venta que se desea modificar", required = true) @PathVariable String idVenta,
                                      @Valid @RequestBody ViviendaRequest viviendaRequest
                , @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        ventasService.putVivienda(idVenta, viviendaRequest.toVivienda());
        return ResponseEntity.noContent().build();
    }


    @PutMapping("/{idVenta}/pago")
    @ApiOperation(value = "Guardar datos de la vivienda ")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Se guardo la informacion"),
            @ApiResponse(code = 400, message = "Solicitud inválida"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")

    })
    public ResponseEntity putPago(@ApiParam(name = "idVenta", value = "Id de la venta que se desea modificar", required = true) @PathVariable String idVenta,
                                  @Valid @RequestBody PagoRequest pagoRequest,
                                  @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        ventasService.putPago(idVenta, pagoRequest.toPago());
        return ResponseEntity.noContent().build();
    }


    @PatchMapping("/{idVenta}/cotizaciones/{idCotizacion}/estado")
    @ApiOperation(value = "Modificar estado de cotizacion ")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Cotizacion modificada"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity patchEstadoCotizacion(@ApiParam(name = "idVenta", value = "Id de la venta que se desea obtener", required = true) @PathVariable String idVenta,
                                                @ApiParam(name = "idCotizacion", value = "Id de la cotizacion que se desea obtener", required = true) @PathVariable String idCotizacion,
                                                @Valid @RequestBody ModificarEstadoCotizacionRequest modificarEstadoCotizacionRequest,
                                                @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        ventasService.patchEstadoCotizacion (idVenta, idCotizacion, modificarEstadoCotizacionRequest.getEstado());
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{idVenta}/especificos")
    @ApiOperation(value = "Guardar datos de los detalles especificos de coberturas")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Se guardo la informacion"),
            @ApiResponse(code = 400, message = "Solicitud inválida"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")

    })
    public ResponseEntity putEspecificos(@ApiParam(name = "idVenta", value = "Id de la venta que se desea guardar especificos", required = true) @PathVariable String idVenta,
                                         @Valid @RequestBody EspecificosRequest especificosRequest, @RequestHeader(name = "Authorization", required = false) String authHeader
    ){
        this.keycloakService.validateJwt(authHeader);
        ventasService.putBienesEspecificos(idVenta, especificosRequest.toEspecificos());
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{idVenta}/clausula-estabilizacion")
    @ApiOperation(value = "Guardar datos del tipo de modalidad de comision a utilizar para la venta de la poliza")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Se guardo la informacion"),
            @ApiResponse(code = 400, message = "Solicitud inválida"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")

    })
    public ResponseEntity putClausulaEstabilizacion (   @RequestHeader(name = "Authorization", required = false) String authHeader,
                                                        @ApiParam(name = "idVenta", value = "Id de la venta que se desea guardar especificos", required = true) @PathVariable String idVenta,
                                                        @Valid @RequestBody ClausulaEstabilizacionRequest clausulaEstabilizacionRequest){
        this.keycloakService.validateJwt(authHeader);
        ventasService.putClausulaEstabilizacion(idVenta, clausulaEstabilizacionRequest.getClausulaEstabilizacion());
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{idVenta}/porcentaje-estabilizacion")
    @ApiOperation(value = "Porcentaje de Estabilizacion")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación OK")
    })
    public ResponseEntity getPorcentajeEstabilizacion(@RequestHeader(name = "Authorization", required = false) String authHeader,
                                                      @ApiParam(name = "idVenta", value = "Id de la venta que se desea guardar especificos", required = true) @PathVariable String idVenta
    ){
        this.keycloakService.validateJwt(authHeader);
        return ResponseEntity.ok(ventasService.getPorcentajeEstabilizacion(idVenta));
    }

    @PostMapping("/{idVenta}/emision")
    @ApiOperation(value = "Emitir cotizacion ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 400, message = "La solicitud tiene parametros invalidos o incorrectos"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 422, message = "La solicitud esta bien formada pero no puede ser procesada por motivo de negocio"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")

    })
    public ResponseEntity postEmision(@ApiParam(name = "idVenta", value = "Id de la venta que se desea emitir", required = true) @PathVariable String idVenta,
                                        @Valid @RequestBody (required=false) EmisionRequest emisionRequest,
                                      @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        if (emisionRequest != null)
            ventasService.postEmision(idVenta,
                    emisionRequest.getCliente() != null ? emisionRequest.getCliente().toCliente() : null,
                    emisionRequest.getVivienda() != null ? emisionRequest.getVivienda().toVivienda() : null,
                    emisionRequest.getPago() != null ? emisionRequest.getPago().toPago() : null,
                    emisionRequest.getEspecificos() != null ? emisionRequest.getEspecificos().toEspecificos() :null,
                    emisionRequest.getNumeroReferencia()
            );
        else
            ventasService.postEmision(idVenta);

        return ResponseEntity.noContent().build();
    }

    @GetMapping(value="/{idVenta}/cotizacion.pdf", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ApiOperation(value = "Descargar cotizacion en PDF")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity getImpresionCotizacion(@ApiParam(name = "idVenta", value = "Id de la venta que se desea imprimir la cotizacion", required = true) @PathVariable String idVenta,
                                       @ApiParam(name = "ids", value = "Lista de cotizaciones que se desean incluir", required = false) @RequestParam(required = false) List<String> ids
    ,  @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        Map<String, Object> map = ventasService.getImpresionCotizacion(idVenta, ids);
        ByteArrayInputStream bArrayInputStream = new ByteArrayInputStream((byte[]) map.get("bytes"));

        ContentDisposition contentDisposition = ContentDisposition.builder("inline")
                .filename((String)map.get("filename"))
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentDisposition(contentDisposition);

        return ResponseEntity.ok().headers(headers).body(new InputStreamResource(bArrayInputStream));
    }

    @GetMapping(value="/{idVenta}/poliza.pdf", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ApiOperation(value = "Descargar la póliza en PDF")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion exitosa"),
            @ApiResponse(code = 400, message = "Errónea petición"),
            @ApiResponse(code = 404, message = "Recurso no encontrado"),
            @ApiResponse(code = 502, message = "Servicio no disponible")})
    public ResponseEntity getImpresionPoliza( @ApiParam(name = "idVenta", value = "Id de la venta que se desea imprimir la poliza", required = true) @PathVariable String idVenta,
                                              @RequestHeader(name = "Authorization", required = false) String authHeader){
        this.keycloakService.validateJwt(authHeader);
        Map<String, Object> map = ventasService.getImpresionPoliza(idVenta);
        ByteArrayInputStream bArrayInputStream = new ByteArrayInputStream((byte[]) map.get("bytes"));

        ContentDisposition contentDisposition = ContentDisposition.builder("inline")
                .filename((String)map.get("filename"))
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentDisposition(contentDisposition);

        return ResponseEntity.ok().headers(headers).body(new InputStreamResource(bArrayInputStream));
    }
}
