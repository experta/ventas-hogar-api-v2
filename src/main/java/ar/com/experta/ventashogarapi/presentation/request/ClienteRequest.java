package ar.com.experta.ventashogarapi.presentation.request;

import ar.com.experta.ventashogarapi.core.model.*;
import ar.com.experta.ventashogarapi.core.model.enums.CondicionImpositiva;
import ar.com.experta.ventashogarapi.core.model.enums.Sexo;
import ar.com.experta.ventashogarapi.core.model.enums.TipoDocumento;
import ar.com.experta.ventashogarapi.core.model.enums.TipoPersona;
import ar.com.experta.ventashogarapi.presentation.validations.Age18;
import ar.com.experta.ventashogarapi.presentation.validations.Numeric;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class ClienteRequest {

    public static class DocumentoClienteRequest {

        @NotNull (message = "El tipo de documento no puede ser nulo")
        private TipoDocumento tipo;
        @NotBlank (message = "El número de documento no puede estar vacío")
        @Numeric(message = "El número de documento debe ser numérico")
        private String numero;

        public DocumentoClienteRequest() {
        }

        public TipoDocumento getTipo() {
            return tipo;
        }

        public void setTipo(TipoDocumento tipo) {
            this.tipo = tipo;
        }

        public String getNumero() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero = numero;
        }
    }

    public static class LocalizacionRequest{
        @NotBlank (message = "El id de localización no puede ser nulo")
        private String id;

        public LocalizacionRequest() {
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class DireccionClienteRequest {

        private String calle;
        private String numero;
        private String piso;
        private String departamento;
        @Valid
        @NotNull (message = "La localizacion no puede ser nula")
        private LocalizacionRequest localizacion;

        public DireccionClienteRequest() {
        }

        public String getCalle() {
            return calle;
        }

        public void setCalle(String calle) {
            this.calle = calle;
        }

        public String getNumero() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero = numero;
        }

        public String getPiso() {
            return piso;
        }

        public void setPiso(String piso) {
            this.piso = piso;
        }

        public String getDepartamento() {
            return departamento;
        }

        public void setDepartamento(String departamento) {
            this.departamento = departamento;
        }

        public LocalizacionRequest getLocalizacion() {
            return localizacion;
        }

        public void setLocalizacion(LocalizacionRequest localizacion) {
            this.localizacion = localizacion;
        }
    }

    public static class TelefonoClienteRequest {

        @NotBlank (message = "El prefijo del teléfono del cliente no puede estar vacío")
        @Numeric (message = "El prefijo del teléfono del cliente  debe ser numérico")
        private String prefijo;
        @NotBlank (message = "El número del teléfono del cliente no puede estar vacío")
        @Numeric (message = "El número del teléfono del cliente  debe ser numérico")
        private String numero;

        public TelefonoClienteRequest() {
        }

        public String getPrefijo() {
            return prefijo;
        }

        public void setPrefijo(String prefijo) {
            this.prefijo = prefijo;
        }

        public String getNumero() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero = numero;
        }
    }

    @Valid
    private DocumentoClienteRequest documento;
    private String nombre;
    private String apellido;
    private String razonSocial;
    private String email;
    @NotNull (message = "El tipo de persona no puede ser nulo")
    private TipoPersona tipoPersona;
    @NotNull (message = "La condición del IVA no puede ser nulo")
    private CondicionImpositiva condicionImpositiva;
    @Valid
    private TelefonoClienteRequest telefono;
    private String telefonoTexto;
    @Valid
    private DireccionClienteRequest direccion;
    private String nacionalidad;
    @Age18(message = "El tomador de la póliza debe ser mayor de edad")
    private LocalDate fechaNacimiento;
    private Sexo sexo;
    private String actividad;
    private Boolean esPep;
    private String motivoPep;
    private Boolean sujetoObligado;

    public Cliente toCliente() {
//        if (this.tipoPersona.equals(TipoPersona.FISICA) && (this.nombre != null || this.apellido != null) && (this.nombre == null || this.apellido == null))
//            throw new NombreClienteBadRequestException();
//        if (this.tipoPersona.equals(TipoPersona.JURIDICA) && (this.razonSocial == null))
//            throw new RazonSocialClienteValidationException();

        return new Cliente( documento != null ? new Documento(documento.getTipo(), documento.getNumero()) : null,
                            nombre != null && nombre.isEmpty() ? null : nombre,
                            apellido != null && apellido.isEmpty() ? null : apellido,
                            razonSocial != null && razonSocial.isEmpty() ? null : razonSocial,
                            email != null && email.isEmpty() ? null : email,
                            nacionalidad,
                            fechaNacimiento,
                            sexo,
                            actividad,
                            esPep,
                            motivoPep != null && motivoPep.isEmpty() ? null : motivoPep,
                            sujetoObligado,
                            telefono != null ? new Telefono(telefono.getPrefijo(),telefono.getNumero()) : null,
                            telefonoTexto,
                            direccion != null ? new Direccion(this.direccion.getCalle() != null && this.direccion.getCalle().isEmpty() ? null : this.direccion.getCalle(),
                                                                this.direccion.getNumero() != null && this.direccion.getNumero().isEmpty() ? null : this.direccion.getNumero(),
                                                                this.direccion.getPiso() != null && this.direccion.getPiso().isEmpty() ? null : this.direccion.getPiso(),
                                                                this.direccion.getDepartamento() != null && this.direccion.getDepartamento().isEmpty() ? null : this.direccion.getDepartamento(),
                                                                new Localizacion(this.direccion.getLocalizacion().getId())) : null,
                            tipoPersona,
                            condicionImpositiva);
    }

    public ClienteRequest() {
    }

    public void setDocumento(DocumentoClienteRequest documento) {
        this.documento = documento;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelefono(TelefonoClienteRequest telefono) {
        this.telefono = telefono;
    }

    public void setDireccion(DireccionClienteRequest direccion) {
        this.direccion = direccion;
    }

    public void setCondicionImpositiva(CondicionImpositiva condicionImpositiva) {
        this.condicionImpositiva = condicionImpositiva;
    }

    public DocumentoClienteRequest getDocumento() {
        return documento;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public String getEmail() {
        return email;
    }

    public TelefonoClienteRequest getTelefono() {
        return telefono;
    }

    public DireccionClienteRequest getDireccion() {
        return direccion;
    }

    public TipoPersona getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(TipoPersona tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public CondicionImpositiva getCondicionImpositiva() {
        return condicionImpositiva;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public Boolean getEsPep() {
        return esPep;
    }

    public void setEsPep(Boolean esPep) {
        this.esPep = esPep;
    }

    public String getMotivoPep() {
        return motivoPep;
    }

    public void setMotivoPep(String motivoPep) {
        this.motivoPep = motivoPep;
    }

    public Boolean getSujetoObligado() {
        return sujetoObligado;
    }

    public void setSujetoObligado(Boolean sujetoObligado) {
        this.sujetoObligado = sujetoObligado;
    }

    public String getTelefonoTexto() {
        return telefonoTexto;
    }

    public void setTelefonoTexto(String telefonoTexto) {
        this.telefonoTexto = telefonoTexto;
    }
}
