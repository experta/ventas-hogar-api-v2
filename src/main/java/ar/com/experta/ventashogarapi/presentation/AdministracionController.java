package ar.com.experta.ventashogarapi.presentation;

import ar.com.experta.ventashogarapi.core.AdministracionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/administracion")
@Api(tags="Administración", description="Endpoints para administración interna de la API")
public class AdministracionController {

    @Autowired
    private AdministracionService administracionService;

    @DeleteMapping("/flush-cache-enlatados")
    @ApiOperation(value = "Limpiar el cache de enlatados para que se refresquen los precios")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity flushCacheEnlatados(){
        administracionService.flushCacheEnlatados();
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/flush-cache-porcentajes")
    @ApiOperation(value = "Limpiar el cache de porcentajes de actualizacion y ajuste")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operacion OK"),
            @ApiResponse(code = 401, message = "Permismos insuficientes"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity flushCachePorcentajes(){
        administracionService.flushCachePorcentajes();
        return ResponseEntity.noContent().build();
    }
}
