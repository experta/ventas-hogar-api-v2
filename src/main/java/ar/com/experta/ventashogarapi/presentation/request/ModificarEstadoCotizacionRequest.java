package ar.com.experta.ventashogarapi.presentation.request;

import ar.com.experta.ventashogarapi.core.model.enums.EstadoCotizacion;

public class ModificarEstadoCotizacionRequest {

    private EstadoCotizacion estado;

    public ModificarEstadoCotizacionRequest() {
    }

    public ModificarEstadoCotizacionRequest(EstadoCotizacion estado) {
        this.estado = estado;
    }

    public EstadoCotizacion getEstado() {
        return estado;
    }

    public void setEstado(EstadoCotizacion estado) {
        this.estado = estado;
    }
}
