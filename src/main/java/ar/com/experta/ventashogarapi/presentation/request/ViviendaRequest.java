package ar.com.experta.ventashogarapi.presentation.request;

import ar.com.experta.ventashogarapi.core.model.Direccion;
import ar.com.experta.ventashogarapi.core.model.Localizacion;
import ar.com.experta.ventashogarapi.core.model.Vivienda;
import ar.com.experta.ventashogarapi.core.model.enums.TipoVivienda;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class ViviendaRequest {

    public static class LocalizacionRequest{
        @NotNull (message = "El id de localización no puede ser nulo")
        private String id;

        public LocalizacionRequest() {
        }

        public LocalizacionRequest(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public class DireccionViviendaRequest {

        private String calle;
        private String numero;
        private String piso;
        private String departamento;
        @Valid
        @NotNull (message = "La localizacion no puede ser nula")
        private LocalizacionRequest localizacion;

        public DireccionViviendaRequest() {
        }

        public DireccionViviendaRequest(String calle, String numero, String piso, String departamento, LocalizacionRequest localizacion) {
            this.calle = calle;
            this.numero = numero;
            this.piso = piso;
            this.departamento = departamento;
            this.localizacion = localizacion;
        }

        public String getCalle() {
            return calle;
        }

        public void setCalle(String calle) {
            this.calle = calle;
        }

        public String getNumero() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero = numero;
        }

        public String getPiso() {
            return piso;
        }

        public void setPiso(String piso) {
            this.piso = piso;
        }

        public String getDepartamento() {
            return departamento;
        }

        public void setDepartamento(String departamento) {
            this.departamento = departamento;
        }

        public LocalizacionRequest getLocalizacion() {
            return localizacion;
        }

        public void setLocalizacion(LocalizacionRequest localizacion) {
            this.localizacion = localizacion;
        }
    }

    @NotNull (message = "El tipo de vivienda no puede ser nulo")
    private TipoVivienda tipoVivienda;
    private Boolean direccionTomador;
    @Valid
    @NotNull (message = "La dirección no puede ser nula")
    private DireccionViviendaRequest direccion;
    private Boolean tieneRejas;
    private Integer metrosCuadrados;

    public ViviendaRequest() {
    }

    public ViviendaRequest(@NotNull TipoVivienda tipoVivienda, Boolean direccionTomador, DireccionViviendaRequest direccion, Boolean tieneRejas, Integer metrosCuadrados) {
        this.tipoVivienda = tipoVivienda;
        this.direccionTomador = direccionTomador;
        this.direccion = direccion;
        this.tieneRejas = tieneRejas;
        this.metrosCuadrados = metrosCuadrados;
    }

    public Vivienda toVivienda() {
        Direccion direccion = new Direccion(this.direccion.getCalle() != null && this.direccion.getCalle().isEmpty() ? null : this.direccion.getCalle(),
                                            this.direccion.getNumero() != null && this.direccion.getNumero().isEmpty() ? null : this.direccion.getNumero(),
                                            this.direccion.getPiso() != null && this.direccion.getPiso().isEmpty() ? null : this.direccion.getPiso(),
                                            this.direccion.getDepartamento() != null && this.direccion.getDepartamento().isEmpty() ? null : this.direccion.getDepartamento(),
                                            new Localizacion(this.direccion.getLocalizacion().getId()));

        return new Vivienda (   tipoVivienda,
                                direccionTomador,
                                direccion,
                                tieneRejas,
                                metrosCuadrados);

    }

    public void setTipoVivienda(TipoVivienda tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    public void setDireccionTomador(Boolean direccionTomador) {
        this.direccionTomador = direccionTomador;
    }

    public void setDireccion(DireccionViviendaRequest direccion) {
        this.direccion = direccion;
    }

    public void setTieneRejas(Boolean tieneRejas) {
        this.tieneRejas = tieneRejas;
    }

    public TipoVivienda getTipoVivienda() {
        return tipoVivienda;
    }

    public Boolean getDireccionTomador() {
        return direccionTomador;
    }

    public DireccionViviendaRequest getDireccion() {
        return direccion;
    }

    public Boolean getTieneRejas() {
        return tieneRejas;
    }

    public Integer getMetrosCuadrados() {
        return metrosCuadrados;
    }

    public void setMetrosCuadrados(Integer metrosCuadrados) {
        this.metrosCuadrados = metrosCuadrados;
    }
}
