package ar.com.experta.ventashogarapi.presentation;


import ar.com.experta.ventashogarapi.core.KeycloakService;
import ar.com.experta.ventashogarapi.presentation.request.RequestKeycloakLogin;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


@Validated
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("auth")
@Api(tags = "Auth", description = "Endpoints para login de ART")
public class LoginController {

    @Autowired
    private KeycloakService keycloakService;

    @PostMapping("/login")
    @ApiOperation(value = "Login de usuario en keycloakclient")
    @ApiResponses(value = {
            @ApiResponse(code = 502, message = "Servicio no disponible"),
            @ApiResponse(code = 200, message = "Login exitoso"),
            @ApiResponse(code = 401, message = "Login fallido")})
    public ResponseEntity<?> login(@RequestBody @Valid @NotNull RequestKeycloakLogin requestKeycloakLogin) {
        return ResponseEntity.ok(keycloakService.login(requestKeycloakLogin.getUsername(), requestKeycloakLogin.getPassword()));

    }


    @GetMapping("/refresh")
    @ApiOperation(value = "Refresh token en keycloakclient")
    @ApiResponses(value = {
            @ApiResponse(code = 502, message = "Servicio no disponible"),
            @ApiResponse(code = 200, message = "Login exitoso"),
            @ApiResponse(code = 401, message = "Login fallido")})
    public ResponseEntity<?> refreshToken(@RequestHeader("Authorization") String token) {
        return ResponseEntity.ok(keycloakService.refreshToken(token));

    }
}
