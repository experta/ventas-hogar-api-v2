package ar.com.experta.ventashogarapi.presentation.request;

import ar.com.experta.ventashogarapi.core.model.BienEspecifico;
import ar.com.experta.ventashogarapi.core.model.Especifico;
import ar.com.experta.ventashogarapi.core.model.Personal;
import ar.com.experta.ventashogarapi.core.model.enums.TipoBienEspecifico;
import ar.com.experta.ventashogarapi.core.model.enums.TipoCobertura;
import ar.com.experta.ventashogarapi.presentation.validations.Age18;
import ar.com.experta.ventashogarapi.presentation.validations.NotAge65;
import ar.com.experta.ventashogarapi.presentation.validations.Numeric;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.ResolvableDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EspecificosRequest {

    public class BienEspecificoRequest{
        @NotNull (message = "El tipo de cobertura no puede ser nulo")
        private TipoCobertura tipoCobertura;
        @NotNull (message = "El monto asegurado del bien a asegurar no puede ser nulo")
        private BigDecimal montoAsegurado;
        @NotNull (message = "El tipo de bien a asegurar no puede ser nulo")
        private TipoBienEspecifico tipoBienEspecifico;
        @NotBlank (message = "El modelo del bien a asegurar no puede estar vacío")
        @Size(max = 50, message = "El modelo del bien a asegurar puede tener como máximo 50 caracteres")
        private String modelo;
        @NotBlank (message = "El número de serie del bien a asegurar no puede estar vacío")
        @Size(max = 50, message = "El número de serie del bien a asegurar puede tener como máximo 50 caracteres")
        private String numeroSerie;

        public BienEspecificoRequest() {
        }

        public BienEspecificoRequest(@NotNull TipoCobertura tipoCobertura, @NotNull BigDecimal montoAsegurado, @NotNull TipoBienEspecifico tipoBienEspecifico, @NotBlank String modelo, @NotBlank String numeroSerie) {
            this.tipoCobertura = tipoCobertura;
            this.montoAsegurado = montoAsegurado;
            this.tipoBienEspecifico = tipoBienEspecifico;
            this.modelo = modelo;
            this.numeroSerie = numeroSerie;
        }

        public TipoCobertura getTipoCobertura() {
            return tipoCobertura;
        }

        public void setTipoCobertura(TipoCobertura tipoCobertura) {
            this.tipoCobertura = tipoCobertura;
        }

        public String getModelo() {
            return modelo;
        }

        public void setModelo(String modelo) {
            this.modelo = modelo;
        }

        public String getNumeroSerie() {
            return numeroSerie;
        }

        public void setNumeroSerie(String numeroSerie) {
            this.numeroSerie = numeroSerie;
        }

        public BigDecimal getMontoAsegurado() {
            return montoAsegurado;
        }

        public void setMontoAsegurado(BigDecimal montoAsegurado) {
            this.montoAsegurado = montoAsegurado;
        }

        public TipoBienEspecifico getTipoBienEspecifico() {
            return tipoBienEspecifico;
        }

        public void setTipoBienEspecifico(TipoBienEspecifico tipoBienEspecifico) {
            this.tipoBienEspecifico = tipoBienEspecifico;
        }
    }

    public static final class BienEspecificoRequestDeserializer extends StdDeserializer<BienEspecificoRequest> implements ResolvableDeserializer {
        private JsonDeserializer<Object> underlyingDeserializer;

        public BienEspecificoRequestDeserializer() {
            super(BienEspecificoRequest.class);
        }

        @Override
        public void resolve(DeserializationContext ctxt) throws JsonMappingException {
            underlyingDeserializer = ctxt
                    .findRootValueDeserializer(ctxt.getTypeFactory().constructType(BienEspecificoRequest.class));
        }

        @Override
        public BienEspecificoRequest deserialize(JsonParser p, DeserializationContext ctxt)
                throws IOException, JsonProcessingException {
            JsonStreamContext ourContext = p.getParsingContext();
            JsonStreamContext listContext = ourContext.getParent();
            JsonStreamContext containerContext = listContext.getParent();
            EspecificosRequest container = (EspecificosRequest) containerContext.getCurrentValue();
            BienEspecificoRequest value = container.new BienEspecificoRequest();
            // note use of three-argument deserialize method to specify instance to populate
            underlyingDeserializer.deserialize(p, ctxt, value);
            return value;
        }
    }

    public class PersonalRequest{
        @NotNull (message = "El tipo de cobertura no puede ser nulo")
        private TipoCobertura tipoCobertura;
        @NotNull (message = "El monto asegurado del bien a asegurar no puede ser nulo")
        private BigDecimal montoAsegurado;
        @NotBlank (message = "El DNI de la persona a asegurar no puede estar vacío")
        @Numeric (message = "El DNI de la persona debe ser un valor numérico")
        private String dni;
        @NotBlank (message = "El nombre de la persona a asegurar no puede estar vacío")
        private String nombre;
        @NotBlank (message = "El apellido de la persona a asegurar no puede estar vacío")
        private String apellido;
        @Age18 (message = "La persona a asegurar debe tener como mínimo 18 años de edad")
        @NotAge65 (message = "La persona a asegurar debe tener como máximo 65 años de edad")
        private LocalDate fechaNacimiento;

        public PersonalRequest() {
        }

        public PersonalRequest(@NotNull TipoCobertura tipoCobertura, @NotNull BigDecimal montoAsegurado, @NotBlank String dni, @NotBlank String nombre, @NotBlank String apellido, LocalDate fechaNacimiento) {
            this.tipoCobertura = tipoCobertura;
            this.montoAsegurado = montoAsegurado;
            this.dni = dni;
            this.nombre = nombre;
            this.apellido = apellido;
            this.fechaNacimiento = fechaNacimiento;
        }

        public TipoCobertura getTipoCobertura() {
            return tipoCobertura;
        }

        public void setTipoCobertura(TipoCobertura tipoCobertura) {
            this.tipoCobertura = tipoCobertura;
        }

        public BigDecimal getMontoAsegurado() {
            return montoAsegurado;
        }

        public void setMontoAsegurado(BigDecimal montoAsegurado) {
            this.montoAsegurado = montoAsegurado;
        }

        public String getDni() {
            return dni;
        }

        public void setDni(String dni) {
            this.dni = dni;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getApellido() {
            return apellido;
        }

        public void setApellido(String apellido) {
            this.apellido = apellido;
        }

        public LocalDate getFechaNacimiento() {
            return fechaNacimiento;
        }

        public void setFechaNacimiento(LocalDate fechaNacimiento) {
            this.fechaNacimiento = fechaNacimiento;
        }
    }

    public static final class PersonalRequestDeserializer extends StdDeserializer<PersonalRequest> implements ResolvableDeserializer {
        private JsonDeserializer<Object> underlyingDeserializer;

        public PersonalRequestDeserializer() {
            super(PersonalRequest.class);
        }

        @Override
        public void resolve(DeserializationContext ctxt) throws JsonMappingException {
            underlyingDeserializer = ctxt
                    .findRootValueDeserializer(ctxt.getTypeFactory().constructType(PersonalRequest.class));
        }

        @Override
        public PersonalRequest deserialize(JsonParser p, DeserializationContext ctxt)
                throws IOException, JsonProcessingException {
            JsonStreamContext ourContext = p.getParsingContext();
            JsonStreamContext listContext = ourContext.getParent();
            JsonStreamContext containerContext = listContext.getParent();
            EspecificosRequest container = (EspecificosRequest) containerContext.getCurrentValue();
            PersonalRequest value = container.new PersonalRequest();
            // note use of three-argument deserialize method to specify instance to populate
            underlyingDeserializer.deserialize(p, ctxt, value);
            return value;
        }
    }

    @JsonDeserialize(contentUsing = BienEspecificoRequestDeserializer.class)
    @Valid
    List<BienEspecificoRequest> bienesEspecificos;
    @JsonDeserialize(contentUsing = PersonalRequestDeserializer.class)
    @Valid
    List<PersonalRequest> personal;

    public EspecificosRequest() {
    }

    public EspecificosRequest(List<BienEspecificoRequest> bienesEspecificos, List<PersonalRequest> personal) {
        this.bienesEspecificos = bienesEspecificos;
        this.personal = personal;
    }

    public List<BienEspecificoRequest> getBienesEspecificos() {
        return bienesEspecificos;
    }

    public void setBienesEspecificos(List<BienEspecificoRequest> bienesEspecificos) {
        this.bienesEspecificos = bienesEspecificos;
    }

    public List<PersonalRequest> getPersonal() {
        return personal;
    }

    public void setPersonal(List<PersonalRequest> personal) {
        this.personal = personal;
    }

    public List<Especifico> toEspecificos(){

        List<Especifico> especificosList;

        if (this.bienesEspecificos != null) {
            especificosList = this.bienesEspecificos.stream()
                                                    .map(bienEspecifico -> new BienEspecifico(bienEspecifico.getTipoCobertura(),
                                                            bienEspecifico.getMontoAsegurado(),
                                                            bienEspecifico.getTipoBienEspecifico(),
                                                            bienEspecifico.getModelo() != null && bienEspecifico.getModelo().isEmpty() ? null : bienEspecifico.getModelo(),
                                                            bienEspecifico.getNumeroSerie() != null && bienEspecifico.getNumeroSerie().isEmpty() ? null : bienEspecifico.getNumeroSerie()))
                                                    .collect(Collectors.toList());
        }else{
            especificosList = new ArrayList<>();
        }

        if (this.personal != null) {
            List<Especifico> personalList = this.personal.stream()
                    .map(persona -> new Personal(persona.getTipoCobertura(),
                            persona.getMontoAsegurado(),
                            persona.getDni(),
                            persona.getNombre(),
                            persona.getApellido(),
                            persona.getFechaNacimiento()))
                    .collect(Collectors.toList());
            especificosList.addAll(personalList);
        }

        return especificosList;
    }

}
