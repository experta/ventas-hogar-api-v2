package ar.com.experta.ventashogarapi.presentation.request;

import ar.com.experta.ventashogarapi.core.model.enums.Beneficio;

import javax.validation.constraints.NotNull;

public class BeneficioRequest {

    @NotNull (message = "El beneficio no puede ser nulo")
    private Beneficio beneficio;

    public BeneficioRequest() {
    }

    public BeneficioRequest(@NotNull(message = "El beneficio no puede ser nulo") Beneficio beneficio) {
        this.beneficio = beneficio;
    }

    public Beneficio getBeneficio() {
        return beneficio;
    }

    public void setBeneficio(Beneficio beneficio) {
        this.beneficio = beneficio;
    }

}
