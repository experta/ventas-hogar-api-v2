package ar.com.experta.ventashogarapi.presentation.request;

import ar.com.experta.ventashogarapi.presentation.validations.FutureMonth;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class FechaRequest {

    @NotNull (message = "La fecha de inicio de cobertura no puede ser nula")
    @FutureMonth(message = "La fecha de inicio de cobertura debe ser posterior a hoy, hasta un mes")
    private LocalDate fechaInicioCobertura;

    public FechaRequest() {
    }

    public FechaRequest(LocalDate fechaInicioCobertura) {
        this.fechaInicioCobertura = fechaInicioCobertura;
    }

    public LocalDate getFechaInicioCobertura() {
        return fechaInicioCobertura;
    }

    public void setFechaInicioCobertura(LocalDate fechaInicioCobertura) {
        this.fechaInicioCobertura = fechaInicioCobertura;
    }
}
