package ar.com.experta.ventashogarapi.presentation.request;

import ar.com.experta.ventashogarapi.core.model.Vendedor;
import ar.com.experta.ventashogarapi.presentation.validations.Numeric;

import javax.validation.constraints.NotBlank;

public class VendedorRequest {
    @NotBlank(message = "El id del vendedor no puede estar vacío")
    @Numeric(message = "El id del vendedor no es un valor numérico válido")
    private String id;

    public VendedorRequest() {}

    public VendedorRequest(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Vendedor toVendedor() {
        return new Vendedor(id);
    }
}
