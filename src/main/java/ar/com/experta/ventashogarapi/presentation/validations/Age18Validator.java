package ar.com.experta.ventashogarapi.presentation.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.Period;

public class Age18Validator implements ConstraintValidator<Age18, Object> {
    private Age18 age18;
    private final static int AGE = 18;

    @Override
    public void initialize(Age18 constraintAnnotation) {
        this.age18 = constraintAnnotation;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        // Check the state of the Adminstrator.
        if (value == null) {
            return true;
        }

        // Initialize it.
        LocalDate date = LocalDate.parse(String.valueOf(value));
        return Period.between(date, LocalDate.now()).getYears() >= AGE;
    }

}