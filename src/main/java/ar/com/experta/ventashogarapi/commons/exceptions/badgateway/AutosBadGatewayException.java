package ar.com.experta.ventashogarapi.commons.exceptions.badgateway;

public class AutosBadGatewayException extends BadGatewayException {
    public AutosBadGatewayException(Exception exception) {
        super("No se pudo validar si el cliente tiene póliza de Autos. Intente nuevamente mas tarde. Disculpe las molestias", exception);
    }
}
