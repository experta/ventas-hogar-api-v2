package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class FaltanCoberturasObligatoriasException extends BadRequestException {
    public FaltanCoberturasObligatoriasException(String coberturasFaltantes) {
        super("Las siguientes coberturas no se encuentran y son obligatorias: " + coberturasFaltantes);
    }
}
