package ar.com.experta.ventashogarapi.commons.exceptions.notfound;

import ar.com.experta.ventashogarapi.commons.exceptions.badrequest.BadRequestException;

public class VendedorNotFoundException extends BadRequestException {
    public VendedorNotFoundException() {
        super("El vendedor no existe");
    }
}
