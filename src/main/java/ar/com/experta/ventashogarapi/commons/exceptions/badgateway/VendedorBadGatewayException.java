package ar.com.experta.ventashogarapi.commons.exceptions.badgateway;

public class VendedorBadGatewayException extends BadGatewayException {

    public VendedorBadGatewayException(Exception exception) {
        super(null, "No se pudo recuperar la información del vendedor. " + exception.getLocalizedMessage());
    }
}
