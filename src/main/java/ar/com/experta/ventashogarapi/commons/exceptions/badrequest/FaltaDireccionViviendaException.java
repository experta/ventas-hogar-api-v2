package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;


public class FaltaDireccionViviendaException extends BadRequestException {

    public static final String MENSAJE = "La dirección de la vivienda es obligatoria";

    public FaltaDireccionViviendaException() {
        super(MENSAJE);
    }
}
