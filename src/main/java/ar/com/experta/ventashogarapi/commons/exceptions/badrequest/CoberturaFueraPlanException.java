package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class CoberturaFueraPlanException extends BadRequestException {
    public CoberturaFueraPlanException(String cobertura) {
        super("La cobertura " + cobertura + " es invalida para el plan a cotizar");
    }
}
