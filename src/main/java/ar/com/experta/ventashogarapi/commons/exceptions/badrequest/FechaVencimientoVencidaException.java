package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class FechaVencimientoVencidaException extends BadRequestException {
    public FechaVencimientoVencidaException() {
        super("La fecha de vencimiento de la tarjeta debe ser igual o mayor a la fecha actual");
    }
}
