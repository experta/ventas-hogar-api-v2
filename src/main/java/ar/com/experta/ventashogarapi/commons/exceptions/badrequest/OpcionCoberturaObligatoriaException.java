package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class OpcionCoberturaObligatoriaException extends BadRequestException {
    public OpcionCoberturaObligatoriaException(String cobertura) {
        super("Es obligatorio que para la cobertura " + cobertura + " se seleccione alguna de las opciones");
    }
}
