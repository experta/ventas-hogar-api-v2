package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class RefreshJWTIncorrectoException extends BadRequestException {
    public RefreshJWTIncorrectoException() {
        super("Refresh token invalido o vencido");
    }
}
