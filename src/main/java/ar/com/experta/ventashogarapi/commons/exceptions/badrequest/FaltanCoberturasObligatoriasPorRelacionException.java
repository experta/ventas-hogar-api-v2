package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class FaltanCoberturasObligatoriasPorRelacionException extends BadRequestException {

    public FaltanCoberturasObligatoriasPorRelacionException(String coberturaFaltante, String coberturaRelacionada) {
        super("No se puede cotizar.  La cobertura " + coberturaFaltante + " debe estar obligatoriamente si se cotiza la cobertura " + coberturaRelacionada);
    }
}
