package ar.com.experta.ventashogarapi.commons.exceptions;

import java.util.List;

public class ConflictException extends RuntimeException {

    private List<String> messages;

    public ConflictException(List<String> messages) {
        super();
        this.messages = messages;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

}
