package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

import java.math.BigDecimal;

public class CoberturaFueraRangoRelacionadoException extends BadRequestException {
    public CoberturaFueraRangoRelacionadoException(String cobertura, String relacionada, BigDecimal porcentajeMinimoRelacionado, BigDecimal porcentajeMaximoRelacionado, BigDecimal montoMinimoRelacionado, BigDecimal montoMaximoRelacionado) {
        super("El monto de la cobertura " + cobertura +
                " debe estar entre el " + porcentajeMinimoRelacionado + "% y " + porcentajeMaximoRelacionado + "% de la cobertura " + relacionada +
                " (entre $" + montoMinimoRelacionado + " y $" + montoMaximoRelacionado + ")");
    }
}
