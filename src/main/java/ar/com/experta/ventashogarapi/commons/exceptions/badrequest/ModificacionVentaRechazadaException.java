package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class ModificacionVentaRechazadaException extends BadRequestException {

    public enum MotivoRechazo {
        VENTA_EMITIDA("No se puede realizar la modificación ya que la venta se encuentra emitida"),
        COTIZACION_VENCIDA_NO_VALIDA ("No se puede modificar la cotizacion a estado VENCIDA"),
        VENTA_A_EMITIR_PRECIO ("No se pude modificar el tipo de vivienda o la ubicación de la misma una vez iniciado el proceso de emisión");

        private String message;

        MotivoRechazo(String message) {
            this.message = message;
        }

        public String message() {
            return message;
        }
    }

    public ModificacionVentaRechazadaException(MotivoRechazo motivoRechazo) {
        super(motivoRechazo.message);
    }
}
