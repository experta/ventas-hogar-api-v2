package ar.com.experta.ventashogarapi.commons.exceptions.badgateway;

public class NominaGrupoWBadGatewayException extends BadGatewayException {

    public static final String MENSAJE = "No se pudo verificar que el cliente pertenezca a la nómina del Grupo Werthein. Intente nuevamente más tarde. Disculpa las molestias";

    public NominaGrupoWBadGatewayException(Exception exception) {
        super(MENSAJE, exception.getMessage());
    }
}
