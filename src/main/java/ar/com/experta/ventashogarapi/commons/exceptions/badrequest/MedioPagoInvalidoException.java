package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

import ar.com.experta.ventashogarapi.core.model.enums.MedioPago;
import ar.com.experta.ventashogarapi.core.model.enums.TipoMedioPago;

public class MedioPagoInvalidoException extends BadRequestException {

    public MedioPagoInvalidoException(MedioPago medioPago, TipoMedioPago tipoMedioPago) {
        super("El medio de pago " + medioPago.label() + "no es un medio de pago permitido para la venta. Debe ser un medio de pago del tipo " + tipoMedioPago.label());
    }
}
