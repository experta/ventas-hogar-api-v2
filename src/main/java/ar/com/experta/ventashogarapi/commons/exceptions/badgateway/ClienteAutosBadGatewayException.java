package ar.com.experta.ventashogarapi.commons.exceptions.badgateway;

public class ClienteAutosBadGatewayException extends BadGatewayException {

    public static final String MENSAJE = "No se pudo verificar que el cliente tiene póliza de autos vigente. Intente nuevamente mas tarde. Disculpe las molestias";

    public ClienteAutosBadGatewayException(Exception exception) {
        super(MENSAJE, exception.getMessage());
    }
}
