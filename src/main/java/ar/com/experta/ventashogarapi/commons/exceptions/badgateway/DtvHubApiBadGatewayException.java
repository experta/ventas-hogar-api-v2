package ar.com.experta.ventashogarapi.commons.exceptions.badgateway;

public class DtvHubApiBadGatewayException extends BadGatewayException {

    public DtvHubApiBadGatewayException(Exception ex) {
        super(null, ex.getMessage());
    }

    public DtvHubApiBadGatewayException(String error) {
        super(null, error);
    }
}
