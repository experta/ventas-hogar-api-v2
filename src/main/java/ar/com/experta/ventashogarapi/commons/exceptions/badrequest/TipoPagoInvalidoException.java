package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class TipoPagoInvalidoException extends BadRequestException {

    public TipoPagoInvalidoException() {
        super("Tipo de pago inválido");
    }
}
