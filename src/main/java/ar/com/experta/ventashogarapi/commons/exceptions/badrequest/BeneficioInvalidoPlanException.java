package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

import ar.com.experta.ventashogarapi.core.model.enums.Beneficio;
import ar.com.experta.ventashogarapi.core.model.enums.TipoPlan;

public class BeneficioInvalidoPlanException extends BadRequestException {
    public BeneficioInvalidoPlanException(Beneficio beneficio, TipoPlan tipoPlan) {
        super("El beneficio " + beneficio.label() + " es inválido para el tipo de plan " + tipoPlan.label());
    }
}
