package ar.com.experta.ventashogarapi.commons.exceptions.badgateway;

public class NegocioBadGatewayException extends BadGatewayException {

    public NegocioBadGatewayException(Exception exception) {
        super("No se pudo recuperar el objeto de negocio. Intente nuevamente más tarde. Disculpe las molestias", exception);
    }

}
