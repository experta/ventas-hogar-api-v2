package ar.com.experta.ventashogarapi.commons.exceptions.notfound;

public class PolizaNotFoundException extends NotFoundException {

    public PolizaNotFoundException() {
        super("No se pudo encontrar la impresion del contrato. Disculpe las molestias. Intente mas tarde");
    }

}
