package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class PagoCreditoValidationException extends BadRequestException {

    public enum MotivoRechazo {
        DATOS_INCOMPLETOS("Para el método de pago Crédito es obligatorio ingresar la tarjeta, el titular y la fecha de vencimiento de la misma"),
        NUMERO_DIGITO_INCOMPLETO_AMEX ("El numero de la tarjeta de credito debe tener 15 dígitos"),
        NUMERO_DIGITO_INCOMPLETO ("El numero de la tarjeta de credito debe tener 16 dígitos");

        private String message;

        MotivoRechazo(String message) {
            this.message = message;
        }

        public String message() {
            return message;
        }
    }

    public PagoCreditoValidationException(MotivoRechazo motivoRechazo) {
        super(motivoRechazo.message);
    }
}
