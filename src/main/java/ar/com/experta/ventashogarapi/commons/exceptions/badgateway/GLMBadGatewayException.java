package ar.com.experta.ventashogarapi.commons.exceptions.badgateway;

public class GLMBadGatewayException extends BadGatewayException {
    public GLMBadGatewayException(Exception e) {
        super(null, e);
    }
}
