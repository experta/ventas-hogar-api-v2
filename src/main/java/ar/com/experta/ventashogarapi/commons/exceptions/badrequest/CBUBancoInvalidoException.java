package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class CBUBancoInvalidoException extends BadRequestException {
    public CBUBancoInvalidoException() {
        super("El CBU ingresado no corresponde a un banco válido");
    }
}
