package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class NominaGrupoWInvalidoException extends BadRequestException {

    public static final String MENSAJE = "El cliente no pertenece a la nómina del Grupo Werthein o no se ha completado su alta";

    public NominaGrupoWInvalidoException() {
        super(MENSAJE);
    }
}
