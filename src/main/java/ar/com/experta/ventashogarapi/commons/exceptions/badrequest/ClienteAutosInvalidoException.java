package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class ClienteAutosInvalidoException extends BadRequestException {

    public static final String MENSAJE = "El cliente no tiene póliza de autos vigente. Recuerde emitir la póliza en caso de tener una cotización de autos pendiente";

    public ClienteAutosInvalidoException() {
        super(MENSAJE);
    }
}
