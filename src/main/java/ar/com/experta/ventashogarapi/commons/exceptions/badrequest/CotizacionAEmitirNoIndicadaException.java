package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class CotizacionAEmitirNoIndicadaException extends BadRequestException {

    public CotizacionAEmitirNoIndicadaException() {
        super("No se ha seleccionado que cotizacion se desea emitir");
    }
}
