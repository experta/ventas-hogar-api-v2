package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

import ar.com.experta.ventashogarapi.core.model.enums.TipoPlan;

public class VendedorNoPlanesException extends BadRequestException {
    public VendedorNoPlanesException(TipoPlan tipoPlan) {
        super("El vendedor no tiene planes configurados para el tipo de plan: " + tipoPlan.label());
    }
}
