package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class PagoDebitoValidationException extends BadRequestException {
    public PagoDebitoValidationException() {
        super("Para Pago Débito es obligatorio ingresar el CBU y el Banco");
    }
}
