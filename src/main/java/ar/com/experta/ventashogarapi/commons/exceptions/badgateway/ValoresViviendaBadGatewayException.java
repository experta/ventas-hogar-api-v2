package ar.com.experta.ventashogarapi.commons.exceptions.badgateway;

public class ValoresViviendaBadGatewayException extends BadGatewayException {
    public ValoresViviendaBadGatewayException(Exception exception) {
        super(null, "No se pudo recuperar los valores de mercado de viviendas. " + exception.getLocalizedMessage());
    }
}
