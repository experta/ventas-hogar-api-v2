package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class RecotizandoEnlatadoException extends BadRequestException {
    public RecotizandoEnlatadoException() {
        super("Los planes enlatados son planes cerrados. No está permitido recotizarlos ni modificar las coberturas.");
    }
}
