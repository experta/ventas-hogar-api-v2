package ar.com.experta.ventashogarapi.commons.exceptions.notfound;

public class VentaNotFoundException extends NotFoundException {

    public VentaNotFoundException(String id) {
        super("No se pudo encontrar la venta con id: " + id);
    }
}
