package ar.com.experta.ventashogarapi.commons.exceptions.notfound;

import ar.com.experta.ventashogarapi.core.model.enums.IdPlan;

public class PlanInexistenteException extends NotFoundException {

    public PlanInexistenteException(IdPlan idPlan) {
        super("No se ha encontrado el plan " + idPlan);
    }
}
