package ar.com.experta.ventashogarapi.commons.exceptions;

import ar.com.experta.ventashogarapi.commons.exceptions.badrequest.BadRequestException;

public class CotizadorDeshabilatodException extends BadRequestException {
    public CotizadorDeshabilatodException() {
        super("El cotizador se encuentra momentaneamente deshabilitado por problemas técnicos. Disculpa las molestias");
    }
}
