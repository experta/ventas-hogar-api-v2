package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class CoberturasNoCumplenRelacionException extends BadRequestException {
    public CoberturasNoCumplenRelacionException(String coberturasNoCumplen) {
        super("Las siguientes coberturas no cumplen con los minimos o maximos respecto con cobertura relacionada: " + coberturasNoCumplen);

    }
}
