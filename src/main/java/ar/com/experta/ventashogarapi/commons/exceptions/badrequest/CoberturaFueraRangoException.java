package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

import java.math.BigDecimal;

public class CoberturaFueraRangoException extends BadRequestException {
    public CoberturaFueraRangoException(String cobertura, BigDecimal montoMinimoFijo, BigDecimal montoMaximoFijo) {
        super("El monto de la cobertura " + cobertura + " debe estar entre $ " + montoMaximoFijo + " y $ " + montoMaximoFijo);
    }
}
