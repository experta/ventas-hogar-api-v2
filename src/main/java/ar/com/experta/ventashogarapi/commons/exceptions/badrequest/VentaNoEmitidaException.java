package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class VentaNoEmitidaException extends BadRequestException {
    public VentaNoEmitidaException() {
        super("La venta no se encuentra en estado emitida");
    }
}
