package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class VentaEmitiendoException extends BadRequestException {
    public VentaEmitiendoException() {
        super("La venta ya se encuentra en proceso de emisión. Aguardá a que termine el proceso actual.");
    }
}
