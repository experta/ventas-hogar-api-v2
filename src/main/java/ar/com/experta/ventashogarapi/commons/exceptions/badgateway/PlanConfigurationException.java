package ar.com.experta.ventashogarapi.commons.exceptions.badgateway;

public class PlanConfigurationException extends BadGatewayException {

    public PlanConfigurationException(String detail) {
        super(null, detail);
    }

}
