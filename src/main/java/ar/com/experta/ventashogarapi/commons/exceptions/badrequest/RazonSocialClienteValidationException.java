package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class RazonSocialClienteValidationException extends BadRequestException {

    public RazonSocialClienteValidationException() {
        super("La Razon Social del cliente no puede ser nula");
    }

}
