package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class OpcionCoberturaInvalidaException extends BadRequestException {
    public OpcionCoberturaInvalidaException(String cobertura) {
        super("La opcion seleccionada para la cobertura " + cobertura + " es inválida");
    }
}
