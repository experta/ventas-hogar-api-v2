package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class SexoClienteBadRequestException extends BadRequestException {

    public SexoClienteBadRequestException() {
        super("El sexo del cliente no puede ser nulo");
    }
}
