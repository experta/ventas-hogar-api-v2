package ar.com.experta.ventashogarapi.commons;

import ar.com.experta.ventashogarapi.commons.exceptions.ConflictException;
import ar.com.experta.ventashogarapi.commons.exceptions.InternalErrorException;
import ar.com.experta.ventashogarapi.commons.exceptions.NotImplementedException;
import ar.com.experta.ventashogarapi.commons.exceptions.UnauthorizedException;
import ar.com.experta.ventashogarapi.commons.exceptions.badgateway.BadGatewayException;
import ar.com.experta.ventashogarapi.commons.exceptions.badrequest.BadRequestException;
import ar.com.experta.ventashogarapi.commons.exceptions.notfound.NotFoundException;
import ar.com.experta.ventashogarapi.commons.exceptions.unprocessable.UnprocessableEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String MESSAGGE_DEFAULT = "Ocurrió un error inesperado. Disculpá la molestia ocasionada.";
    private static final Logger logger = LoggerFactory.getLogger(ResponseEntityExceptionHandler.class);

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.info(ex.getMessage(), ex);
        String mensaje = "El parámetro '" + ex.getParameterName() + "' del tipo " + ex.getParameterType() + " es obligatorio.";
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.BAD_REQUEST.value(),
                        HttpStatus.BAD_REQUEST.getReasonPhrase(),
                        mensaje,
                        Arrays.asList(mensaje),
                        null),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);
    }

    @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
            MethodArgumentTypeMismatchException ex, WebRequest request) {
        logger.info(ex.getMessage(), ex);
        String mensaje = "El parámetro '" + ex.getName() + "' tiene formato incorrecto";
        return handleExceptionInternal(ex, new ErrorMessage(
                        Integer.valueOf(HttpStatus.BAD_REQUEST.value()),
                        HttpStatus.BAD_REQUEST.getReasonPhrase(),
                        mensaje,
                        Arrays.asList(mensaje),
                        null),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);

    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.info(ex.getMessage(), ex);
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.BAD_REQUEST.value(),
                        HttpStatus.BAD_REQUEST.getReasonPhrase(),
                        ex.getMessage(),
                        Arrays.asList(ex.getMessage()),
                        null),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);
    }

    // this code is to  @Valid when wrong request arrive
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(  MethodArgumentNotValidException ex,
                                                                    HttpHeaders headers,
                                                                    HttpStatus status,
                                                                    WebRequest request) {
        logger.info(ex.getMessage(), ex);
        List<String> errores = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.BAD_REQUEST.value(),
                        HttpStatus.BAD_REQUEST.getReasonPhrase(),
                        String.join(". ", errores),
                        errores,
                        ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);
    }

    @ExceptionHandler(value = {ConstraintViolationException.class})
    protected ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex) {
        logger.info(ex.getMessage(), ex);
        List<String> listaErrores = Arrays.stream(ex.getMessage().split("(:)|(,)")).collect(Collectors.toList());
        String errores = null;
        // Filtro los impares para eliminar el nombre del metodo de la api
        if (listaErrores.size() > 1 && listaErrores.size()%2 == 0)
            errores = IntStream.range(0,listaErrores.size())
                    .filter(i -> i%2 != 0)
                    .mapToObj(listaErrores::get)
                    .collect(Collectors.toList())
                    .stream()
                    .map(s -> s.trim())
                    .collect(Collectors.joining(", "));

        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.BAD_REQUEST.value(),
                        HttpStatus.BAD_REQUEST.getReasonPhrase(),
                        errores != null ? errores : ex.getMessage(),
                        null,
                        null),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                null);
    }

    @ExceptionHandler(value = {UnauthorizedException.class})
    protected ResponseEntity<Object> handleUnauthorizedException(UnauthorizedException ex, WebRequest request) {
        logger.info(ex.getMessage(), ex);
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.UNAUTHORIZED.value(),
                        HttpStatus.UNAUTHORIZED.getReasonPhrase(),
                        ex.getMessage(),
                        null,
                        null),
                new HttpHeaders(),
                HttpStatus.UNAUTHORIZED,
                request);
    }

    @ExceptionHandler(value = {NotImplementedException.class})
    protected ResponseEntity<Object> handleNotImplementedException(NotImplementedException ex, WebRequest request) {
        logger.info(ex.getMessage(), ex);
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.NOT_IMPLEMENTED.value(),
                        HttpStatus.NOT_IMPLEMENTED.getReasonPhrase(),
                        null,
                        null,
                        null),
                new HttpHeaders(),
                HttpStatus.NOT_IMPLEMENTED,
                request);
    }

    @ExceptionHandler(value = {InternalErrorException.class})
    protected ResponseEntity<Object> handleOutOfServiceException(InternalErrorException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.INTERNAL_SERVER_ERROR.value(),
                        HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                        MESSAGGE_DEFAULT,
                        null,
                        ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR,
                request);
    }

    @ExceptionHandler(value = {NotFoundException.class})
    protected ResponseEntity<Object> handleNotFoundException(NotFoundException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.NOT_FOUND.value(),
                        HttpStatus.NOT_FOUND.getReasonPhrase(),
                        ex.getMessage(),
                        null,
                        ex.getDetail()),
                new HttpHeaders(),
                HttpStatus.NOT_FOUND,
                request);
    }

    @ExceptionHandler(value = {BadRequestException.class})
    protected ResponseEntity<Object> handleBadRequestException(BadRequestException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.BAD_REQUEST.value(),
                        HttpStatus.BAD_REQUEST.getReasonPhrase(),
                        ex.getMessage(),
                        ex.getMessages(),
                        null),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);
    }

    @ExceptionHandler(value = {UnprocessableEntityException.class})
    protected ResponseEntity<Object> handleUnprocessableEntityException(UnprocessableEntityException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.UNPROCESSABLE_ENTITY.value(),
                        HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase(),
                        ex.getMessage(),
                        ex.getMessages(),
                        null),
                new HttpHeaders(),
                HttpStatus.CONFLICT,
                request);
    }

    @ExceptionHandler(value = {ConflictException.class})
    protected ResponseEntity<Object> handleConflictException(ConflictException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.CONFLICT.value(),
                        HttpStatus.CONFLICT.getReasonPhrase(),
                        ex.getMessage(),
                        ex.getMessages(),
                        null),
                new HttpHeaders(),
                HttpStatus.CONFLICT,
                request);
    }

    @ExceptionHandler(value = {BadGatewayException.class})
    protected ResponseEntity<Object> handleBadGatewayException(BadGatewayException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.BAD_GATEWAY.value(),
                        HttpStatus.BAD_GATEWAY.getReasonPhrase(),
                        ex.getMessage() != null ? ex.getMessage() : MESSAGGE_DEFAULT,
                        null,
                        ex.getDetail()),
                new HttpHeaders(),
                HttpStatus.BAD_GATEWAY,
                request);
    }

    @ExceptionHandler({ IllegalStateException.class })
    public ResponseEntity<Object> handleIllegalStateException(IllegalStateException ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.BAD_REQUEST.value(),
                        HttpStatus.BAD_REQUEST.getReasonPhrase(),
                        MESSAGGE_DEFAULT,
                        null,
                        ex.getLocalizedMessage()
                ),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);
    }

    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
        logger.error(ex.getMessage(), ex);
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.INTERNAL_SERVER_ERROR.value(),
                        HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                        MESSAGGE_DEFAULT,
                        null,
                        ex.getLocalizedMessage()
                ),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR,
                request);
    }


}