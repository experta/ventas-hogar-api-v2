package ar.com.experta.ventashogarapi.commons.exceptions.notfound;

public abstract class NotFoundException extends RuntimeException {

    private String detail;

    protected NotFoundException(String message) {
        super(message);
    }

    protected NotFoundException(String message, String detail) {
        super(message);
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }

}
