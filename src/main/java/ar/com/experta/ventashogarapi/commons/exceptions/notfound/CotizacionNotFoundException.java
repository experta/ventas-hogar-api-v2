package ar.com.experta.ventashogarapi.commons.exceptions.notfound;

public class CotizacionNotFoundException extends NotFoundException {

    public CotizacionNotFoundException() {
        super("La cotizacion buscada no existe");
    }
}
