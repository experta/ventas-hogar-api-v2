package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class ParametroNoNumericoException extends BadRequestException {

    public ParametroNoNumericoException(String parametro) {
        super("El parámetro " + parametro + " debe ser numérico.");
    }
}
