package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

import java.math.BigDecimal;

public class CoberturaMontoInvalidoRelacionadoException extends BadRequestException {
    public CoberturaMontoInvalidoRelacionadoException(String cobertura, String relacionada, BigDecimal porcentaje, BigDecimal monto) {
        super("El monto de la cobertura " + cobertura +
                " debe ser el " + porcentaje + "% de la cobertura " + relacionada +
                " ( $" + monto + ")");
    }
}
