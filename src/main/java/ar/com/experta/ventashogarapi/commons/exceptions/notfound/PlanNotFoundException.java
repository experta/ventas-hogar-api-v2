package ar.com.experta.ventashogarapi.commons.exceptions.notfound;

public class PlanNotFoundException extends NotFoundException {

    public PlanNotFoundException() {
        super("El plan buscado no existe");
    }
}
