package ar.com.experta.ventashogarapi.commons.exceptions.unprocessable;

public class CodigoDGONotFoudException extends UnprocessableEntityException {

    public static final String MENSAJE = "No hay codigo de DGO disponible para la emisión. Por favor contactate con tu ejecutivo comercial.";

    public CodigoDGONotFoudException() {
        super(MENSAJE);
    }
}
