package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class VentaEmitidaException extends BadRequestException {

    public VentaEmitidaException() {
        super("La póliza ya fue emitida");
    }
}
