package ar.com.experta.ventashogarapi.commons.exceptions;

public class InternalErrorException extends RuntimeException{

    public InternalErrorException(String message) {
        super(message);
    }

}
