package ar.com.experta.ventashogarapi.commons.exceptions.badgateway;

public class NominaDirectvBadGatewayException extends BadGatewayException {

    public static final String MENSAJE = "No se pudo verificar que el cliente pertenezca a la nómina de DirecTV. Intente nuevamente más tarde. Disculpa las molestias";

    public NominaDirectvBadGatewayException(Exception exception) {
        super(MENSAJE, exception.getMessage());
    }
}
