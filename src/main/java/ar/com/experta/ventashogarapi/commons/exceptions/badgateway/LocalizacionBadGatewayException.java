package ar.com.experta.ventashogarapi.commons.exceptions.badgateway;

public class LocalizacionBadGatewayException extends BadGatewayException {

    public LocalizacionBadGatewayException(Exception exception) {
        super("No se pudo recuperar la localidad. Intente nuevamente mas tarde. Disculpe las molestias", exception);
    }


}
