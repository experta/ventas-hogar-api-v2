package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class CoberturaRepetidaException extends BadRequestException {
    public CoberturaRepetidaException(String cobertura) {
        super("La cobertura " + cobertura + "está repetida");
    }
}
