package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

import ar.com.experta.ventashogarapi.core.model.enums.Beneficio;

public class VendedorBeneficioInvalidoException extends BadRequestException {
    public VendedorBeneficioInvalidoException(String nombre, Beneficio beneficio) {
        super("El vendedor " + nombre + " no tiene permisos para operar el beneficio: " + beneficio.label());
    }
}
