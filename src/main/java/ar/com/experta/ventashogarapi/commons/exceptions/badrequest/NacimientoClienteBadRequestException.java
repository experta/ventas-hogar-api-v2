package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class NacimientoClienteBadRequestException extends BadRequestException {

    public NacimientoClienteBadRequestException() {
        super("La fecha de nacimiento no puede ser nula");
    }
}
