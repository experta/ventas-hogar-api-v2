package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class UsuarioPasswordIncorrectosException extends BadRequestException {

    public UsuarioPasswordIncorrectosException() {
        super("Usuario o contraseña incorrecta");
    }
}
