package ar.com.experta.ventashogarapi.commons.exceptions.badgateway;

import ar.com.experta.ventashogarapi.commons.exceptions.badrequest.BadRequestException;
import ar.com.experta.ventashogarapi.commons.exceptions.unprocessable.ClienteFraudeException;
import ar.com.experta.ventashogarapi.core.model.enums.CanalVenta;

public class ClienteDuplicadoException extends BadRequestException {

    private static String mensajeProductores = "No se puede emitir la póliza con el tipo de documento ingresado para el cliente. Por favor comunícate con tu ejecutivo.";
    private static String mensajeClientes = "No se puede emitir la póliza con el tipo de documento ingresado para el cliente. Para más información comunícate con nuestro Centro de Atención al Cliente.";

    public ClienteDuplicadoException(CanalVenta canalVenta) {
        super(ClienteFraudeException.getMensaje(canalVenta));
    }

    public static String getMensaje(CanalVenta canalVenta) {
        return canalVenta.equals(CanalVenta.WEB_ECOMMERCE_WL) ? mensajeClientes : mensajeProductores;
    }

}
