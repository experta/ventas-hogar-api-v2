package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

import java.math.BigDecimal;

public class CoberturaMontoInvalidoException extends BadRequestException {
    public CoberturaMontoInvalidoException(String cobertura, BigDecimal monto) {
        super("El monto de la cobertura " + cobertura + " debe ser obligatoriamente $ " + monto);
    }
}
