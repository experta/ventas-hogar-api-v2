package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class CotizacionBadRequestException extends BadRequestException {

    public CotizacionBadRequestException(String cantidad) {
        super("Debe seleccionar como máximo "+cantidad+" cotizaciones para poder compartir");
    }
}
