package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class NominaDirectvInvalidoException extends BadRequestException {

    public static final String MENSAJE = "El cliente no pertenece a la nómina de Directv o no se ha completado su alta";

    public NominaDirectvInvalidoException() {
        super(MENSAJE);
    }
}
