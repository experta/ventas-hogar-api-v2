package ar.com.experta.ventashogarapi.commons.exceptions.unprocessable;

import ar.com.experta.ventashogarapi.core.model.enums.CanalVenta;

public class ClienteFraudeException extends UnprocessableEntityException{

    private static String mensajeProductores = "El cliente no es apto para emitir en Experta Seguros. Para más información comunícate con tu ejecutivo.";
    private static String mensajeClientes = "El cliente no es apto para emitir en Experta Seguros. Para más información comunícate con nuestro Centro de Atención al Cliente.";

    public ClienteFraudeException(CanalVenta canalVenta) {
        super(ClienteFraudeException.getMensaje(canalVenta));
    }

    public static String getMensaje(CanalVenta canalVenta) {
        return canalVenta.equals(CanalVenta.WEB_ECOMMERCE_WL) ? mensajeClientes : mensajeProductores;
    }
}
