package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class FaltaOpcionCobertura extends BadRequestException {

    public FaltaOpcionCobertura(String tipoCobertura) {
        super("No se ha seleccionado que tipo de opcion desea para la cobertura " + tipoCobertura);
    }
}
