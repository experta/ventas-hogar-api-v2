package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class NombreClienteBadRequestException extends BadRequestException {

    public NombreClienteBadRequestException() {
        super("El nombre y apellido del cliente no pueden ser nulos");
    }
}
