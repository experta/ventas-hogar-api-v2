package ar.com.experta.ventashogarapi.commons.exceptions.badrequest;

public class DireccionTomadorNoCotizadaException extends BadRequestException {

    public static final String MENSAJE = "La direccion del cliente no coincide con la dirección cotizada";

    public DireccionTomadorNoCotizadaException() {
        super(MENSAJE);
    }
}
