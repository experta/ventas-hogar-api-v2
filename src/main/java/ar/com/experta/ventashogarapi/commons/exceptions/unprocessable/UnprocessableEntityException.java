package ar.com.experta.ventashogarapi.commons.exceptions.unprocessable;

import java.util.List;

public class UnprocessableEntityException extends RuntimeException {

    private List<String> messages;

    public UnprocessableEntityException(String message) {
        super(message);
    }

    public UnprocessableEntityException(List<String> messages) {
        super();
        this.messages = messages;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

}
