package ar.com.experta.ventashogarapi.commons.exceptions.badgateway;

public class IdentityManagerBadGatewayException extends BadGatewayException {
    public IdentityManagerBadGatewayException(Exception exception) {
        super(null, exception);
    }
}
