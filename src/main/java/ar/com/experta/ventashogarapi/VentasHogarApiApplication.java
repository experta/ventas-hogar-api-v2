package ar.com.experta.ventashogarapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VentasHogarApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VentasHogarApiApplication.class, args);
	}
}
